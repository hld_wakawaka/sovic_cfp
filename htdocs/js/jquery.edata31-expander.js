/**
 * cfp環境専用
 * 所属機関名のadd,delボタン実装
 * 
 * ex) $("#formadd").edata31expander({edata31:data, maxNum:coAuthNum});
 *
 */
(function( $, undefined ) {



function AffiliationClass() {
    this.version = "1.0.0",
    this._target = "";
    this._defaults = {
         'target'       : ""
        ,'maxNum'       : 15                // 設定可能な最大の所属機関数
        ,'edata31'      : []
        ,'field'        : "input.edata31"
        ,'btn_add'      : ".btn_edata31_add"
        ,'btn_del'      : ".btn_edata31_del"
        ,'addMark'      : "+"               // 追加ボタンの初期値
        ,'delMark'      : "-"               // 削除ボタンの初期値
    }
}


// AffiliationClassの機能を拡張する
$.extend(AffiliationClass.prototype, {
    /* デフォルト値を返す */
    _initsetting: function() {
        return this._defaults;
    },


    
    /**
     * 第1引数をターゲットに選択した金額の自動計算を行い出力する
     * @param  target    合計金額の対象の親要素
     * @param  options    拡張引数 #AffiliationClass._defaults参照
     * @return 
     */
    _showDefaultAffiliation: function(target, options){
        this._target = target;
        var setting = $.extend(this._initsetting(), options);

        // addボタン名称設定
        $('.btn_edata31_add').val(setting.addMark);
        
        // 初期状態の所属機関を表示
        for(i in setting.edata31){
        	$.edata31expander.addAction(setting, i, setting.edata31[i]);
        }
        // 最小表示を1とする。何も入力していない場合は初期値1とするために１を追加
        if($(setting.field).size() == 0){
        	$.edata31expander.addAction(setting, 1, "");
        }
        
        // 入力フォームに入力した値を以下の関数にて登録。その際に入力した値によっては、javascriptのアクションも行っている
        $.edata31expander._attachAction(setting);
    },


    /**
     * イベント情報を登録
     * @param  setiting   設定情報
     * @return void
     */
    _attachAction: function(setting){
    	// 追加ボタンをクリックしたときのアクション
    	// 追加ボタンと削除ボタンのアクションは独立している
        $(setting.btn_add).bind("click", function(){
            $(this).prop("disabled", false);
            var num = $(setting.field).size();
            
            // 上限数に達していないかチェック
            // 達していれば非活性
            if(setting.maxNum <= num){
                $(this).prop("disabled", true);
            }
            num++;
            
           // ここで初めて、実際の追加、つまり追加機能による追加アクションを行う
            $.edata31expander.addAction(setting, num, "");
            
            // 追加ボタンをクリックした後に削除ボタンを押した時の実際の削除のアクション
            $(setting.btn_del).bind("click", function(){
                $.edata31expander.delAction(setting, this);
            });
        });

        // 追加機能による削除のアクション
        // 単独で削除ボタンを押したときのアクション。既に入力を終えたフォームで、項目を削除するときのアクション
        $(setting.btn_del).bind("click", function(){
            $.edata31expander.delAction(setting, this);
        });
    },



    /**
     * 所属機関の入力フィールドを追加
     * @param  setiting   設定情報
     * @param  num        通し番号
     * @param  value      初期値
     * @return void
     */
    // 追加アクション
    addAction: function(setting, num, value){
        delMark = setting.delMark;
        var _form = '<input type="text" name="edata31_[]" id="edata31_'+ num +'" class="edata31" value="'+value+'" />'
        var _btn  = '<input type="button" name="btn_edata31_del[]" class="btn_edata31_del" value="'+ delMark +'" id="add_'+ num +'" />'
        element = _form + _btn;
        $(this._target).append('<p class="edata31_row" id="add_'+ num +'_row">'+element+'</p>');
    },


    /**
     * 所属機関の入力フィールドを削除
     * @param  setiting   設定情報
     * @param  obj        element情報
     * @return void
     */
    delAction: function(setting, obj){
        var id = $(obj).attr("id")+"_row";
        $('#'+id).remove();
    }


});


$.fn.edata31expander = function(options){
    if(!this.selector) return this;
    $.edata31expander._showDefaultAffiliation(this.selector, options);
};

$.edata31expander = new AffiliationClass();


})(jQuery);


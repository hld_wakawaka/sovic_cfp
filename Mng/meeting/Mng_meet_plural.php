<?php

include('../../application/cnf/include.php');

/**
 * 管理者　一括会議参加情報編集
 * 
 * @author salon
 *
 */
class meetplural extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function meetplural(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
ini_set("error_reporting", E_ALL);
		
		// 表示HTMLの設定
		$this->_processTemplate = "Mng/meeting/Mng_meet_plural.html";
		$this->_title = "管理者ページ";
		
//		$this->assign("isTop", true);

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "テスト１　事務局");
		
		//-------------------------------
		//管理者メニュー
		//-------------------------------
		$su_menu[0] = array("menu_name" => "エントリー管理", "prg" => "index.php");
		$su_menu[1] = array("menu_name" => "会議管理", "prg" => "Mng_meetinglist.php");
		$su_menu[2] = array("menu_name" => "メール送信履歴", "prg" => "Mng_mailhistory.php");
		$su_menu[3] = array("menu_name" => "パスワード変更", "prg" => "");
		
		$this->assign("va_menu", $su_menu);

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["act"]) ? $_REQUEST["act"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;
			
		//----------------------
		//一括処理種別
		//----------------------
		//採択
		if($_REQUEST["set_stat"] == "1"){
			$wk_exec_name = "キャンセル";
		}

		//削除
		else if($_REQUEST["set_stat"] == "4"){
			$wk_exec_name = "削除";
		}
				
		$this->assign("exec_name", $wk_exec_name);

				for($i=1; $i < 6; $i++){
					$form_data[] = array("ent_id" => $i,
										"user_name" => "会議".$i. " 次郎"
										,"user_kname" => "かいぎ".$i. " じろう"
										,"email" => "test".$i."@aaa.co,jp"
										,"status" => ""
										,"rdate" => "2010/09/03"
										,"udate" => "2010/08/10");
				}
				
				$form_data[3]["status"] = "キャンセル";
				
				$this->assign("list", $form_data);
		$this->assign("list", $form_data);



		//---------------------------------
		//
		//---------------------------------

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){


			case "complete":
			
				$this->_processTemplate = "Mng/Mng_complete.html";

				//完了画面表示
				$this->_title = "会議参加情報　一括処理（".$wk_exec_name."）";
				
				//完了メッセージ

				$this->assign("msg", "エントリーの".$wk_exec_name."処理を実行しました。");
				
				$this->arrForm["url"] = "Mng_meetentrant.php";		
			
			break;
			
			
			
		}		


		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 
	 



}

/**
 * メイン処理開始
 **/

$c = new meetplural();
$c->main();







?>
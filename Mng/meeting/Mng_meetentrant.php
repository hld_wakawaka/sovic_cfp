<?php

include('../../application/cnf/include.php');
include_once('../../application/module/custom/Meeting.class.php');
include_once('../function.php');

/**
 * 管理者 会議 参加者一覧
 * 
 * @package 論文フォーム
 * @subpackage Mng
 * @author salon
 *
 */
class meetentrant extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function meetentrant(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
ini_set("error_reporting", E_ALL);
		
		//------------------------------		
		// 表示HTMLの設定
		//------------------------------
		$this->_processTemplate = "Mng/meeting/Mng_meetentrant.html";
		$this->_title = "会議応募一覧";
		
//		$this->assign("isTop", true);

		//------------------------------
		//インスタンス
		//------------------------------
		$this->db = new DbGeneral;
		$this->objErr = New Validate;

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "テスト１　事務局");
		
		//-------------------------------
		//管理者メニュー
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";


		//-----------------------
		//会議ID
		//-----------------------
		$this->mid = $_REQUEST["mid"];
		$this->assign("mid", $this->mid);
		

		//-----------------------
		//ページ番号取得
		//-----------------------
		$page_num = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;

		 //検索パラメータが渡ってきた場合
		 if(isset($_REQUEST["sk_user_name"])){
		 	
			//リクエストデータ取得
			$this->arrForm = $_REQUEST;

		 	$GLOBALS["session"]->setVar("applies_param", $this->arrForm);
		 	
		 	//-------------------------------
		 	// 検索条件のチェック
		 	//-------------------------------
		 	//$arrErr = $this->check();
//		 	if(count($arrErr) > 0){
//	
//				//検索条件NG
//				$search_chk = "1";
//										
//				//フォームパラメータにセッションの値をセット
//				$this->arrForm=$GLOBALS["session"]->getVar("form_param");
//		 	}
	 	
		 }
		else{
			
		
			//一度検索が行われている場合
			if($ws_action == "search"){
				//フォームパラメータにセッションの値をセット
				$this->arrForm=$GLOBALS["session"]->getVar("applies_param");
			}
		}




		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){


			//-------------------------
			//キャンセル
			//削除
			//-------------------------
			case "cancel":
			case "del":
			
				//------------------
				//トランザクション
				//------------------
				$this->db->begin();
			
				//会議応募ID取得
				$this->wk_id = array();
				foreach($_REQUEST["aid"] as $wk_id){
				 	
				 	//数値チェック
				 	if(!is_numeric($wk_id)){
						$this->db->rollback();
				 	}
				
			

					//キャンセル処理
					if($ws_action == "cancel"){
						$param["status"] = "1";
					}
					//削除処理
					else{
						$param["del_flg"] = "1";
					}
						
					$param["udate"] = "NOW";
					
					$where = "aid = ".$wk_id;
			
					
					$rs = $this->db->update("applies", $param, $where, __FILE__, __LINE__);
					if(!$rs) {
						$this->db->rollback();
					}
				}
				
				//---------------------
				//コミット
				//---------------------
				$this->db->commit();


			
			break;				
			
		}		



		//----------------------------
		//一覧データ取得
		//----------------------------
		//if($search_chk == ""){
			$form_data = Meeting::getAppliesList($this->mid, $page_num, ROW_LIMIT);
			$this->assign("list", $form_data);			
		//}

		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 
	 



}

/**
 * メイン処理開始
 **/

$c = new meetentrant();
$c->main();







?>
<?php

include('../../application/cnf/include.php');
include_once('../../application/module/custom/Meeting.class.php');
include_once('../function.php');

/**
 * 管理者　会議フォーム登録・編集
 * 	
 * @package packagename
 * @subpackage Mng
 * @author salon
 *
 */
class edit_meet extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function edit_meet(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
ini_set("error_reporting", E_ALL);

		//-----------------------
		//初期化
		//-----------------------
		$wa_meeting = "";
		$form_id = 1;
		$wa_tmp_files = "";

		//-----------------------
		//インスタンス
		//-----------------------
		$this->objErr = New Validate;
		$this->db = new DbGeneral;	

		//-----------------------
		//フォームパラメータ取得
		//-----------------------
		$this->arrForm = GeneralFnc::convertParam($this->_init(), $_REQUEST);

	
		//-------------------------
		//フォームID
		//-------------------------
		//ログイン者の情報から取得する
		$this->arrForm["form_id"] = "1";
		
		
		//-------------------------
		//会議ID
		//-------------------------
		if(isset($this->arrForm["mid"]) && !is_null($this->arrForm["mid"])){
			$this->mid = $this->arrForm["mid"];
		}
		else{
			$this->mid = "";
		}
		
		//--------------------------
		//ファイル関連　ディレクトリ設定
		//--------------------------
		$this->uploadDir = UPLOAD_PATH."Mng/form".$form_id."/";	//ファイルアップロードディレクトリ
		
		if(!is_dir($this->uploadDir)){
			mkdir($this->uploadDir, 0777);
		}	
		
		$this->wk_dir = "tmp";			//ファイルアップロード ワークディレクトリ
		$this->head_wk_dir = "h_tmp";		//ファイルアップロード ワークディレクトリ

		list($wk_h_up_dir, $wk_h_tmp_dir) = $this->_makeDirName("1");
		list($wk_up_dir, $wk_tmp_dir) = $this->_makeDirName("2");






		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Mng/meeting/Mng_edit_meet.html";
		$this->_title = "会議参加フォーム";
		$this->_title = ($this->mid == "") ? $this->_title."登録" : $this->_title."編集";
		
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//管理者メニュー取得
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);


		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";



		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		//リストボックスデータ
		list($year,$month, $day) = Mng_function::makeDateListBox();


		$this->assign("syear", $year);				//開始（年）
		$this->assign("smonth", $month);			//開始（月）
		$this->assign("sday", $day);				//開始（日）
		
		$this->assign("eyear", $year);				//終了（年）
		$this->assign("emonth", $month);			//終了（月）
		$this->assign("eday", $day);				//終了（日）

		$this->assign("oyear", $year);				//開催（年）
		$this->assign("omonth", $month);			//開催（月）
		$this->assign("oday", $day);				//開催（日）
	

		//-------------------------------
		//会議情報取得
		//-------------------------------
		if($this->mid != ""){
			$this->meetingData  = Meeting::getMeeting($_REQUEST["mid"]);
		}

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){
			
			//----------------------
			//確認画面表示
			//----------------------
			case "confirm":

				//---------------------------
				//入力チェック
				//---------------------------
				$arrErr = $this->_check();

				if(count($arrErr)){
					$this->assign("arrErr", $arrErr);
				}
				else{
					
					//---------------------
					//プレビュー用URL生成
					//---------------------
					$preview_url = URL_ROOT."Usr/meeting/Usr_meeting.php?mid=".$this->mid."&preview=1";
					
					$this->assign("preview_url", $preview_url);


					//----------------------
					//項目をセッションに登録
					//----------------------
					$GLOBALS["session"]->setVar("formItem", $this->arrForm);
					
					
					$this->_processTemplate = "Mng/meeting/Mng_edit_meet_confirm.html";
				}
			
			break;
			
			//----------------------
			//登録・編集処理
			//----------------------
			case "complete":
			
				//---------------------------------
				//2重起動チェック
				//---------------------------------
			
			
			
					//------------------------
					//トランザクション
					//------------------------
					$this->db->begin();
			
					//---------------------------------
					//新規登録処理
					//---------------------------------
					if($this->mid == ""){

						$wb_ret = $this->_insMeeting();
						if(!$wb_ret){
							$this->db->rollback();
						}
						$wk_mode="登録しました。";
						
						
						//本番ディレクトリ名生成
						mkdir($this->uploadDir.$this->ins_mid);
						mkdir($this->uploadDir.$this->ins_mid."/header");
						
						$wk_h_up_dir = $this->uploadDir.$this->ins_mid ."/header";
						$wk_up_dir =  $this->uploadDir.$this->ins_mid;
						
					}
					//----------------------------------
					//更新処理
					//----------------------------------
					else{
						$wb_ret = $this->_updMeeting();
						if(!$wb_ret){
							$this->db->rollback();
						}
						$wk_mode = "編集しました。";
					}
			

					//----------------------------------
					//添付資料、画像登録
					//----------------------------------
					//本番ディレクトリ内のファイルを削除
					Mng_function::deleteFile($wk_h_up_dir);					
					
					//一次格納ディレクトリから本番ディレクトリに移動
					list($wb_ret, $msg) = $this->_moveFile($wk_h_tmp_dir, $wk_h_up_dir);
					if(!$wb_ret){
						$this->db->rollback();
						$this->dispEnd($msg);
					}					
					
					
					//本番ディレクトリ内のファイルを削除
					Mng_function::deleteFile($wk_up_dir);
					
					
					//一次格納ディレクトリから本番ディレクトリに移動
					list($wb_ret, $msg) = $this->_moveFile($wk_tmp_dir, $wk_up_dir);
					if(!$wb_ret){
						$this->db->rollback();
						$this->dispEnd($msg);
					}
					
			
					//------------------------
					//コミット
					//------------------------
					$this->db->commit();			
			
			
			
					$this->arrForm["url"] = "Mng_meetinglist.php";
			
					//--------------------------
					//完了画面表示
					//--------------------------
					$this->dispEnd("会議情報を".$wk_mode);


			break;



			//------------------------
			//ヘッダ画像アップロード時
			//------------------------
			case "headimg":

				//ファイル選択チェック
				if (!file_exists($_FILES["head_image"]["tmp_name"])) {
					$this->objErr->addErr("ヘッダ画像を指定してください。", "head_image");
					$this->assign("arrErr", $this->objErr->_err);					
				}
				else{
					
					//既に登録済みのファイルがある場合
					if(is_dir($wk_h_tmp_dir)){
						$wa_headimg = Mng_function::getFiles($wk_h_tmp_dir);
					}
					
					if(count($wa_headimg)> 0){
						$this->objErr->addErr("登録できるヘッダ画像は１つです。他の画像をアップロードする場合は「削除」ボタンをクリックした後、再度アップロードを行ってください。", "head_image");
						$this->assign("arrErr", $this->objErr->_err);			

					}
					else{
						list($wb_ret, $ws_file_error) = $this->_file_upload("head_image", "1");
						if(!$wb_ret){
							$this->objErr->addErr($ws_file_error, "head_image");
							$this->assign("arrErr", $this->objErr->_err);
						}						
					}
					
					
				}				
				
				

			 break;


			//------------------------
			//ヘッダ画像削除
			//------------------------
			case "del_headimg":

			
				if(is_file($wk_h_tmp_dir."/".$_REQUEST["del_fname"])){
					unlink($wk_h_tmp_dir."/".$_REQUEST["del_fname"]);					
				}
					
			
			 break;
			 
			//------------------------
			//添付資料アップロード時
			//------------------------
			case "tmpfile":

				//ファイル選択チェック
				if (!file_exists($_FILES["tmp_file"]["tmp_name"])) {
					$this->objErr->addErr("添付資料を指定してください。", "tmp_file");
					$this->assign("arrErr", $this->objErr->_err);					
				}
				else{
					list($wb_ret, $ws_file_error) = $this->_file_upload("tmp_file", "2");
					if(!$wb_ret){
						$this->objErr->addErr($ws_file_error, "tmp_file");
						$this->assign("arrErr", $this->objErr->_err);
					}					
				}

			 break;			 

			//------------------------
			//添付資料削除
			//------------------------
			case "del_tmpfile":

			
				if(is_file($wk_tmp_dir."/".$_REQUEST["del_fname"])){
					unlink($wk_tmp_dir."/".$_REQUEST["del_fname"]);					
				}
					
			
			 break;
			 
			//----------------------
			//戻る
			//----------------------
			case "back":
			
			 break;	
			 			 
			//------------------------			
			//初期表示時
			//------------------------
			default:

				if($this->mid == ""){


				}
				else{

					//DBから取得したデータ
					$this->arrForm = $this->meetingData;

					$this->arrForm["syear"] = substr($this->arrForm["reception_date1"],0,4);
					$this->arrForm["smonth"] = substr($this->arrForm["reception_date1"],5,2);
					$this->arrForm["sday"] = substr($this->arrForm["reception_date1"],8,2);
					$this->arrForm["eyear"] = substr($this->arrForm["reception_date2"],0,4);
					$this->arrForm["emonth"] = substr($this->arrForm["reception_date2"],5,2);
					$this->arrForm["eday"] = substr($this->arrForm["reception_date2"],8,2);
					$this->arrForm["oyear"] = substr($this->arrForm["open_date"],0,4);
					$this->arrForm["omonth"] = substr($this->arrForm["open_date"],5,2);
					$this->arrForm["oday"] = substr($this->arrForm["open_date"],8,2);
					

					//----------------------------------------
					//現在登録されているヘッダ画像をtmpディレクトリにコピー
					//----------------------------------------
					if(!is_dir($wk_h_tmp_dir)){
						mkdir($wk_h_tmp_dir,0777);
					}						

					//画像が存在すればファイルを取得
					if(is_dir($wk_h_up_dir)){
						$wa_tmp_files = Mng_function::getFiles($wk_h_up_dir);
					
						if(count($wa_tmp_files) > 0){
							foreach($wa_tmp_files as $wk_data){
								//ファイルコピー
								if(is_file($wk_h_up_dir."/".$wk_data)){
									if (!copy($wk_h_up_dir."/".$wk_data, $wk_h_tmp_dir."/".$wk_data)) {
										
									}									
								}
							}
						}
					}



					
					//----------------------------------------
					//現在登録されている添付資料をtmpディレクトリにコピー
					//----------------------------------------	
					if(!is_dir($wk_tmp_dir)){
						mkdir($wk_tmp_dir,0777);
					}



					//添付資料が存在すればファイルを取得
					if(is_dir($wk_up_dir)){
						$wa_tmp_files = Mng_function::getFiles($wk_up_dir);
					
						if(count($wa_tmp_files) > 0){
							foreach($wa_tmp_files as $wk_data){
								//ファイルコピー
								if(is_file($wk_up_dir."/".$wk_data)){
									if (!copy($wk_up_dir."/".$wk_data, $wk_tmp_dir."/".$wk_data)) {
										
									}									
								}
							}
						}
					}

				}
				
				
//				$this->wk_dir = date('YmdHis');			//ファイルアップロード ワークディレクトリ
//				$this->head_wk_dir = date('YmdHis');			//ファイルアップロード ワークディレクトリ
				
			 break;						
			
			
			
		}		



		//---------------------------------
		//アップロード画像取得
		//---------------------------------
		//list($wk_up_dir, $wk_tmp_dir) = $this->_makeDirName("1");

		//ヘッダ画像が存在する場合はファイルを取得
		if(is_dir($wk_h_tmp_dir)){
			$wa_headimg = Mng_function::getFiles($wk_h_tmp_dir);
		
			if(count($wa_headimg) > 0){
				$this->assign("va_headimg", $wa_headimg);
			}
		}

		
		//添付資料が存在すればファイルを取得
		if(is_dir($wk_tmp_dir)){
			$wa_tmp_files = Mng_function::getFiles($wk_tmp_dir);
		
			if(count($wa_tmp_files) > 0){
				$this->assign("va_tmp_files", $wa_tmp_files);
			}
		}		


		//---------------------------------------
		//データ表示
		//---------------------------------------


		
		
		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 

	/**
	 * 画面項目初期化
	 * 
	 */
	function _init(){
		
		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)）
		$key[] = array("mid", "会議ID", array(),	array(),	"n",	0);
		$key[] = array("form_id", "フォームID", array(),	array(),	"n",	0);
		$key[] = array("title", "会議名", array(),	array(),	"KV",	1);
		$key[] = array("body", "本文", array(),	array(),	"KV",	1);
		
		$key[] = array("syear", "受付期間　開始", array(),	array(),	"",	0);
		$key[] = array("smonth", "受付期間　開始", array(),	array(),	"",	0);
		$key[] = array("sday", "受付期間　開始", array(),	array(),	"",	0);

		$key[] = array("eyear", "受付期間　終了", array(),	array(),	"",	0);
		$key[] = array("emonth", "受付期間　終了", array(),	array(),	"",	0);
		$key[] = array("eday", "受付期間　終了", array(),	array(),	"",	0);

		$key[] = array("oyear", "開催日", array(),	array(),	"",	0);
		$key[] = array("omonth", "開催日", array(),	array(),	"",	0);
		$key[] = array("oday", "開催日", array(),	array(),	"",	0);
		
		
		$key[] = array("contact", "問い合せ先", array(),	array(),	"KV",	1);
		$key[] = array("head_image", "ヘッダ画像", array(),	array(),	"",	1);
		
		$key[] = array("f_name", "氏名", array(),	array(),	"KV",	1);
		$key[] = array("f_name_show", "氏名　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_name_need", "氏名　必須", array(),	array(),	"n",	1);
		
		$key[] = array("f_kname", "氏名（かな）", array(),	array(),	"KV",	1);
		$key[] = array("f_kname_show", "氏名（かな）　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_kname_need", "氏名（かな）　必須", array(),	array(),	"n",	1);
		
		$key[] = array("f_email", "メールアドレス", array(),	array(),	"KV",	1);
		$key[] = array("f_email_show", "メールアドレス　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_email_need", "メールアドレス　必須", array(),	array(),	"n",	1);
		
		$key[] = array("f_zip", "郵便番号", array(),	array(),	"KV",	1);
		$key[] = array("f_zip_show", "郵便番号　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_zip_need", "郵便番号　必須", array(),	array(),	"n",	1);
		
		$key[] = array("f_address", "住所", array(),	array(),	"KV",	1);
		$key[] = array("f_address_show", "住所　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_address_need", "住所　必須", array(),	array(),	"n",	1);
		
		$key[] = array("f_tel", "電話番号", array(),	array(),	"KV",	1);
		$key[] = array("f_tel_show", "電話番号　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_tel_need", "電話番号　必須", array(),	array(),	"n",	1);

		$key[] = array("f_extension", "電話番号（内線）", array(),	array(),	"KV",	1);
		$key[] = array("f_extension_show", "電話番号（内線）　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_extension_need", "電話番号（内線）　必須", array(),	array(),	"n",	1);

		$key[] = array("f_fax", "FAX", array(),	array(),	"KV",	1);
		$key[] = array("f_fax_show", "FAX　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_fax_need", "FAX　必須", array(),	array(),	"n",	1);

		$key[] = array("f_zip2", "その他連絡先　郵便番号", array(),	array(),	"KV",	1);
		$key[] = array("f_zip2_show", "その他連絡先　郵便番号　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_zip2_need", "その他連絡先　郵便番号　必須", array(),	array(),	"n",	1);

		$key[] = array("f_address2", "その他連絡先　住所", array(),	array(),	"KV",	1);
		$key[] = array("f_address2_show", "その他連絡先　住所　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_address2_need", "その他連絡先　住所　必須", array(),	array(),	"n",	1);

		$key[] = array("f_tel2", "その他連絡先　電話番号", array(),	array(),	"KV",	1);
		$key[] = array("f_tel2_show", "その他連絡先　電話番号　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_tel2_need", "その他連絡先　電話番号　必須", array(),	array(),	"n",	1);

		$key[] = array("f_extension2", "その他連絡先　内線", array(),	array(),	"KV",	1);
		$key[] = array("f_extension2_show", "その他連絡先　内線　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_extension2_need", "その他連絡先　内線　必須", array(),	array(),	"n",	1);

		$key[] = array("f_fax2", "その他連絡先　FAX", array(),	array(),	"KV",	1);
		$key[] = array("f_fax2_show", "その他連絡先　FAX　表示/非表示設定", array(),	array(),	"n",	1);
		$key[] = array("f_fax2_need", "その他連絡先　FAX　必須", array(),	array(),	"n",	1);

		return $key;
	}	 	 


	/**
	 * 入力チェック
	 * 
	 * @access public
	 * @return object
	 */
	function _check(){
		
		//---------------------
		//日付必須入力チェック
		//---------------------
		if($this->arrForm["syear"] == "" && $this->arrForm["smonth"] == "" && $this->arrForm["sday"] == ""){
			$this->objErr->addErr("受付期間（開始）を入力してください。", "reception_date1");
		}

		if($this->arrForm["eyear"] == "" && $this->arrForm["emonth"] == "" && $this->arrForm["eday"] == ""){
			$this->objErr->addErr("受付期間（終了）を入力してください。", "reception_date2");
		}

		if($this->arrForm["oyear"] == "" && $this->arrForm["omonth"] == "" && $this->arrForm["oday"] == ""){
			$this->objErr->addErr("開催日を入力してください。", "open_date");
		}
		
		$sparam = array("受付期間（開始）", $this->arrForm["syear"]
			,$this->arrForm["smonth"],  $this->arrForm["sday"]
			);
		
		list($wb_ret, $error_msg) = Mng_function::check_date($sparam);
		if(!$wb_ret){
			$this->objErr->addErr($error_msg, "reception_date1");
		}
		
		$eparam = array("受付期間（終了）", $this->arrForm["eyear"]
			,$this->arrForm["emonth"],  $this->arrForm["eday"]
			);
		
		list($wb_ret, $error_msg) = Mng_function::check_date($eparam);
		if(!$wb_ret){
			$this->objErr->addErr($error_msg, "reception_date2");
		}

		$oparam = array("開催日", $this->arrForm["oyear"]
			,$this->arrForm["omonth"],  $this->arrForm["oday"]
			);
		
		list($wb_ret, $error_msg) = Mng_function::check_date($oparam);
		if(!$wb_ret){
			$this->objErr->addErr($error_msg, "open_date");
		}


		/**
		 * 大小関係
		 */
		if(($this->arrForm["syear"] != "") && ($this->arrForm["smonth"] != "") && ($this->arrForm["sday"] != "") && $this->arrForm["eyear"] != "" && $this->arrForm["emonth"] != "" && $this->arrForm["eday"] != ""){

			 	$chk_sdate = $this->arrForm["syear"].$this->arrForm["smonth"].$this->arrForm["sday"];
				$chk_edate = $this->arrForm["eyear"].$this->arrForm["emonth"].$this->arrForm["eday"];
			 
			 if(strtotime($chk_sdate) > strtotime($chk_edate)){
	            $error_msg = "日付の指定が正しくありません。";
				$this->objErr->addErr($error_msg, "reception_date1");
			 }
		}


		return $this->objErr->_err;
	}	 


	/**
	 * 新規会議登録処理
	 * 
	 * @access public
	 * @return bool
	 */
	function _insMeeting(){

		$key = $this->_init();
		
		foreach($key as $val) {
			if($val[5] == 1) {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
		}
		
		$param["form_id"] = 1;
		
		$param["reception_date1"] = $this->arrForm["syear"].$this->arrForm["smonth"].$this->arrForm["sday"];
		$param["reception_date2"] = $this->arrForm["eyear"].$this->arrForm["emonth"].$this->arrForm["eday"];
		$param["open_date"] = $this->arrForm["oyear"].$this->arrForm["omonth"].$this->arrForm["oday"];

		//$this->ins_mid = $this->db->getOne("select nextval('meeting_mid_seq')");
		
		
		$this->ins_mid  = $GLOBALS["db"]->getOne("select nextval('meeting_mid_seq')");

		$param["mid"] = $this->ins_mid;
		
		$rs = $this->db->insert("meeting", $param, __FILE__, __LINE__);
		
		if(!$rs) {
			return false;
		}		
		

		return true;	
	}
	
	/**
	 * 会議情報更新処理
	 * 
	 * @access public
	 * @return bool
	 */
	function _updMeeting(){

		$key = $this->_init();
		
		foreach($key as $val) {
			if($val[5] == 1) {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
		}

		$param["reception_date1"] = $this->arrForm["syear"].$this->arrForm["smonth"].$this->arrForm["sday"];
		$param["reception_date2"] = $this->arrForm["eyear"].$this->arrForm["emonth"].$this->arrForm["eday"];
		$param["open_date"] = $this->arrForm["oyear"].$this->arrForm["omonth"].$this->arrForm["oday"];
		
		$param["udate"] = "NOW";
		
		$where = "mid = ".$this->mid;

		
		$rs = $this->db->update("meeting", $param, $where, __FILE__, __LINE__);
		
		if(!$rs) {
			return false;
		}

		return true;
		
	} 	 

	/**
	 * ファイルアップロード処理
	 * 
	 * @access public
	 * 
	 */
	function _file_upload($item_key, $ps_mode){
		//--------------------------------------		
		//アップロードディレクトリとワークディレクトリ設定
		//--------------------------------------
		//ヘッダ画像
		if($ps_mode == "1"){

			list($up_dir, $tmp_dir) = $this->_makeDirName("1");				
		}
		//添付資料
		else{
			list($up_dir, $tmp_dir) = $this->_makeDirName("2");
		
		}


		//ディレクトリ存在チェック
		if(!is_dir($up_dir)){
			mkdir($up_dir, 0777);
		}		

		if(!is_dir($tmp_dir)){
			mkdir($tmp_dir, 0777);
		}

		//------------------------------------
		//ファイルチェック
		//------------------------------------
		if($_FILES[$item_key]['error'] == 1){
			return array(false, "アップロード可能なサイズを超えています。");
		}
		
		//------------------------------------
		//アップロード処理
		//------------------------------------
		//テンポラリファイル存在チェック
		if (!file_exists($_FILES[$item_key]["tmp_name"])) {
			return array(false,"テンポラリーファイルが存在しません。");
		}


		//ファイルコピー
		if (!copy($_FILES[$item_key]["tmp_name"], $tmp_dir."/".$_FILES[$item_key]["name"])) {
			return array(false,"ファイルのコピーに失敗しました。");
		}

		//テンポラリファイル削除
		unlink($_FILES[$item_key]["tmp_name"]);

		return array(true,"");
		
	}	 

	/**
	 * ディレクトリ名生成
	 */
	function _makeDirName($ps_mode){
		
		//ヘッダ画像
		if($ps_mode == "1"){
			if($this->mid == ""){
				$up_dir = $this->uploadDir.$this->wk_dir. "/header/";
				$tmp_dir = $this->uploadDir.$this->wk_dir ."/header/". $this->wk_dir;
			}
			else{
				$up_dir = $this->uploadDir.$this->mid ."/header/";
				$tmp_dir = $this->uploadDir.$this->mid ."/header/". $this->wk_dir;
			}				
		}
		//添付資料
		else{
			if($this->mid == ""){
				$up_dir = $this->uploadDir.$this->wk_dir;
				$tmp_dir = $this->uploadDir.$this->wk_dir ."/". $this->wk_dir;
			}
			else{
				$up_dir = $this->uploadDir.$this->mid;
				$tmp_dir = $this->uploadDir.$this->mid ."/". $this->wk_dir;
			}			
		}
		
		return array($up_dir, $tmp_dir);		
		
	}	 

	/**
	 * ファイル移動 
	 * 　一次格納ディレクトリに上がったファイルを本ディレクトリに移動する
	 * 
	 * @param string 移動元ディレクトリ
	 * @param string 移動先ディレクトリ
	 * @return array 
	 */
	function _moveFile($ps_moto_dir, $ps_saki_dir){
		

		if (is_dir($ps_moto_dir)) {
			$wo_dir_h = opendir($ps_moto_dir);

			//先に全てのディレクトリ、フォルダをコピーする
			while (false != ($ws_file = readdir($wo_dir_h)))
			{
				if($ws_file!="." && $ws_file != "..") {

					if (is_file($ps_moto_dir . "/" . $ws_file)) {
						if (!copy($ps_moto_dir . "/" . $ws_file, $ps_saki_dir."/". $ws_file)) {
							closedir($wo_dir_h);
							return array(false,"ファイルの移動に失敗しました。");
						}
					}
				}
			}
			closedir($wo_dir_h);

			//ディレクトリ削除
			if(!Mng_function::deleteDir($ps_moto_dir)){
				return array(false,"ディレクトリの削除に失敗しました。");
			}
		}
		
			
		return array(true,"");
		
		
	}	 
	 
	 
	/**
	 * 終了処理
	 * 
	 * @access public
	 * @param  string
	 */
	function dispEnd($ps_msg){

		$this->_processTemplate = "Mng/Mng_complete.html";


		$this->assign("msg", $ps_msg);		
		
		// 親クラスに処理を任せる
		parent::main();
		exit;
		
	}	 

}

/**
 * メイン処理開始
 **/

$c = new edit_meet();
$c->main();







?>
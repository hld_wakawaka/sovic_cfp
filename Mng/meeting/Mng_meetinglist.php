<?php

include('../../application/cnf/include.php');
include_once('../../application/module/custom/Meeting.class.php');
include_once('../function.php');

/**
 * 管理者 会議一覧
 * 
 * @package 論文フォーム
 * @subpackage Mng
 * @author salon
 *
 */
class meetinglist extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function meetinglist(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
ini_set("error_reporting", E_ALL);

		//------------------------------
		//初期化
		//------------------------------
		$search_chk = "";
		$arrErr = "";
		
		
		//------------------------------		
		// 表示HTMLの設定
		//------------------------------
		$this->_processTemplate = "Mng/meeting/Mng_meetinglist.html";
		$this->_title = "会議一覧";
		
//		$this->assign("isTop", true);

		//------------------------------
		//インスタンス
		//------------------------------
		$this->db = new DbGeneral;
		$this->objErr = New Validate;

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "テスト１　事務局");

		
		//-------------------------------
		//管理者メニュー
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);


		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";


		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		//リストボックスデータ
		list($year,$month, $day) = Mng_function::makeDateListBox();

		$this->assign("syear", $year);				//開始（年）
		$this->assign("smonth", $month);			//開始（月）
		$this->assign("sday", $day);				//開始（日）
		
		$this->assign("eyear", $year);				//終了（年）
		$this->assign("emonth", $month);			//終了（月）
		$this->assign("eday", $day);				//終了（日）


		//-----------------------
		//ページ番号取得
		//-----------------------
		$page_num = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;



		 //検索パラメータが渡ってきた場合
		 if(isset($_REQUEST["sk_name"])){
		 	
			//リクエストデータ取得
			$this->arrForm = $_REQUEST;


				
		 	$GLOBALS["session"]->setVar("form_param", $this->arrForm);
		 	
		 	//-------------------------------
		 	// 検索条件のチェック
		 	//-------------------------------
		 	$arrErr = $this->check();
			
			 	
		 	if(count($arrErr) > 0){
	
				//検索条件NG
				$search_chk = "1";
										
				//フォームパラメータにセッションの値をセット
				$this->arrForm=$GLOBALS["session"]->getVar("form_param");
		 	}
	 	
		 }
		else{
			
		
			//一度検索が行われている場合
			if($ws_action == "search"){
				//フォームパラメータにセッションの値をセット
				$this->arrForm=$GLOBALS["session"]->getVar("form_param");
			}

	
		}
		

		//----------------------------
		//一覧データ取得
		//----------------------------
		if($search_chk == ""){
			$form_data = Meeting::getMeetingList($page_num, ROW_LIMIT);
			$this->assign("list", $form_data);			
		}


		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			//-----------------------
			//削除ボタンクリック時
			//-----------------------
			case "del":
			
				//---------------------
				//トランザクション
				//---------------------
				$this->db->begin();

				$where = "mid = ".$_REQUEST["mid"];
				$param["del_flg"] = 1;
				$rs = $this->db->update("meeting", $param, $where, __FILE__, __LINE__);
				
				if(!$rs){
					$this->db->rollback();
					$this->objErr->addErr("会議の削除に失敗しました。", "");
				}

				
				
				//---------------------
				//コミット
				//---------------------
				$this->db->commit();

			
			break;
			
			//-----------------------
			//初期表示時
			//-----------------------
			default:
				//検索パラメータ初期化
				$GLOBALS["session"]->unsetVar('form_param');
				
			 break;	
			
			
			
		}		


		$this->assign("arrErr", $arrErr);				//エラーメッセージ


		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 
	 

	/**
	 * フォームパラメタ初期化
	 * 
	 * @access public
	 */
	function init(){
		
		$key[] = array("sk_name", "会議名", array(),	array(),	"KV",	0);
		$key[] = array("sk_oyear", "カテゴリ", array(),	array(),	"a",	0);
		$key[] = array("sk_omonth", "ショップ", array(),	array(),	"a",	0);
		
		return $key;
	}	 
	 
	/**
	 * 入力チェック
	 * 
	 * @access public
	 * @return object
	 */
	function check(){
		


		/**
		 * 大小関係
		 */
		if(($this->arrForm["sk_syear"] != "") && ($this->arrForm["sk_smonth"] != "") && ($this->arrForm["sk_sday"] != "") && $this->arrForm["sk_eyear"] != "" && $this->arrForm["sk_emonth"] != "" && $this->arrForm["sk_eday"] != ""){

			 	$chk_sdate = $this->arrForm["sk_syear"].$this->arrForm["sk_smonth"].$this->arrForm["sk_sday"];
				$chk_edate = $this->arrForm["sk_eyear"].$this->arrForm["sk_emonth"].$this->arrForm["sk_eday"];
			 
			 if(strtotime($chk_sdate) > strtotime($chk_edate)){
	            $error_msg = "日付の指定が正しくありません。";
				$this->objErr->addErr($error_msg, "reception_date1");
			 }
		}


		return $this->objErr->_err;
	}
}

/**
 * メイン処理開始
 **/

$c = new meetinglist();
$c->main();







?>
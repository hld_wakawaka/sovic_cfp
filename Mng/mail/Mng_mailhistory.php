<?php

include('../../application/cnf/include.php');
include_once('../function.php');

/**
 * 管理者メール送信履歴
 * 
 * @author salon
 *
 */
class mailhistory extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function mailhistory(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){


		//-------------------------------
		// ログインチェック
		//-------------------------------
		LoginMember::checkLoginRidirect();

		
		// 表示HTMLの設定
		$this->_processTemplate = "Mng/mail/Mng_mailhistory.html";
		$this->_title = "管理者ページ";

		//------------------------------
		//初期化
		//------------------------------
		$search_chk = "";
		$arrErr = "";
		$this->limit = ROW_LIMIT;	//１ページ内の表示件数
		$all_count = 0;
		
		

		//-------------------------------
		//インスタンス生成
		//-------------------------------
		$this->db = new DbGeneral;
		$this->objErr = New Validate;
		

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", $GLOBALS["userData"]["user_name"]);
		
		//-------------------------------
		//管理者メニュー
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);
		

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";


		//------------------------
		//ページ番号
		//------------------------
		$page_num = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;
		

		//-------------------------
		//リクエストデータ取得
		//-------------------------
		$this->arrForm = $_REQUEST;


		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));
		

		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;
	
		$eyear[$wk_year0] = $wk_year0;
		$eyear[$wk_year1] = $wk_year1;
		$eyear[$wk_year2] = $wk_year2;
	

		//月	
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$smonth[$wk_month] = $wk_month;
			$emonth[$wk_month] = $wk_month;
	
		}
		//日付
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$sday[$wk_day] = $wk_day;
			$eday[$wk_day] = $wk_day;
		}

		//時間
		for($i=1; $i<25; $i++){
			$wk_hour = sprintf('%02d', $i);
			$hour[$wk_hour] = $wk_hour;
		}

		for($i=1; $i<61; $i++){
			$wk_time = sprintf('%02d', $i);
			$time[$wk_time] = $wk_time;
		}

		$this->assign("syear", $syear);				//開始（年）
		$this->assign("smonth", $smonth);			//開始（月）
		$this->assign("sday", $sday);				//開始（日）
		
		$this->assign("eyear", $eyear);				//終了（年）
		$this->assign("emonth", $emonth);			//終了（月）
		$this->assign("eday", $eday);				//終了（日）

		$this->assign("hour", $hour);				//時間
		$this->assign("timer", $time);				//時間

		
		//----------------------------------
		//検索条件セット
		//----------------------------------
		//検索パラメータが渡ってきた場合
		if(isset($_REQUEST["mail_ttl"])){

			$GLOBALS["session"]->setVar("search_key", $this->arrForm);
			
			//検索条件の妥当性チェック
			$arrErr = $this->check();
			
			if(count($arrErr) > 0){

				//検索条件NG
				$search_chk = "1";
										
				//フォームパラメータにセッションの値をセット
				$this->arrForm=$GLOBALS["session"]->getVar("search_key");
				
			}
		}
		else{
			
			//一度検索が行われている場合
			if($ws_action == "search"){
				//フォームパラメータにセッションの値をセット
				$this->arrForm=$GLOBALS["session"]->getVar("search_key");
			}			
			
		}


		//----------------------------
		//一覧データ取得
		//----------------------------
		if($search_chk == ""){
			list($all_count, $list) = $this->_getList($page_num, $this->limit);
			$this->assign("list", $list);

			//改ページリンク
			$this->pager = array("allcount"=>$all_count, "limit"=>$this->limit, "page"=>$page_num );
		}

		//取得件数
		$this->assign("count", $all_count);
		
		
		
		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			//---------------------------
			//検索ボタンクリック時
			//---------------------------
			case "search":
			
			break;
			//---------------------------
			//初期表示時
			//---------------------------
			default:
			
				//検索パラメータ初期化
				$GLOBALS["session"]->unsetVar('search_key');				
			
			 break;
			
		}		



		$this->assign("arrErr", $arrErr);


		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 

	/**
	 * 入力チェック
	 * 
	 * @access public
	 * @return object
	 */
	function check(){
		
		//妥当姓チェック（存在しない日付の場合はエラー）
		if(($this->arrForm["syear"] != "") && ($this->arrForm["smonth"] != "") && ($this->arrForm["sday"] != "") ){
			if(!checkdate((int)$this->arrForm["smonth"], (int)$this->arrForm["sday"], (int)$this->arrForm["syear"])){
				$this->objErr->addErr("配信日（開始）に存在しない日付が指定されています。", "search_sday");
			}
			
		}
		
		if($this->arrForm["eyear"] != "" && $this->arrForm["emonth"] != "" && $this->arrForm["eday"] != ""){
			
			if(!checkdate((int)$this->arrForm["emonth"], (int)$this->arrForm["eday"], (int)$this->arrForm["eyear"])){
				$this->objErr->addErr("配信日（終了）に存在しない日付が指定されています。", "search_eday");
			}
		}

		return $this->objErr->_err;
	}
	 

	/**
	 * メール送信履歴検索
	 * 
	 * @param int ページ番号
	 * @param int limit
	 */
	function _getList($page = 1, $limit){
		
		//SQL生成
		$column = "*";
		$from = "mail_history";
		
		$where[] = "form_id = ".$this->db->quote($GLOBALS["userData"]["form_id"]);
		
		//--------------------------------------
		//検索条件付与
		//--------------------------------------
    	// セッションに保存した検索条件を取得
    	$search_key = $GLOBALS["session"]->getVar("search_key"); 
    	
    	
    	if($search_key != ""){
    		
    		//タイトルが指定されている場合
    		if($search_key["mail_ttl"] != ""){
    			$where[] = "subject like ".$this->db->quote("%".$search_key["mail_ttl"]."%");
    		}
    		
    		
    		//キーワードが指定されている場合
    		if($search_key["keyword"] != ""){
    			$where[] = "( subject like ".$this->db->quote("%".$search_key["mail_ttl"]."%").") OR  (body like ".$this->db->quote("%".$search_key["mail_ttl"]."%").")";
    		}
    		
    		
    		//開始日が指定されいる場合
    		if($search_key["syear"] > 0){
    			
    			//月、日
    			if($search_key["smonth"] == ""){
    				$search_key["smonth"] = "1";
    			}
    			
    			if($search_key["sday"] == ""){
    				$search_key["sday"] = "1";
    			}
    			
    			//日付フォーマット生成
    			$search_sdate = sprintf("%04d", $search_key["syear"])."-".sprintf("%02d", $search_key["smonth"])."-".sprintf("%02d", $search_key["sday"]);
    			
    			$where[] = "rdate >= ".$this->db->quote($search_sdate);
    		}
    		
    		//終了日が指定されている場合
    		if($search_key["eyear"] > 0){
    			
    			//月、日
    			if($search_key["emonth"] == ""){
    				$search_key["emonth"] = "12";
    			}
    			
    			if($search_key["eday"] == ""){
  					$search_edate = date("Y-m-d", mktime(0, 0, 0, $search_key["emonth"]+1, 0, $search_key["eyear"]));
    			}
    			else{
 					$search_edate = sprintf("%04d", $search_key["eyear"])."-".sprintf("%02d", $search_key["emonth"])."-".sprintf("%02d", $search_key["eday"]);
    			}
    			
    			$where[] = "rdate <= ".$this->db->quote($search_edate);
    		}
    	}		
		
		$orderby = "rdate desc";


		//------------------------------------
		//全件数取得
		//------------------------------------
		$count = $this->db->_getOne("COUNT(send_id)", $from, $where, __FILE__, __LINE__);	
		
		if(!$count){
			return array("0", "");
		}

		
		//------------------------------------
		//一覧取得
		//------------------------------------
		$offset = $limit * ($page - 1);		
		
		$rs = $this->db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
    	if(!$rs) {
    		return array($count, "");
    	}		
		
		
		return array($count, $rs);
		
		
	}


}

/**
 * メイン処理開始
 **/

$c = new mailhistory();
$c->main();







?>
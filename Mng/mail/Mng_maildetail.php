<?php

include('../../application/cnf/include.php');
include_once('../../application/module/custom/Entry.class.php');
include_once('../function.php');

/**
 * 管理者メール送信詳細
 * 
 * @author salon
 *
 */
class maildetail extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function maildetail(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){


		//------------------------------
		//初期化
		//------------------------------
		$this->limit = ROW_LIMIT;	//１ページ内の表示件数

		//-------------------------------
		// ログインチェック
		//-------------------------------
		LoginMember::checkLoginRidirect();

		
		// 表示HTMLの設定
		$this->_processTemplate = "Mng/mail/Mng_maildetail.html";
		$this->_title = "管理者ページ";


		//-------------------------------
		//インスタンス生成
		//-------------------------------
		$this->db = new DbGeneral;
		$this->o_entry = new Entry();
		
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", $GLOBALS["userData"]["user_name"]);
		
		//-------------------------------
		//管理者メニュー
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";


		//-----------------------------
		//送信ID
		//-----------------------------
		$this->send_id = isset($_REQUEST["send_id"]) ? $_REQUEST["send_id"] : "";
		if($this->send_id == ""){
			$this->_ViewEnd("処理に必要なパラメータが存在しません。");
		}
		$this->assign("send_id", $this->send_id);

		//------------------------
		//ページ番号
		//------------------------
		$page_num = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;
		

		//---------------------------------
		//メール情報取得
		//---------------------------------
		$wk_mail = $this->_getMail();
		if(!$wk_mail){
			$this->_ViewEnd("対象データが存在しません。");
		}
		$this->assign("va_mail", $wk_mail);


		//------------------------------------
		//送信対象情報取得
		//------------------------------------
		list($all_count, $send_list) = $this->_getSendList($page_num, $this->limit);
		$this->assign("send_list", $send_list);
	
	
		//改ページリンク
		$this->pager = array("allcount"=>$all_count, "limit"=>$this->limit, "page"=>$page_num );

		//取得件数
		$this->assign("count", $all_count);


		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 
	 

	/**
	 * 処理終了
	 */
	function _ViewEnd($msg){
		
		$this->assign("va_mail", $msg);
		$this->_processTemplate = "Mng/mail/Mng_complete.html";
		
		// 親クラスに処理を任せる
		parent::main();		
		
	}	 

	/**
	 * 送信メール情報取得
	 * 	送信メールの内容を取得する
	 */
	function _getMail(){
		
		//SQL
    	$column = "*";
    	$from = "mail_history";
    	$where[] = "send_id = ".$this->db->quote($this->send_id);
 
    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);
    	
    	return $rs;		
		
	}	 

	/**
	 * メール送信詳細情報取得
	 * 	送信したユーザの一覧情報を取得する
	 * 
	 */
	function _getSendList($page = 1, $limit){
		
		$e_table = $this->o_entry->getTable($GLOBALS["userData"]["type"]);
		
		//SQL
    	$column = "a.send_id, a.mailaddress, a.result, b.edata1, b.edata2, b.edata3, b.edata4, b.edata5, b.edata6";
    	$from = "mail_sendentry a inner join ".$e_table." b on (a.eid = b.eid and b.form_id = ".$GLOBALS["userData"]["form_id"].")";
    	$where[] = "a.send_id = ".$this->db->quote($this->send_id);
 		$orderby = "a.eid";


		//全体件数取得
		$count = $this->db->_getOne("COUNT(a.send_id)", $from, $where, __FILE__, __LINE__);	
		
		if(!$count){
			return array("0", "");
		}
		

		//一覧データ取得
		
		$offset = $limit * ($page - 1);
		
		$rs = $this->db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
    	if(!$rs) {
    		return array($count, "");
    	}		
		
		return array($count, $rs);
	}	 


}

/**
 * メイン処理開始
 **/

$c = new maildetail();
$c->main();







?>
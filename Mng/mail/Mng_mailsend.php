<?php

include('../../application/cnf/include.php');
include('../../application/module/custom/Entry.class.php');
include_once('../function.php');

/**
 * 管理者メール送信
 *
 * @author salon
 *
 */
class mailsend extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function mailsend(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){

		ini_set("error_reporting", E_ALL);

		LoginMember::checkLoginRidirect();

		$this->objEntry = new Entry;

		// 表示HTMLの設定
		$this->_processTemplate = "Mng/mail/Mng_mailsend.html";
		$this->_title = "管理者ページ";

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", $GLOBALS["userData"]["user_name"]);

		//-------------------------------
		//管理者メニュー取得
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		$this->arrForm = $_REQUEST;
		// 処理対象のエントリーIDを取得
		$this->getEids($this->arrForm);

		if(!isset($this->arrForm["eid"]) || !count($this->arrForm["eid"]) > 0) {
			$this->complete("エントリーが指定されていません。");
		}

		// パラメータチェック
		$res = $this->checkParam();
		if(!$res) {
			Error::showErrorPage("不正なアクセスです。");
			exit;
		}


		$arrData = $this->objEntry->getListFromId($this->arrForm["eid"]);

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			//----------------------
			//確認画面表示
			//----------------------
			case "confirm":

				$arrErr = array();
				if($this->arrForm["mail_ttl"] == "") $arrErr[] = "件名を入力してください。";
				if($this->arrForm["mail_body"] == "") $arrErr[] = "本文を入力してください。";

				if(count($arrErr) > 0) {
					$this->assign("arrErr", $arrErr);
					break;
				}

				$this->_processTemplate = "Mng/mail/Mng_mailsend_confirm.html";

			break;

			//----------------------
			//登録・編集処理
			//----------------------

			case "complete":

				$this->_title = "メール送信完了";
				$this->lfSendMail($arrData);

			break;

			//----------------------
			//確認画面表示
			//----------------------
			case "back":


			break;

			default:


			break;


		}

		$this->assign("arrData", $arrData);

		// 親クラスに処理を任せる
		parent::main();


	}


	function complete($msg) {

		$this->assign("msg", $msg);
		$this->_processTemplate = "Mng/Mng_complete.html";
		parent::main();
		exit;

	}

	function lfSendMail(&$arrData) {

		//---------------------------------
		//各種文言情報取得
		//---------------------------------
		$_REQUEST["form_id"] = $GLOBALS["userData"]["form_id"];
		$this->o_form = new Form();
		$this->formWord =  $this->o_form->getFormWord($GLOBALS["userData"]["form_id"]);
        $this->formdata = $this->o_form->formData;

		mb_internal_encoding("utf-8");
		mb_language("japanese");

		$count = 0;

		foreach($arrData as $key=>$val) {
            $body = "";

			if($val["edata25"] == "") continue;

			$subject = $this->arrForm["mail_ttl"];
			if($GLOBALS["userData"]["lang"] == "1") {
			    // 置換文字列対応のため入力した本文のみを利用する
//				$body = $val["edata1"]." ".$val["edata2"]."様\n\n";
			}

			$body .= $this->arrForm["mail_body"];
			$body  = $this->replaceStr($body, $val);

			$to = $val["edata25"];

			//送信元
			if( $this->formWord["word13"] != "" ){
				$ps_fromname = mb_convert_encoding($this->formWord["word13"], "ISO-2022-JP", "utf-8");
				$ps_from = mb_encode_mimeheader($ps_fromname)."<".$GLOBALS["userData"]["form_mail"]."> ";
	        }
	        else{
	        	$ps_from = $GLOBALS["userData"]["form_mail"];
	        }

            // 改行コードを統一
            $body =  str_replace(array("\r\n", "\r"), "\n", $body);


			$head = "From: ".$ps_from;


			$head .= "\n";
			$head .= "Cc: ".$GLOBALS["userData"]["form_mail"];
			//$head .= "\n";
			//$head .= "Bcc: ".SYSTEM_MAIL;

			$rs = mb_send_mail($to, $subject, $body, $head);

			if(!$rs) {
				$arrData[$key]["send"] = "0";
			} else {
				$arrData[$key]["send"] = "1";
				$count++;
			}
			$arrData[$key]["send_date"] = date("Y-m-d H:i:s");
		}

		$db = new DbGeneral();
		$db->begin();

		// 履歴を残す
		$param["form_id"] = $GLOBALS["userData"]["form_id"];
		$param["subject"] = $this->arrForm["mail_ttl"];
		$param["body"] = $this->arrForm["mail_body"];
		$param["send_num"] = count($arrData);
		$param["success"] = $count;
		$rs = $db->insert("mail_history", $param, __FILE__, __LINE__);

		if(!$rs) {
			$db->rollback();
			$this->complete("履歴の登録に失敗しました。");
		}

		$where = "form_id = ".$db->quote($GLOBALS["userData"]["form_id"]);
		$send_id = $db->_getOne("max(send_id)", "mail_history", $where, __FILE__, __LINE__);

		if(!$send_id) {
			$db->rollback();
			$this->complete("送信番号の取得に失敗しました。");
		}

		unset($param);
		unset($where);

		foreach($arrData as $val) {

			if($val["edata25"] == "") continue;

			$param["send_id"] = $send_id;
			$param["eid"] = $val["eid"];
			$param["mailaddress"] = $val["edata25"];
			$param["result"] = $val["send"];
			$param["send_date"] = $val["send_date"];
			$rs = $db->insert("mail_sendentry", $param, __FILE__, __LINE__);
			if(!$rs) {
				$db->rollback();
				$this->complete("履歴の登録に失敗しました。");
			}
			unset($param);
		}

		$db->commit();

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

		$this->complete("メールを送信しました。");

	}



    /*
     * Sysと同じ置換文字列
     * @see Usr_entry#makeMailBody2
     */
    function replaceStr($body, $arrForm){

        //--------------------------------
        //置換文字列
        //--------------------------------
        //文言設定がある場合
        $wa_replaceStr= array();
        $wa_replaceStr["ID"] = $arrForm['e_user_id'];
        $wa_replaceStr["SEI"] = $arrForm["edata1"];
        $wa_replaceStr["MEI"] = $arrForm["edata2"];
        $wa_replaceStr["MIDDLE"]= $arrForm["edata7"];

        $wk_title = ($arrForm["edata57"] == "") ? "" : $GLOBALS["titleList"][$arrForm["edata57"]];
        $wa_replaceStr["TITLE"]= $wk_title;

        $wa_replaceStr["SECTION"]= $arrForm["edata10"];

        if($this->formdata["type"] != "3"){
            if($this->formdata["edit_flg"] == "1"){
                $wa_replaceStr["PASSWORD"]= $arrForm['e_user_passwd'];
            }
        }

        $db = new DbGeneral();
        $wk_day = Mng_function::getEntryDate($db, $arrForm['eid']);

        $wa_replaceStr["INSERT_DAY"]= $wk_day["insday"];
        $wa_replaceStr["UPDATE_DAY"]= $wk_day["upday"];

        // 置換文字反映
        $body = Mng_function::wordReplace($body, $wa_replaceStr);

        // 改行コードを統一
        return  str_replace(array("\r\n", "\r"), "\n", $body);
    }


	// 値のチェック
	function checkParam() {

		// eidは数字の配列でなければならない
		if(isset($this->arrForm["eid"])) {

			if(is_array($this->arrForm["eid"])) {
				foreach($this->arrForm["eid"] as $id) {
					if(!is_numeric($id)) return false;
				}
			} else {
				return false;
			}

		}


		return true;
	}


    function getEids(&$arrForm){
        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid();

        $arrForm['eid'] = array();
        foreach($arrEid as $_page => $_arrEid){
            foreach($_arrEid as $_eid => $_checked){
                if($_checked != 1) continue;

                $key = $_page ."-". $_eid;
                $arrForm['eid'][$key] = $_eid;
            }
        }

        $this->arrEid = $arrEid;
    }
}

/**
 * メイン処理開始
 **/

$c = new mailsend();
$c->main();







?>
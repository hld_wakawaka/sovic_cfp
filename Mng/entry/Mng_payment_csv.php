<?php


include_once(MODULE_DIR.'custom/Form.class.php');

/**
 * 管理者　決済CSVダウンロード
 *
 * @author salon
 *
 */
class payment_csv{


	/**
	 * コンストラクタ
	 */
	function payment_csv(){

		//----------------------------
		//インスタンス
		//----------------------------
		$this->db = new DbGeneral;
		$this->o_crypt = new CryptClass;
		$this->objEntry = new Entry;

	}

	/**
	 * メイン処理
	 */
	function main(){


		//------------------------------
		//初期化
		//------------------------------
		$this->arrErr = "";
		$this->delimiter = "\t";

		LoginMember::checkLoginRidirect();


		//------------------------------------------
		//外部ファイル読み込み
		//------------------------------------------
		$this->inc_dir = "entry/include/";
		$form_id = $GLOBALS["userData"]["form_id"];
		$wb_ret = $this->_chkIncFiles($this->inc_dir, $form_id, "payment_csv", "php");
		if($wb_ret){
			$class_name = "payment_csv".$form_id;
			$this->o_include = new $class_name(
											array(
												 $this->objEntry
												,$this->db
												,$this->o_crypt
												)
											);

			return $this->o_include->main();
		}


		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;


		//--------------------------------------------
		//定数情報取得
		//--------------------------------------------
		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_jpo1 = $GLOBALS["jpo1_J"];			//支払回数
			$this->wa_method = $GLOBALS["method_J"];		//支払方法
			$this->wa_card_type = $GLOBALS["card_type_J"];	//カードの種類
			$this->wa_pay_status = $GLOBALS["paymentstatusList"];	//決済ステータス
		}
		else{
			$this->wa_jpo1 = $GLOBALS["jpo1_E"];			//支払回数
			$this->wa_method = $GLOBALS["method_E"];		//支払方法
			$this->wa_card_type = $GLOBALS["card_type_E"];	//カードの種類
			$this->wa_pay_status = $GLOBALS["paymentstatusList"];	//決済ステータス
		}

		//--------------------------------
		//フォーム情報取得
		//--------------------------------
		//その他決済項目
		$this->o_form = new Form();
		$this->wa_ather_price = array("カテゴリー");

		if($this->o_form->formData["ather_price"] != ""){
			$wk_price = explode("\n", $this->o_form->formData["ather_price"]);
			
			foreach($wk_price as $key => $data){

				$data = str_replace(array("　"," "), "|",trim($data));
				$wk_price_data = explode("|", $data);
				$this->wa_ather_price[] = $wk_price_data[0];
			}
		}



		//--------------------------------
		//対象データ取得
		//--------------------------------
		//データ取得（決済情報）
		list($wb_ret, $list) = $this->_getData();
		if(!$wb_ret){
			//終了処理
			return array(false, "出力可能なデータはありません。");
		}


		//-----------------------------
		//CSVヘッダ部分
		//-----------------------------
		$this->header = $this->_makeHeader();

		//-----------------------------
		//CSVデータ部分
		//-----------------------------
		$this->csvdata = "";

		foreach($list as $data){


			//支払明細情報取得
			$payment_detail = $this->objEntry->getPaymentDetail($this->db, $data["entry_id"]);

			$this->csvdata .= $this->_makeData($data, $payment_detail);
		}


		//ヘッダとデータを連結して出力
		$exp_data = mb_convert_encoding($this->header.$this->csvdata, "SJIS", "UTF8");
		

		return array(true, $exp_data);




	}




	/**
	 * 外部ファイルが存在するかチェック
	 * 	拡張子がPHPの場合はファイルをinclude_onceする
	 *
	 * @param stirng ディレクトリ
	 * @param int form_id
	 * @param string ファイル名
	 * @param string 拡張子
	 */
	function _chkIncFiles($ps_dir, $pn_form_id, $ps_file_name, $ps_extension){

		//探したいファイルの名前
		$find_file = $ps_file_name.$pn_form_id.".".$ps_extension;

		if (is_dir($ps_dir)) {
		    if ($dh = opendir($ps_dir)) {
		        while (($file = readdir($dh)) !== false) {
		        	//外部読み込みファイルが見つかった場合
		            if($file == $find_file){
		            	//PHPファイルの場合
		            	if($ps_extension == "php"){
							include_once($ps_dir.$find_file);
		            	}

						return true;
		            }
		        }
		        closedir($dh);
		    }
		}

		return false;
	}




	/**
	 * 対象データ取得
	 */
	function _getData(){

		//$table = $this->objEntry->getTable($GLOBALS["userData"]["type"]);

		//フォーム頭文字の長さ
		$len_formhead = strlen($GLOBALS["userData"]["head"])  + 1;

		$column = "a.eid as entry_id, a.edata1, a.edata2, a.edata3, a.edata4, a.edata5, a.edata6, a.edata7, a.invalid_flg, substr(a.e_user_id, length(a.e_user_id)-4 ,5) as entry_no";
		$column .= ",b.*, to_char(b.rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(b.udate, 'yyyy/mm/dd hh24:mi:ss') as upday";

		$from = "entory_r a left join payment b on a.form_id = b.form_id and a.eid = b.eid and a.del_flg = 0 and b.del_flg=0";
		$from .= " and a.form_id = ".$GLOBALS["userData"]["form_id"];
		$where[] = "a.form_id = ".$this->db->quote($GLOBALS["userData"]["form_id"]);
		$where[] = "a.del_flg = 0";

		// 有効・無効
		if(!isset($this->arrForm["invalid"])) {
			$where[] = "a.invalid_flg = 0";
		} else {
			if(!isset($this->arrForm["invalid_flg1"])) $this->arrForm["invalid_flg1"] = "0";
			if(!isset($this->arrForm["invalid_flg2"])) $this->arrForm["invalid_flg2"] = "0";
			if($this->arrForm["invalid_flg1"] == "1" && $this->arrForm["invalid_flg2"] == "0") {
				$where[] = "a.invalid_flg = 0";
			} elseif($this->arrForm["invalid_flg1"] == "0" && $this->arrForm["invalid_flg2"] == "1") {
				$where[] = "a.invalid_flg = 1";
			}
		}

		//　応募者
		if($this->arrForm["user_name"] != "") {
			$arrval = GeneralFnc::customExplode($this->arrForm["user_name"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "a.edata1 like ".$this->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}

		// ステータス
		if(isset($this->arrForm["status1"]) && $this->arrForm["status1"] == "1") $subwhere[] = "a.status = 1";
		if(isset($this->arrForm["status2"]) && $this->arrForm["status2"] == "2") $subwhere[] = "a.status = 0";
		if(isset($this->arrForm["status3"]) && $this->arrForm["status3"] == "3") $subwhere[] = "a.status = 2";
		if(isset($this->arrForm["status4"]) && $this->arrForm["status4"] == "4") $subwhere[] = "a.status = 99";


		if(isset($subwhere)) {
			$where[] = "(".implode(" or ", $subwhere).")";
			unset($subwhere);
		}

		// 受付期間
		if($this->arrForm["syear"] > 0) {
			if($this->arrForm["smonth"] == "") $this->arrForm["smonth"] = "1";
			if($this->arrForm["sday"] == "") $this->arrForm["sday"] = "1";
			$sdate = sprintf("%04d", $this->arrForm["syear"])."-".sprintf("%02d", $this->arrForm["smonth"])."-".sprintf("%02d", $this->arrForm["sday"]);
			$where[] = "a.rdate >= ".$this->db->quote($sdate);
		}
		if($this->arrForm["eyear"] > 0) {
			if($this->arrForm["emonth"] == "") $this->arrForm["emonth"] = "12";
			if($this->arrForm["eday"] == "") {
				$edate = date("Y-m-d", mktime(0, 0, 0, $this->arrForm["emonth"]+1, 0, $this->arrForm["eyear"]));
			} else {
				$edate = sprintf("%04d", $this->arrForm["eyear"])."-".sprintf("%02d", $this->arrForm["emonth"])."-".sprintf("%02d", $this->arrForm["eday"]);
			}

			$where[] = "a.rdate <= ".$this->db->quote($edate);
		}


		// 支払方法
		if(isset($this->arrForm["payment_method"])){
			if(is_array($this->arrForm["payment_method"])) {
				if(count($this->arrForm["payment_method"]) > 0) {
					foreach($this->arrForm["payment_method"] as $val) {
						$subwhere[] = "b.payment_method = ".$this->db->quote($val);
					}
					$where[] = "(".implode(" or ", $subwhere).")";
					unset($subwhere);
				}

			}
		}


		// 支払ステータス
		if(isset($this->arrForm["payment_status"])){
			if(is_array($this->arrForm["payment_status"])) {
				if(count($this->arrForm["payment_status"]) > 0) {
					foreach($this->arrForm["payment_status"] as $val) {
						$subwhere[] = "b.payment_status = ".$this->db->quote($val);
					}
					$where[] = "(".implode(" or ", $subwhere).")";
					unset($subwhere);
				}
			}
		}


		//登録No 開始
		if($this->arrForm["s_entry_no"] != "") {
			//桁数を0埋め５桁にする
			$s_entry_no = sprintf("%05d", $this->arrForm["s_entry_no"]);
			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) >= ".$this->db->quote($s_entry_no);

		}

		//登録No 終了
		if($this->arrForm["e_entry_no"] != "") {
			//桁数を0埋め５桁にする
			$e_entry_no = sprintf("%05d", $this->arrForm["e_entry_no"]);
			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) <= ".$this->db->quote($e_entry_no);

		}



		//国
		if(isset($this->arrForm["country"])){
			if($this->arrForm["country"] != "") {
				$where[] = "edata60 like ".$this->db->quote("%".$this->arrForm["country"]."%");
			}
		}

		// 受付期間
		if($this->arrForm["upd_syear"] > 0) {
			if($this->arrForm["upd_smonth"] == "") $this->arrForm["upd_smonth"] = "1";
			if($this->arrForm["upd_sday"] == "") $this->arrForm["upd_sday"] = "1";
			$upd_sdate = sprintf("%04d", $this->arrForm["upd_syear"])."-".sprintf("%02d", $this->arrForm["upd_smonth"])."-".sprintf("%02d", $this->arrForm["upd_sday"]);
			$where[] = "udate >= ".$this->db->quote($upd_sdate);
		}
		if($this->arrForm["upd_eyear"] > 0) {
			if($this->arrForm["upd_emonth"] == "") $this->arrForm["upd_emonth"] = "12";
			if($this->arrForm["upd_eday"] == "") {
				$upd_edate = date("Y-m-d", mktime(0, 0, 0, $this->arrForm["upd_emonth"]+1, 0, $this->arrForm["upd_eyear"]));
			} else {
				$upd_edate = sprintf("%04d", $this->arrForm["upd_eyear"])."-".sprintf("%02d", $this->arrForm["upd_emonth"])."-".sprintf("%02d", $this->arrForm["upd_eday"]);
			}

			$where[] = "udate <= ".$this->db->quote($upd_edate);
		}

		//カナ
		if($this->arrForm["user_name_kana"] != "") {
			$arrval = GeneralFnc::customExplode($this->arrForm["user_name_kana"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata3 like ".$this->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}

		//------------------------------------
		//表示順
		//------------------------------------
		$wk_order_by = "udate desc";

		if($this->arrForm["sort_name"] != "" && $this->arrForm["sort"] != ""){
			$wk_order_by  = "";
			if($this->arrForm["sort_name"] == "2"){
				$wk_order_by .= "entry_no ";
			}
			else{
				$wk_order_by .= "udate ";
			}

			if($this->arrForm["sort"] == "1"){
				$wk_order_by .= "desc";
			}
			else{
				$wk_order_by .= "asc";
			}
		}


		$orderby = $wk_order_by;
		//$orderby = "a.eid";

		$list_data = $this->db->getListData($column, $from, $where, $orderby);


		if(!$list_data){
			return array(false, "決済情報の取得に失敗しました。");
		}




		return array(true, $list_data);


	}




	/**
	 * CSVヘッダ生成
	 */
	function _makeHeader(){

		$header = "";
		$header[] = "応募者氏名";
		$header[] = "応募者氏名（カナ）";
		$header[] = "取引ID";
		$header[] = "お支払い方法";
		$header[] = "ステータス";

		$header[] = "支払期限";
		$header[] = "クレジット番号";
		$header[] = "カード会社";
		$header[] = "カード名義人";
		$header[] = "有効期限";
		$header[] = "クレジット支払い区分";
		$header[] = "クレジット支払い回数";
		$header[] = "セキュリティコード";

		$header[] = "お振込み人名義";
		$header[] = "お支払銀行名";
		$header[] = "お振込み日";

		$header[] = "コンビニ　ショップコード";
		$header[] = "姓";
		$header[] = "名";
		$header[] = "セイ";
		$header[] = "メイ";
		$header[] = "電話番号";
		$header[] = "メールアドレス";
		$header[] = "郵便番号";
		$header[] = "住所";
		$header[] = "成約日";
		$header[] = "決済機関コード";

		//$header[] = "支払詳細";


		//------------------------------
		//決済項目
		//------------------------------
		// 43番フォーム:その他決済項目のみCSV出力 : 金額　-> カテゴリ
//		if($this->o_form->formData['form_id'] == 43){
//			$header[] = "カテゴリー";
//			foreach($this->wa_ather_price as $data){
//				if(strstr($data, "Additional") == true){
////					$header[] = $data;
//					$header[] = str_replace(array("_"), " ",$data);
//				}
//			}
//
//		}else{
			foreach($this->wa_ather_price as $data){
//				$header[] = $data;

				// 2011/09/21 タグ除去追加 TODO
				$header[] = strip_tags(str_replace(array("_"), " ",$data));
			}
//		}

		$header[] = "合計金額";
		$header[] ="登録日";
		$header[] ="更新日";
		$header[] ="有効・無効";

		//生成
		$ret_buff = implode($this->delimiter, $header);

		$ret_buff .="\n";

		return $ret_buff;


	}

	/**
	 * CSV出力データ生成
	 */
	function _makeData($pa_param, $pa_detail){

		$ret_buff = "";
		$wk_buff = "";

		//応募者氏名
		$wk_buff[] =  "\"".$pa_param["edata1"]."　".$pa_param["edata2"]. "\"";

		//応募者氏名（カナ）
		$wk_buff[] =  "\"".$pa_param["edata3"]."　".$pa_param["edata4"]. "\"";

		//取引ID
		$wk_buff[] = "\"".$pa_param["order_id"]."\"";

		//お支払い方法
		$wk_buff[] = ($pa_param["payment_method"] != "") ? "\"".$this->wa_method[$pa_param["payment_method"]]."\"" : "";

		//ステータス
		$wk_buff[] = ($pa_param["payment_status"] != "") ? "\"".$this->wa_pay_status[$pa_param["payment_status"]]."\"" : "";



		//支払期限
		$wk_buff[] = "\"".$pa_param["limit_date"]."\"";

		//クレジット番号
		$wk_buff[] = ($pa_param["c_number"] != "") ? "\"".$this->o_crypt->crypt_decode($pa_param["c_number"])."\"" : "";

		//カード会社
		$wk_buff[] = ($pa_param["c_company"] != "") ? "\"".$this->wa_card_type[$pa_param["c_company"]]."\"" : "";

		//カード名義人
		$wk_buff[] = "\"".$pa_param["c_holder"]."\"";

		//有効期限
		$wk_buff[] = ($pa_param["c_date"] != "") ? "\"".$this->o_crypt->crypt_decode($pa_param["c_date"])."\"" : "";


		//クレジット支払い方法
		$wk_buff[] = ($pa_param["c_method"] != "") ? "\"".$this->wa_jpo1[$pa_param["c_method"]]."\"" : "";

		//クレジット支払い回数
		$wk_buff[] = "\"".$pa_param["c_split"]."\"";

		//セキュリティコード
		$wk_buff[] = ($pa_param["c_scode"] != "") ? "\"".$this->o_crypt->crypt_decode($pa_param["c_scode"])."\"" : "";


		//お振込み人名義
		$wk_buff[] = "\"".$pa_param["lessee"]."\"";

		//お支払銀行名
		$wk_buff[] = "\"".$pa_param["bank"]."\"";


		//お振込み日
		$wk_buff[] = "\"".$pa_param["closure"]."\"";


		//コンビニ　ショップコード
		$wk_buff[] = "";

		//姓
		$wk_buff[] = "\"".$pa_param["name1"]."\"";

		//名
		$wk_buff[] = "\"".$pa_param["name2"]."\"";

		//セイ
		$wk_buff[] =  "\"".$pa_param["kana1"]."\"";

		//メイ
		$wk_buff[] = "\"".$pa_param["kana2"]."\"";

		//電話番号
		$wk_buff[] = "\"".$pa_param["tel"]."\"";

		//メールアドレス
		$wk_buff[] = "\"".$pa_param["mail"]."\"";


		//郵便番号
		$wk_buff[] = "\"".$pa_param["zip"]."\"";

		//住所
		$wk_buff[] = "\"".$pa_param["addr"]."\"";

		//成約日
		$wk_buff[] = "\"".$pa_param["sold_date"]."\"";

		//決済機関コード
		$wk_buff[] = "\"".$pa_param["bank_code"]."\"";


		//支払詳細
		$wk_detail = "";
//		$wa_item = array();
//
//		if($pa_detail){
//
//			foreach($pa_detail as $data){
//				$wa_item[] = $data["name"].":".$data["price"]."円 ×".$data["quantity"];
//			}
//
//			$wk_detail=implode("/", $wa_item);
//			$wk_detail = "\"".$wk_detail."\"";
//
//		}
//$wk_buff[] = $wk_detail;



		if($pa_detail){
			
			// 43専用
			if($this->o_form->formData['form_id'] == 43){
				$wk_buff = array_merge($wk_buff, $this->pay_43($pa_detail));
			}else{
				$wk_buff = array_merge($wk_buff, $this->pay_c($pa_detail));
			}
		}

		else{
			foreach($this->wa_ather_price as $ather_name){
				$wk_buff[] = "";
			}
		}



		//金額
		$wk_buff[] = "\"".$pa_param["price"]."\"";



		//登録日
		$wk_buff[] = ($pa_param["insday"] != "") ? "\"".$pa_param["insday"]."\"" : "";

		//更新日
		$wk_buff[] = ($pa_param["upday"] != "") ? "\"".$pa_param["upday"]."\"" : "";
		
		//有効・無効
		$wk_buff[] = ($pa_param["invalid_flg"] == "0") ? "\"有効\"" : "\"無効\"";

		$ret_buff = implode($this->delimiter, $wk_buff);

		$ret_buff .= "\n";

		return $ret_buff;


	}
	
	
	// 金額、その他決済項目のCSV整形
	function pay_c($pa_detail){
	
			foreach($this->wa_ather_price as $key => $ather_name){
				$quantity = "";
				$match_flg = "";

				if($key == 0){
					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
						

						$pos = strpos($data["name"], "事前登録料金");
						if($pos !== false){
							$quantity = $data["name"];

							// CSV用に整形--------------------------------------------------------------
							if($csv_header['csv_price_head0'] != ""){
								$quantity = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity);
							}
							$quantity = "\"".$quantity."\"";
							


							$match_flg = "1";
							break 1;
						}
					}
				}
				else{

					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);


						if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
//						if(str_replace(array("_"), " ",$ather_name) == $data["name"]){
							$quantity = $data["quantity"];

//							// CSV用に整形--------------------------------------------------------------
//							if($csv_header['csv_ather_price_head0'] != ""){
//								$quantity = $csv_header['csv_ather_price_head0']."<".$quantity.">";
//							}
							$match_flg = "1";
							break 1;
						}
					}
				}
				if($match_flg == "1"){
					$wk_buff[] = $quantity;
				}
				else{
					$wk_buff[] = "";
				}

			}
			return $wk_buff;
	}

	// 金額、その他決済項目のCSV整形
	function pay_43($pa_detail){

			foreach($this->wa_ather_price as $key => $ather_name){
				$quantity = "";
				$match_flg = "";

				if($key == 0){
					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
						

						$pos = strpos($data["name"], "事前登録料金");
						if($pos !== false){
							$quantity = $data["name"];

							// CSV用に整形--------------------------------------------------------------
							if($csv_header['csv_price_head0'] != ""){
								$quantity = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity);
							}
							$quantity = "\"".$quantity."\"";
							


							$match_flg = "1";
							break 1;
						}
					}
				}
				else{

					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);


						if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
//						if(str_replace(array("_"), " ",$ather_name) == $data["name"]){
							$quantity = $data["quantity"];

//							// CSV用に整形--------------------------------------------------------------
//							if($csv_header['csv_ather_price_head0'] != ""){
//								$quantity = $csv_header['csv_ather_price_head0']."<".$quantity.">";
//							}
							$match_flg = "1";
							break 1;
						}
					}
				}
				if($match_flg == "1"){
					$wk_buff[] = $quantity;
				}
				else{
					$wk_buff[] = "";
				}

			}
			return $wk_buff;
	}

//	function pay_43($pa_detail){
//
//		// 金額用
//		$quantity0 = "";
//		$match_flg0 = "";
//		$index = "0";
//		
//		$quantity00 = "";
//
//		foreach($this->wa_ather_price as $key => $ather_name){
//				
//				
//			// その他決済項目用
//			$quantity = "";
//			$match_flg = "";
//			
//			// 金額	
//			if(strstr($ather_name, "Additional") != true){
//				foreach($pa_detail as $data){
//					
//					
//
//					if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
////					if( ($data['name'] == str_replace("_", " ", $ather_name))){
//
//						if($index == "0"){
//							$quantity0[] = $data['name'];						
//						}else{
//							$quantity0[] = $data['name'];
//						}
//
//
//						$from = "payment";
//						$column = "csv_price_head0";
//						$where = "eid = {$data['eid']}";
//						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
//
//						// CSV用に整形--------------------------------------------------------------
//						$quantity00 = "事前登録料金";
//						if($csv_header['csv_price_head0'] != ""){
//							$quantity00 = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity00);
//						}
//						$quantity00 = "\"".$quantity00."\"";
//
//						$index ="1";
//						$match_flg0 = "1";
//						break 1;
//					}
//				}
//			}
//			
//			// その他決済項目
//			else{
//				if($match_flg0 == "1"){
//
////					$wk_buff[] = "\"".implode(", ", $quantity0)."\"";
//					$wk_buff[] = $quantity00;
//
//
//				}
//				else{
//					$wk_buff[] = "";
//				}
//				$match_flg0 = "";
//
//				foreach($pa_detail as $data){
//					
//						$from = "payment";
//						$column = "csv_ather_price_head0";
//						$where = "eid = {$data['eid']}";
//						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
//
//
//					if(str_replace(array("_"), " ",$ather_name) == $data["name"]){
//						$quantity = $data["quantity"];
//							
////						if($csv_header['csv_ather_price_head0'] != ""){
////							$quantity = $csv_header['csv_ather_price_head0']."(".$quantity.")";
////						}
//						$match_flg = "1";
//						break 1;
//					}
//				}
//			}
//				
//			if($match_flg == "1"){
//				$wk_buff[] = $quantity;
//			}
//			else{
//				$wk_buff[] = "";
//			}
//		}
//		unset($quantity0);
//
//		foreach($wk_buff as $key => $data){
//			if($key <= 14){
//				unset($wk_buff[$key]);
//			}
//		}
//		unset($wk_buff[17]);
//		unset($wk_buff[19]);
//
//		return $wk_buff;
//	}

//	function pay_43($pa_detail){
//
//		// 金額用
//		$quantity0 = "";
//		$match_flg0 = "";
//		$index = "0";
//		
//		foreach($this->wa_ather_price as $key => $ather_name){
//				
//				
//			// その他決済項目用
//			$quantity = "";
//			$match_flg = "";
//			
//			// 金額	
//			if(strstr($ather_name, "Additional") != true){
//				foreach($pa_detail as $data){
//
//
//					if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
////					if( ($data['name'] == str_replace("_", " ", $ather_name))){
//
//						if($index == "0"){
//							$quantity0[] = $data['name'];						
//						}else{
//							$quantity0[] = $data['name'];
//						}
//						$index ="1";
//						$match_flg0 = "1";
//						break 1;
//					}
//				}
//			}
//			
//			// その他決済項目
//			else{
//				if($match_flg0 == "1"){
//	
//						$from0 = "payment";
//						$column0 = "csv_price_head0";
//						$where0 = "eid = {$data['eid']}";
//						$csv_header0 =  $this->db->getData($column0, $from0, $where0, __FILE__, __LINE__);
//
//						if($csv_header0['csv_price_head0'] != ""){
//							$wk_buff[] = $csv_header0['csv_price_head0'];
//						}else{
//							$wk_buff[] = "事前登録料金";
//						}
//	
//				}
//				else{
//					$wk_buff[] = "";
//				}
//				$match_flg0 = "";
//
//				foreach($pa_detail as $data){
//					
//						$from = "payment";
//						$column = "csv_ather_price_head0";
//						$where = "eid = {$data['eid']}";
//						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
//						
//					
//					if(str_replace(array("_"), " ",$ather_name) == $data["name"]){
//						$quantity = $data["quantity"];
//							
//						if($csv_header['csv_ather_price_head0'] != ""){
//							$quantity = $csv_header['csv_ather_price_head0']."<".$quantity.">";
//						}
//						$match_flg = "1";
//						break 1;
//					}
//				}
//			}
//				
//			if($match_flg == "1"){
//				$wk_buff[] = $quantity;
//			}
//			else{
//				$wk_buff[] = "";
//			}
//		}
//		unset($quantity0);
//
//		foreach($wk_buff as $key => $data){
//			if($key <= 14){
//				unset($wk_buff[$key]);
//			}
//		}
//		unset($wk_buff[17]);
//		unset($wk_buff[19]);
//
//		return $wk_buff;
//	}

//	金額の部分をカンマでつなげて全部出すver
//	function pay_43($pa_detail){
//
//		// 金額用
//		$quantity0 = "";
//		$match_flg0 = "";
//		$index = "0";
//		
//		foreach($this->wa_ather_price as $key => $ather_name){
//				
//				
//			// その他決済項目用
//			$quantity = "";
//			$match_flg = "";
//			
//			// 金額	
//			if(strstr($ather_name, "Additional") != true){
//				foreach($pa_detail as $data){
//					
//					
//
//					if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
////					if( ($data['name'] == str_replace("_", " ", $ather_name))){
//
//						if($index == "0"){
//							$quantity0[] = $data['name'];						
//						}else{
//							$quantity0[] = $data['name'];
//						}
//						$index ="1";
//						$match_flg0 = "1";
//						break 1;
//					}
//				}
//			}
//			
//			// その他決済項目
//			else{
//				if($match_flg0 == "1"){
//					$wk_buff[] = "\"".implode(", ", $quantity0)."\"";
//
//
//				}
//				else{
//					$wk_buff[] = "";
//				}
//				$match_flg0 = "";
//
//				foreach($pa_detail as $data){
//					
//						$from = "payment";
//						$column = "csv_ather_price_head0";
//						$where = "eid = {$data['eid']}";
//						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
//						
//					
//					if(str_replace(array("_"), " ",$ather_name) == $data["name"]){
//						$quantity = $data["quantity"];
//							
//						if($csv_header['csv_ather_price_head0'] != ""){
//							$quantity = $csv_header['csv_ather_price_head0']."(".$quantity.")";
//						}
//						$match_flg = "1";
//						break 1;
//					}
//				}
//			}
//				
//			if($match_flg == "1"){
//				$wk_buff[] = $quantity;
//			}
//			else{
//				$wk_buff[] = "";
//			}
//		}
//		unset($quantity0);
//
//		foreach($wk_buff as $key => $data){
//			if($key <= 14){
//				unset($wk_buff[$key]);
//			}
//		}
//		unset($wk_buff[17]);
//		unset($wk_buff[19]);
//
//		return $wk_buff;
//	}
}







?>
<?php

include_once(MODULE_DIR.'custom/Form.class.php');

/**
 * 管理者　決済CSVダウンロード
 *
 * @author salon
 *
 */
class all_csv extends entry_csv{


	/**
	 * コンストラクタ
	 */
	function all_csv(){

		//----------------------------
		//インスタンス
		//----------------------------
		$this->db = new DbGeneral;
		$this->o_crypt = new CryptClass;
		$this->objEntry = new Entry;
		$this->o_itemini = new item_ini();
	}


	/**
	 * メイン処理
	 */
	function main($o_payment_csv){


		//------------------------------
		//初期化
		//------------------------------
		$this->arrErr = "";
		$this->delimiter = "\t";

		LoginMember::checkLoginRidirect();


		//------------------------------------------
		//外部ファイル読み込み
		//------------------------------------------
		$this->inc_dir = "entry/include/";
		$form_id = $GLOBALS["userData"]["form_id"];
		$wb_ret = $this->_chkIncFiles($this->inc_dir, $form_id, "all_csv", "php");
		if($wb_ret){
			$class_name = "all_csv".$form_id;
			$this->o_include = new $class_name(
											array(
												 $this->db
												,$this->o_crypt
												,$this->objEntry
												,$this->o_itemini
												)
											);

			return $this->o_include->main($o_payment_csv);
		}


		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;


		//--------------------------------------------
		//定数情報取得
		//--------------------------------------------
		//連絡先
		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_contact = $GLOBALS["contact_J"];
		}
		else{
			$this->wa_contact = $GLOBALS["contact_E"];
		}

		//会員・非会員
		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_kaiin = $GLOBALS["member_J"];
		}
		else{
			$this->wa_kaiin = $GLOBALS["member_E"];
		}

		//共著者の有無
		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_coAuthor = $GLOBALS["coAuthor_J"];
		}
		else{
			$this->wa_coAuthor = $GLOBALS["coAuthor_E"];
		}



		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_jpo1 = $GLOBALS["jpo1_J"];			//支払回数
			$this->wa_method = $GLOBALS["method_J"];		//支払方法
			$this->wa_card_type = $GLOBALS["card_type_J"];	//カードの種類
			$this->wa_pay_status = $GLOBALS["paymentstatusList"];	//決済ステータス
		}
		else{
			$this->wa_jpo1 = $GLOBALS["jpo1_E"];			//支払回数
			$this->wa_method = $GLOBALS["method_E"];		//支払方法
			$this->wa_card_type = $GLOBALS["card_type_E"];	//カードの種類
			$this->wa_pay_status = $GLOBALS["paymentstatusList"];	//決済ステータス
		}

		//--------------------------------
		//フォーム情報取得
		//--------------------------------
		//その他決済項目
		$this->o_form = new Form();
		$this->wa_ather_price = array("カテゴリー");

		if($this->o_form->formData["ather_price"] != ""){
			$wk_price = explode("\n", $this->o_form->formData["ather_price"]);
			foreach($wk_price as $key => $data){

				$data = str_replace(array("　"," "), "|",trim($data));
				$wk_price_data = explode("|", $data);
				$this->wa_ather_price[] = $wk_price_data[0];
			}
		}

		//----------------------------------
		//フォームの項目名称取得
		//----------------------------------
		$this->ini = $this->o_itemini->getList(1, $GLOBALS["userData"]["lang"]);
		if(!$this->ini){
			//終了処理
			return array(false, "フォーム項目情報の取得に失敗しました。");
		}


		//---------------------------------
		//フォーム項目情報取得
		//---------------------------------
		$formitem = $this->objEntry->getFormItem($this->db, $GLOBALS["userData"]["form_id"]);
		if(!$formitem){
			//終了処理
			return array(false, "フォーム項目情報の取得に失敗しました。");
		}
		foreach($formitem as $data){
			$wk_item[$data["item_id"]] = $data;
		}


		//項目情報生成
		foreach($this->ini as $i_key => $data){
			$this->itemData[$i_key] = $wk_item[$i_key];
			$this->itemData[$i_key]["select"] = "";

			$this->itemData[$i_key]["item_name"] = ($wk_item[$i_key]["item_name"] != "") ? $wk_item[$i_key]["item_name"] : $data["defname"];


			//チェック・ラジオボタン・セレクトボックス
			if($wk_item[$i_key]["item_select"] != ""){
				$wk_select  = explode("\n", $wk_item[$i_key]["item_select"]);
				$i =  "";
				$set_select = array();
				foreach($wk_select as $num => $val){
					$i = $num+1;
					$set_select[$i] = str_replace(array("#"), "", $val);
				}
				$this->itemData[$i_key]["select"] = $set_select;

			}
		}



		//--------------------------------
		//対象データ取得
		//--------------------------------
		//データ取得（エントリー情報）
		list($wb_ret, $list) = $this->_getData($this->objEntry);
		if(!$wb_ret){
			//終了処理
			return array(false, "出力可能なデータはありません。");
		}


		//-----------------------------
		//CSVヘッダ部分
		//-----------------------------
		//エントリー情報ヘッダー
		$this->entry_header = $this->_makeHeader($this->itemData);

		//決済情報ヘッダー
		$this->payment_header = $this->_makePaymentHeader();


		//エントリー情報ヘッダーの改行を削除
		$this->entry_header = str_replace(array("\n\r","\r","\n"), "", $this->entry_header);

		//連結
		$this->header =$this->entry_header.$this->delimiter.$this->payment_header;



		//-----------------------------
		//CSVデータ部分
		//-----------------------------
		$this->csvdata = "";
		$this->entry_csvdata = "";
		$this->payment_csvdata = "";

		//エントリー情報ループ
		foreach($list as $data){

			//----------------------------
			//エントリー情報CSVデータ生成
			//----------------------------
			$wk_entry_csvdata = $this->_makeData($data, $this->itemData);

			//決済情報取得
			list($wb_ret, $wk_paymentdata) = $this->_getPaymentData($data["eid"]);
			if(!$wb_ret){
				//終了処理
				return array(false, "決済情報の取得に失敗しました。");
			}


			//支払明細情報取得
			$payment_detail = $this->objEntry->getPaymentDetail($this->db, $data["eid"]);

			//-----------------------------
			//決済情報CSVデータ生成
			//-----------------------------
			$wk_payment_csvdata = $this->_makePaymentData($wk_paymentdata[0], $payment_detail);


			//-----------------------------
			//エントリー情報と決済情報を連結
			//-----------------------------
			$wk_entry_csvdata = str_replace(array("\n\r","\r","\n"), "", $wk_entry_csvdata);
			$this->csvdata .=$wk_entry_csvdata.$this->delimiter.$wk_payment_csvdata;



		}



		//ヘッダとデータを連結して出力
		$exp_data = mb_convert_encoding($this->header.$this->csvdata, "SJIS", "UTF8");

		return array(true, $exp_data);



	}


	/**
	 * 対象データ取得
	 */
	function _getPaymentData($pn_eid){


		$column = "a.eid, a.edata1, a.edata2, a.edata3, a.edata4, a.edata5, a.edata6, a.edata7, a.invalid_flg";
		$column .= ",b.*, to_char(b.rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(b.udate, 'yyyy/mm/dd hh24:mi:ss') as upday";

		$from = "entory_r a left join payment b on a.form_id = b.form_id and a.eid = b.eid and a.del_flg = 0 and b.del_flg=0";
		$from .= " and a.form_id = ".$this->db->quote($GLOBALS["userData"]["form_id"]);
		$where[] = "a.eid = ".$this->db->quote($pn_eid);

		$orderby = "a.eid";

		$list_data = $this->db->getListData($column, $from, $where, $orderby);
		if(!$list_data){
			return array(false, "決済情報の取得に失敗しました。");
		}


		return array(true, $list_data);


	}




	/**
	 * CSVヘッダ生成
	 */
	function _makePaymentHeader(){

		$header = "";
//		$header[] = "応募者氏名";
//		$header[] = "応募者氏名（カナ）";
		$header[] = "取引ID";
		$header[] = "お支払い方法";
		$header[] = "ステータス";
		//$header[] = "金額";
		$header[] = "支払期限";
		$header[] = "クレジット番号";
		$header[] = "カード会社";
		$header[] = "カード名義人";
		$header[] = "有効期限";
		$header[] = "クレジット支払い区分";
		$header[] = "クレジット支払い回数";
		$header[] = "セキュリティコード";

		$header[] = "お振込み人名義";
		$header[] = "お支払銀行名";
		$header[] = "お振込み日";

		$header[] = "コンビニ　ショップコード";
		$header[] = "姓";
		$header[] = "名";
		$header[] = "セイ";
		$header[] = "メイ";
		$header[] = "電話番号";
		$header[] = "メールアドレス";
		$header[] = "郵便番号";
		$header[] = "住所";
		$header[] = "成約日";
		$header[] = "決済機関コード";

		//$header[] = "支払詳細";
		//------------------------------
		//決済項目
		//------------------------------
		// 43番フォーム:その他決済項目のみCSV出力 : 金額　-> カテゴリ
//		if($this->o_form->formData['form_id'] == 43){
//			$header[] = "カテゴリー";
//			foreach($this->wa_ather_price as $data){
//				if(strstr($data, "Additional") == true){
////					$header[] = $data;
//					$header[] = str_replace(array("_"), " ",$data);
//				}
//			}
//
//		}else{
			foreach($this->wa_ather_price as $data){
//				$header[] = $data;

				// 2011/09/21 タグ除去追加 TODO
				$header[] = strip_tags(str_replace(array("_"), " ",$data));
			}
//		}


		$header[] = "合計金額";
		$header[] ="登録日";
		$header[] ="更新日";
		$header[] ="有効・無効";


		//生成
		$ret_buff = implode($this->delimiter, $header);

		$ret_buff .="\n";

		return $ret_buff;


	}

	/**
	 * CSV出力データ生成
	 */
	function _makePaymentData($pa_param, $pa_detail){

		$ret_buff = "";
		$wk_buff = "";

//		//応募者氏名
//		$wk_buff[] =  "\"".$pa_param["edata1"]."　".$pa_param["edata2"]. "\"";
//
//		//応募者氏名（カナ）
//		$wk_buff[] =  "\"".$pa_param["edata3"]."　".$pa_param["edata4"]. "\"";

		//取引ID
		$wk_buff[] = "\"".$pa_param["order_id"]."\"";

		//お支払い方法
		$wk_buff[] = ($pa_param["payment_method"] != "") ? "\"".$this->wa_method[$pa_param["payment_method"]]."\"" : "";

		//ステータス
		$wk_buff[] = ($pa_param["payment_status"] != "") ? "\"".$this->wa_pay_status[$pa_param["payment_status"]]."\"" : "";

		//金額
//		$wk_buff[] = "\"".$pa_param["price"]."\"";

		//支払期限
		$wk_buff[] = "\"".$pa_param["limit_date"]."\"";

		//クレジット番号
		$wk_buff[] = ($pa_param["c_number"] != "") ? "\"".$this->o_crypt->crypt_decode($pa_param["c_number"])."\"" : "";

		//カード会社
		$wk_buff[] = ($pa_param["c_company"] != "") ? "\"".$this->wa_card_type[$pa_param["c_company"]]."\"" : "";

		//カード名義人
		$wk_buff[] = "\"".$pa_param["c_holder"]."\"";

		//有効期限
		$wk_buff[] = ($pa_param["c_date"] != "") ? "\"".$this->o_crypt->crypt_decode($pa_param["c_date"])."\"" : "";


		//クレジット支払い方法
		$wk_buff[] = ($pa_param["c_method"] != "") ? "\"".$this->wa_jpo1[$pa_param["c_method"]]."\"" : "";

		//クレジット支払い回数
		$wk_buff[] = "\"".$pa_param["c_split"]."\"";

		//セキュリティコード
		$wk_buff[] = ($pa_param["c_scode"] != "") ? "\"".$this->o_crypt->crypt_decode($pa_param["c_scode"])."\"" : "";


		//お振込み人名義
		$wk_buff[] = "\"".$pa_param["lessee"]."\"";

		//お支払銀行名
		$wk_buff[] = "\"".$pa_param["bank"]."\"";


		//お振込み日
		$wk_buff[] = "\"".$pa_param["closure"]."\"";


		//コンビニ　ショップコード
		$wk_buff[] = "";

		//姓
		$wk_buff[] = "\"".$pa_param["name1"]."\"";

		//名
		$wk_buff[] = "\"".$pa_param["name2"]."\"";

		//セイ
		$wk_buff[] =  "\"".$pa_param["kana1"]."\"";

		//メイ
		$wk_buff[] = "\"".$pa_param["kana2"]."\"";

		//電話番号
		$wk_buff[] = "\"".$pa_param["tel"]."\"";

		//メールアドレス
		$wk_buff[] = "\"".$pa_param["mail"]."\"";


		//郵便番号
		$wk_buff[] = "\"".$pa_param["zip"]."\"";

		//住所
		$wk_buff[] = "\"".$pa_param["addr"]."\"";

		//成約日
		$wk_buff[] = "\"".$pa_param["sold_date"]."\"";

		//決済機関コード
		$wk_buff[] = "\"".$pa_param["bank_code"]."\"";


		//支払詳細
//		$wk_detail = "";
//		$wa_item = array();
//
//		if($pa_detail){
//
//			foreach($pa_detail as $data){
//				$wa_item[] = $data["name"].":".$data["price"]."円 ×".$data["quantity"];
//			}
//
//			$wk_detail=implode("/", $wa_item);
//			$wk_detail = "\"".$wk_detail."\"";
//
//		}
//
//		$wk_buff[] = $wk_detail;


		if($pa_detail){
			// 43専用
			if($this->o_form->formData['form_id'] == 43){
				$wk_buff = array_merge($wk_buff, $this->pay_43($pa_detail));
			}else{
				$wk_buff = array_merge($wk_buff, $this->pay_c($pa_detail));
			}
		}
		else{
			foreach($this->wa_ather_price as $ather_name){
				$wk_buff[] = "";
			}
		}

		//金額
		$wk_buff[] = "\"".$pa_param["price"]."\"";

		//登録日
		$wk_buff[] = ($pa_param["insday"] != "") ? "\"".$pa_param["insday"]."\"" : "";

		//更新日
		$wk_buff[] = ($pa_param["upday"] != "") ? "\"".$pa_param["upday"]."\"" : "";
		
		//有効・無効
		$wk_buff[] = ($pa_param["invalid_flg"] == "0") ? "\"有効\"" : "\"無効\"";

		$ret_buff = implode($this->delimiter, $wk_buff);

		$ret_buff .= "\n";

		return $ret_buff;


	}
	
	
	
	
	// 金額、その他決済項目のCSV整形
	function pay_c($pa_detail){
	
			foreach($this->wa_ather_price as $key => $ather_name){
				$quantity = "";
				$match_flg = "";

				if($key == 0){
					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
						

						$pos = strpos($data["name"], "事前登録料金");
						if($pos !== false){
							$quantity = $data["name"];

							// CSV用に整形--------------------------------------------------------------
							if($csv_header['csv_price_head0'] != ""){
								$quantity = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity);
							}
							$quantity = "\"".$quantity."\"";
							


							$match_flg = "1";
							break 1;
						}
					}
				}
				else{

					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);


					if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
//					if( ($data['name'] == str_replace("_", " ", $ather_name))){
//						if(str_replace(array("_"), " ",$ather_name) == $data["name"]){
							$quantity = $data["quantity"];

							// CSV用に整形--------------------------------------------------------------
//							if($csv_header['csv_ather_price_head0'] != ""){
//								$quantity = $csv_header['csv_ather_price_head0']."(".$quantity.")";
//							}
							$match_flg = "1";
							break 1;
						}
					}
				}
				if($match_flg == "1"){
					$wk_buff[] = $quantity;
				}
				else{
					$wk_buff[] = "";
				}

			}
			return $wk_buff;
	}


	// 金額、その他決済項目のCSV整形
	function pay_43($pa_detail){

			foreach($this->wa_ather_price as $key => $ather_name){
				$quantity = "";
				$match_flg = "";

				if($key == 0){
					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
						

						$pos = strpos($data["name"], "事前登録料金");
						if($pos !== false){
							$quantity = $data["name"];

							// CSV用に整形--------------------------------------------------------------
							if($csv_header['csv_price_head0'] != ""){
								$quantity = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity);
							}
							$quantity = "\"".$quantity."\"";
							


							$match_flg = "1";
							break 1;
						}
					}
				}
				else{

					foreach($pa_detail as $data){

						$from = "payment";
						$column = "csv_price_head0, csv_ather_price_head0";
						$where = "eid = {$data['eid']}";
						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);


						if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
//						if(str_replace(array("_"), " ",$ather_name) == $data["name"]){
							$quantity = $data["quantity"];

//							// CSV用に整形--------------------------------------------------------------
//							if($csv_header['csv_ather_price_head0'] != ""){
//								$quantity = $csv_header['csv_ather_price_head0']."<".$quantity.">";
//							}
							$match_flg = "1";
							break 1;
						}
					}
				}
				if($match_flg == "1"){
					$wk_buff[] = $quantity;
				}
				else{
					$wk_buff[] = "";
				}

			}
			return $wk_buff;
	}


//	function pay_43($pa_detail){
//
//		// 金額用
//		$quantity0 = "";
//		$match_flg0 = "";
//		$index = "0";
//		
//		foreach($this->wa_ather_price as $key => $ather_name){
//				
//				
//			// その他決済項目用
//			$quantity = "";
//			$match_flg = "";
//			
//			// 金額	
//			if(strstr($ather_name, "Additional") != true){
//				foreach($pa_detail as $data){
//					
//					
//						
//					if( ($data['name'] == str_replace("_", " ", $ather_name))){
//
//						if($index == "0"){
//							$quantity0[] = $data['name'];						
//						}else{
//							$quantity0[] = $data['name'];
//						}
//						$index ="1";
//						$match_flg0 = "1";
//						break 1;
//					}
//				}
//			}
//			
//			// その他決済項目
//			else{
//				if($match_flg0 == "1"){
//					$wk_buff[] = "\"".implode(", ", $quantity0)."\"";
//
//
//				}
//				else{
//					$wk_buff[] = "";
//				}
//				$match_flg0 = "";
//
//				foreach($pa_detail as $data){
//					
//						$from = "payment";
//						$column = "csv_ather_price_head0";
//						$where = "eid = {$data['eid']}";
//						$csv_header =  $this->db->getData($column, $from, $where, __FILE__, __LINE__);
//						
//					
//					if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
////					if( ($data['name'] == str_replace("_", " ", $ather_name))){
//						$quantity = $data["quantity"];
//							
//						if($csv_header['csv_ather_price_head0'] != ""){
//							$quantity = $csv_header['csv_ather_price_head0']."(".$quantity.")";
//						}
//						$match_flg = "1";
//						break 1;
//					}
//				}
//			}
//				
//			if($match_flg == "1"){
//				$wk_buff[] = $quantity;
//			}
//			else{
//				$wk_buff[] = "";
//			}
//		}
//		unset($quantity0);
//
//		foreach($wk_buff as $key => $data){
//			if($key <= 14){
//				unset($wk_buff[$key]);
//			}
//		}
//		unset($wk_buff[17]);
//		unset($wk_buff[19]);
//
//		return $wk_buff;
//	}

}







?>
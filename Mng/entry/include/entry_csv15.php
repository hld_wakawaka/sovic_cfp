<?php



/**
 * 管理者　CSVダウンロード
 *
 * @author salon
 *
 */
class entry_csv15{


	/**
	 * コンストラクタ
	 */
	function entry_csv15($initParam){

		$this->loop = $initParam[0];

		$this->objEntry = $initParam[1];
		$this->o_itemini= $initParam[2];
		$this->objErr   = $initParam[3];
		$this->db		= $initParam[4];
		$this->download = $initParam[5];
	}

	/**
	 * メイン処理
	 */
	function main(){
		//------------------------------
		//初期化
		//------------------------------
		$this->arrErr = "";
		$this->delimiter = "\t";
		
		//------------------------------
		// 言語切り替え
		//------------------------------
		// 日本語
		$lang = $GLOBALS["userData"]["lang"];
		$this->langname["entrant"] = $GLOBALS["langtxt"]["entrant"][$lang];
		$this->langname["author"] = $GLOBALS["langtxt"]["author"][$lang];
		$this->langname["number"] = $GLOBALS["langtxt"]["number"][$lang];
		

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;


		//--------------------------------------------
		//定数情報取得
		//--------------------------------------------
		//連絡先
		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_contact = $GLOBALS["contact_J"];
		}
		else{
			$this->wa_contact = $GLOBALS["contact_E"];
		}

		//会員・非会員
		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_kaiin = $GLOBALS["member_J"];
		}
		else{
			$this->wa_kaiin = $GLOBALS["member_E"];
		}

		//共著者の有無
		if($GLOBALS["userData"]["lang"] == "1"){
			$this->wa_coAuthor = $GLOBALS["coAuthor_J"];
		}
		else{
			$this->wa_coAuthor = $GLOBALS["coAuthor_E"];
		}


		//----------------------------------
		//フォームの項目名称取得
		//----------------------------------
		$this->ini = $this->o_itemini->getList(1, $GLOBALS["userData"]["lang"]);
		if(!$this->ini){
			//終了処理
			return array(false, "フォーム項目情報の取得に失敗しました。");
		}

		//---------------------------------
		//フォーム項目情報取得
		//---------------------------------
		$formitem = $this->objEntry->getFormItem($this->db, $GLOBALS["userData"]["form_id"]);
		if(!$formitem){
			//終了処理
			return array(false, "フォーム項目情報の取得に失敗しました。");
		}
		foreach($formitem as $data){
			$wk_item[$data["item_id"]] = $data;
		}
		
		//$this->loop = count(explode("\n", $wk_item[30]["item_select"]));


		//項目情報生成
		foreach($this->ini as $i_key => $data){
			$this->itemData[$i_key] = $wk_item[$i_key];
			$this->itemData[$i_key]["select"] = "";

			$this->itemData[$i_key]["item_name"] = ($wk_item[$i_key]["item_name"] != "") ? $wk_item[$i_key]["item_name"] : $data["defname"];


			//チェック・ラジオボタン・セレクトボックス
			if($wk_item[$i_key]["item_select"] != ""){
				$wk_select  = explode("\n", $wk_item[$i_key]["item_select"]);
				$i =  "";
				$set_select = array();
				foreach($wk_select as $num => $val){
					$i = $num+1;

					$set_select[$i] = str_replace(array("#","\n"), "", $val);

				}
				$set_select_tags = array();
				foreach($set_select as $key => $item){
					$set_select_tags[$key] = strip_tags($item);
				}
				$this->itemData[$i_key]["select"] = $set_select_tags;

			}
		}


		//--------------------------------
		//対象データ取得
		//--------------------------------

		//データ取得（エントリー情報）
		list($wb_ret, $list) = $this->_getData($this->objEntry);
		if(!$wb_ret){
			//終了処理
			return array(false, "出力可能なデータはありません。");
		}


		//-----------------------------
		//CSVヘッダ部分
		//-----------------------------
		$this->header = $this->_makeHeader($this->itemData);



		//-----------------------------
		//CSVデータ部分
		//-----------------------------
		$this->csvdata = "";
		foreach($list as $data){
			$this->csvdata .= $this->_makeData($data, $this->itemData);
		}


		//ヘッダとデータを連結して出力
		$exp_data = mb_convert_encoding($this->header.$this->csvdata, "SJIS", "UTF8");

		return array(true, $exp_data);
//		$this->download->csv($exp_data, "entory_".date('Ymdhis').".csv");
//
	}

	/**
	 * 外部ファイルが存在するかチェック
	 * 	拡張子がPHPの場合はファイルをinclude_onceする
	 *
	 * @param stirng ディレクトリ
	 * @param int form_id
	 * @param string ファイル名
	 * @param string 拡張子
	 */
	function _chkIncFiles($ps_dir, $pn_form_id, $ps_file_name, $ps_extension){

		//探したいファイルの名前
		$find_file = $ps_file_name.$pn_form_id.".".$ps_extension;

		if (is_dir($ps_dir)) {
		    if ($dh = opendir($ps_dir)) {
		        while (($file = readdir($dh)) !== false) {
		        	//外部読み込みファイルが見つかった場合
		            if($file == $find_file){
		            	//PHPファイルの場合
		            	if($ps_extension == "php"){
							include_once($ps_dir.$find_file);
		            	}

						return true;
		            }
		        }
		        closedir($dh);
		    }
		}

		return false;
	}



	/**
	 * 対象データ取得
	 */
	function _getData($po_objEntry){

		//フォーム頭文字の長さ
		$len_formhead = strlen($GLOBALS["userData"]["head"])  + 1;

		$column = "*, to_char(rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(udate, 'yyyy/mm/dd hh24:mi:ss') as upday,  substr(e_user_id, length(e_user_id)-4 ,5) as entry_no";
		$from = $po_objEntry->getTable($GLOBALS["userData"]["type"]);

		$where[] = "del_flg = 0";
		$where[] = "form_id = ".$this->db->quote($GLOBALS["userData"]["form_id"]);
		
		// 有効・無効
		/*if(!isset($this->arrForm["invalid"])) {
			$where[] = "invalid_flg = 0";
		} else {
			if(!isset($this->arrForm["invalid_flg1"])) $this->arrForm["invalid_flg1"] = "0";
			if(!isset($this->arrForm["invalid_flg2"])) $this->arrForm["invalid_flg2"] = "0";
			if($this->arrForm["invalid_flg1"] == "1" && $this->arrForm["invalid_flg2"] == "0") {
				$where[] = "invalid_flg = 0";
			} elseif($this->arrForm["invalid_flg1"] == "0" && $this->arrForm["invalid_flg2"] == "1") {
				$where[] = "invalid_flg = 1";
			}
		}*/
		

		//　応募者
		if($this->arrForm["user_name"] != "") {
			$arrval = GeneralFnc::customExplode($this->arrForm["user_name"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata1 like ".$this->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}

		// ステータス
		if(isset($this->arrForm["status1"]) && $this->arrForm["status1"] == "1") $subwhere[] = "status = 1";
		if(isset($this->arrForm["status2"]) && $this->arrForm["status2"] == "2") $subwhere[] = "status = 0";
		if(isset($this->arrForm["status3"]) && $this->arrForm["status3"] == "3") $subwhere[] = "status = 2";
		if(isset($this->arrForm["status4"]) && $this->arrForm["status4"] == "4") $subwhere[] = "status = 99";


		if(isset($subwhere)) {
			$where[] = "(".implode(" or ", $subwhere).")";
			unset($subwhere);
		}

		// 受付期間
		if($this->arrForm["syear"] > 0) {
			if($this->arrForm["smonth"] == "") $this->arrForm["smonth"] = "1";
			if($this->arrForm["sday"] == "") $this->arrForm["sday"] = "1";
			$sdate = sprintf("%04d", $this->arrForm["syear"])."-".sprintf("%02d", $this->arrForm["smonth"])."-".sprintf("%02d", $this->arrForm["sday"]);
			$where[] = "rdate >= ".$this->db->quote($sdate);
		}
		if($this->arrForm["eyear"] > 0) {
			if($this->arrForm["emonth"] == "") $this->arrForm["emonth"] = "12";
			if($this->arrForm["eday"] == "") {
				$edate = date("Y-m-d", mktime(0, 0, 0, $this->arrForm["emonth"]+1, 0, $this->arrForm["eyear"]));
			} else {
				$edate = sprintf("%04d", $this->arrForm["eyear"])."-".sprintf("%02d", $this->arrForm["emonth"])."-".sprintf("%02d", $this->arrForm["eday"]);
			}

			$where[] = "rdate <= ".$this->db->quote($edate);
		}



		// 支払方法
		if(isset($this->arrForm["payment_method"])){
			if(is_array($this->arrForm["payment_method"])) {
				if(count($this->arrForm["payment_method"]) > 0) {
					foreach($this->arrForm["payment_method"] as $val) {
						$subwhere[] = "payment_method = ".$this->db->quote($val);
					}
					$where[] = "(".implode(" or ", $subwhere).")";
					unset($subwhere);
				}

			}
		}


		// 支払ステータス
		if(isset($this->arrForm["payment_status"])){
			if(is_array($this->arrForm["payment_status"])) {
				if(count($this->arrForm["payment_status"]) > 0) {
					foreach($this->arrForm["payment_status"] as $val) {
						$subwhere[] = "payment_status = ".$this->db->quote($val);
					}
					$where[] = "(".implode(" or ", $subwhere).")";
					unset($subwhere);
				}
			}
		}


		//登録No 開始
		if($this->arrForm["s_entry_no"] != "") {
			//桁数を0埋め５桁にする
			$s_entry_no = sprintf("%05d", $this->arrForm["s_entry_no"]);
			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) >= ".$this->db->quote($s_entry_no);

		}

		//登録No 終了
		if($this->arrForm["e_entry_no"] != "") {
			//桁数を0埋め５桁にする
			$e_entry_no = sprintf("%05d", $this->arrForm["e_entry_no"]);
			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) <= ".$this->db->quote($e_entry_no);

		}

		//国
		if(isset($this->arrForm["country"])){
			if($this->arrForm["country"] != "") {
				$where[] = "edata60 like ".$this->db->quote("%".$this->arrForm["country"]."%");
			}
		}


		// 受付期間
		if($this->arrForm["upd_syear"] > 0) {
			if($this->arrForm["upd_smonth"] == "") $this->arrForm["upd_smonth"] = "1";
			if($this->arrForm["upd_sday"] == "") $this->arrForm["upd_sday"] = "1";
			$upd_sdate = sprintf("%04d", $this->arrForm["upd_syear"])."-".sprintf("%02d", $this->arrForm["upd_smonth"])."-".sprintf("%02d", $this->arrForm["upd_sday"]);
			$where[] = "udate >= ".$this->db->quote($upd_sdate);
		}
		if($this->arrForm["upd_eyear"] > 0) {
			if($this->arrForm["upd_emonth"] == "") $this->arrForm["upd_emonth"] = "12";
			if($this->arrForm["upd_eday"] == "") {
				$upd_edate = date("Y-m-d", mktime(0, 0, 0, $this->arrForm["upd_emonth"]+1, 0, $this->arrForm["upd_eyear"]));
			} else {
				$upd_edate = sprintf("%04d", $this->arrForm["upd_eyear"])."-".sprintf("%02d", $this->arrForm["upd_emonth"])."-".sprintf("%02d", $this->arrForm["upd_eday"]);
			}

			$where[] = "udate <= ".$this->db->quote($upd_edate);
		}

		//カナ
		if($this->arrForm["user_name_kana"] != "") {
			$arrval = GeneralFnc::customExplode($this->arrForm["user_name_kana"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata3 like ".$this->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}



		//------------------------------------
		//表示順
		//------------------------------------
		$wk_order_by = "udate desc";

		if($this->arrForm["sort_name"] != "" && $this->arrForm["sort"] != ""){
			$wk_order_by  = "";
			if($this->arrForm["sort_name"] == "2"){
				$wk_order_by .= "entry_no ";
			}
			else{
				$wk_order_by .= "udate ";
			}

			if($this->arrForm["sort"] == "1"){
				$wk_order_by .= "desc";
			}
			else{
				$wk_order_by .= "asc";
			}
		}


		$orderby = $wk_order_by;
		//$orderby = "eid";

		$list_data = $this->db->getListData($column, $from, $where, $orderby);
		if(!$list_data){
			return array(false, "");
		}


		//共著者情報を表示する場合


		//if($this->itemData[29]["item_pview"] != "1"){

			//エントリーに紐づく共著者情報を取得
			foreach($list_data as $data){

				//共著者以外のデータ
				$wk_entory[$data["eid"]] = $data;

				$wa_aff = $this->objEntry->getRntry_aff($this->db, $data["eid"]);

				if(!$wa_aff){
					$wk_entory[$data["eid"]]["chosya"] = array();
				}
				else{
					$wk_entory[$data["eid"]]["chosya"] = $wa_aff;
				}
			}
		//}

		return array(true, $wk_entory);


	}




	/**
	 * CSVヘッダ生成
	 */
	function _makeHeader($pa_itemData){

		$header_aff = "";
		$ret_buff = "";

		$header[]= "登録No.";


		// 入れ替え
		$array = array();
		foreach($pa_itemData as $key => $data){
			switch($key){
				case 7:
					$array[$key] = $data;
					$array[26] = $pa_itemData[26];
					$array[27] = $pa_itemData[27];
					break;

				case 48:
					$array[$key] = $data;
					$array[54] = $pa_itemData[54];
					break;

				case 26:
				case 27:
				case 54:
					break;

				default:
					$array[$key] = $data;
					break;
			}
		}
		$pa_itemData = $array;


		foreach($pa_itemData as $i_key => $data){

			//表示する設定の場合出力
			if($data["item_view"] != "1"){

				//画面に表示する項目の名称
				$name = strip_tags($data["item_name"]);

				switch($i_key){
					case 32:	// 所属機関　番号と、文字と分ける
						$header_aff[] =  $name.$this->langname["number"];
						$header_aff[] =  $name;
					break;

					case 33:
					case 34:
					case 35:
					case 36:
					case 37:
					case 38:
					case 39:
					case 40:
					case 41:
					case 42:
					case 43:
					case 44:
					case 45:
					case 61:
					case 62:
					case 65:
					case 66:
					case 84:
					case 85:
					case 86:
					case 87:
					case 88:
					case 89:
					case 90:
					case 91:
					case 92:
					case 93:
					case 94:
					case 95:
					case 96:
					case 97:
					case 98:


						$header_aff[] = $name;

					break;

					default:
					
						$header[]= "\"".$name."\"";
						
				}

			}

		}

		$header_stat[]="状態";
		$header_stat[]="エントリー登録日";
		$header_stat[]="エントリー更新日";
		
		$loop_cnt = $this->loop +1;

		for($i=0; $i < $loop_cnt; $i++ ){

			foreach($header_aff as $k_name){
				
				if($i == 0) {
					$chosya_header[] = "\"".$this->langname["entrant"]." ".str_replace("Author's ", "", $k_name)."\"";
				} else {
					$chosya_header[] = "\"".$this->langname["author"].$i." ".$k_name."\"";
				}

			}
		}

		$header = array_merge((array)$header, (array)$chosya_header);


		$header = array_merge($header, $header_stat);


		//生成
		$ret_buff = implode($this->delimiter, $header);

		$ret_buff .="\n";

		return $ret_buff;


	}




	/**
	 * CSV出力データ生成
	 */
	function _makeData($pa_param, $pa_itemData){

		// 入れ替え
		$array = array();
		foreach($pa_itemData as $key => $data){
			switch($key){
				case 7:
					$array[$key] = $data;
					$array[26] = $pa_itemData[26];
					$array[27] = $pa_itemData[27];
					break;

				case 48:
					$array[$key] = $data;
					$array[54] = $pa_itemData[54];
					break;

				case 26:
				case 27:
				case 54:
					break;

				default:
					$array[$key] = $data;
					break;
			}
		}
		$pa_itemData = $array;


		$ret_buff = "";
		$wk_buff_arr = array();
		$key_buff = array();

		$wk_buff[] =$pa_param["entry_no"];


		foreach($pa_itemData as $i_key => $data){


			//表示する設定の場合出力
			if($data["item_view"] != "1"){

				//画面に表示する項目の名称
				$name = $data["item_name"];

				switch($i_key){
					//共著者の情報キー
					case 32:
					case 33:
					case 34:
					case 35:
					case 36:
					case 37:
					case 38:
					case 39:
					case 40:
					case 41:
					case 42:
					case 43:
					case 44:
					case 45:
					case 61:
					case 62:
					case 65:
					case 66:
					case 84:
					case 85:
					case 86:
					case 87:
					case 88:
					case 89:
					case 90:
					case 91:
					case 92:
					case 93:
					case 94:
					case 95:
					case 96:
					case 97:
					case 98:


						array_push($key_buff, $i_key);
					break;

					//共著者以外
					default:
						switch($i_key){
							//会員・非会員
							case 8:
								$wk_buff[] = ($pa_param["edata".$i_key] != "") ? "\"".$this->wa_kaiin[$pa_param["edata".$i_key]]."\"" : "\"\"";
							break;
							//連絡先
							case 16:
								$wk_buff[] = ($pa_param["edata".$i_key] != "") ?  "\"".$this->wa_contact[$pa_param["edata".$i_key]]."\"" : "\"\"";
							break;
							//都道府県
							case 18:
								//英語フォームの場合
								if($GLOBALS["userData"]["lang"] == "2"){
									$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$pa_param["edata".$i_key]."\"" : "\"\"";
								}
								else{
									$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$GLOBALS["prefectureList"][$pa_param["edata".$i_key]]."\"" : "\"\"";
								}
							break;
							//共著者の有無
							case 29:
								$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$this->wa_coAuthor[$pa_param["edata".$i_key]]."\"" : "";
							break;

							//共著者の所属機関
							case 31:
								//$wk_buff[] = "\"".str_replace(array("\r\n","\n","\r"), ' ',$pa_param["edata".$i_key]). "\"";
								$wk_kikanlist = explode("\n", $pa_param["edata".$i_key]);

								$i =  "";
								$set_select = array();
								foreach($wk_kikanlist as $num => $val){
									$i = $num+1;
									$set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
									$wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
								}



								$wk_buff[] = "\"".str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names)). "\"";

							break;
							//発表形式
							case 48:
								$keisiki = "";
								if($pa_param["edata".$i_key] != ""){
									if($pa_itemData[$i_key]["select"]){
										$keisiki = "\"".$this->itemData[$i_key]["select"][$pa_param["edata".$i_key]]."\"";
									}

								}
								//$wk_buff[] = ($pa_param["edata".$i_key] != "") ? "\"".$this->itemData[$i_key]["select"][$pa_param["edata".$i_key]]."\"" : "";
								$wk_buff[] = str_replace(array("\r\n","\n","\r"), '', $keisiki);
							break;


							//筆頭者部分　任意１ 任意２ 任意３
							//論文部分　任意１ 任意２ 任意３
							case 26:
							case 27:
							case 28:
							case 63:
							case 64:
							case 69:
							case 70:
							case 71:
							case 72:
							case 73:
							case 74:
							case 75:
							case 76:
							case 77:
							case 78:
							case 79:
							case 80:
							case 81:
							case 82:
							case 83:
							case 54:
							case 55:
							case 56:
							case 67:
							case 68:
							case 67:
							case 68:
							case 99:
							case 100:
							case 101:
							case 102:
							case 103:
							case 104:
							case 105:
							case 106:
							case 107:
							case 108:
							case 109:
							case 110:
							case 111:
							case 112:
							case 113:
							// 2011 12/06 追加
							case 115:
							case 116:
							case 117:
							case 118:
							case 119:
							case 120:
							case 121:
							case 122:
							case 123:
							case 124:
							case 125:
							case 126:
							case 127:
							case 128:
							case 129:
							case 130:
							case 131:
							case 132:
							case 133:
							case 134:
							case 135:
							case 136:
							case 137:
							case 138:
							case 139:
							case 140:
							case 141:
							case 142:
							case 143:
							case 144:
							case 145:
							case 146:
							case 147:
							case 148:
							case 149:
							case 150:
							case 151:
							case 152:
							case 153:
							case 154:
							case 155:
							case 156:
							case 157:
							case 158:
							case 159:

								$wk_buff[] = $this->_makeNini($pa_param["edata".$i_key], $i_key, $pa_itemData);
							break;

							//Mr. Mis Dr
							case 57:
								if($pa_param["edata".$i_key] != ""){
									$wk_buff[] = $GLOBALS["titleList"][$pa_param["edata".$i_key]];
								}
								else{
									$wk_buff[] = "";
								}

							break;

							//国名リストボックス
							case 114:
								$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$GLOBALS["country"][$pa_param["edata".$i_key]]."\"" : "\"\"";
							break;
							default:
								$wk_buff[] = ($pa_param["edata".$i_key] != "") ? "\"".$pa_param["edata".$i_key]."\"" : "";
						}

					break;
				}

			}

		}


		//共著者の情報
		//表示する場合


			//共著者情報が存在する場合
		/*if(count($pa_param["chosya"]) > 0){*/
			
			//15人以下の場合はNULLで埋めて出力
			$loop = $this->loop - count($pa_param["chosya"]) + 1;
			
			foreach($pa_param["chosya"] as $wk_kyochosya1){
				
				foreach($key_buff as $k_key ){
					
					if( isset($this->itemData[$k_key]["item_view"]) && $this->itemData[$k_key]["item_view"] != "1"){
						switch($k_key){
						//共著者所属期間
							case 32:
								if(count($set_select) > 0){
									
									$chkeck = array();
									if($wk_kyochosya1["edata".$k_key] != ""){
										
										$chkeck = explode("|", $wk_kyochosya1["edata".$k_key]);
										$chkeck_names = array();
										
										foreach($chkeck as $val){
											if($val != "" && array_key_exists($val, $set_select)){
												$chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $set_select[$val]);
											}
										}
										
										if(count($chkeck_names) > 0){
											$wk_aaa =   "\"". implode(",", $chkeck_names). "\"";
										}
										
									}
									else{
										$wk_aaa = "";
									}
									
									$wk_buff_arr[] = "\"".implode(",", $chkeck)."\"";							//所属機関番号
									$wk_buff_arr[] = str_replace(array("\r\n","\n","\r"), '', trim($wk_aaa));	//所属機関
								}
								else{
									$wk_buff_arr[] = "";	//所属機関番号
									$wk_buff_arr[] = "";	//所属機関
								}
								
								
								break;
								//共著者姓
							case 33:
								$cyosya_name = "";
								
								/*
								$wk_mark = array();
								if($wk_kyochosya1["edata32"] != ""){
									$chkeck = explode("|", $wk_kyochosya1["edata32"]);
									foreach($chkeck as $val){
										$wk_mark[] = $val.")";
									}
									$cyosya_name = implode(", ", $wk_mark);
									
								} */

								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
								break;
								
								//共著者名
							case 34:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
							
								break;
								//共著者姓（カナ）
							case 35:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
								
								break;
								//共著者名（カナ）
							case 36:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
								break;
								
								//会員・非会員
							case 41:
								$wk_buff_arr[] = ($wk_kyochosya1["edata".$k_key] != "") ? "\"".$this->wa_kaiin[$wk_kyochosya1["edata".$k_key]]."\"" : "\"\"";
								break;
								//任意項目
							case 43:
							case 44:
							case 45:
							case 65:
							case 66:
							case 84:
							case 85:
							case 86:
							case 87:
							case 88:
							case 89:
							case 90:
							case 91:
							case 92:
							case 93:
							case 94:
							case 95:
							case 96:
							case 97:
							case 98:
								$wk_buff_arr[]= $this->_makeNini($wk_kyochosya1["edata".$k_key], $k_key, $pa_itemData);
								break;
								
								//Mr. Mis Dr
							case 61:
								if($wk_kyochosya1["edata".$k_key] != ""){
									$wk_buff_arr[] = $GLOBALS["titleList"][$wk_kyochosya1["edata".$k_key]];
								}
								else{
									$wk_buff_arr[] = "";
								}
								break;
								
							case 46:
							case 47:
							default:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
						}
					}
				}
			}
			
			
			
			//15人以下の場合データを埋める
			for($a=0; $a < $loop; $a++){
				
				$this->_makeDefault($key_buff, $pa_itemData, $wk_buff_arr);
				
			}
			
		/*}*/



		//状態
		$wk_buff_stat[] = ($pa_param["status"] == "0") ? "-" : "\"".$GLOBALS["entryStatusList"][$pa_param["status"]]."\"";

		//登録日
		$wk_buff_stat[] = "\"".$pa_param["insday"]."\"";


		//更新日
		$wk_buff_stat[] = "\"".$pa_param["upday"]."\"";

		$wk_buff = array_merge((array)$wk_buff, (array)$wk_buff_arr);
		$wk_buff = array_merge((array)$wk_buff, (array)$wk_buff_stat);
		$ret_buff .= implode($this->delimiter, $wk_buff);
		// データ中の改行を外す
		$ret_buff = str_replace(array("\r\n","\n","\r"), '', $ret_buff);

		$ret_buff .= "\n";

		return $ret_buff;




	}



	function _makeDefault($pa_key, $pa_itemData, &$make_buff){

		$string = "\"\"";


		foreach($pa_key as $key){

			if($pa_itemData[$key]["item_view"] == "0"){
				if($key == "32") {
					$make_buff[] = $string;
					$make_buff[] = $string;
				}
				else{
					$make_buff[] = $string;
				}

			}

		}

		return 	$make_buff;


	}



	/**
	 * 任意項目の出力値設定
	 */
	function _makeNini($pa_param, $pn_num, $pa_itemData){

		$ret_string = "\"\"";

		if($pa_param == ""){
			return $ret_string;
		}

		//テキスト
		if($pa_itemData[$pn_num]["item_type"] == "0" || $pa_itemData[$pn_num]["item_type"] == "1"){
			$ret_string = "\"".str_replace(array("\n\r","\r","\n"), "", $pa_param). "\"";
		}
		//ラジオ、セレクト
		else if($pa_itemData[$pn_num]["item_type"] == "2" || $pa_itemData[$pn_num]["item_type"] == "4"){
			$ret_string = ($pa_param != "") ? "\"". str_replace(array("\n\r","\r","\n"), "", $pa_itemData[$pn_num]["select"][$pa_param]). "\"" : "";
		}
		else{
			if($pa_param != ""){

				$chkeck = explode("|", $pa_param);
				$chkeck_names = array();

				foreach($chkeck as $val){
					if($val != "" && array_key_exists($val, $pa_itemData[$pn_num]["select"])){
						$chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $pa_itemData[$pn_num]["select"][$val]);
					}
				}

				if(count($chkeck_names) > 0){
					$ret_string =   "\"". implode(",", $chkeck_names). "\"";
				}

			}

		}

		return $ret_string;
	}

	/**
	 * 共著者のデフォルト
	 */
	function _setDefault(&$wk_buff){

		$string = "\"\"";

		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;
		$wk_buff[] = $string;

	}
}


?>
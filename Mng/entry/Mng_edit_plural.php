<?php

include('../../application/cnf/include.php');
include('../../application/module/custom/Entry.class.php');
include_once('../function.php');

/**
 * 管理者　一括投稿編集
 *
 * @author salon
 *
 */
class plural extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function plural(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){
ini_set("error_reporting", E_ALL);

		LoginMember::checkLoginRidirect();

		$this->objEntry = new Entry;

		// 表示HTMLの設定
		$this->_processTemplate = "Mng/entry/Mng_edit_plural.html";
		$this->_title = "管理者ページ";

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", $GLOBALS["userData"]["user_name"]);

		//-------------------------------
		//管理者メニュー取得
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;
		// 処理対象のエントリーIDを取得
		$this->getEids($this->arrForm);

		$freemsg = "";

		// パラメータチェック
		$res = $this->checkParam();
		if(!$res) {
			Error::showErrorPage("不正なアクセスです。");
			exit;
		}

		//----------------------
		//一括処理種別
		//----------------------
		//採択
		if($this->arrForm["set_stat"] == "1"){
			$wk_exec_name = "採択";
			$function = "chgStatus";
		}
		//取り下げ
		else if($this->arrForm["set_stat"] == "2"){
			$wk_exec_name = "初期状態に戻す";
			$function = "chgStatus";
		}
		//不採択
		else if($this->arrForm["set_stat"] == "3"){
			$wk_exec_name = "不採択";
			$function = "chgStatus";
		}
		//キャンセル
		else if($this->arrForm["set_stat"] == "99"){
			$wk_exec_name = "キャンセル";
			$function = "chgStatus";
		}
		//削除
		else if($this->arrForm["set_stat"] == "4"){
			$wk_exec_name = "削除";
			$function = "delEntry";
		}

		//支払いステータスの変更
		else if($this->arrForm["set_stat"] == "5"){
			$wk_exec_name = "ステータス変更";
			$function = "chgPaymentStatus";

			if(!isset($this->arrForm["chg_status"]) || $this->arrForm["chg_status"] == "") {
				$this->complete("変更するステータスを選択して下さい。");
			}

			if(!is_numeric($this->arrForm["chg_status"])) {
				Error::showErrorPage("処理に必要なデータの取得に失敗しました。");
				exit;
			}

			$freemsg = "変更するステータス：".$GLOBALS["paymentstatusList"][$this->arrForm["chg_status"]]."<br /><br />";

		} else {

			Error::showErrorPage("処理に必要なデータの取得に失敗しました。");
			exit;

		}



// 		// ここでCSVインポートのアクションを動かす
// 		$this->mode = isset($_REQUEST["set_stat"]) ? $_REQUEST["set_stat"] : "";
// 		$actionName = $this->mode."Action";
// 		$exAction   = 'Mng_plural_'.$actionName; ⇦ Sys_form_importのexecInput()を参考にしたCSVインポートメソッドを入れる
// 		// 外部クラス読み込み
// 		$this->exClass = null;
// 		$isOverride = parent::isOverrideClass($this->form_id, $c);
// 		if($isOverride && is_object($c)) {
// 			$this->exClass = $c;

// 			if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
// 				$this->exClass->$exAction($this);
// 			}else{
// 				$this->$actionName();
// 			}
// 		}


		$this->assign("exec_name", $wk_exec_name);
		$this->assign("freemsg", $freemsg);

		$this->_title = "一括処理（".$wk_exec_name."）";

		if(!isset($this->arrForm["eid"]) || !count($this->arrForm["eid"]) > 0) {
			$this->complete("エントリーが指定されていません。");
		}

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){


			case "complete":

				$rs = $this->$function($this->arrForm["set_stat"]);

				//完了メッセージ
				if(!$rs) {
					$this->complete("エントリーの".$wk_exec_name."処理に失敗しました。");
				} else {
					$this->complete("エントリーの".$wk_exec_name."処理を実行しました。");
				}


			break;

			default:

				$arrData = $this->objEntry->getListFromId($this->arrForm["eid"]);
				$this->assign("arrData", $arrData);

                if($arrData === false){
        			$this->complete("エントリーが指定されていません。");
                }
				break;



		}

		// 親クラスに処理を任せる
		parent::main();

	}

	function complete($msg) {

		$this->assign("msg", $msg);
		$this->_processTemplate = "Mng/Mng_complete.html";
		parent::main();
		exit;

	}

	// ステータスの変更
	function chgStatus($new_stat) {

		switch($new_stat) {
			case "1":
				$param["status"] = "1";
				break;
			case "3":
				$param["status"] = "2";
				break;
			case "99":
				$param["status"] = "99";
				break;
			default:
				$param["status"] = "0";
				break;
		}

		$db = new DbGeneral;

		$param["udate"] = "NOW";

		//$from = $this->objEntry->getTable($GLOBALS["userData"]["type"]);
		$from = "entory_r";
		$where[] = "eid in (".implode(",", $this->arrForm["eid"]).")";
		$rs = $db->update($from, $param, $where, __FILE__, __LINE__);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

		if(!$rs) return false;

		return true;

	}

	// 削除
	function delEntry($dummy="") {

		$db = new DbGeneral;

		$param["del_flg"] = "1";
		$param["udate"] = "NOW";
		//$from = $this->objEntry->getTable($GLOBALS["userData"]["type"]);
		$from = "entory_r";
		$where[] = "eid in (".implode(",", $this->arrForm["eid"]).")";
		$rs = $db->update($from, $param, $where, __FILE__, __LINE__);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

		if(!$rs) return false;
		return true;
	}

	// 支払いステータスの変更
	function chgPaymentStatus($dummy="") {

		$db = new DbGeneral;

		$param["payment_status"] = $this->arrForm["chg_status"];
		$param["udate"] = "NOW";

		$from = "payment";
		$where[] = "eid in (".implode(",", $this->arrForm["eid"]).")";
		$rs = $db->update($from, $param, $where, __FILE__, __LINE__);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

		if(!$rs) return false;

		return true;

	}

	// 値のチェック
	function checkParam() {

		// eidは数字の配列でなければならない
		if(isset($this->arrForm["eid"])) {

			if(is_array($this->arrForm["eid"])) {
				foreach($this->arrForm["eid"] as $id) {
					if(!is_numeric($id)) return false;
				}
			} else {
				return false;
			}

		}

		return true;
	}


    function getEids(&$arrForm){
        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid();

        $arrForm['eid'] = array();
        foreach($arrEid as $_page => $_arrEid){
            foreach($_arrEid as $_eid => $_checked){
                if($_checked != 1) continue;

                $key = $_page ."-". $_eid;
                $arrForm['eid'][$key] = $_eid;
            }
        }

        $this->arrEid = $arrEid;
    }

}

/**
 * メイン処理開始
 **/

$c = new plural();
$c->main();







?>
<?php

include('../../application/cnf/include.php');
include(MODULE_DIR.'custom/Entry.class.php');
include(MODULE_DIR.'Download.class.php');
include('crypt.class.php');
include_once('../function.php');

/**
 * 管理者TOP
 *
 * @author salon
 *
 */
class entrydetail extends ProcessBase {

    /**
     * コンストラクタ
     */
    function entrydetail(){
        /** ログインチェック */
        LoginMember::checkLoginRidirect();

        $this->eid = isset($_GET["eid"]) ? $_GET["eid"] : "0";
        if(!is_numeric($this->eid) || !$this->eid > 0){
            $this->complete("エントリーが指定されていません。");
        }

        parent::ProcessBase();

        //-------------------------------
        // 初期化
        //-------------------------------
        $this->_title   = "管理者ページ";
        $this->formdata = $GLOBALS["userData"];
        $this->form_id  = $this->formdata["form_id"];
        $this->arrErr   = array();

        //-------------------------------
        //ログイン者情報
        //-------------------------------
        $this->assign("user_name", $this->formdata["user_name"]);

        //-------------------------------
        //管理者メニュー取得
        //-------------------------------
        $menu = Mng_function::makeMenu();
        $this->assign("va_menu", $menu);
    }


    /**
     * メイン処理
     */
    function main(){
        ini_set("error_reporting", E_ALL);

        // インスタンス生成
        $this->o_entry   = new Entry;
        $this->o_itemini = new item_ini;
        $this->db        = new DBGeneral;
        $this->o_form    = new Form;
        $this->objErr    = New Validate;

		$this->inc_dir = "../../templates/Mng/entry/include/";
		$wb_ret = $this->_chkIncFiles($this->inc_dir, $this->form_id, "entry_detail", "html");

		$this->_processTemplate = (!$wb_ret)
								? "Mng/entry/Mng_entry_detail.html"
								: "Mng/entry/include/".$this->form_id."entry_detail.html";

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        Usr_initial::setFormIni($this);
        Usr_initial::setFormData($this);

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
        $this->assign("formItem",    $this->itemData);    // 項目マスタ
        $this->assign("arrItemData", $this->arrItemData);


        // エントリー情報
        $this->arrForm   = $this->o_form->get($this->form_id);
        $arrData = $this->o_entry->getRntry_r($this->db, $this->eid, $this->form_id);
        $arrAff  = $this->o_entry->getRntry_aff($this->db, $this->eid);
        $this->assign("arrAff", $arrAff);


        //----------------------------------
        //添付ファイルダウンロード
        //----------------------------------
        $ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        if($ws_action == "download"){
            if($_REQUEST["down_num"] != ""){

                $down_num = $_REQUEST["down_num"];

                //ダウンロードファイル
                $this->uploadDir = UPLOAD_PATH."Usr/form".$this->form_id."/".$this->eid."/";    //ファイルアップロードディレクトリ
                $this->file_name =$this->uploadDir.$arrData["edata".$down_num];


                if(is_file($this->file_name)){

                    //ダウンロード実行
                    $wo_download = new Download();
                    $wo_download->file($this->file_name, basename($this->file_name) , "");
                    exit;
                }
                else{
                    $this->objErr->addErr("対象ファイルが存在しません。", "edata".$down_num);
                    $this->arrErr = $this->objErr->_err;
                }
            }


        }


        $this->assign("Maxnum", $this->o_itemini->cnt);
        $this->assign("arrData", $arrData);
        $this->assign("arrErr",  $this->arrErr);
        $this->assign("form_id", $this->form_id);

        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_detail_premain')){
            $this->exClass->mng_detail_premain($this);
        }

        // 親クラスに処理を任せる
        parent::main();

        /*
        print_r("<pre>");
        print_r("formItem:");
        print_r($formItem);
        print_r("ItemIni:");
        print_r($ItemIni);
        print_r("arrData:");
        print_r($arrData);
        print_r("arrAff::");
        if(isset($arrAff))    print_r($arrAff);
        print_r("arrPayment:");
        if(isset($arrPayment))    print_r($arrPayment);
        print_r("arrForm:");
        print_r($this->arrForm);
        print_r("</pre>");
        */

    }

    function complete($msg) {

        $this->assign("msg", $msg);
        $this->_processTemplate = "Mng/Mng_complete.html";
        parent::main();
        exit;
    }

	/**
	 * 一次応募期間中チェック
	 *
	 */
	function _chkTerm(){
        return false;
	}

	/**
	 * 二次応募期間中チェック
	 *
	 */
	function _chkTerm2(){
        return false;
	}


	/**
	 * 外部ファイルが存在するかチェック
	 * 	拡張子がPHPの場合はファイルをinclude_onceする
	 *
	 * @param stirng ディレクトリ
	 * @param int form_id
	 * @param string ファイル名
	 * @param string 拡張子
	 */
	function _chkIncFiles($ps_dir, $pn_form_id, $ps_file_name, $ps_extension){

		//探したいファイルの名前
		$find_file = $pn_form_id.$ps_file_name.".".$ps_extension;

		if (is_dir($ps_dir)) {
		    if ($dh = opendir($ps_dir)) {
		        while (($file = readdir($dh)) !== false) {
		        	//外部読み込みファイルが見つかった場合
		            if($file == $find_file){
		            	//PHPファイルの場合
		            	if($ps_extension == "php"){
							include_once($ps_dir.$find_file);
		            	}

						return true;
		            }
		        }
		        closedir($dh);
		    }
		}

		return false;
	}

}

/**
 * メイン処理開始
 **/

$c = new entrydetail();
$c->main();







?>
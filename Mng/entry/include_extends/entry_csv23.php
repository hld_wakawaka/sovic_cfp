<?php
/*
 * CreateDate: 2013/08/23
 *
 */

class entry_csv23 extends entry_csv {

    function itemShift($pa_itemData){
        // 入れ替え
        $array = array();
        foreach($pa_itemData as $key => $data){
            switch($key){
                case 13:
                    $array[$key] = $data;
                    $array[26]  = $pa_itemData[26];
                    break;

                case 13:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        return $array;
    }


    /**
     * CSVヘッダ生成
     */
    function _makeHeader($pa_itemData){
        return parent::_makeHeader($this->itemShift($pa_itemData));
    }


    /**
     * CSV出力データ生成
     */
    function _makeData($pa_param, $pa_itemData){
        return parent::_makeData($pa_param, $this->itemShift($pa_itemData));
    }


}
?>

<?php
/*
 * CreateDate: 2012/12/22
 *
 */

class entry_csv13 extends entry_csv {
	

	/**
	 * CSVヘッダ生成
	 */
	function _makeHeader($pa_itemData){

		$header_aff = "";
		$ret_buff = "";

		$header[]= "登録No.";

		foreach($pa_itemData as $i_key => $data){

			//表示する設定の場合出力
			if($data["item_view"] != "1"){

				//画面に表示する項目の名称
				$name = strip_tags($data["item_name"]);

				switch($i_key){
					case 32:	// 所属機関　番号と、文字と分ける
						$header_aff[] =  $name.$this->langname["number"];
						$header_aff[] =  $name;
					break;

					case 33:
					case 34:
					case 35:
					case 36:
					case 37:
					case 38:
					case 39:
					case 40:
					case 41:
					case 42:
					case 43:
					case 44:
					case 45:
					case 61:
					case 62:
					case 65:
					case 66:
					case 84:
					case 85:
					case 86:
					case 87:
					case 88:
					case 89:
					case 90:
					case 91:
					case 92:
					case 93:
					case 94:
					case 95:
					case 96:
					case 97:
					case 98:


						$header_aff[] = $name;

					break;
					
					case "26":
					case "27":
					case "28":
					case "63":
					case "64":
					case "69":
					case "70":
					
						$header[] = $this->langname["entrant"]." ".$name;
						
						break;

					default:
					
						$header[]= "\"".$name."\"";
						
				}

			}

		}

		$header_stat[]="状態";
		$header_stat[]="エントリー登録日";
		$header_stat[]="エントリー更新日";
		
		$loop_cnt = $this->loop +1;

		for($i=1; $i < $loop_cnt; $i++ ){

			foreach($header_aff as $k_name){
				
				if($i == 0) {
					$chosya_header[] = "\"".$this->langname["entrant"]." ".str_replace("Author's ", "", $k_name)."\"";
				} else {
					$chosya_header[] = "\"".$this->langname["author"].$i." ".$k_name."\"";
				}

			}
		}

		$header = array_merge((array)$header, (array)$chosya_header);


		$header = array_merge($header, $header_stat);


		//生成
		$ret_buff = implode($this->delimiter, $header);

		$ret_buff .="\n";

		return $ret_buff;


	}
	
		/**
	 * CSV出力データ生成
	 */
	function _makeData($pa_param, $pa_itemData){

		$ret_buff = "";
		$wk_buff_arr = array();
		$key_buff = array();

		$wk_buff[] =$pa_param["entry_no"];


		foreach($pa_itemData as $i_key => $data){


			//表示する設定の場合出力
			if($data["item_view"] != "1"){

				//画面に表示する項目の名称
				$name = $data["item_name"];

				switch($i_key){
					//共著者の情報キー
					case 32:
					case 33:
					case 34:
					case 35:
					case 36:
					case 37:
					case 38:
					case 39:
					case 40:
					case 41:
					case 42:
					case 43:
					case 44:
					case 45:
					case 61:
					case 62:
					case 65:
					case 66:
					case 84:
					case 85:
					case 86:
					case 87:
					case 88:
					case 89:
					case 90:
					case 91:
					case 92:
					case 93:
					case 94:
					case 95:
					case 96:
					case 97:
					case 98:


						array_push($key_buff, $i_key);
					break;

					//共著者以外
					default:
						switch($i_key){
							//会員・非会員
							case 8:
								$wk_buff[] = ($pa_param["edata".$i_key] != "") ? "\"".$this->wa_kaiin[$pa_param["edata".$i_key]]."\"" : "\"\"";
							break;
							//連絡先
							case 16:
								$wk_buff[] = ($pa_param["edata".$i_key] != "") ?  "\"".$this->wa_contact[$pa_param["edata".$i_key]]."\"" : "\"\"";
							break;
							//都道府県
							case 18:
								//英語フォームの場合
								if($GLOBALS["userData"]["lang"] == "2"){
									$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$pa_param["edata".$i_key]."\"" : "\"\"";
								}
								else{
									$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$GLOBALS["prefectureList"][$pa_param["edata".$i_key]]."\"" : "\"\"";
								}
							break;
							//共著者の有無
							case 29:
								$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$this->wa_coAuthor[$pa_param["edata".$i_key]]."\"" : "";
							break;

							//共著者の所属機関
							case 31:
								//$wk_buff[] = "\"".str_replace(array("\r\n","\n","\r"), ' ',$pa_param["edata".$i_key]). "\"";
								$wk_kikanlist = explode("\n", $pa_param["edata".$i_key]);

								$i =  "";
								$set_select = array();
								foreach($wk_kikanlist as $num => $val){
									$i = $num+1;
									$set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
									$wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
								}



								$wk_buff[] = "\"".str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names)). "\"";

							break;
							//発表形式
							case 48:
								$keisiki = "";
								if($pa_param["edata".$i_key] != ""){
									if($pa_itemData[$i_key]["select"]){
										$keisiki = "\"".$this->itemData[$i_key]["select"][$pa_param["edata".$i_key]]."\"";
									}

								}
								//$wk_buff[] = ($pa_param["edata".$i_key] != "") ? "\"".$this->itemData[$i_key]["select"][$pa_param["edata".$i_key]]."\"" : "";
								$wk_buff[] = str_replace(array("\r\n","\n","\r"), '', $keisiki);
							break;


							//筆頭者部分　任意１ 任意２ 任意３
							//論文部分　任意１ 任意２ 任意３
							case 26:
							case 27:
							case 28:
							case 63:
							case 64:
							case 69:
							case 70:
							case 71:
							case 72:
							case 73:
							case 74:
							case 75:
							case 76:
							case 77:
							case 78:
							case 79:
							case 80:
							case 81:
							case 82:
							case 83:
							case 54:
							case 55:
							case 56:
							case 67:
							case 68:
							case 67:
							case 68:
							case 99:
							case 100:
							case 101:
							case 102:
							case 103:
							case 104:
							case 105:
							case 106:
							case 107:
							case 108:
							case 109:
							case 110:
							case 111:
							case 112:
							case 113:
							// 2011 12/06 追加
							case 115:
							case 116:
							case 117:
							case 118:
							case 119:
							case 120:
							case 121:
							case 122:
							case 123:
							case 124:
							case 125:
							case 126:
							case 127:
							case 128:
							case 129:
							case 130:
							case 131:
							case 132:
							case 133:
							case 134:
							case 135:
							case 136:
							case 137:
							case 138:
							case 139:
							case 140:
							case 141:
							case 142:
							case 143:
							case 144:
							case 145:
							case 146:
							case 147:
							case 148:
							case 149:
							case 150:
							case 151:
							case 152:
							case 153:
							case 154:
							case 155:
							case 156:
							case 157:
							case 158:
							case 159:

								$wk_buff[] = $this->_makeNini($pa_param["edata".$i_key], $i_key, $pa_itemData);
							break;

							//Mr. Mis Dr
							case 57:
								if($pa_param["edata".$i_key] != ""){
									$wk_buff[] = $GLOBALS["titleList"][$pa_param["edata".$i_key]];
								}
								else{
									$wk_buff[] = "";
								}

							break;

							//国名リストボックス
							case 114:
								$wk_buff[] =  ($pa_param["edata".$i_key] != "") ? "\"".$GLOBALS["country"][$pa_param["edata".$i_key]]."\"" : "\"\"";
							break;
							default:
								$wk_buff[] = ($pa_param["edata".$i_key] != "") ? "\"".$pa_param["edata".$i_key]."\"" : "";
						}

					break;
				}

			}

		}


		//共著者の情報
		//表示する場合


			//共著者情報が存在する場合
		/*if(count($pa_param["chosya"]) > 0){*/
			
			//15人以下の場合はNULLで埋めて出力
			$loop = $this->loop - count($pa_param["chosya"]) + 1;
			if($loop > $this->loop){
    			$loop = $this->loop;
			}
			
			$cnt = 1;
			foreach($pa_param["chosya"] as $wk_kyochosya1){
				
				if($cnt == 1)  {
					$cnt++; continue;
				}
				
				foreach($key_buff as $k_key ){
					
					if( isset($this->itemData[$k_key]["item_view"]) && $this->itemData[$k_key]["item_view"] != "1"){
						switch($k_key){
						//共著者所属期間
							case 32:
								if(count($set_select) > 0){
									
									$chkeck = array();
									if($wk_kyochosya1["edata".$k_key] != ""){
										
										$chkeck = explode("|", $wk_kyochosya1["edata".$k_key]);
										$chkeck_names = array();
										
										foreach($chkeck as $val){
											if($val != "" && array_key_exists($val, $set_select)){
												$chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $set_select[$val]);
											}
										}
										
										if(count($chkeck_names) > 0){
											$wk_aaa =   "\"". implode(",", $chkeck_names). "\"";
										}
										
									}
									else{
										$wk_aaa = "";
									}
									
									$wk_buff_arr[] = "\"".implode(",", $chkeck)."\"";							//所属機関番号
									$wk_buff_arr[] = str_replace(array("\r\n","\n","\r"), '', trim($wk_aaa));	//所属機関
								}
								else{
									$wk_buff_arr[] = "";	//所属機関番号
									$wk_buff_arr[] = "";	//所属機関
								}
								
								
								break;
								//共著者姓
							case 33:
								$cyosya_name = "";
								
								/*
								$wk_mark = array();
								if($wk_kyochosya1["edata32"] != ""){
									$chkeck = explode("|", $wk_kyochosya1["edata32"]);
									foreach($chkeck as $val){
										$wk_mark[] = $val.")";
									}
									$cyosya_name = implode(", ", $wk_mark);
									
								} */

								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
								break;
								
								//共著者名
							case 34:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
							
								break;
								//共著者姓（カナ）
							case 35:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
								
								break;
								//共著者名（カナ）
							case 36:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
								break;
								
								//会員・非会員
							case 41:
								$wk_buff_arr[] = ($wk_kyochosya1["edata".$k_key] != "") ? "\"".$this->wa_kaiin[$wk_kyochosya1["edata".$k_key]]."\"" : "\"\"";
								break;
								//任意項目
							case 43:
							case 44:
							case 45:
							case 65:
							case 66:
							case 84:
							case 85:
							case 86:
							case 87:
							case 88:
							case 89:
							case 90:
							case 91:
							case 92:
							case 93:
							case 94:
							case 95:
							case 96:
							case 97:
							case 98:
								$wk_buff_arr[]= $this->_makeNini($wk_kyochosya1["edata".$k_key], $k_key, $pa_itemData);
								break;
								
								//Mr. Mis Dr
							case 61:
								if($wk_kyochosya1["edata".$k_key] != ""){
									$wk_buff_arr[] = $GLOBALS["titleList"][$wk_kyochosya1["edata".$k_key]];
								}
								else{
									$wk_buff_arr[] = "";
								}
								break;
								
							case 46:
							case 47:
							default:
								$wk_buff_arr[] = "\"".$wk_kyochosya1["edata".$k_key]. "\"";
						}
					}
				}
				
				$cnt++;
			}
			
			
			
			//15人以下の場合データを埋める
			for($a=0; $a < $loop; $a++){
				
				$this->_makeDefault($key_buff, $pa_itemData, $wk_buff_arr);
				
			}
			
		//状態
		$wk_buff_stat[] = ($pa_param["status"] == "0") ? "-" : "\"".$GLOBALS["entryStatusList"][$pa_param["status"]]."\"";

		//登録日
		$wk_buff_stat[] = "\"".$pa_param["insday"]."\"";


		//更新日
		$wk_buff_stat[] = "\"".$pa_param["upday"]."\"";

		$wk_buff = array_merge((array)$wk_buff, (array)$wk_buff_arr);
		$wk_buff = array_merge((array)$wk_buff, (array)$wk_buff_stat);
		$ret_buff .= implode($this->delimiter, $wk_buff);

		$ret_buff .= "\n";

		return $ret_buff;
		
		}
	
}
?>

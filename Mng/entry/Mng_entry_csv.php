<?php



/**
 * 管理者　CSVダウンロード
 *
 * @author salon
 *
 */
class entry_csv{


	/**
	 * コンストラクタ
	 */
	function entry_csv(){
		LoginMember::checkLoginRidirect();

		//----------------------------
		//インスタンス
		//----------------------------
		$this->o_entry   = new Entry;
		$this->o_itemini = new item_ini();
		$this->objErr    = new Validate;
		$this->db        = new DbGeneral;
		$this->download  = new Download;

		//------------------------------
		//初期化
		//------------------------------
		$this->start     = 1;
		$this->loop      = 15;
		$this->arrErr    = "";
		$this->delimiter = "\t";
        $this->formdata = $GLOBALS["userData"];
        $this->form_id  = $this->formdata["form_id"];
        $this->fix      = "\"";

		//--------------------------------------------
		//定数情報取得
		//--------------------------------------------
		$lang = ($GLOBALS["userData"]["lang"] == "1") ? "J" : "E";
		$this->wa_contact  = $GLOBALS["contact_".$lang];     // 連絡先
		$this->wa_kaiin    = $GLOBALS["member_".$lang];        // 会員・非会員
		$this->wa_coAuthor = $GLOBALS["coAuthor_".$lang];   // 共著者の有無

		//フォームパラメータ
		$this->arrForm = $_REQUEST;

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');
        Usr_initial::setFormIni($this);

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isSortItemClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }

            // フォーム管理者CSV用設定の読み込み
            if(method_exists($this->exClass, "__constructMngCSV")){
                $this->exClass->__constructMngCSV($this);
            }

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }


	/**
	 * メイン処理
	 */
	function main(){
		//--------------------------------
		//対象データ取得
		//--------------------------------
		//データ取得（エントリー情報）
		list($wb_ret, $list) = $this->_getData($this);
		if(!$wb_ret){
			//終了処理
			return array(false, "出力可能なデータはありません。");
		}


		//-----------------------------
		//CSVヘッダ部分
		//-----------------------------
		$this->header = $this->entryMakeHeader($this);



		//-----------------------------
		//CSVデータ部分
		//-----------------------------
		$this->csvdata = "";
		foreach($list as $data){
			$this->csvdata .= $this->entryMakeData($this,$data);
		}


		//ヘッダとデータを連結して出力
		$exp_data = mb_convert_encoding($this->header.$this->csvdata, "SJIS", "UTF8");

		return array(true, $exp_data);
//		$this->download->csv($exp_data, "entory_".date('Ymdhis').".csv");
//
	}



	/**
	 * 対象データ取得
	 */
	function _getData($obj){

		//フォーム頭文字の長さ
		$len_formhead = strlen($GLOBALS["userData"]["head"])  + 1;

		$column = "*, to_char(rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(udate, 'yyyy/mm/dd hh24:mi:ss') as upday,  substr(e_user_id, length(e_user_id)-4 ,5) as entry_no";
		$from = $obj->o_entry->getTable($GLOBALS["userData"]["type"]);

		$where[] = "del_flg = 0";
		$where[] = "form_id = ".$obj->db->quote($GLOBALS["userData"]["form_id"]);

		//　応募者
		if($obj->arrForm["user_name"] != "") {
			$arrval = GeneralFnc::customExplode($obj->arrForm["user_name"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata1 like ".$obj->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}

		// ステータス
		if(isset($obj->arrForm["status1"]) && $obj->arrForm["status1"] == "1") $subwhere[] = "status = 1";
		if(isset($obj->arrForm["status2"]) && $obj->arrForm["status2"] == "2") $subwhere[] = "status = 0";
		if(isset($obj->arrForm["status3"]) && $obj->arrForm["status3"] == "3") $subwhere[] = "status = 2";
		if(isset($obj->arrForm["status4"]) && $obj->arrForm["status4"] == "4") $subwhere[] = "status = 99";


		if(isset($subwhere)) {
			$where[] = "(".implode(" or ", $subwhere).")";
			unset($subwhere);
		}

		// 受付期間
		if($obj->arrForm["syear"] > 0) {
			if($obj->arrForm["smonth"] == "") $obj->arrForm["smonth"] = "1";
			if($obj->arrForm["sday"] == "") $obj->arrForm["sday"] = "1";
			$sdate = sprintf("%04d", $obj->arrForm["syear"])."-".sprintf("%02d", $obj->arrForm["smonth"])."-".sprintf("%02d", $obj->arrForm["sday"]);
			$where[] = "rdate >= ".$obj->db->quote($sdate)."";
		}
		if($obj->arrForm["eyear"] > 0) {
			if($obj->arrForm["emonth"] == "") $obj->arrForm["emonth"] = "12";
			if($obj->arrForm["eday"] == "") {
				$edate = date("Y-m-d", mktime(0, 0, 0, $obj->arrForm["emonth"]+1, 0, $obj->arrForm["eyear"]));
			} else {
				$edate = sprintf("%04d", $obj->arrForm["eyear"])."-".sprintf("%02d", $obj->arrForm["emonth"])."-".sprintf("%02d", $obj->arrForm["eday"]);
			}

			$where[] = "rdate <= ".$obj->db->quote($edate)."";
		}



		// 支払方法
		if(isset($obj->arrForm["payment_method"])){
			if(is_array($obj->arrForm["payment_method"])) {
				if(count($obj->arrForm["payment_method"]) > 0) {
					foreach($obj->arrForm["payment_method"] as $val) {
						$subwhere[] = "payment_method = ".$obj->db->quote($val);
					}
					$where[] = "(".implode(" or ", $subwhere).")";
					unset($subwhere);
				}

			}
		}


		// 支払ステータス
		if(isset($obj->arrForm["payment_status"])){
			if(is_array($obj->arrForm["payment_status"])) {
				if(count($obj->arrForm["payment_status"]) > 0) {
					foreach($obj->arrForm["payment_status"] as $val) {
						$subwhere[] = "payment_status = ".$obj->db->quote($val);
					}
					$where[] = "(".implode(" or ", $subwhere).")";
					unset($subwhere);
				}
			}
		}


		//登録No 開始
		if($obj->arrForm["s_entry_no"] != "") {
			//桁数を0埋め５桁にする
			$s_entry_no = sprintf("%05d", $obj->arrForm["s_entry_no"]);
			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) >= ".$obj->db->quote($s_entry_no)."";

		}

		//登録No 終了
		if($obj->arrForm["e_entry_no"] != "") {
			//桁数を0埋め５桁にする
			$e_entry_no = sprintf("%05d", $obj->arrForm["e_entry_no"]);
			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) <= ".$obj->db->quote($e_entry_no)."";

		}

		//国
		if(isset($obj->arrForm["country"])){
			if($obj->arrForm["country"] != "") {
				$where[] = "edata60 like ".$obj->db->quote("%".$obj->arrForm["country"]."%");
			}
		}


		// 受付期間
		if($obj->arrForm["upd_syear"] > 0) {
			if($obj->arrForm["upd_smonth"] == "") $obj->arrForm["upd_smonth"] = "1";
			if($obj->arrForm["upd_sday"] == "") $obj->arrForm["upd_sday"] = "1";
			$upd_sdate = sprintf("%04d", $obj->arrForm["upd_syear"])."-".sprintf("%02d", $obj->arrForm["upd_smonth"])."-".sprintf("%02d", $obj->arrForm["upd_sday"]);
			$where[] = "udate >= ".$obj->db->quote($upd_sdate)."";
		}
		if($obj->arrForm["upd_eyear"] > 0) {
			if($obj->arrForm["upd_emonth"] == "") $obj->arrForm["upd_emonth"] = "12";
			if($obj->arrForm["upd_eday"] == "") {
				$upd_edate = date("Y-m-d", mktime(0, 0, 0, $obj->arrForm["upd_emonth"]+1, 0, $obj->arrForm["upd_eyear"]));
			} else {
				$upd_edate = sprintf("%04d", $obj->arrForm["upd_eyear"])."-".sprintf("%02d", $obj->arrForm["upd_emonth"])."-".sprintf("%02d", $obj->arrForm["upd_eday"]);
			}

			$where[] = "udate <= ".$obj->db->quote($upd_edate)."";
		}

		//カナ
		if($obj->arrForm["user_name_kana"] != "") {
			$arrval = GeneralFnc::customExplode($obj->arrForm["user_name_kana"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata3 like ".$obj->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}



		//------------------------------------
		//表示順
		//------------------------------------
		$wk_order_by = "udate desc";

		if($obj->arrForm["sort_name"] != "" && $obj->arrForm["sort"] != ""){
			$wk_order_by  = "";
			if($obj->arrForm["sort_name"] == "2"){
				$wk_order_by .= "entry_no ";
			}
			else{
				$wk_order_by .= "udate ";
			}

			if($obj->arrForm["sort"] == "1"){
				$wk_order_by .= "desc";
			}
			else{
				$wk_order_by .= "asc";
			}
		}


		$orderby = $wk_order_by;
		//$orderby = "eid";

		$list_data = $obj->db->getListData($column, $from, $where, $orderby);
		if(!$list_data){
			return array(false, "");
		}


		//共著者情報を表示する場合


		//if($obj->itemData[29]["item_pview"] != "1"){

			//エントリーに紐づく共著者情報を取得
			foreach($list_data as $data){

				//共著者以外のデータ
				$wk_entory[$data["eid"]] = $data;

				$wa_aff = $obj->o_entry->getRntry_aff($obj->db, $data["eid"]);

				if(!$wa_aff){
					$wk_entory[$data["eid"]]["chosya"] = array();
				}
				else{
					$wk_entory[$data["eid"]]["chosya"] = $wa_aff;
				}
			}
		//}

		return array(true, $wk_entory);


	}


    /**  CSVヘッダ生成 */
    function entryMakeHeader($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader")){
            return $this->exClass->entry_csv_entryMakeHeader($obj, $all_flg);
        }

        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());

        // グループ1ッダ
        $groupHeader[1] = $obj->entryMakeHeader1($obj, $all_flg);

        // グループ2ヘッダ # 日英で分岐
        $group2_func = 'entryMakeHeader2';
        if($GLOBALS["userData"]["lang"] == LANG_ENG) $group2_func = $group2_func.'_'.$GLOBALS["userData"]["lang"]; // 英語
        $groupHeader[2] = $obj->$group2_func($obj, $all_flg);

        // グループ3ヘッダ
        $groupHeader[3] = $obj->entryMakeHeader3($obj, $all_flg);


        $header = $groupHeader[1];
        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[]="状態";
        $header[]="エントリー登録日";
        $header[]="エントリー更新日";

        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }


    /** CSVヘッダ-グループ1生成 */
    function entryMakeHeader1($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader1")){
            return $this->exClass->entry_csv_entryMakeHeader1($obj, $all_flg);
        }

        $group = 1;
        $groupHeader[$group][] = "登録No.";
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }


    /** CSVヘッダ-グループ2生成 */
    function entryMakeHeader2($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader2")){
            return $this->exClass->entry_csv_entryMakeHeader2($obj, $all_flg);
        }

        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $loop_cnt = $obj->loop +1;

            for($i=$this->start; $i <= $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    // 画面に表示する項目の名称
                    $name = strip_tags($_data["item_name"]);

                    $prefix = ($i == 1) ? "筆頭者" : "共著者".($i-1);
                    $prefix = $obj->fix.$prefix." ";

                    $item_id = $_data["item_id"];
                    switch($item_id){
                            case 32:
                                $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                                break;

                            // 共著者　姓名
                            case 33:
                                $groupHeader[$group][] =  $prefix."姓名".$obj->fix;
                                break;

                            // 共著者　姓名カナ
                            case 35:
                                $groupHeader[$group][] =  $prefix."姓名（カナ）".$obj->fix;
                                break;

                            case 34:
                            case 36:
                                break;

                            default:
                                $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                                break;
                    }
                }
            }
        }
        return $groupHeader[$group];
    }


    /** CSVヘッダ-グループ2生成 # 英語用 */
        function entryMakeHeader2_2($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader2")){
            return $this->exClass->entry_csv_entryMakeHeader2($obj, $all_flg);
        }

        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $loop_cnt = $obj->loop +1;

            for($i=$this->start; $i <= $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    // 画面に表示する項目の名称
                    $name = strip_tags($_data["item_name"]);

                    $prefix = ($i == 1) ? "筆頭者" : "共著者".($i-1);
                    $prefix = $obj->fix.$prefix." ";

                    $item_id = $_data["item_id"];
                    switch($item_id){
                        case 32:
                            $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                            $groupHeader[$group][] =  $prefix.$name." No.".$obj->fix;
                            break;

                        default:
                            $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                            break;
                    }
                }
            }
        }

        return $groupHeader[$group];
    }


    /** CSVヘッダ-グループ3生成 */
    function entryMakeHeader3($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader3")){
            return $this->exClass->entry_csv_entryMakeHeader3($obj, $all_flg);
        }

        $group = 3;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }



    /**
     * CSV出力データ生成
     */
    function entryMakeData($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData")){
            return $this->exClass->entry_csv_entryMakeData($obj, $pa_param, $all_flg);
        }

        foreach($pa_param as $key=>$data) {
            $pa_param[$key] = str_replace('"', '""', $data);
        }


        // グループ別に生成
        $groupBody = array("1" => array(), "2" => array(), "3" => array());

        // グループ1 データ生成
        $groupBody[1] = $obj->entryMakeData1($obj, $pa_param, $all_flg);

        // グループ2 データ生成
        $group2_func = 'entryMakeData2';
        if($GLOBALS["userData"]["lang"] == LANG_ENG) $group2_func = $group2_func.'_'.$GLOBALS["userData"]["lang"]; // 英語
        $groupBody[2] = $obj->$group2_func($obj, $pa_param, $all_flg);

        // グループ3 データ生成
        $groupBody[3] = $obj->entryMakeData3($obj, $pa_param, $all_flg);

        $body = $groupBody[1];
        if(count($groupBody[3]) > 0) $body = array_merge($body, $groupBody[3]);
        if(count($groupBody[2]) > 0 && !$all_flg) $body = array_merge($body, $groupBody[2]);
        //状態
        $body[] = ($pa_param["status"] == "0") ? $obj->fix."-".$obj->fix : $obj->fix.$GLOBALS["entryStatusList"][$pa_param["status"]].$obj->fix;
        $body[] = $obj->fix.$pa_param["insday"].$obj->fix;    // 登録日
        $body[] = $obj->fix.$pa_param["upday"].$obj->fix;     // 更新日

        $ret_buff .= implode($obj->delimiter, $body);
        $ret_buff .= "\n";

        return $ret_buff;
    }



    /** CSV出力データ グループ1生成 */
    function entryMakeData1($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData1")){
            return $this->exClass->entry_csv_entryMakeData1($obj, $pa_param, $all_flg);
        }

        $group = 1;
        $groupBody[$group][] = $pa_param["entry_no"];
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            $methodname = "csvfunc".$item_id;
            // カスタマイズあり
            if(method_exists($this->exClass, $methodname)) {
                $wk_body = $this->exClass->$methodname($obj, $group, $pa_param, $item_id);

            // カスタマイズなし
            } else {
                // 任意項目
                if($_data["controltype"] == "1"){
                    $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
                    $wk_body = trim($wk_body, ",");

                // 標準項目
                }else{
                    switch($item_id){
                        //会員・非会員
                        case 8:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_kaiin[$pa_param["edata".$item_id]] : "";
                            break;
                        //連絡先
                        case 16:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_contact[$pa_param["edata".$item_id]] : "";
                            break;
                        //都道府県
                        case 18:
                            //英語フォームの場合
                            if($GLOBALS["userData"]["lang"] == "2"){
                                $wk_body =  ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            }
                            else{
                                $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["prefectureList"][$pa_param["edata".$item_id]] : "";
                            }
                            break;
                        //共著者の有無
                        case 29:
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $obj->wa_coAuthor[$pa_param["edata".$item_id]] : "";
                            break;

                        //共著者の所属機関
                        case 31:
                            $wk_kikan_names = array();
                            $wk_kikanlist   = explode("\n", $pa_param["edata".$item_id]);

                            $i =  "";
                            $obj->set_select = array();
                            foreach($wk_kikanlist as $num => $val){
                                $i = $num+1;
                                $obj->set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
                                $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
                            }
                            $wk_body = str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names));
                            break;

                        //Mr. Mis Dr
                        case 57:
                            if($pa_param["edata".$item_id] != ""){
                                $wk_body = $GLOBALS["titleList"][$pa_param["edata".$item_id]];
                            }
                            else{
                                $wk_body = "";
                            }
                            break;
                        //国名リストボックス
                        case 114:
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["country"][$pa_param["edata".$item_id]] : "";
                            break;
                        default:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            break;
                    }
                }
            }
            $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
        }
        return $groupBody[$group];
    }


    /** CSV出力データ グループ2生成 */
    function entryMakeData2($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData2")){
            return $this->exClass->entry_csv_entryMakeData2($obj, $pa_param, $all_flg);
        }

        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $cnt      = count($pa_param["chosya"]);
            $loop_cnt = $obj->loop +1;

            for($i=$this->start; $i <= $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    $item_id = $_data["item_id"];

                    // 共著者の員数を越えた場合はNULL埋め
                    if($cnt < $i){
                        if($item_id == "34") continue;
                        if($item_id == "36") continue;

                        $groupBody[$group][] = "\"\"";
                        continue;
                    }

                    $wk_body = "";
                    $chosya  = $pa_param["chosya"][$i-1];


                    $methodname = "csvfunc".$item_id;
                    // カスタマイズあり
                    if(method_exists($this->exClass, $methodname)) {
                        $wk_body = $this->exClass->$methodname($obj, $group, $chosya, $item_id);

                    // カスタマイズなし
                    } else {
                        // 任意項目
                        if($_data["controltype"] == "1"){
                            $wk_body = trim(Usr_Assign::nini($obj, $group, $item_id, $chosya["edata".$item_id], array(" ", ","), true), ",");

                        // 標準項目
                        }else{
                            switch($item_id){
                                //共著者所属期間
                                case 32:
                                    if(count($obj->set_select) > 0){
                                        if($chosya["edata".$item_id] != ""){
                                            $chkeck = explode("|", $chosya["edata".$item_id]);
                                            $chkeck_names = array();

                                            foreach($chkeck as $val){
                                                if($val != "" && array_key_exists($val, $obj->set_select)){
                                                    $chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $obj->set_select[$val]);
                                                }
                                            }

                                            if(count($chkeck_names) > 0){
                                                $wk_aaa =   implode(",", $chkeck_names);
                                            }

                                        }
                                        else{
                                            $wk_aaa = "";
                                        }

                                        $wk_body = str_replace(array("\r\n","\n","\r"), '', trim($wk_aaa));
                                    }
                                    else{
                                        $wk_body = "";
                                    }
                                    break;

                                //共著者姓名
                                case 33:
                                    $cyosya_name = "";
                                    $wk_mark = array();
                                    if($chosya["edata32"] != ""){
                                        $chkeck = explode("|", $chosya["edata32"]);
                                        foreach($chkeck as $val){
                                            $wk_mark[] = $val.")";
                                        }
                                        $cyosya_name = implode(", ", $wk_mark);

                                    }
                                    $cyosya_name .= $chosya["edata".$item_id]." ".$chosya["edata34"];
                                    $wk_body = $cyosya_name;
                                    break;

                                //共著者姓名（カナ）
                                case 35:
                                    $wk_body = $chosya["edata".$item_id]." ".$chosya["edata36"];
                                    break;

                                case 34:
                                case 36:
                                    continue 2;
                                    break;


                                //会員・非会員
                                case 41:
                                    $wk_body = ($chosya["edata".$item_id] != "") ? $obj->wa_kaiin[$chosya["edata".$item_id]] : "";
                                    break;

                                //Mr. Mis Dr
                                case 61:
                                    if($chosya["edata".$item_id] != ""){
                                        $wk_body = $GLOBALS["titleList"][$chosya["edata".$item_id]];
                                    }
                                    else{
                                        $wk_body = "";
                                    }
                                    break;

                                case 46:
                                case 47:
                                default:
                                    $wk_body = $chosya["edata".$item_id];
                                    break;
                            }
                        }
                    }

//                    if(strlen($wk_body) == 0) continue;
                    $groupBody[$group][] = "\"".$wk_body."\"";
                }
            }
        }
        return $groupBody[$group];
    }


    /** CSV出力データ グループ2生成 # 英語用 */
    function entryMakeData2_2($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData2")){
            return $this->exClass->entry_csv_entryMakeData2($obj, $pa_param, $all_flg);
        }

        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $cnt      = count($pa_param["chosya"]);
            $loop_cnt = $obj->loop +1;

            for($i=$this->start; $i <= $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    $item_id = $_data["item_id"];

                    // 共著者の員数を越えた場合はNULL埋め
                    if($cnt < $i){
                        if($item_id == "36") continue;

                        $groupBody[$group][] = "\"\"";
                        if($item_id == 32){ // affiliation番号
                            $groupBody[$group][] = "\"\"";
                        }
                        continue;
                    }

                    $wk_body = "";
                    $chosya  = $pa_param["chosya"][$i-1];


                    $methodname = "csvfunc".$item_id;
                    // カスタマイズあり
                    if(method_exists($this->exClass, $methodname)) {
                        $wk_body = $this->exClass->$methodname($obj, $group, $chosya, $item_id);

                        // カスタマイズなし
                    } else {
                        // 任意項目
                        if($_data["controltype"] == "1"){
                            $wk_body = trim(Usr_Assign::nini($obj, $group, $item_id, $chosya["edata".$item_id], array(" ", ","), true), ",");

                            // 標準項目
                        }else{
                            switch($item_id){
                                //共著者所属期間
                                case 32:
                                    $wk_affiliation_name = "";   // 選択したaffiliation名
                                    $wk_affiliation_mark = "";   // 選択したaffiliation番号

                                    if(count($obj->set_select) > 0){
                                        if($chosya["edata".$item_id] != ""){
                                            $chkeck = explode("|", $chosya["edata".$item_id]);
                                            $chkeck_names = array();    // 選択したAffiliation名の配列
                                            $wk_mark = array();         // 選択したAffiliation番号の配列

                                            foreach($chkeck as $val){
                                                if($val != "" && array_key_exists($val, $obj->set_select)){
                                                    $chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $obj->set_select[$val]);
                                                    $wk_mark[] = $val.")";
                                                }
                                            }

                                            if(count($chkeck_names) > 0){
                                                $wk_affiliation_name =   implode(",", $chkeck_names);
                                                $wk_affiliation_mark =   implode(",", $wk_mark);
                                            }

                                        }
                                        else{
                                            $wk_affiliation_name = "";
                                        }

                                        $wk_body = str_replace(array("\r\n","\n","\r"), '', trim($wk_affiliation_name));
                                    }
                                    else{
                                        $wk_body = "";
                                    }
                                    break;

                                    //共著者姓名
                                case 33:
                                    $wk_body = $chosya["edata".$item_id];
                                    break;

                                    //共著者姓名（カナ）
                                case 35:
                                    $wk_body = $chosya["edata".$item_id]." ".$chosya["edata36"];
                                    break;

                                case 36:
                                    continue 2;
                                    break;


                                    //会員・非会員
                                case 41:
                                    $wk_body = ($chosya["edata".$item_id] != "") ? $obj->wa_kaiin[$chosya["edata".$item_id]] : "";
                                    break;

                                    //Mr. Mis Dr
                                case 61:
                                    if($chosya["edata".$item_id] != ""){
                                        $wk_body = $GLOBALS["titleList"][$chosya["edata".$item_id]];
                                    }
                                    else{
                                        $wk_body = "";
                                    }
                                    break;

                                case 46:
                                case 47:
                                default:
                                    $wk_body = $chosya["edata".$item_id];
                                    break;
                            }
                        }
                    }
                    //                    if(strlen($wk_body) == 0) continue;
                    $groupBody[$group][] = "\"".$wk_body."\"";
                    // affiliationの次の列にaffiliation番号を表示
                    if($item_id == 32){
                        $groupBody[$group][] = "\"".$wk_affiliation_mark."\"";
                        $wk_affiliation_mark = "";
                    }
                }
            }
        }

        return $groupBody[$group];
    }


    /** CSV出力データ グループ3生成 */
    function entryMakeData3($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData3")){
            return $this->exClass->entry_csv_entryMakeData3($obj, $pa_param, $all_flg);
        }

        $group = 3;
        $groupBody = array();
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            $methodname = "csvfunc".$item_id;
            // カスタマイズあり
            if(method_exists($this->exClass, $methodname)) {
                $wk_body = $this->exClass->$methodname($obj, $group, $pa_param, $item_id);

            // カスタマイズなし
            } else {
                // 任意項目
                if($_data["controltype"] == "1"){
                    $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
                    $wk_body = trim($wk_body, ",");

                // 標準項目
                }else{
                    switch($item_id){
                        //発表形式
                        case 48:
                            $keisiki = "";
                            if($pa_param["edata".$item_id] != ""){
                                if($obj->itemData[$item_id]["select"]){
                                    $keisiki = $obj->itemData[$item_id]["select"][$pa_param["edata".$item_id]];
                                }
                            }
                            $wk_body = str_replace(array("\r\n","\n","\r"), '', $keisiki);
                            break;

                        default:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            break;
                    }
                }
            }
            $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
        }
        return $groupBody[$group];
    }



    function _chkTerm(){
        return true;
    }

    function _chkTerm2(){
        return true;
    }

}


?>
<?php

include('../../application/cnf/include.php');
include(MODULE_DIR.'custom/Entry.class.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once('../function.php');
include_once('./Mng_entry_csv.php');
include_once('./Mng_payment_csv.php');
include_once('./Mng_all_csv.php');
include(MODULE_DIR.'Download.class.php');
include_once(TGMDK_ORG_DIR.'/crypt.class.php');


/**
 * 管理者TOP
 *
 * @author salon
 *
 */
class Entry_Error extends ProcessBase {

	var $limit = 10;
	var $searchkey = array(
					's_entry_no', 'e_entry_no', 'user_name', 'status1', 'status2'
					, 'status3', 'status4','syear', 'smonth', 'sday', 'eyear'
					, 'emonth', 'eday','payment_method', 'payment_status', "entry_no", "country"
					,'upd_syear', 'upd_smonth', 'upd_sday', 'upd_eyear', 'upd_emonth', 'upd_eday'
					, "country_list", "invalid_flg1", "invalid_flg2", "invalid");
	var $sesskey = "mng_formlist_error";

	/**
	 * コンストラクタ
	 */
	function Entry_Error(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){


		/**
		 * ログインチェック
		 */
		LoginMember::checkLoginRidirect();


		//--------------------------------
		//初期化
		//--------------------------------
		$this->arrErr= array();


		// 表示HTMLの設定
		$this->_processTemplate = "Mng/entry/Mng_formlist_error.html";
		$this->_title = "管理者ページ";



//		$this->assign("isTop", true);

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", $GLOBALS["userData"]["user_name"]);



		//-------------------------------
		//管理者メニュー取得
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";



		$objEntry = new Entry;
		$this->objErr = new Validate;
		$this->download = new Download;
		$this->db = new DbGeneral;
		$this->arrForm = $_REQUEST;

		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));


		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;

		$eyear[$wk_year0] = $wk_year0;
		$eyear[$wk_year1] = $wk_year1;
		$eyear[$wk_year2] = $wk_year2;

		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;



		//月
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$smonth[$wk_month] = $wk_month;
			$emonth[$wk_month] = $wk_month;

		}
		//日付
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$sday[$wk_day] = $wk_day;
			$eday[$wk_day] = $wk_day;
		}

		$this->assign("syear", $syear);				//開始（年）
		$this->assign("smonth", $smonth);			//開始（月）
		$this->assign("sday", $sday);				//開始（日）

		$this->assign("eyear", $eyear);				//終了（年）
		$this->assign("emonth", $emonth);			//終了（月）
		$this->assign("eday", $eday);				//終了（日）

		//-----------------------------
		//ソート順　リストボックス
		//-----------------------------
		$sort_name["1"] = "更新日";
		$sort_name["2"] = "登録番号";

		$sort["1"] = "降順";
		$sort["2"] = "昇順";

		$this->assign("sort_name", $sort_name);
		$this->assign("sort", $sort);

		//-----------------------------



		//-----------------------------
		//フォーム項目取得
		//-----------------------------
		$this->formitem = $objEntry->getFormItem($this->db, $GLOBALS["userData"]["form_id"]);
		if(!$this->formitem){
			Error::showErrorPage("フォーム項目情報の取得に失敗しました。");
		}

		$paper = "";	//論文フラグ

//プレビュー機能は無くなったため、コメントアウト
//		if($GLOBALS["userData"]["lang"] == "1") {
//			if($this->formitem["49"]["item_view"] == "0" || $this->formitem["50"]["item_view"] == "0") $paper = "1";
//		} else {
//			if($this->formitem["28"]["item_view"] == "0") $paper = "1";
//		}
//		$this->assign("paper", $paper);

        foreach($this->formitem as $data){
            $wk_item[$data["item_id"]] = $data;
        }
        $formitemview["60"] = $wk_item["60"]["item_view"];
        $formitemview["114"] = $wk_item["114"]["item_view"];



        $this->assign("formitemview", $formitemview);

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			case "search":

				//---------------------------------
				//検索条件　日付の妥当性チェック
				//	存在しない日付の場合はエラー
				//---------------------------------
				$this->arrErr = $this->check();

				$this->setSearchkey();

				break;

			case "clear":
				$this->clearSearchkey();
				break;

			case "entry_csv":

				//---------------------------------
				//検索条件　日付の妥当性チェック
				//	存在しない日付の場合はエラー
				//---------------------------------
				$this->arrErr = $this->check();
				$this->setSearchkey();

				if(count($this->arrErr) == 0){
					$wo_csv = new entry_csv;

					list($wb_ret, $csv_data) = $wo_csv->main();
					if(!$wb_ret){
						$this->objErr->addErr($csv_data, "csv_data");
						$this->arrErr = $this->objErr->_err;

					}
					else{
						$this->download->csv($csv_data, "entry_".date('Ymdhis').".csv");
						exit;
					}
				}
					break;

			case "payment_csv":

				//---------------------------------
				//検索条件　日付の妥当性チェック
				//	存在しない日付の場合はエラー
				//---------------------------------
				$this->arrErr = $this->check();
				$this->setSearchkey();

				if(count($this->arrErr) == 0){

					$wo_csv = new payment_csv;

					list($wb_ret, $csv_data) = $wo_csv->main();
					if(!$wb_ret){
						$this->objErr->addErr($csv_data, "csv_data");
						$this->arrErr = $this->objErr->_err;

					}
					else{
						$this->download->csv($csv_data, "payment_".date('Ymdhis').".csv");
						exit;
					}

				}
				break;

			case "all_csv":

				//---------------------------------
				//検索条件　日付の妥当性チェック
				//	存在しない日付の場合はエラー
				//---------------------------------
				$this->arrErr = $this->check();
				$this->setSearchkey();


				if(count($this->arrErr) == 0){

					$wo_csv = new all_csv;
					$o_entry_csv = new entry_csv;
					$o_payment_csv = new payment_csv;


					list($wb_ret, $csv_data) = $wo_csv->main($o_payment_csv);
					if(!$wb_ret){
						$this->objErr->addErr($csv_data, "csv_data");
						$this->arrErr = $this->objErr->_err;

					}
					else{
						$this->download->csv($csv_data, "all_".date('Ymdhis').".csv");
						exit;
					}

				}
				break;


			break;

			default:
				//セッションクリア
				$GLOBALS["session"]->unsetVar($this->sesskey);

				break;

		}

		$this->getSearchkey();
		if(count($this->arrErr) > 0){
			//エラーがあった場合はセッションクリア
			$GLOBALS["session"]->unsetVar($this->sesskey);

		}

		$arrData = array();
		$this->arrForm["page"] = !isset($this->arrForm["page"]) ? "1" : $this->arrForm["page"];

		//検索条件にエラーが無い場合に一覧情報を取得
		if(count($this->arrErr) ==  0){
			
			$this->arrForm["invalid"] = 1;				//無効の物の対象
			$this->arrForm["payment_method"][] = 3;		// 銀聯決済のみ
			
			list($arrData["count"], $arrData["list"]) = $objEntry->getListEntry($GLOBALS["userData"]["form_id"], $this->arrForm, $this->arrForm["page"], $this->limit);
			// 78番フォームは6桁表示
			if($GLOBALS["userData"]["form_id"] == "78"){
				foreach($arrData['list'] as $key => $edata){
					if(stristr($edata["e_user_id"], '-') === false){
						$entry_no = $edata["e_user_id"];
					}else{
						$arrNo = explode("-", $edata["e_user_id"]);
						$entry_no = sprintf("%06d", $arrNo[1]);
					}
					$arrData['list'][$key]['entry_no'] = $entry_no;
				}
			}
			$this->pager = array("allcount"=>$arrData["count"], "limit"=>$this->limit, "page"=>$this->arrForm["page"]);
		}
		else{
			$arrData["count"] = 0;
		}


		//if(!isset($this->arrForm["payment_method"])) $this->arrForm["payment_method"] = array();
		//if(!isset($this->arrForm["payment_status"])) $this->arrForm["payment_status"] = array();
		if(!isset($this->arrForm["chg_status"])) $this->arrForm["chg_status"] = "";

		//$form["payment_method"] = SmartyForm::createRadioChecked("payment_method", $GLOBALS["method_J"], $this->arrForm["payment_method"], "checkbox", "form");
		//$form["payment_status"] = SmartyForm::createRadioChecked("payment_status", $GLOBALS["paymentstatusList"], $this->arrForm["payment_status"], "checkbox", "form");
		$form["chg_status"] = SmartyForm::createCombo("chg_status", $GLOBALS["paymentstatusList"], $this->arrForm["chg_status"], "form", "↓選択");
		$this->assign("form", $form);

		$this->assign("arrData", $arrData);
		$this->assign("arrErr", $this->arrErr);


		// 親クラスに処理を任せる
		parent::main();


	}

	/**
	 * 入力チェック
	 *
	 * @access public
	 * @return object
	 */
	function check(){

		//妥当姓チェック（存在しない日付の場合はエラー）
		if(($this->arrForm["syear"] != "") && ($this->arrForm["smonth"] != "") && ($this->arrForm["sday"] != "") ){
			if(!checkdate((int)$this->arrForm["smonth"], (int)$this->arrForm["sday"], (int)$this->arrForm["syear"])){
				$this->objErr->addErr("受付期間（開始）に存在しない日付が指定されています。", "search_sday");
			}

		}

		if($this->arrForm["eyear"] != "" && $this->arrForm["emonth"] != "" && $this->arrForm["eday"] != ""){

			if(!checkdate((int)$this->arrForm["emonth"], (int)$this->arrForm["eday"], (int)$this->arrForm["eyear"])){
				$this->objErr->addErr("受付期間（終了）に存在しない日付が指定されています。", "search_eday");
			}
		}

		return $this->objErr->_err;
	}


	function setSearchkey() {

		$GLOBALS["session"]->unsetVar($this->sesskey);

		foreach($this->searchkey as $val) {
			if(isset($_REQUEST[$val])) {
				if(is_array($_REQUEST[$val])) {
					$param[$val] = $_REQUEST[$val];
				} else {
					$param[$val] = trim($_REQUEST[$val]);
				}
			} else {
				$param[$val] = "";
			}
		}

		$GLOBALS["session"]->setVar($this->sesskey, $param);

	}

	function clearSearchkey() {

		$GLOBALS["session"]->unsetVar($this->sesskey);

		foreach($this->searchkey as $key) {
			$this->arrForm[$key] = "";
		}
	}

	function getSearchkey() {

		if(!$GLOBALS["session"]->issetVar($this->sesskey)) return;

		$sessvar = $GLOBALS["session"]->getVar($this->sesskey);

		foreach($this->searchkey as $key) {
			if(isset($sessvar[$key])) {
				$this->arrForm[$key] = $sessvar[$key];
			} else {
				$this->arrForm[$key] = "";
			}
		}

	}
}

/**
 * メイン処理開始
 **/

$c = new Entry_Error();
$c->main();







?>

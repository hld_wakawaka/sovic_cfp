<?php
include('../application/cnf/include.php');
include_once('./function.php');

class message extends ProcessBase {

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function message() {
		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 * @access	public
	 */
	function main() {
	
		
		// 表示HTMLの設定
		$this->_processTemplate = TMPL_FILE_ERROR;
		$this->_title = "管理者ページ　エラー";
		
		$this->assign("user_name", $GLOBALS["userData"]["user_name"]);
		
		//-------------------------------
		//管理者メニュー取得
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);
		
		$errorParam = $GLOBALS["session"]->getVar("errorParam");

		$this->_smarty->assign("errorParam", $errorParam);

		// 親クラスに処理を任せる
		parent::main();
	}

}

/**
 * メイン処理開始
 **/

$c = new message();
$c->main();

?>
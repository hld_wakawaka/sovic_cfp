<?php

include('../../application/cnf/include.php');
include('../function.php');

/**
 * システム管理者
 * 	ユーザ登録・編集
 *
 * @subpackage Sys
 * @author salon
 *
 */
class passwrodedit extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function passwrodedit(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){
ini_set("error_reporting", E_ALL);

		LoginMember::checkLoginRidirect();

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", $GLOBALS["userData"]["user_name"]);

		//-------------------------------
		//管理者メニュー取得
		//-------------------------------
		$menu = Mng_function::makeMenu();
		$this->assign("va_menu", $menu);

		if($GLOBALS["userData"]["pass_chg_flg"] != "1") {
			$this->complete("パスワードを変更する権限がありません。");
		}

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = GeneralFnc::convertParam($this->init(), $_REQUEST);


		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Mng/account/edit.html";
		$this->_title = "管理者ページ";

		$arrErr = array();

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			//-----------------
			//確認画面
			//-----------------
			case "confirm":

				if($this->_reload->isReload()) $this->complete("既に実行した可能性があります。");

				$arrErr = $this->check();
				if(count($arrErr) > 0) break;

				$this->update();
			break;

			 case "back":

			 break;

			/**
			 * 初期表示時
			 */
			default:


			 break;

		}

		$this->assign("arrErr", $arrErr);



		// 親クラスに処理を任せる
		parent::main();


	}

	function init() {

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
		$key[] = array("password",		"現在のパスワード", 	array(5, 12),	array("NULL"),			"a",	0,	0);
		$key[] = array("newpassword",	"新しいパスワード", 		array(5, 12),	array("NULL", "LEN"),	"a",	1,	0);
		$key[] = array("newpassword2",	"確認用パスワード",		array(5, 12),	array("NULL"),			"a",	0,	0);

		return $key;

	}
	function check() {

		$objErr = New Validate;

		$objErr->check($this->init(), $this->arrForm);

		if($this->arrForm["password"] != "" and md5($this->arrForm["password"].PASS_PHRASE) != $GLOBALS["userData"]["admin_password"]) {
			$objErr->addErr("現在のパスワードが間違っています。", "password");
		}

		if($this->arrForm["newpassword"] != $this->arrForm["newpassword2"]) {
			$objErr->addErr("新しいパスワードと確認用パスワードが一致しません。");
		}

		return $objErr->_err;

	}

	function update() {

		$db = new DbGeneral;

		$db->begin();

		$param["password"] = md5($this->arrForm["newpassword"].PASS_PHRASE);
		$param["udate"] = "NOW";
		$where[] = "form_id = ".$db->quote($GLOBALS["userData"]["form_id"]);

		// スーパーフラグが立っている場合はformテーブルの更新
		if($GLOBALS["userData"]["super_flg"] == "1") {
			$rs = $db->update("form", $param, $where, __FILE__, __LINE__);
			if(!$rs) 	{
				$db->rollback();
				$this->complete("パスワードの変更に失敗しました。");
			}
		}

		// form_adminテーブルの更新
		$where[] = "formadmin_id = ".$db->quote($GLOBALS["userData"]["formadmin_id"]);

		$rs = $db->update("form_admin", $param, $where, __FILE__, __LINE__);
		if(!$rs) 	{
			$db->rollback();
			$this->complete("パスワードの変更に失敗しました。");
		}

		$db->commit();
		$this->complete("パスワードを変更しました。");

	}

	function complete($msg) {

		$this->assign("msg", $msg);
		$this->_processTemplate = "Mng/Mng_complete.html";
		$this->arrForm["url"] = "../index.php";

		// 親クラスに処理を任せる
		parent::main();
		exit;

	}
}

/**
 * メイン処理開始
 **/

$c = new passwrodedit();
$c->main();







?>
<?php
/**
 * 管理者　function
 *
 * @package packagename
 * @subpackage Mng
 * @author salon
 *
 */
class Mng_function{


	/**
	 * 管理者メニュー生成
	 *
	 * @access public
	 * @return array
	 */
	function makeMenu(){

		$menu[0] = array("menu_name" => "エントリー管理", "prg" => APP_ROOT."Mng/index.php");
		//$menu[1] = array("menu_name" => "会議管理", "prg" => APP_ROOT."Mng/meeting/Mng_meetinglist.php");
		$menu[2] = array("menu_name" => "メール送信履歴", "prg" => APP_ROOT."Mng/mail/Mng_mailhistory.php");

        //
        if($GLOBALS["userData"]['use_csvbasic'] == 0){
    		$menu[3] = array("menu_name" => "CSVDL-Basic認証設定"
    		               , "prg"       => APP_ROOT."Mng/csv/Mng_csvBasic.php");
        }

		$menu[4] = array("menu_name" => "パスワード変更", "prg" => APP_ROOT."Mng/account/edit.php");
		$menu[5] = array("menu_name" => "ログアウト", "prg" => APP_ROOT."Mng/login/index.php?mode=logout");


		return $menu;

	}


	/**
	 * 年月日　リストボックス生成
	 *
	 * @access public
	 * @return array
	 */
	function makeDateListBox(){

		//------------------
		//年
		//------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));

		$year[$wk_year0] = $wk_year0;
		$year[$wk_year1] = $wk_year1;
		$year[$wk_year2] = $wk_year2;


		//------------------
		//月
		//------------------
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$month[$wk_month] = $wk_month;
		}

		//-------------------
		//日
		//-------------------
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$day[$wk_day] = $wk_day;
		}

		return array($year, $month, $day);

	}


	/**
	 * 年月日　妥当性チェック
	 *
	 * @access public
	 * @param array
	 * @return array
	 */
	function check_date($param){

        // 少なくともどれか一つが入力されている。
        if(($param[1] != "") || ($param[2] != "") || ($param[3] != "")) {

            // 年月日のどれかが入力されていない。
            if(!(strlen($param[1]) > 0 && strlen($param[2]) > 0 && strlen($param[3]) > 0)) {
                $error_msg = $param[0] . "はすべての項目を入力して下さい。";

                return array(false,$error_msg);

            }


            if ( ! checkdate((int)$param[2], (int)$param[3], (int)$param[1])) {
                $error_msg = $param[0] . "が正しくありません。";
                return array(false,$error_msg);
            }

        }

        return array(true,"");

	}


	/**
	 * ファイル取得処理
	 *
	 * @param string 取得先ディレクトリ
	 * @return array 取得ファイル名
	 */
	function getFiles($ps_dir){

		//取得ファイル名格納変数
		$wa_files = array();

		if( is_readable($ps_dir) ){
			@$wo_handle = opendir($ps_dir);
		} else {
			$wo_handle = "";
		}

		while ( $wo_handle != '' && $ws_wk_file = readdir($wo_handle) ) {
			if( $ws_wk_file != '.' && $ws_wk_file != '..' ) {
				if(is_file($ps_dir."/".$ws_wk_file)){
					//画面表示用にファイル名を配列に格納する
					array_push($wa_files, $ws_wk_file);
				}
			}
		}

		if($wo_handle !=""){
			closedir($wo_handle);
		}

		return $wa_files;
	}


	/**
	 * ディレクトリ削除
	 *
	 * @param string 対象ディレクトリ
	 * @return bool
	 */
	function deleteDir($ps_dir){

		if (is_dir($ps_dir)) {
			$wo_dir_h = opendir($ps_dir);
			while (false != ($ws_file = readdir($wo_dir_h)))
			{
				if($ws_file!="." && $ws_file != "..") {
					if (is_dir($ps_dir . "/" . $ws_file)) {
					    $this->deleteDir($ps_dir . "/" . $ws_file);
					}
					else if (is_file($ps_dir . "/" . $ws_file)) {
					    unlink($ps_dir . "/" . $ws_file);
					}
				}
			}
			closedir($wo_dir_h);
			return rmdir($ps_dir);
		}
		else {
		    return true;
		}

	}

	/**
	 * ファイル削除
	 * 	指定したディレクトリ内のファイルを削除する
	 *
	 * @param string 対象ディレクトリ
	 * @return bool
	 */
	function deleteFile($ps_dir){

		if (is_dir($ps_dir)) {
			$wo_dir_h = opendir($ps_dir);
			while (false != ($ws_file = readdir($wo_dir_h)))
			{
				if($ws_file!="." && $ws_file != "..") {
					if (is_file($ps_dir . "/" . $ws_file)) {
					    unlink($ps_dir . "/" . $ws_file);
					}
				}
			}
			closedir($wo_dir_h);
			return true;
		}
		else {
		    return true;
		}

	}


    /**
     * 登録日、更新日の取得
     * @see Usr/form/function#getEntryDate
     */
    function getEntryDate($po_db, $pn_id){

        $column = "to_char(rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(udate, 'yyyy/mm/dd hh24:mi:ss') as upday";
        $from = "entory_r";
        $where[] = "del_flg = 0";
        $where[] = "eid = ".$po_db->quote($pn_id);

        $rs = $po_db->getData($column, $from, $where, __FILE__, __LINE__);

        return $rs;
    }

    /**
     * 文字列置換
     * @see Usr/form/function#wordReplace
     */
    function wordReplace($ps_val, $pa_param){


        $ret_val = "";

        if($ps_val == ""){
            return $ret_val;
        }


// 		// ここでCSVインポートのアクションを動かす(cfp用はここで外部クラス定義)
// 		$this->mode = isset($_REQUEST["set_stat"]) ? $_REQUEST["set_stat"] : "";
// 		$actionName = $this->mode."Action";
// 		$exAction   = 'Mng_plural_'.$actionName; ⇦ Sys_form_importのexecInput()を参考にしたCSVインポートメソッドを入れる
// 		// 外部クラス読み込み
// 			if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
// 				$this->exClass->$exAction($this);
// 			}else{
// 				$this->$actionName();
// 			}
// 		}

        $ret_val = $ps_val;
        $ret_val = ereg_replace("_ID_", $pa_param["ID"], $ret_val);
        $ret_val = ereg_replace("_PASSWORD_", $pa_param["PASSWORD"], $ret_val);
        $ret_val = ereg_replace("_INSERT_DAY_", $pa_param["INSERT_DAY"], $ret_val);
        $ret_val = ereg_replace("_UPDATE_DAY_", $pa_param["UPDATE_DAY"], $ret_val);
        $ret_val = ereg_replace("_SEI_", $pa_param["SEI"], $ret_val);

        $ret_val = ereg_replace("_MEI_", $pa_param["MEI"], $ret_val);
        $ret_val = ereg_replace("_MIDDLE_", $pa_param["MIDDLE"], $ret_val);

        //敬称が入力されていない場合
        if($pa_param["TITLE"] == ""){
            $ret_val = str_replace(array("_TITLE_"), "", $ret_val);
        }
        $ret_val = ereg_replace("_TITLE_", $pa_param["TITLE"], $ret_val);

        $ret_val = ereg_replace("_SECTION_", $pa_param["SECTION"], $ret_val);

        return $ret_val;
    }


    /**
     * eid#checkboxの保持
     * 改ページリンクや戻るボタンに対応
     * @param  void
     * @return array $arrEid
     */
    function gf_set_eid($limit=""){
        $arrEid = $GLOBALS["session"]->getVar("eid");
        $pre_page = !isset($_POST["pre_page"]) ? "0" : $_POST["pre_page"];

        if($pre_page > 0){
            $arrEid[$pre_page] = $_POST['eid'];

            // ページ番号でソート
            $keys = array_keys($arrEid);
            asort($keys);
            foreach($arrEid as $key => $row){
                $foo[$key] = $key;
            }
            array_multisort($foo,SORT_ASC,$arrEid);
            $arrEid = array_combine($keys, $arrEid);
            //------

            // 前回と異なる表示件数
            if($limit != "" && $limit != $_POST['pre_limit']){
                // ページ数を振り直す
                $newArrEid = array();

                $count = max(array_keys($arrEid));

                // 前回より多い表示件数の場合 # 10->50etc
                if($limit > $_POST['pre_limit']){
                    $index = $limit/$_POST['pre_limit'];

                    for($i=1; $i<=$count; $i++){
                        if(!isset($arrEid[$i])) continue;

                        // ページ数を再計算
                        $page_no = floor(($i-1)/$index)+1;

                        if(!isset($newArrEid[$page_no])) $newArrEid[$page_no] = array();
                        $newArrEid[$page_no] = (array)$newArrEid[$page_no] + (array)$arrEid[$i];
                    }
                    $arrEid = $newArrEid;

                // 前回より少ない表示件数の場合 # 50->10etc
                }else{
                    $index = $_POST['pre_limit']/$limit;

                    for($i=1; $i<=$count; $i++){
                        if(!isset($arrEid[$i])) continue;

                        for($j=0; $j<$index; $j++){
                            $key = ($i-1)*$index+($j+1);
                            $newArrEid[$key] = array_slice($arrEid[$i], $j*$limit, $limit, true);
                        }
                    }
                    $arrEid = $newArrEid;
                }
            }
            $GLOBALS["session"]->setVar("eid", $arrEid);
        }

        return $arrEid;
    }


}
?>

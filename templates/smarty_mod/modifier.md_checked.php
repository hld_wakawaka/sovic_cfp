<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_modifier_md_checked($str, $num)
{

	if(!is_array($str) || $num == "") {
		return "";
	}
	
	if(in_array($num, $str)) {
		return " checked";
	}
	
	return "";
	
}

/* vim: set expandtab: */

?>

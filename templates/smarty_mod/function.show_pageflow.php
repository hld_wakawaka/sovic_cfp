<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_show_pageflow($params, &$smarty)
{
	
	if(!isset($params["pageflow"])) return;
	if(!is_array($params["pageflow"])) return;
	if(!count($params["pageflow"]) > 0) return;
	
	$active = isset($params["active"]) ? $params["active"] : "";
	
	$buf = "<table id=\"pageflow\"><tr>";
	foreach($params["pageflow"] as $key=>$val) {
		$cls = ($active !== "" && $key == $active) ? " class=\"active\"" : "";
		$buf .= sprintf("<td%s>%s</td>", $cls, $val);
	}
	
	$buf .= "</tr></table>";
	
	return $buf;
	

}

/* vim: set expandtab: */

?>

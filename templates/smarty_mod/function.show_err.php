<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_show_err($str, &$smarty)
{

	if(!is_array($str)) {
		if($str == "") return "";
		$arr[] = $str;
	} else {
		$arr = $str;
	}
	
	$buf = array();
	
    if(!count($arr) > 0) return "";
    
    foreach($arr as $val) {
    	if(is_array($val)) {
    		foreach($val as $val2) {
    			if($val2 != "") $buf[] = $val2;
    		}
    	} else {
    		if($val != "") $buf[] = $val;
    	}
    }
    
    if(!count($buf) > 0) return "";
    
    return '<div class="errmsg">'.implode("<br />", $buf).'</div>';
}

/* vim: set expandtab: */

?>

<?php

/**
 * Smarty plugin
 * 各項目のカスタマイズ用の項目ID配列をassign
 * 
 * @package Smarty
 * @subpackage plugins
 */
function smarty_function_setIncFormItem($args, &$smarty) {

    $smarty->assign("exitems", array());

    if(!isset($args["type"]))    return;

    // 0:input, 1:confirm
    $type    = $args["type"];
    $suffix  = ($type == 0) ? "" : "_confirm";

    // assign変数
    $params  = $smarty->get_template_vars();
    $includeDir = $params["includeDir"]."parts/";
    $form_id    = $params["formData"]['form_id'];

    // ファイル名
    $parts1 = $form_id."formItem*". $suffix .".html";
    $parts2 = $form_id."formItem([0-9]+)". $suffix ."\.html";

    // オーバーライドする項目IDを取得
    $ex = glob($includeDir.$parts1);

    $arrItemIds = array();
    if(is_array($ex)){
        foreach($ex as $_key => $script){
            if(preg_match("/".$parts2."$/u", $script, $match) !== 1) continue;

            $item_id = $match[1];

            // item_idをセット
            $arrItemIds[$item_id] = $script;
        }
    }
    $smarty->assign("exitems", $arrItemIds);
    return;
}

/* vim: set expandtab: */

?>

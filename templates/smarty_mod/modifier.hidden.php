<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_modifier_hidden($string, $moji = '*')
{
    
	if($string == "") return;
	
	$buffer = preg_replace("/(.+?)/", $moji ,$string);
	
	return $buffer;
}

/* vim: set expandtab: */

?>

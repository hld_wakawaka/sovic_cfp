<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_modifier_show_selectvalue($string, $arrValue = '')
{

	if($string == "") return;
	if($arrValue == "") return;
	
	$array = explode("\n", $arrValue);
	
	if(strpos($string, "|") !== false) {
		
		$arrKey = explode("|", $string);
		
		foreach($arrKey as $val) {
			if($val != "") {
				$arrRet[] = $array[$val-1];
			}
		}
		
		if(isset($arrRet)) {
			return implode(", ", $arrRet);
		}

		return;
		
	} else {
		return $array[$string-1];
	}
	
}

/* vim: set expandtab: */

?>

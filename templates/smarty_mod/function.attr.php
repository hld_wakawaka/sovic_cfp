<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_attr($str, &$smarty){

    $params  = $smarty->get_template_vars();
    $_formItem = $params['_formItem'];
    $arrForm   = $params['arrForm'];
    $edata     = $params['edata'];
    $admin_flg = $params['admin_flg'];
    $s_key     = isset($params['s_key']) ? $params['s_key'] : "";
    $key54     = isset($params['key54']) ? $params['key54'] : "";


    $attr = array();

    // Attribute # disabled
    $_formItem['disabled'] = isset($_formItem['disabled']) ? $_formItem['disabled'] : false;
    if($admin_flg == ""){ // User only
        switch($_formItem['item_type']){
            case "2": // radio
                if($_formItem['disabled']   // Usr_entry[f].premain
                || ($_formItem['disable'][$s_key] == "1" && $arrForm[$edata] != $s_key) // strpos'#'
                ){
                    $attr[] = 'disabled="disabled"';
                }
                break;

            case "3": // checkbox
                if($_formItem['disabled']   // Usr_entry[f].premain
                || ($_formItem['disable'][$s_key] == "1" && $arrForm[$key54] != $s_key) // strpos'#'
                ){
                    $attr[] = 'disabled="disabled"';
                }
                break;

            default:
                if($_formItem['disabled']){
                    $attr[] = 'disabled="disabled"';
                }
                break;
        }
    }


    return implode(" ", $attr);
}

/* vim: set expandtab: */

?>

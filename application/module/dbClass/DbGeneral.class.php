<?php
require_once("DbBase.class.php");

/**
 * DbGeneralクラス
 *
 * DB汎用操作クラス
 *
 * @package	include.dbClass.DbGeneral
 * @access	public
 * @create	2005/04/27
 * @version	$Id: DbGeneral.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
 **/
class DbGeneral extends DbBase {

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function DbGeneral() {
		parent::DbBase();
	}

	/**
	 * デストラクタ
	 * @access	public
	 */
	function _DbGeneral() {
	}

//	PHP5の場合のコンストラクタ
//	function __construct() {
//	}

//	PHP5の場合のデストラクタ
//	function __destruct() {
//	}

	/**
	 * データの取得処理を行います（一覧取得）
	 * @access	public
	 * @param	string	$column		データ取得時列名
	 * @param	string	$from		データ取得時FROM
	 * @param	mixed	$whereMix	検索条件（配列の場合と文字列で処理方法が変わる）
	 * @param	string	$orderBy	データ取得時並び順文
	 * @param	string	$offset		取得開始行数
	 * @param	string	$limit		取得行数
	 * @param	string	$file		呼び出し元ファイル名
	 * @param	string	$line		呼び出し元行数
	 * @return	mixed	取得成功時はデータ一覧配列、取得失敗時は、false
	 */
	function getListData($column, $from, $whereMix = null, $orderBy = "", $offset = -1, $limit = -1, $file = "", $line = "") {



		// SQL生成
		$sql		= "";
		$whereSql	= "";
		$orderSql	= "";
		$offsetSql	= "";
		$limitSql	= "";

		$sql .= "SELECT ".$column
			   ."  FROM ".$from;

		if ($whereMix) {
			if (is_array($whereMix)) {
				$whereSql = " WHERE ".implode(" AND ", $whereMix);
			} else {
				$whereSql = " WHERE ".$whereMix;
			}
		}

		if (!empty($orderBy)) {
			$orderSql .= " ORDER BY ".$orderBy;
		}

		if ($offset > -1) {
			$offsetSql .= " OFFSET ".$offset;
		}

		if ($limit > -1) {
			$limitSql .= " LIMIT ".$limit;
		}

		$sql1	= $sql.$whereSql.$orderSql.$offsetSql.$limitSql;
		$sql2	= $sql.$whereSql.$orderSql;


		// 実際のデータ取得処理は、親クラスに任せる
		return parent::getListData($sql1, $sql2, $offset, $limit, $file, $line);

	}


	/**
	 * データの取得処理を行います
	 * @access	public
	 * @param	string	$column		データ取得時列名
	 * @param	string	$from		データ取得時FROM
	 * @param	mixed	$whereMix	検索条件（配列の場合と文字列で処理方法が変わる）
	 * @param	string	$file		呼び出し元ファイル名
	 * @param	string	$line		呼び出し元行数
	 * @return	mixed	取得成功時はデータ格納連想配列、取得失敗時は、false
	 */
	function getData($column, $from, $whereMix = null, $file = "", $line = "") {

		// SQL生成
		$sql = "";
		$where = "";

		$sql .= "SELECT ".$column
			   ."  FROM ".$from;

		if ($whereMix) {
			if (is_array($whereMix)) {
				$where = " WHERE ".implode(" AND ", $whereMix);
			} else {
				$where = " WHERE ".$whereMix;
			}
		}

		// 実際のデータ取得処理は、親クラスに任せる
		return parent::getData($sql.$where, $file, $line);
	}
	
	function select($sql, $sql2, $file="", $line="") {
	
		// 実際のデータ取得処理は、親クラスに任せる
		return parent::getListData($sql, $sql2, -1, -1, $file, $line);
	
	}

	function begin($file="", $line="") {
		$GLOBALS["db"]->simpleQuery("BEGIN", null, $file, $line);
	}

	function commit($file="", $line="") {
		$GLOBALS["db"]->simpleQuery("COMMIT", null, $file, $line);
	}

	function rollback($file="", $line="") {
		$GLOBALS["db"]->simpleQuery("ROLLBACK", null, $file, $line);
	}

	/**
	 * データの追加処理を行います
	 * @access	public
	 * @param	string	$from		テーブル名称
	 * @param	array	$param		カラム名称と値の連想配列
	 * @param	string	$file		呼び出し元ファイル名
	 * @param	string	$line		呼び出し元行数
	 * @return	mixed	取得成功時はtrue、取得失敗時は、false
	 */
	function insert($from, $param, $file = "", $line = "") {

		// SQL生成
		$sql			= "";
		$column			= array();
		$columnValue	= array();

		$sql .= "INSERT INTO ".$from;

		foreach ($param as $key => $value) {
			$value = $GLOBALS["db"]->quoteSmart($value);

			if ($value === "'NULL'") {
				$value = "NULL";
			}
			
			if($value === "'NOW'") {
				$value = "now()";
			}
			//NONの場合は、登録に入れない
			if($value === "'NON'") {
			
			} else {

			$column[]		= $key;
			$columnValue[]	= $value;
			
			}
		}
		$sql .= " (".implode(",", $column).")";
		$sql .= " VALUES(".implode(",", $columnValue).")";

		$rows = $GLOBALS["db"]->simpleQuery($sql, null, $file, $line);

		if (!empty($this->_errorText)) {
			return false;
		}

		return true;
	}

	/**
	 * データの更新処理を行います
	 * @access	public
	 * @param	string	$from		テーブル名称
	 * @param	array	$param		カラム名称と値の連想配列
	 * @param	mixed	$whereMix	検索条件（配列の場合と文字列で処理方法が変わる）
	 * @param	string	$file		呼び出し元ファイル名
	 * @param	string	$line		呼び出し元行数
	 * @return	mixed	取得成功時はデータ格納連想配列、取得失敗時は、false
	 */
	function update($from, $param, $whereMix = null, $file = "", $line = "") {

		// SQL生成
		$sql	= "";
		$where 	= "";
		$column	= array();

		$sql .= "UPDATE ".$from;

		foreach ($param as $key => $value) {
			$value = $GLOBALS["db"]->quoteSmart($value);

			if ($value === "'NULL'") {
				$value = "NULL";
			}
			
			if ($value === "'NOW'") {
				$value = "now()";
			}

			if ($value === "'NON'") {
			} else {
				$column[] = $key." = ".$value;
			}
		}

		$sql .= " SET ".implode(",", $column);

		if ($whereMix) {
			if (is_array($whereMix)) {
				$where = " WHERE ".implode(" AND ", $whereMix);
			} else {
				$where = " WHERE ".$whereMix;
			}
		}

		$sql .= $where;

		$rows = $GLOBALS["db"]->simpleQuery($sql, null, $file, $line);

		if (!empty($this->_errorText)) {
			return false;
		}

		return true;
	}

	/**
	 * データの更新処理を行います
	 * @access	public
	 * @param	string	$from		テーブル名称
	 * @param	mixed	$whereMix	検索条件（配列の場合と文字列で処理方法が変わる）
	 * @param	string	$file		呼び出し元ファイル名
	 * @param	string	$line		呼び出し元行数
	 * @return	mixed	取得成功時はデータ格納連想配列、取得失敗時は、false
	 */
	function delete($from, $whereMix = null, $file = "", $line = "") {

		// SQL生成
		$sql = "";
		$where = "";

		$sql .= "DELETE FROM ".$from;

		if ($whereMix) {
			if (is_array($whereMix)) {
				$where = " WHERE ".implode(" AND ", $whereMix);
			} else {
				$where = " WHERE ".$whereMix;
			}
		}

		$sql .= $where;

		$rows = $GLOBALS["db"]->simpleQuery($sql, null, $file, $line);

		if (!empty($this->_errorText)) {
			return false;
		}

		return true;
	}
	
	function _getOne($column, $from, $whereMix=null, $file="", $line="") {
	
		$sql = "";
		$where = "";
		
		$sql .= "SELECT ".$column." FROM ".$from;
		
		if ($whereMix) {
			if (is_array($whereMix)) {
				$where = " WHERE ".implode(" AND ", $whereMix);
			} else {
				$where = " WHERE ".$whereMix;
			}
		}

		$sql .= $where;
		
		$rows = $GLOBALS["db"]->getOne($sql, null, $file, $line);
		
		if(!empty($this->_errorText)) {
			return false;
		}
		
		return $rows;
	
	}
	
	function quote($str) {
		
		return $GLOBALS["db"]->quoteSmart($str);
		
	}
	

    /**
     * レコードの存在チェック
     * 
     * @param  $table    対象テーブル
     * @mixed  $whereMix where文をarrayまたはstringで指定
     * @return boolean   true:存在
     * 
     */
    function exists($table, $whereMix=NULL, $file = "", $line = ""){

		$where = "";
		if ($whereMix) {
			if (is_array($whereMix)) {
				$where = " WHERE ".implode(" AND ", $whereMix);
			} else {
				$where = " WHERE ".$whereMix;
			}
		}
		$sql = 'select * from '.$table.$where;
		$sql = sprintf('SELECT CASE WHEN EXISTS(%s) THEN 1 ELSE 0 END', $sql);

        return (bool)$GLOBALS["db"]->getOne($sql, null, $file, $line);
    }
}
?>

<?php
require_once("PEAR.php");

/**
 * DbBaseクラス
 *
 * DbBaseクラス
 *
 * @package	include.dbClass.DbBase
 * @access	public
 * @create	2004/10/19
 * @version	$Id: DbBase.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
 **/
class DbBase extends PEAR {

	var $_maxRows			= 0;		// １ページの最大表示件数
	var $_rowCount			= 0;		// ページ内の取得行数
	var $_startDisplayRows	= 0;		// ページ内表示開始行数
	var $_endDisplayRows	= 0;		// ページ内表示終了行数

	var $_rows				= 0;		// 最大行数
	var $_isPrevious		= false;	// 前ページ有無
	var $_isNext			= false;	// 次ページ有無
	var $_isPreviousRows	= 0;		// 前ページ開始行数
	var $_isNextRows		= 0;		// 次ページ開始行数

	var $_errorText		= "";

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function DbBase() {
		parent::PEAR();
	}

	/**
	 * デストラクタ
	 * @access	public
	 */
	function _DbBase() {
	}

//	PHP5の場合のコンストラクタ
//	function __construct() {
//	}

//	PHP5の場合のデストラクタ
//	function __destruct() {
//	}

	/**
	 * データの取得処理を行います（一覧取得）
	 * @access	public
	 * @param	string	$sql1		データ取得時SQL
	 * @param	string	$sql2		データ総件数取得時SQL
	 * @param	string	$offset		取得開始行数
	 * @param	string	$limit		取得行数
	 * @param	string	$file		呼び出し元ファイル名
	 * @param	string	$line		呼び出し元行数
	 * @return	mixed	取得成功時はデータ一覧配列、取得失敗時は、false
	 */
	function getListData($sql1, $sql2, $offset = -1, $limit = -1, $file = "", $line = "") {

		// 変数初期化
		$listData				= false;

		$this->_maxRows			= $limit;

		$this->_rows			= 0;
		$this->_isPrevious		= false;
		$this->_isNext			= false;
		$this->_isPreviousRows	= 0;
		$this->_isNextRows		= 0;

		$res	= $GLOBALS["db"]->query($sql1, null, $file, $line);
		$rows	= $res->numRows();

		if (!empty($rows)) {
			$listData = array();

			for ($curRow = 0; $curRow < $rows; $curRow++) {
				// 1行取得
				$data = $res->fetchRow(DB_FETCHMODE_ASSOC, $curRow);
				// 表示用テキスト設定
				$data = $this->setShowText($data);
				// リスト保存用クラス変数に追加
				array_push($listData, $data);
			}

			// 総件数を取得
			$res = $GLOBALS["db"]->query($sql2, null, $file, $line);
			$this->_rows = $res->numRows();

			// 画面遷移がある場合には、次、前リンクの有無を判定
			if ($limit > -1) {
				if ($offset > 0) {
					$this->_isPrevious		= true;
					$this->_isPreviousRows	= $offset - $limit;
				}

				if ($this->_rows > $offset + $limit) {
					$this->_isNext			= true;
					$this->_isNextRows		= $offset + $limit;
				}
			}

			$this->_rowCount			= count($listData);
			$this->_startDisplayRows	= $offset + 1;
			$this->_endDisplayRows		= $offset + $this->_rowCount;

		}

		return $listData;
	}

	/**
	 * データの取得処理を行います
	 * @access	public
	 * @param	string	$sql		データ取得時SQL
	 * @param	string	$file		呼び出し元ファイル名
	 * @param	string	$line		呼び出し元行数
	 * @return	mixed	取得成功時はデータ格納連想配列、取得失敗時は、false
	 */
	function getData($sql, $file = "", $line = "") {

		// 変数初期化
		$data		= false;

		$res	= $GLOBALS["db"]->query($sql, null, $file, $line);
		$this->_rows	= $res->numRows();

		if (!empty($this->_rows)) {
			// 1行取得
			$data = $res->fetchRow(DB_FETCHMODE_ASSOC, 0);
			// 表示用テキスト設定
			$data = $this->setShowText($data);
		}

		return $data;
	}

	/**
	 * 次のIDの取得処理を行います
	 * @access	public
	 * @param	string	$column		IDカラム名称
	 * @param	string	$table		取得元テーブル名
	 * @param	string	$where		取得時の条件
	 * @param	string	$defaultId	取得できない場合のデフォルトID
	 * @return	boolean	更新結果
	 */
	function getNextId($column, $table, $where = "", $defaultId = "0") {

		// 最大値のIDを取得
		$id = $defaultId;

		$sql =   "SELECT MAX(".$column.") AS ".$column." FROM ".$table;

		if (!empty($where)) {
			$sql .= " WHERE ".$where;
		}

		$res = $GLOBALS["db"]->query($sql);
		$rows = $res->numRows();
		if ($rows > 0) {
			$row  = $res->fetchRow(DB_FETCHMODE_ASSOC);
			$id = $row[$column];
		}

		return $id;
	}

	/**
	 * データの表示用テキストを設定します
	 * @access	public
	 * @param	array	$data		データ格納連想配列
	 * @return	array	$data		データ格納連想配列
	 */
	function setShowText(&$data) {

		return $data;
	}

	/**
	 * データのチェックを行います
	 * @access	public
	 * @param	array	$data		データ格納連想配列
	 * @param	string	$mode		データ操作モード
	 * @return	boolean	チェック結果
	 */
	function validate(&$data, $mode) {

		$this->_errorText = "";

		// プロセスに応じて処理実行
		$validateFunction = "_validate_".$mode;
		if (!method_exists($this, $validateFunction)) {
			// クラスメソッドが定義されていない場合
			echo "クラスメソッド ".$validateFunction." が定義されていません";
			exit;
		}

		return $this->$validateFunction($data);
	}

	/**
	 * データの更新処理を行います
	 * @access	public
	 * @param	array	$data		データ格納連想配列
	 * @param	string	$mode		データ操作モード
	 * @return	boolean	更新結果
	 */
	function update(&$data, $mode) {

		$this->_errorText = "";

		// データ操作モードに応じて処理実行
		$updateFunction = "_".$mode;
		if (!method_exists($this, $updateFunction)) {
			// クラスメソッドが定義されていない場合
			echo "クラスメソッド ".$updateFunction." が定義されていません";
			exit;
		}

		return $this->$updateFunction($data);
	}
}
?>

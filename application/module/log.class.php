<?php
require_once(MODULE_DIR."GeneralFnc.class.php");

class log_class {

	var $message;
	var $filename;

	function log_class() {

		$this->filename = LOG_DIR."daily/".date('Ymd').".log";
	}

	function write_log($str, $option="") {

		$str = str_replace(array("\r\n", "\r", "\n", "\t"), '', $str);

		if ($GLOBALS["session"]->getVar("isLogin")) {
			$auth = "User_".$GLOBALS["userData"]["form_id"];
		} elseif($GLOBALS["session"]->getVar("isLoginAdmin")) {
			$auth = "Admin_".$GLOBALS["adminData"]["admin_id"];
		} else {
			$auth = "General";
		}
		$ua = $this->getUserAgent();
		$logstr = date('Y-m-d H:i:s')."\t".$ua."\t".$_SERVER["REMOTE_ADDR"]."\t".$str."\t".$auth."\t".$_SERVER["REQUEST_URI"]."\t".$option."\n";

		error_log($logstr, 3, $this->filename);

	}

	/**
	 * DBログ
	 */
	function write_log_DB($str) {

		$str = str_replace(array("\r\n", "\r", "\n", "\t"), '', $str);
		$logstr = date('Y-m-d H:i:s')."\t".$_SERVER["REMOTE_ADDR"]."\t".$str."\t".$_SERVER["REQUEST_URI"]."\n";

		error_log($logstr, 3, LOG_DIR."DBLog/DB_".date('Ymd').".log");
	}

    function getUserAgent(){
        $ua = strtolower($_SERVER["HTTP_USER_AGENT"]);
        $arrUA = array("chrome", "opera", "safari", "msie", "firefox", "rv");
        foreach($arrUA as $_key => $_ua){
            $pattern = '([ \t\n\r\f\v]|^)'.$_ua.'(\/|[ \t\n\r\f\v]|\:)([0-9]+($|\.|[ \t\n\r\f\v]|\;))+';
            // chrome, opera, safari, ie<=11, firefox
            if(preg_match('/'.$pattern.'/u', $ua, $matches) === 1){
                $ua = trim($matches[0], " \t\n\r\f\v;.");
                $ua = str_replace(array(":", " "), "/", $ua);
                $ua = str_replace("rv",   "msie", $ua);
                $ua = str_replace("msie", "ie",   $ua);
                break;

            // ie11, other
            }else{
                $ua = "unknown:".$ua;
            }
        }
        return $ua;
    }

}
?>
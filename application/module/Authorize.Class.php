<?php
/*
 * クレジットカード決済 与クラス
 *
 */



$form_id = $_REQUEST["form_id"];

define('INPUT_PAGE', 'Authorize.php');

define('TRUE_FLAG_CODE', 'true');
define('FALSE_FLAG_CODE', 'false');

define('ERROR_PAGE_TITLE', 'System Error');
define('NORMAL_PAGE_TITLE', '取引結果');

//require_once(MDK_DIR."3GPSMDK.php");


class Authorize{
	
	var $form_id;

	/**
	 * コンストラクタ
	 */
	function Authorize($LIB_PATH){

			require_once($LIB_PATH."3GPSMDK.php");
	}

	/**
	 * 決済処理
	 */
	function card_exec($ps_head, $pn_price, $pn_entry_number){

		//------------------------------------
		//セッション情報取得
		//------------------------------------
		$wk_sesssion = $GLOBALS["session"]->getVar("form_parampay");
		//$wk_order_no = $GLOBALS["session"]->getVar("ss_order_no");
        $wk_order_no = $pn_entry_number;


		//取引ID
		$this->order_id = $ps_head."-".sprintf("%05d", $wk_order_no);

		//支払金額
		//$this->payment_amount = $wk_sesssion["amount"];
		$this->payment_amount = (string)$pn_price;



		//与信方法
		$this->is_with_capture = "1";	//売上まで上げるか？

		if ("1" == $this->is_with_capture) {
			$this->is_with_capture = TRUE_FLAG_CODE;
		} else {
			$this->is_with_capture = FALSE_FLAG_CODE;
		}

		//カード番号
		$this->card_number = $wk_sesssion["cardNumber"];

		//カード有効期限 MM/YY
		$this->card_expire = $wk_sesssion["cardExpire1"]."/".substr($wk_sesssion["cardExpire2"],2,2);

		//支払オプション
		$this->jpo1 = "10";
		$this->jpo2 = "1";

		if ((!empty($this->jpo1)) && (("10" == $this->jpo1)|| ("80" == $this->jpo1))) {
			$this->jpo = $this->jpo1;
		}else if ((!empty($this->jpo1) && ("61" == $this->jpo1)) && (!empty($this->jpo2))) {
			$this->jpo = $this->jpo1."C".$this->jpo2;
		}

		//セキュリティーコード
		$this->security_code = $wk_sesssion["securityCode"];




		//--------------------------------
		//要求電文パラメータ値の指定
		//--------------------------------
		$request_data = new CardAuthorizeRequestDto();

		$request_data->setOrderId($this->order_id);
		$request_data->setAmount($this->payment_amount);
		$request_data->setCardNumber($this->card_number);
		$request_data->setCardExpire($this->card_expire);
		$request_data->setWithCapture($this->is_with_capture);

		if (isset($thi->jpo)) {
		 	$request_data->setJpo($this->jpo);
		}
		if (isset($this->security_code)) {
			$request_data->setSecurityCode($this->security_code);
		}

		//------------------------------------------------
		//実施
		//------------------------------------------------
		$transaction = new TGMDK_Transaction();
		$response_data = $transaction->execute($request_data);


		//戻り値配列
		$wk_ret_arr = array();


		//-----------------------
		//予期しない例外
		//-----------------------
		if (!isset($response_data)) {
			$page_title = ERROR_PAGE_TITLE;
			$main_message = "応答が取得できませんでした。";

		//-----------------------
		//想定応答の取得
		//-----------------------
		} else {
			$page_title = NORMAL_PAGE_TITLE;

		  	//結果コード取得
		 	$txn_status = $response_data->getMStatus();
		 	$wk_ret_arr["status"] = $txn_status;

		    //詳細コード取得
		 	$txn_result_code = $response_data->getVResultCode();
		 	$wk_ret_arr["result_code"] = $txn_result_code;

		    //エラーメッセージ取得
		  	$error_message = $response_data->getMerrMsg();
		  	$wk_ret_arr["error_message"] = $error_message;

		    //trAd URL取得
		  	//$trad_url = $response_data->getTradUrl();

		  	//カード番号
		  	$txn_cardnum = $response_data->getReqCardNumber();
		  	$wk_ret_arr["txn_cardnum"] = $txn_cardnum;

			//カード有効期限
			$txn_cardexprie = $response_data->getReqCardExpire();
			$wk_ret_arr["txn_cardexprie"] = $txn_cardexprie;

		  	//取引ID
		  	$txn_order_id = $response_data->getOrderId();


		  	$wk_ret_arr["order_id"] = $txn_order_id;

			//セキュリティーコード
//			$sec_code = $response_data->getSecurityCode();
//			$wk_ret_arr["sec_code"] = $sec_code;

			//-------------------------------
			// 成功
			//-------------------------------
			$wb_ret = true;

		 	if (TXN_SUCCESS_CODE === $txn_status) {
		 		$main_message = "取引が成功しました。";
		 	}
		 	else if (TXN_PENDING_CODE === $txn_status) {
		 		$main_message = "取引がペンディング状態になっています。";
		 	}

		 	//-------------------------------
		 	// 失敗
		 	//-------------------------------
		 	else if (TXN_FAILURE_CODE === $txn_status) {
		 		$wb_ret = false;
		 		$main_message = "取引に失敗しました。";
		 	}
		 	else {
		 		$wb_ret = false;
		 		$page_title = ERROR_PAGE_TITLE;
		 		$main_message = "応答が取得できませんでした。";
		 	}
		 }

		$wk_ret_arr["main_message"] = $main_message;


		//-------------------------------------
		//実行結果を返す
		//-------------------------------------
		return array($wb_ret, $wk_ret_arr);


	}








}



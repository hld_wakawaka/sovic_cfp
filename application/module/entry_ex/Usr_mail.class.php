<?php

class Usr_mail {

    /** 
     * 登録完了メールの送信
     */
    function sendRegistMail($obj, $user_id, $passwd) {
        
        //タイトル
        $obj->subject = ($obj->formdata["lang"] == "1") ? "エントリー完了通知メール" : "Notice: Your registration has been completed";
        if($obj->formWord["word11"] != ""){
            $obj->subject = $obj->formWord["word11"];
        }
            
        // メール送信
        $obj->sendCompleteMail($user_id, $passwd, 1);
        
    }
    
    /**
     * 編集完了メールの送信
     */
    function sendEditMail($obj, $user_id, $passwd) {
        
        //タイトル
        $obj->subject = ($obj->formdata["lang"] == "1") ? "エントリー情報変更通知メール" : "Notice: Information has been edited";
        if($obj->formWord["word12"] != ""){
            $obj->subject = $obj->formWord["word12"];
        }
        
        // メール送信
        $obj->sendCompleteMail($user_id, $passwd, 2);
        
    }
    
    /*
     * 完了メールの送信
     */
    function sendCompleteMail($obj, $user_id, $passwd, $exec_type=1) {
        
        if(in_array($obj->form_id, $obj->sendmail_not)) return;
        
        //------------------------------------
        //メール送信
        //------------------------------------
        //本文
        //メソッドチェック
        $ws_mailBody = "";

        $ws_mailBody = $obj->makeMailBody($user_id, $passwd, $exec_type);
        
        
        $subject = @ereg_replace("_ID_", $user_id, $obj->subject);
        
        // 登録者へのメール
        if($obj->admin_flg == "") {
            if($obj->arrForm["edata25"] != ""){
                $obj->o_mail->SendMail($obj->arrForm["edata25"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"], $obj->formWord["word13"]);
            }
        }
        
        // 管理者へのメール
        $obj->o_mail->SendMail($obj->formdata["form_mail"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"],$obj->formWord["word13"]);

        // 管理者2へのメール
        if(strlen($obj->formdata["form_mail2"]) > 0){
            $obj->o_mail->SendMail($obj->formdata["form_mail2"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"],$obj->formWord["word13"]);
        }
        
        return;
        
    }


    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $form_id = $obj->formdata['form_id'];
        
        // 置換文字列適用
        switch($exec_type){
            // 新規の場合
            case "1":
                $eid     = $obj->ins_eid;
                $ret_str = Usr_function::wordReplace($obj->formWord["word3"], $obj, compact("form_id", "eid", "exec_type"));
                break;
            
            // 更新の場合
            case "2":
                $eid     = $obj->eid;
                $ret_str = Usr_function::wordReplace($obj->formWord["word4"], $obj, compact("form_id", "eid", "exec_type"));
                break;
            
            default:
                $ret_str = "";
                break;
        }
        
        return $ret_str;
    }


    /**
     * 完了メール作成
     */
    function makeMailBody($obj, $user_id="", $passwd ="", $exec_type = ""){

        $body = "";
        // メール上部コメントの作成
        $head_comment = $obj->makeMailBody_header($user_id, $passwd, $exec_type);
        $body .= $head_comment."\n\n";

        // 言語別設定
        $obj->point_mark = ($obj->formdata["lang"] == LANG_JPN) ? "■" : "*";

        // グループ別メール本文
        $arrbody = array();

        // グループ1
        $obj->makeBodyGroup1($arrbody);

        // グループ2
        $obj->makeBodyGroup2($arrbody);

        // グループ3
        $obj->makeBodyGroup3($arrbody);


        //----------------------------------------
        //本文生成
        //----------------------------------------
        $body .= $arrbody[1];
        if($obj->formdata["group2_use"] != "1") $body .= $arrbody[2];
        if($obj->formdata["group3_use"] != "1") $body .= $arrbody[3];
        
        //$body .= print_r($obj->arrForm, true);

        $body .= "\n\n------------------------------------------------------------------------------\n";
        $body .= $obj->formdata["contact"];
        $body .= "\n------------------------------------------------------------------------------\n";
        
        // 2011/09/21 タグ除去追加 TODO
//        $body = strip_tags($body);
        

        return str_replace(array("\r\n", "\r"), "\n", $body);

    }

    function makeBodyGroup1($obj, &$arrbody){
        // グループ1
        $group = 1;

        // 言語別設定
        if($obj->formdata["lang"] == LANG_JPN){
            $arrbody[1] = ($obj->formdata["group1"] != "") ? "【".$obj->formdata["group1"]."】\n\n" : "";
        }else{
            $arrbody[1] = ($obj->formdata["group1"] != "") ? "[".$obj->formdata["group1"]."]\n\n" : "";
        }


        foreach($obj->arrItemData[$group] as $_key => $data){
            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

            $key = $data["item_id"];

            // 項目名（タグをとる)
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            if(!isset($obj->arrForm["edata".$key])){
                $obj->arrForm["edata".$key] = "";
            }

            $methodname = "mailfunc".$key;
            if(method_exists($this, $methodname)) {
                $arrbody[$group] .= $obj->$methodname($name);
            } else {
                if($data["controltype"] == "1") {
                    $arrbody[$group] .= $obj->mailfuncNini($key, $name);    //任意
                } else {
                    $arrbody[$group] .= $obj->point_mark.$name.": ".$obj->arrForm["edata".$key]."\n";
                }
            }
        }
    }


    function makeBodyGroup2($obj, &$arrbody){
        // グループ2が存在する時
        $group = 2;
        if($obj->formdata["group2_use"] != "1") {

            // 言語別設定
            $arrbody[$group] = "";

            // 言語別設定
            $body2_title = array();
            if($obj->formdata["lang"] == LANG_JPN){
                $body2_title[1] = "\n\n【".$GLOBALS["msg"]["entrant"]."】\n\n";
                $body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n【".$this->formdata["group2"]."】\n\n" : "\n\n";
            }else{
                $body2_title[1] = "\n\n[".$GLOBALS["msg"]["entrant"]."]\n\n";
                $body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n[".$this->formdata["group2"]."]\n\n" : "\n\n";
            }


            if(isset($obj->arrForm["edata31"]) && $obj->arrForm["edata31"] != "") {
                if(!isset($obj->arrForm["edata30"])) $obj->arrForm["edata30"] = 0;
                for($i = $obj->author_start_index; $i < $obj->arrForm["edata30"]+1; $i++) {
                    if(isset($obj->arrForm["group2_del"])) {
                        if(in_array($i, $obj->arrForm["group2_del"])) {
                            continue;
                        }
                    }

                    if($i == "0") {
                        $arrbody[$group] .= $body2_title[1];
                    } elseif($i == 1) {
                        $arrbody[$group] .= $body2_title[2];
                    }

                    foreach($obj->arrItemData[$group] as $_key => $data){
                        if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

                        $key = $data["item_id"];
                        $name = $data["strip_tags"];

                        if(!isset($obj->arrForm["edata".$key])){
                            $obj->arrForm["edata".$key] = "";
                        }

                        $methodname = "mailfunc".$key;
                        if(method_exists($this, $methodname)) {
                            $arrbody[$group] .= $obj->$methodname($name, $i);
                        } else {
                            if($data["controltype"] == "1") {
                                $arrbody[$group] .= $obj->mailfuncNini($key, $name, $i);    //任意
                            } else {
                                if(!isset($obj->arrForm["edata".$key.$i])) $obj->arrForm["edata".$key.$i] = "";
                                $arrbody[$group] .= $obj->point_mark.$name.": ".$obj->arrForm["edata".$key.$i]."\n";
                            }
                        }
                    }
                    $arrbody[$group] .= "\n";
                }
            }
        }
    }


    function makeBodyGroup3($obj, &$arrbody){
        // グループ3
        $group = 3;

        $arrbody[$group] = "";

        $preGroup3No = 0;
        foreach($obj->arrItemData[$group] as $_key => $data){
            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

            $group3no = $data["group3"];
            if($preGroup3No != $group3no){
                $preGroup3No = $group3no;

                // 言語別設定
                if(isset($obj->formdata["arrgroup3"][$preGroup3No-1])){
                    $format = "";
                    if($obj->formdata["lang"] == LANG_JPN){
                        $format = ($obj->formdata["arrgroup3"][$preGroup3No-1] != "") ? "\n\n【%s】\n\n" : "\n";
                    }
                    else{
                        $format = ($obj->formdata["arrgroup3"][$preGroup3No-1] != "") ? "\n\n[%s]\n\n" : "\n";
                    }
                    $arrbody[$group] .= sprintf($format, $obj->formdata["arrgroup3"][$preGroup3No-1]);
                }
            }


            $key = $data["item_id"];

            // 項目名（タグをとる)
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            if(!isset($obj->arrForm["edata".$key])){
                $obj->arrForm["edata".$key] = "";
            }

            $methodname = "mailfunc".$key;
            if(method_exists($this, $methodname)) {
                $arrbody[$group] .= $obj->$methodname($name);
            } else {
                if($data["controltype"] == "1") {
                    $arrbody[$group] .= $obj->mailfuncNini($key, $name);    //任意
                } else {
                    $arrbody[$group] .= $obj->point_mark.$name.": ".$obj->arrForm["edata".$key]."\n";
                }
            }
        }
    }


    /**
     * 会員・非会員
     */
    function mailfunc8($obj, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
        if(isset($obj->arrForm["edata8"]) && isset($obj->wa_kaiin[$obj->arrForm["edata8"]])){
            $str .=$obj->wa_kaiin[$obj->arrForm["edata8"]]."\n";
        }
        else{
            $str .= "\n";
        }
        
        return $str;
        
    }
    
    /**
     * 連絡先(勤務先・自宅)
     */
    function mailfunc16($obj, $name) {
        
        $str = $obj->point_mark.$name.": ";

        if(isset($obj->arrForm["edata16"])){
            $str .= $obj->wa_contact[$obj->arrForm["edata16"]]."\n";
        }
        else{
            $str .= "\n";
        }
        
        return $str;
        
    }
    
    /**
     * 都道府県
     */
    function mailfunc18($obj, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
        if($obj->formdata["lang"] == "2"){

            if(isset($obj->arrForm["edata18"])){
                $str .= $obj->arrForm["edata18"]."\n";
            }
            else{
                $str .= "\n";
            }
        }
        else{
            if(isset($obj->arrForm["edata18"]) && isset($GLOBALS["prefectureList"][$obj->arrForm["edata18"]])){
                $str .= $GLOBALS["prefectureList"][$obj->arrForm["edata18"]]."\n";
            }
            else{
                $str .= "\n";
            }
        }
        
        return $str;
        
    }
    
    /**
     * 共著者の有無
     */
    function mailfunc29($obj, $name) {
        
        return $obj->point_mark.$name.": ".$obj->wa_coAuthor[$obj->arrForm["edata29"]]."\n";
        
    }
    
    /*
     * 共著者の所属期間
     */
    function mailfunc31($obj, $name) {
        
        return $obj->point_mark.$name.":\n".$obj->arrForm["edata31"]."\n\n";
        
    }
    
    /**
     * 共著者　所属期間リスト
     */
    function mailfunc32($obj, $name, $i) {
        
        $str = $obj->point_mark.$name.": ";
        
        if(!isset($obj->arrForm["edata31"]) || $obj->arrForm["edata31"] == "") return $str."\n";
        
        $arrList = explode("\n", $obj->arrForm["edata31"]);
        
        foreach($arrList as $key=>$val) {
            
            $p_key = $key + 1;
            
            if(isset($obj->arrForm["edata32".$i.$p_key])) {
                if($obj->arrForm["edata32".$i.$p_key] == $p_key) {
                    $str .= trim($val)." ";
                }
            }
            
        }
        
        return $str."\n";

    }
    
    /**
     * 共著者・会員
     */
    function mailfunc41($obj, $name, $i) {
        
        if(!isset($obj->arrForm["edata41".$i]) || !isset($obj->wa_kaiin[$obj->arrForm["edata41".$i]])) {
            return $obj->point_mark.$name.": \n";
        }
        
        return $obj->point_mark.$name.": ".$obj->wa_kaiin[$obj->arrForm["edata41".$i]]."\n";
    }
    
    /**
     * 発表形式
     */
    function mailfunc48($obj, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
        if($obj->arrForm["edata48"] != "" || isset($obj->itemData[48]["select"][$obj->arrForm["edata48"]])){
            $str .= $obj->itemData[48]["select"][$obj->arrForm["edata48"]]."\n";
        }
        else{
            $str .= "\n";
        }
        
        return $str;
        
    }
    
    /**
     * 抄録
     */
    function mailfunc49($obj, $name) {
        
        return $obj->point_mark.$name.": \n".$obj->arrForm["edata49"]."\n\n";
    }
    
    /**
     * 抄録英語
     */
    function mailfunc50($obj, $name) {
        
        return $obj->point_mark.$name.": \n".$obj->arrForm["edata50"]."\n\n";
        
    }
    
    /**
     * ファイルアップロード
     */
    function mailfunc51($obj, $name) {
        return $obj->mailfuncFile(51, $name);
    }
    function mailfunc52($obj, $name) {
        return $obj->mailfuncFile(52, $name);
    }
    function mailfunc53($obj, $name) {
        return $obj->mailfuncFile(53, $name);
    }
    
    /**
     * Mr. Mrs.
     */
    function mailfunc57($obj, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
        if($obj->arrForm["edata57"] != ""){
            $str .=$GLOBALS["titleList"][$obj->arrForm["edata57"]]."\n";
        }
        else{
            $str .="\n";
        }
        
        return $str;
    }
    
    /**
     * 共著者 Mr. Mrs.
     */
    function mailfunc61($obj, $name, $i) {
        
        $str = $obj->point_mark.$name.": ";
        
        if($obj->arrForm["edata61".$i] != ""){
            $str .=$GLOBALS["titleList"][$obj->arrForm["edata61".$i]]."\n";
        }
        else{
            $str .="\n";
        }
        
        return $str;
        
    }
    
    /**
     * 国名　リストボックス
     */
    function mailfunc114($obj, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
            if(isset($obj->arrForm["edata114"]) && isset($GLOBALS["country"][$obj->arrForm["edata114"]])) {
                $str .= $GLOBALS["country"][$obj->arrForm["edata114"]]."\n";
            }
            else{
                $str .= "\n";
            }
        
        return $str;
        
    }
    
    function mailfuncFile($obj, $key, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
        $file_name = $obj->arrForm["hd_file".$key];

        if(isset($obj->arrForm["file_del".$key]) && $obj->arrForm["file_del".$key] == "1"){
            $file_name = "";
        }
        
        $str .= $file_name."\n";
        
        return $str;
        
    }
    
    /**
     * 任意
     */
    function mailfuncNini($obj, $key, $name, $i=null) {
        
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";
        
        //error_log("\n".$key." ".print_r($obj->itemData[$key], true), 3, LOG_DIR."test.log");
        
        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        
        $str.= "\n";
        
        return $str;
        
    }

}

?>

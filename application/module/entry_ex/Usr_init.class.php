<?php

class Usr_init {

    /* 任意項目のitem_type */
    const INPUT_TEXT   = 0;
    const INPUT_AREA   = 1;
    const INPUT_RADIO  = 2;
    const INPUT_CHECK  = 3;
    const INPUT_SELECT = 4;

    /* Usr_init::_initの添え字 */
    const INIT_EDATA  = 0;
    const INIT_NAME   = 1;
    const INIT_LEN    = 2;
    const INIT_CHECK  = 3;
    const INIT_OPTION = 4;
    const INIT_DBFLG  = 5;


    function _init($obj, $ps_block){
        /* 標準項目の中でtext,textarea以外の項目 */
        $obj->defaultItemSelect = array(
                1 => array(8, 16, 18, 57, 114, 29, 30)
                ,2 => array(32, 61, 41)
                ,3 => array(48)
        );
        
        // 項目キー
        $key = array();
        
        //---------------------------------
        // ブロック別処理
        //---------------------------------
        $initName = "_init".$ps_block;
        if(method_exists($obj, $initName)){
            $key = $obj->$initName();
        }
        return $key;
    }
    
    
    function _init1($obj){
        $block = 1;
        $key = array();
        
        foreach($obj->arrItemData[$block] as $item_id => $val) {
            if($val["disp"] == "1") continue;
            
            // 任意項目
            if($val['controltype'] == 1){
                $obj->_initNini($key, $item_id, $block);
            
            // 標準項目
            }else{
                $type = in_array($item_id, $obj->defaultItemSelect[$block]) ? 1 : 0;
                list($arrChk, $arrChklen) = $obj->_setCheckMethod($val["item_check"], $val["item_len"], $type);
                $key[] = array("edata".$item_id, $val['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
                // 確認用メールアドレス
                if($item_id == 25){
                    $key[] = array("chkemail", $obj->itemData[25]['strip_tags']."確認用", array(),    array(),    "",    0);
                }
            }
        }
        
        //認証用
        $key[] = array("agree", "認証", array(),	array(),	"",	0);
        
        return $key;
    }
    
    
    function _init2($obj) {
        $block = 2;
        $key = array();
        
        // グループ1の入力内容を取得
        $form_param = $GLOBALS["session"]->getVar("form_param1");
        // 共著者の所属
        $kaiinList = $obj->_makeListBox($form_param["edata31"]);
        // 共著者の数
        if(!isset($form_param["edata30"])) $form_param["edata30"] = "0";
        
        // 共著者の数分だけループ
        for($i = $obj->author_start_index; $i < $form_param["edata30"]+1; $i++) {
            foreach($obj->arrItemData[$block] as $item_id => $val) {
                if($val["disp"] == "1") continue;
                
                $name = $val["strip_tags"]."(".($i+1).")";
                
                // 任意項目
                if($val['controltype'] == 1){
                    $obj->_initChosyaNini($key, $item_id, $i, $name);
                
                // 標準項目
                }else{
                    switch($item_id) {
                        // 所属機関の場合
                        case "32":
                            if(!count($kaiinList) > 0) break;
                            $key[] = array("edata32".$i, $name, array(), array(), "", 0);
                            
                            foreach($kaiinList as $selectkey=>$selectval) {
                                $key[] = array("edata32".$i.$selectkey, $name, array(), array(), "", 0);
                            }
                            break;
                        
                        default:
                            $type = in_array($item_id, $obj->defaultItemSelect[2]) ? 1 : 0;
                            list($arrChk, $arrChklen) = $obj->_setCheckMethod($val["item_check"], $val["item_len"], $type);
                            $key[] = array("edata".$item_id.$i, $name, $arrChklen, $arrChk, "K", 1);
                            break;
                    }
                }
            }
        }
        
        // 削除フラグ
        $key[] = array("group2_del", "削除フラグ", array(), array(), "", 0);
        
        return $key;
    }
    
    
    function _init3($obj){
        $key      = array();
        $block = 3;
        
        foreach($obj->arrItemData[$block] as $_key => $val){
            if($val["disp"] == "1") continue;
            
            $item_id = $val['item_id'];
            
            // 任意項目
            if($val['controltype'] == 1){
                $obj->_initNini($key, $item_id, $block);
            
            // 標準項目
            }else{
                // ファイルアップロード
                if(in_array($item_id, array_keys($obj->arrfile))){
                    $key[] = array("edata".$item_id      , $val["strip_tags"]                  , array(), array(), "", 1);
                    $key[] = array("hd_file".$item_id    , $val["strip_tags"]."(アップロード済)"  , array(), array(), "", 0);
                    $key[] = array("n_data".$item_id     , $val["strip_tags"]."(登録済み)"       , array(), array(), "", 0);
                    $key[] = array("file_del".$item_id   , $val["strip_tags"]."(削除)"          , array(), array(), "", 0);
                    $key[] = array("file_upload".$item_id, $val["strip_tags"]                  , array(), array(), "", 0);
                    // その他の標準項目
                }else{
                    $type = in_array($item_id, $obj->defaultItemSelect[$block]) ? 1 : 0;
                    list($arrChk, $arrChklen) = $obj->_setCheckMethod($val["item_check"], $val["item_len"], $type);
                    $key[] = array("edata".$item_id, $val['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
                }
            }
        }
        
        return $key;
    }
    
    
    /**
     * 任意項目用のパラメータ初期設定
     *
     * @param  array   $key     パラメータ配列
     * @param  integer $item_id 任意項目の項目ID
     * @param  integer $block   項目IDに対応するグループID
     * @return void
     */
    function _initNini($obj, &$key, $item_id, $block){
        $data = $obj->arrItemData[$block][$item_id];
        $mode = "";
    
        //タイプによって異なる
        if($data["item_type"] == "2" || $data["item_type"] == "4"){
            $mode = "1";
        }
        //入力チェック項目
        list($arrChk, $arrChklen)  = $obj->_setCheckMethod($data["item_check"], $data["item_len"], $mode);
    
        if($data["item_type"] == "3"){
            //チェック・ラジオボタン・セレクトボックス
            if($data["select"] != ""){
                foreach($data["select"] as $num => $val){
                    $key[] = array("edata".$item_id.$num, $data['strip_tags'], array(),    array(),    "",    0);
                }
                // エラーメッセージの並び替え用に仕込んでおく
                // エラーチェック部はUsr_check._checkNiniによって実行される
                $key[] = array("edata".$item_id     , $data['strip_tags'], array(),    array(),    "",    0);
            }
    
        }else{
            $key[] = array("edata".$item_id, $data['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
        }
    }
    
    
    /**
     * パラメータ初期設定　共著者情報の任意項目
     */
    function _initChosyaNini($obj, &$key, $item_id, $i, $name){
        $block = 2;
        $data  = $obj->arrItemData[$block][$item_id];
        $mode  = "";
    
        //タイプによって異なる
        if($data["item_type"] == "2" || $data["item_type"] == "4"){
            $mode = "1";
        }
    
        //入力チェック項目
        list($arrChk, $arrChklen) = $obj->_setCheckMethod($data["item_check"], $data["item_len"], $mode);
    
        if($data["item_type"] == "3"){
            // チェックボックスの場合
            if(count($data["select"]) > 0){
                // エラーメッセージの並び替え用に仕込んでおく
                // エラーチェック部はUsr_check._checkNiniによって実行される
                $key[] = array("edata".$item_id.$i, $name, array(), array(), "", 0);
                foreach($data["select"] as $selectkey=>$selectval) {
                    $key[] = array("edata".$item_id.$i.$selectkey, $name, array(), array(), "", 0);
                }
            }
    
        }else{
            $key[] = array("edata".$item_id.$i, $name, $arrChklen, $arrChk, "K", 1);
        }
    }
    
    
    /**
     * 入力チェックの項目設定
     *
     * @access public
     * @param string パイプ区切りで登録されているチェックする項目
     * @param string 長さ
     * @param stirng モード　１：選択形式の項目 NULLの場合は入力形式の項目
     */
    function _setCheckMethod($obj, $ps_chk="", $pn_len="", $pn_type=""){
        //返却配列
        $ret_chk = array("TOKUSYU");
        $len = array();
        
        if($ps_chk == ""){
            return array($ret_chk, $len);
        }
        
        $arr_chk = explode("|", $ps_chk);
        foreach($arr_chk as $data){
            
            switch($data){
                case "0":
                    if($pn_type == "1"){
                        array_push($ret_chk, "SELECT");
                    }
                    else{
                        array_push($ret_chk, "NULL");
                    }
                    break;
                
                case "1":
                    array_push($ret_chk, "BYTE");
                    break;
                
                case "2":
                    array_push($ret_chk, "MOJICNT");
                    break;
                
                case "3":
                    array_push($ret_chk, "WORDCHK");
                    break;
                
                case "4":
                    array_push($ret_chk, "ZEN");
                    break;
                
                case "5":
                    array_push($ret_chk, "HAN");
                    break;
                
                case "6":
                    array_push($ret_chk, "MAIL");
                    break;
            }
        }
        
        if($pn_len != ""){
            array_push($len, 0);
            array_push($len,$pn_len);
        }
        
        return array($ret_chk, $len);
    }
    
    
    /* 項目情報を取得する */
    function getItemInfo($obj, $item_id, $getString="item_name"){
        return strip_tags($obj->itemData[$item_id][$getString]);
    }


    /* 任意項目のフォーム別のエラーメッセージを返す */
    function getItemErrMsg($obj, $item_id){
        $item_type = Usr_init::getItemInfo($obj, $item_id, "item_type");
        switch($item_type){
            case Usr_init::INPUT_TEXT:
            case Usr_init::INPUT_AREA:
                $method = $GLOBALS["msg"]["err_require_input"];
                break;
            case Usr_init::INPUT_RADIO:
            case Usr_init::INPUT_CHECK:
            case Usr_init::INPUT_SELECT:
                $method = $GLOBALS["msg"]["err_require_select"];
                break;
        }
        return $method;
    }


    /*
     * Usr_init::_initで取得したarray::$keyの添え字を取得する
     * 
     * @param  array  $key   Usr_init::_init[1-3]
     * @param  string $edata 取得対象のedata名 ex.edata26
     * @return int    $i     $keyの一次元の添え字
     */
    function getKey($key, $edata=""){
        $i = -1;
        if(strlen($edata) == 0) return $i;
        if(!is_array($key))     return $i;
        if(count($key) == 0)    return $i;

        foreach($key as $_i => $_key){
            if($edata !== $_key[0]) continue;

            $i = $_i;
            break;
        }
        return $i;
    }


    /* 
     * Usr_init::_initで取得したarray::$keyの添え字を取得する
     * ex.Usr_init::overrideKey($key, "edata28", Usr_init::INIT_OPTION, "nk");
     * 
     * @param  array  $key    Usr_init::_init[1-3]
     * @param  string $edata  取得対象のedata名 ex.edata26
     * @param  string $option 上書き対象の添え字
     * @param  mixed  $val    上書きする値
     * @return boolean
     */
    function overrideKey(&$key, $edata, $option, $val){
        $bool = false;
        if(strlen($edata) == 0) return $i;
        if(!is_array($key))     return $i;
        if(count($key) == 0)    return $i;

        $i = Usr_init::getKey($key ,$edata);
        $key[$i][$option] = $val;
    }


    /*
     * Usr_init::_initで取得したarray::$keyが有効か判定する
     * 
     * @param  object  $obj ページオブジェクト
     * @param  integer $group_id
     * @param  integer $item_id
     * @return boolean true:項目を利用している
     * 
     **/
    function isset_ex($obj, $group_id="", $item_id=""){
        if(strlen($item_id) == 0) return true;

        $key = 'edata'.$item_id;
        if($obj->arrItemData[$group_id][$item_id]['disp'] != 1){
            $item_type = Usr_init::getItemInfo($obj, $item_id, "item_type");
            switch($item_type){
                case Usr_init::INPUT_CHECK:
                    $select  = $obj->arrItemData[$group_id][$item_id]['select'];
                    if(count($select) > 0){
                        foreach($select as $i => $_value){
                            if(isset($obj->arrParam[$key.$i]) && strlen($obj->arrParam[$key.$i]) > 0){
                                return true;
                            }
                        }
                        return false;
                    }
                    return false;
                    break;

                // チェックしないとキーが渡ってこない
                case Usr_init::INPUT_RADIO:
                case Usr_init::INPUT_SELECT;
                    if(!isset($obj->arrParam[$key])) $obj->arrParam[$key] = NULL;
                    return true;
                    break;

                default:
                    return isset($obj->arrParam[$key]);
                    break;
            }
        }
        return false;
    }
}


?>
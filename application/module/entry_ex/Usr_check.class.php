<?php


/**
 * 論文エントリーフォーム
 *
 *
 * @subpackage Usr
 * @author salon
 *
 */
class Usr_Check {


    function _chkPeriod($obj) {
        return $obj->o_form->checkValidData_Ex();
    }


    /**
     * 一次応募期間中チェック
     * 
     * @return boolean
     */
    function _chkTerm($obj){
        $end = mktime(substr($obj->formdata["primary_reception_date"], 11,2),substr($obj->formdata["primary_reception_date"], 14,2),substr($obj->formdata["primary_reception_date"], 17,2),substr($obj->formdata["primary_reception_date"], 5,2), substr($obj->formdata["primary_reception_date"], 8,2), substr($obj->formdata["primary_reception_date"], 0,4));
        $today = time();

        //比較
        if($today < $end){
            return true;
        }
        return false;
    }


    /**
     * 二次応募期間中チェック
     * 
     * @return boolean
     */
    function _chkTerm2($obj){
        $key   = 'secondary_reception_date';
        $start = mktime(substr($obj->formdata[$key], 11,2),substr($obj->formdata[$key], 14,2),substr($obj->formdata[$key], 17,2),substr($obj->formdata[$key], 5,2), substr($obj->formdata[$key], 8,2), substr($obj->formdata[$key], 0,4));
        $today = time();

        //比較
        if($start < $today){
            return true;
        }
        return false;
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check($obj){
        $checkmethod = "_check".$obj->wk_block;

        if(method_exists($obj, $checkmethod)) {
            $obj->$checkmethod();
        }
        
        $obj->objErr->sortErr($obj->_init($obj->wk_block));
        return $obj->objErr->_err;
    }


    function _check1($obj) {
        // 共著者チェック
        $obj->_checkAuthor();
        
        // 管理者はここまで
        if($obj->admin_flg == "1") return;

//        //氏名、メールアドレス一致チェック 2012.6.13
//        if(!$obj->checkRepeat($obj->arrParam)){
//            $msg = ($obj->formdata["lang"] == LANG_JPN)
//                 ? "既にご登録済みです。"
//                 :"You are already registered.";
//            $obj->objErr->addErr($msg, "edata25");
//        }

        // 基本チェック
        $obj->objErr->check($obj->_init(1), $obj->arrParam);
        
        // 同意書
        if($obj->formdata["agree"] == "1"){
            if($obj->arrParam["agree"] == "" ) $obj->objErr->addErr($GLOBALS["msg"]["err_agree_check"], "agree");
        }
        
        //メールアドレス一致チェック
        if($obj->arrParam["edata25"] != "" && $obj->itemData[25]["mail"] == "1"){
            if($obj->arrParam["edata25"] != $_REQUEST["chkemail"]){
                $msg = sprintf($GLOBALS["msg"]["err_mail_match"], $obj->itemData[25]['strip_tags'], $obj->itemData[25]['strip_tags']);
                $obj->objErr->addErr($msg, "edata25");
            }
        }
        
        // 任意項目のチェック
        $obj->_checkNini("1");
        
        if($obj->formdata["lang"] == "2"){
            //国名プルダウンが必須の場合
            if($obj->itemData[114]["disp"] != "1"){
                if($obj->itemData[114]["need"] == "1" && $obj->arrParam["edata114"] == "239"){
                    if($obj->arrParam["edata60"] == ""){
                        $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[60]['strip_tags']);
                        $obj->objErr->addErr($msg, "edata60");
                    }
                }

            }
            else{
                if($obj->itemData[60]["disp"] != "1" && $obj->itemData[60]["need"] == "1"){
                    if($obj->arrParam["edata60"] == ""){
                        $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[60]['strip_tags']);
                        $obj->objErr->addErr($msg, "edata60");
                    }
                }
            }
        }
        
        return;
        
    }


    function _checkAuthor($obj) {
        
        if($obj->itemData[29]["disp"] == "1") return;
        
        if(!isset($obj->arrParam["edata29"])) $obj->arrParam["edata29"] = "";
        if($obj->arrParam["edata29"] == "") {
            
            $msg = sprintf($GLOBALS["msg"]["err_require_select"], $obj->itemData[29]['strip_tags']);
            $obj->objErr->addErr($msg, "edata29");
            return;
            
        }

        //共著ありの場合
        if($obj->arrParam["edata29"] == "2") return;

        //共著者の数
        if($obj->itemData[30]["disp"] != "1"){
            
            if(!isset($obj->arrParam["edata30"])) $obj->arrParam["edata30"] = "";
            if($obj->arrParam["edata30"] == ""){
                $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[30]['strip_tags']);
                $obj->objErr->addErr($msg, "edata30");
            }
            
            
            //半角数字で入力されていない場合
            if(!$obj->objErr->isNumeric($obj->arrParam["edata30"])){
                $msg = sprintf($GLOBALS["msg"]["err_han_numeric"], $obj->itemData[30]['strip_tags']);
                $obj->objErr->addErr($msg, "edata30");
            }

            // 15人より大きい場合
            if($obj->arrParam["edata30"] > $obj->getAuthorMax()){
                $msg = sprintf($GLOBALS["msg"]["err_over_max_author"], $obj->itemData[30]['strip_tags'], $obj->getAuthorMax());
                $obj->objErr->addErr($msg, "edata30");
            }
            
        }

        //共著者　所属機関
        if($obj->itemData[32]["disp"] != "1"){
            if($obj->arrParam["edata31"] == ""){
                $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[31]['strip_tags']);
                $obj->objErr->addErr($msg, "edata31");
            }
        }
        
        return;
        
    }


    function _check2($obj) {
        
        // 管理者はしない
        if($obj->admin_flg == "1") return;
        
        $form1data = $GLOBALS["session"]->getVar("form_param1");
        
        //if(!isset($form1data["edata30"]) || !$form1data["edata30"] > 0) return;
                
        // 基本チェック
        $obj->objErr->check($obj->_init(2), $obj->arrParam);
        
        $group2 = ($obj->o_form->formData['group2'] == "") 
                ? "共著者"
                : $obj->o_form->formData['group2'];
                
        if(!isset($form1data["edata30"])) $form1data["edata30"] = "0";
        
        //共著者の数
        for($i = $this->author_start_index; $i < $form1data["edata30"]+1; $i++) {

            //----------------------------
            //共著者所属機関のチェック
            //----------------------------
            if($obj->itemData[32]["need"] == "1"){

                $ok =0;

                $wa_kikanlist = $obj->_makeListBox($form1data["edata31"]);

                foreach($wa_kikanlist as $key => $val){
                    $wk_val = isset($obj->arrParam["edata32".$i.$key]) ? $obj->arrParam["edata32".$i.$key] : "";
                    if($wk_val != "")$ok++;
                }
                
                if($ok == 0){
                    $msg = sprintf($GLOBALS["msg"]["err_require_select"], $obj->itemData[32]['strip_tags']."(".($i + 1).")");
                    $obj->objErr->addErr($msg, "edata32".$i);
                }
            }

            //任意項目
            $obj->_checkChosyaNini($i);
            
            // 削除フラグが立っている時は、エラーを消す
            if(isset($obj->arrParam["group2_del"])) {
                if(in_array($i, $obj->arrParam["group2_del"])) {
                    foreach($obj->objErr->_err as $key=>$val) {
                        $i_len = "-".strlen($i);
                        if(substr($key, $i_len) == $i) {
                            unset($obj->objErr->_err[$key]);
                        }
                        
                    }
                }
            }
        }
        
        
        
        return;
        
    }


    // 3ページ目のチェック
    function _check3($obj) {
        
        // 管理者はしない
        if($obj->admin_flg != "1") $obj->objErr->check($obj->_init(3), $obj->arrParam);
        
        // ファイルのチェック/アップロード
        $obj->_checkfile();
        
        // 任意項目のチェック
        if($obj->admin_flg != "1") $obj->_checkNini("3");
        
        return;
        
    }


    /**
     * 任意項目のチェック
     *
     */
    function _checkNini($obj, $group){
        
        if(!count($obj->option_item[$group]) > 0) return;
        
        foreach($obj->option_item[$group] as $i) {
            
            if($obj->itemData[$i]["disp"] != "1" && $obj->itemData[$i]["need"] == "1"){
                //チェックボックスの時
                if($obj->itemData[$i]["item_type"] == "3"){
                    $ok =0;
                    foreach($obj->itemData[$i]["select"] as $key => $val){
                        $wk_val = isset($obj->arrParam["edata".$i.$key]) ? $obj->arrParam["edata".$i.$key]: "";
                        if($wk_val != "") $ok++;
                    }

                    if($ok == 0){
                        $msg = sprintf($GLOBALS["msg"]["err_require_select"], $obj->itemData[$i]['strip_tags']);

                        if($obj->formdata["lang"] == "2"){
                            $msg = sprintf($GLOBALS["msg"]["err_require_check"], $obj->itemData[$i]['strip_tags']);
                        }
                        $obj->objErr->addErr($msg, "edata".$i);
                    }
                }
            }
            
        }
        
        return;

    }

    /**
     * 共著者　任意項目のチェック
     */
    function _checkChosyaNini($obj, $i){

        $group2 = ($obj->o_form->formData['group2'] == "") 
                ? "共著者"
                : $obj->o_form->formData['group2'];
                
        $num = $i+1;
                
        foreach($obj->option_item[2] as $item_id) {
            
            if($obj->itemData[$item_id]["disp"] != "1" && $obj->itemData[$item_id]["need"] == "1") {
                
                // チェックボックスじゃなければ次へ
                if($obj->itemData[$item_id]["item_type"] != "3") continue;
                
                $ok =0;
                
                foreach($obj->itemData[$item_id]["select"] as $key => $val){
                    $wk_val = isset($obj->arrParam["edata".$item_id.$i.$key]) ? $obj->arrParam["edata".$item_id.$i.$key] : "";
                    if($wk_val != "") $ok++;
                }

                if($ok == 0){
                    $msg = sprintf($GLOBALS["msg"]["err_require_select"], $obj->itemData[$item_id]['strip_tags']."(".$num.")");
                    $obj->objErr->addErr($msg, "edata".$item_id.$i);
                }
            }
            
        }
        
        return;

    }

    /**
     * メールアドレス、氏名重複チェック
     *
     */
    function checkRepeat($obj, $pa_formparam){
        if($pa_formparam["edata25"] == "" || $pa_formparam["edata1"] == "" || $pa_formparam["edata2"] == ""){
            return true;
        }

        $column = "*";
        $from = "entory_r";
        $where[] = "form_id = ".$obj->o_form->formData["form_id"];
        $where[] = "del_flg = 0";

        //メールアドレス
        $where[] = "edata25 = '".$pa_formparam["edata25"]."'";


        $where[] = "edata1 = '".$pa_formparam["edata1"]."'";
        $where[] = "edata2 = '".$pa_formparam["edata2"]."'";



        //更新の場合
        if($obj->eid != ""){
            $where[] = "eid <> ".$obj->eid;
        }


        $rs = $obj->db->getData($column, $from, $where, __FILE__, __LINE__);
        if(!$rs){
            return true;

        }

        return false;


    }

    /**
     * アップロードファイルチェック
     *
     * @param int 項目開始番号
     * @param int 項目終了番号
     * @param string mode
     */
    function _checkfile($obj){
        
        foreach($obj->arrfile as $key=>$val) {
            
            if($obj->itemData[$key]["disp"] == "1") continue;
            
            // ファイルがアップロードされている場合1
            $upload = 0;
            if(isset($_FILES["edata".$key]) && $_FILES["edata".$key]["size"] != "0") {
                $upload = 1;
            }
            
            // 必須チェック
            if($obj->itemData[$key]["need"] == "1" && $upload == "0" && $obj->admin_flg != "1") {
                
                $wk_file_del = isset($obj->arrParam["file_del".$key]) ? $obj->arrParam["file_del".$key]: 0;
                $wk_hd_file = isset($obj->arrParam["hd_file".$key]) ? $obj->arrParam["hd_file".$key] : "";
                $wk_n_data = isset($obj->arrParam["n_data".$key]) ? $obj->arrParam["n_data".$key] : "";
                
                if(($wk_n_data == "" && $wk_hd_file == "") || $wk_file_del == "1") {
                    $msg = sprintf($GLOBALS["msg"]["err_file_null"], $obj->itemData[$key]['strip_tags']);
                    $obj->objErr->addErr($msg, "edata".$key);
                }
                
            }
            
            if($upload == "0") continue;
            
            // サイズチェック
            $pos = strstr($obj->itemData[$key]["item_check"],"1");
            if($pos !== false){
                if($obj->itemData[$key]["item_len"] < $_FILES["edata".$key]["size"]){
                    $msg = sprintf($GLOBALS["msg"]["err_over_max_filesize"], $obj->itemData[$key]['strip_tags'], $obj->itemData[$key]["item_len"]);
                    $obj->objErr->addErr($msg, "edata".$key);
                }
            }

            // 拡張子チェック
            if(strlen($obj->itemData[$key]['item_select']) > 0){
                $arrExt = Usr_function::gf_to_array_unique_trim($obj->itemData[$key]['item_select']);
                if(!Validate::checkExt($_FILES['edata'.$key], $arrExt)){
                    $strExt = implode(", ", $arrExt);

                    $msg = ($obj->formdata["lang"] == LANG_JPN)
                         ? $obj->itemData[$key]['strip_tags']."の拡張子は「".$strExt."」でアップロードしてください。"
                         : "Please upload ". $obj->itemData[$key]['strip_tags']." in ". preg_replace("/,(.*?)$/u", " or $1", $strExt) ." file."
                         ;

                    $obj->objErr->addErr($msg, "edata".$key);
                }
            }
        }
        return;

    }

}

?>
<?php


class Usr_initial {

    /**
     * メンテナンスを表示するフォームの場合は、メンテナンス画面を表示する
     */
    function showMenteform($obj) {
        
        if(count($obj->mente_form) > 0) {
            if(in_array($obj->form_id, $obj->mente_form)) {
                Error::showErrorPage("只今メンテナンス中です<br />");
                exit;
            }
        }
        
        return;
    }

    /**
     * アップロードディレクトリがない場合は作成
     */
    function createUploadDir($obj) {
        
        $obj->uploadDir = UPLOAD_PATH."Usr/form".$obj->form_id."/";    //ファイルアップロードディレクトリ

        if(!is_dir($obj->uploadDir)){
            mkdir($obj->uploadDir, 0777, true);
            chmod($obj->uploadDir, 0777);
        }
        
        return;
        
    }


    /**
     * 管理者モードか一般モードか判断し、各処理を行う
     */
    function AuthAction($obj) {
        
        //------------------------------------
        //一般利用者モード
        //------------------------------------
        if(!isset($_REQUEST["admin_flg"])) $_REQUEST["admin_flg"] = "";
        $obj->admin_flg = $_REQUEST["admin_flg"] != "" ? "1" : "";
        
        if($obj->admin_flg == ""){

            //-------------------------------
            //エントリー可能期間チェック
            //-------------------------------
            $obj->_chkPeriod();

            //------------------------
            //新規　or 更新判別
            //------------------------
            //ユーザのセッション情報があるか？
            if(isset($GLOBALS["entryData"]["eid"])){
                $obj->eid = $GLOBALS["entryData"]["eid"];
                $obj->assign("va_userdata", $GLOBALS["entryData"]);    //ユーザのデータ
            }
            else{
                $obj->eid = "";
            }

            //----------------------------------------------
            //ログイン認証
            //----------------------------------------------
            if(isset($_REQUEST["eid"]) && (trim($_REQUEST["eid"]) != "")){

                //ログインチェック
                if (!$GLOBALS["session"]->getVar("isLoginEntry")) {
                    $msg = ($obj->o_form->formData["lang"] == "1") ? "ログインしていません。" : "Not logged in";
                    //Error::showErrorPage($msg);
                    
                    $GLOBALS["session"]->setVar("startmsg", $msg);
                    // ログイン画面へリダイレクト
                    $url = URL_ROOT."Usr/Usr_login.php?form_id=".$obj->o_form->formData["form_id"];
                    header("Location: ".$url);
                    exit;
                }
            }
        }
        //------------------------------------
        //管理者モード
        //------------------------------------
        else{
            // ログインチェック
            $url = SSL_URL_ROOT."Usr/Usr_login.php?form_id=".$obj->form_id;
            LoginMember::checkLoginRidirect($url, false);

            $obj->eid = isset($_GET["eid"]) ? $_GET["eid"] : "";
        }

        $obj->assign("eid", $obj->eid);        //エントリーID
        
        return;
        
    }


    /**
     * フォーム基本情報の取得・アサイン
     */
    function setFormData($obj) {
        
        //---------------------------------
        //フォーム基本情報取得
        //---------------------------------
        $obj->formdata = $obj->o_form->formData;
        
        if(!$obj->formdata){
            Error::showErrorPage("フォーム基本情報の取得に失敗しました。");
        }

        // グループ3の見出し
        if($obj->formdata["group3"] != "") {
            $obj->formdata["arrgroup3"] = explode("|", $obj->formdata["group3"]);
        }

        $obj->assign("formData", $obj->formdata);        //画面表示項目
        
        return;
        
    }


    /**
     * フォーム項目の取得及び、整形
     */
    function setFormIni($obj) {
        
        //----------------------------------
        //フォームの項目名称取得
        //----------------------------------
        $obj->ini = $obj->o_itemini->getList(1, $obj->formdata["lang"]);
        if(!$obj->ini){
            Error::showErrorPage("フォームの項目の取得に失敗しました。");
        }

        //---------------------------------
        //フォーム項目情報取得
        //---------------------------------
        $obj->formitem = $obj->o_entry->getFormItem($obj->db, $obj->form_id);
        if(!$obj->formitem){
            Error::showErrorPage("フォーム項目情報の取得に失敗しました。");
        }
        foreach($obj->formitem as $data){
            $wk_item[$data["item_id"]] = $data;
        }

        // 任意項目のitem_idの配列
        $obj->option_item = array("1"=>array(), "2"=>array(), "3"=>array());

        // グループ(ブロック別)のitemData
        $obj->arrItemData = array("1"=>array(), "2"=>array(), "3"=>array());

        // グループ3のグループ分け
        $group3 = array("0" => array(46, 47, 48, 49, 50)    // 論文情報
                      , "1" => array(51, 52, 53)            // ファイルアップロード 
                      , "2" => array());                    // 任意項目

        // グループ3の見出しを表示するフラグ
        $group3use = array("1"=>false, "2"=>false, "3"=>false);

        //項目情報生成
        foreach($obj->ini as $i_key => $data){
            $item_id = $data["item_id"];

            $obj->itemData[$i_key] = $wk_item[$i_key];
            
            $obj->itemData[$i_key]["group_id"] = $data["group_id"];
            $obj->itemData[$i_key]["controltype"] = $data["controltype"];
            $obj->itemData[$i_key]["disp"] = ($wk_item[$i_key]["item_view"] == "1") ? "1" : "";
            $obj->itemData[$item_id]["need"] = "";
            $obj->itemData[$i_key]["select"] = "";
            $obj->itemData[$item_id]["mail"] = "";

            // おまじない
            if(!isset($obj->itemData[$item_id]['item_id'])){
                $obj->itemData[$item_id]['item_id'] = $item_id;
            }

            //画面に表示する項目の名称
            $obj->itemData[$i_key]["item_name"] = ($wk_item[$i_key]["item_name"] != "") ? $wk_item[$i_key]["item_name"] : $data["defname"];
            $obj->itemData[$i_key]["strip_tags"] = strip_tags($obj->itemData[$i_key]["item_name"]);

            //アブストラクトの場合
            if($obj->formdata["type"] == "1"){

                // 一次期間中
                if($obj->_chkTerm() === true){
                    if($wk_item[$i_key]["item_pview"] == "1"){
                        $obj->itemData[$i_key]["disp"] = "1";
                    }
                }

                // 二次期間中 TODO admin_flg時は無視
                if($obj->_chkTerm2() === true && $obj->admin_flg == ""){
                    // 2次非表示
                    if($wk_item[$i_key]["item_pview2"] == "1"){
                        $obj->itemData[$i_key]["disp"] = "1";
                    }
                    // 2次編集不可
                    if($wk_item[$i_key]["item_pview3"] == "1"){
                        $obj->itemData[$i_key]["disp"] = "2";
                    }
                }
            }

            //必須有無
            $pos = strstr($wk_item[$i_key]["item_check"],"0");
            if($pos !== false){
                $obj->itemData[$i_key]["need"] = "1";
            }

            //メールアドレスに関するチェック
            $pos = strstr($wk_item[$i_key]["item_check"],"6");
            if($pos !== false){
                $obj->itemData[$i_key]["mail"] = "1";
            }

            //チェック・ラジオボタン・セレクトボックス
            if($wk_item[$i_key]["item_select"] != ""){
                $wk_select  = explode("\n", $wk_item[$i_key]["item_select"]);
                $i =  "";
                $set_select = array();
                foreach($wk_select as $num => $val){
                    $i = $num+1;

                    //非活性の対象か否か
                    $pos = strpos($val, "#");
                    if($pos !== false){
                        $set_disable[$i] = "1";
                    }
                    else{
                        $set_disable[$i] = "";
                    }

                    //$set_select[$i] = $val;
                    $set_select[$i] = str_replace(array("#"), "", $val);;
                }
                $obj->itemData[$i_key]["select"] = $set_select;
                $obj->itemData[$i_key]["disable"] = $set_disable;
            }

            //発表形式
            if($i_key == "48"){
                //表示する場合
                if($wk_item[48]["item_view"] != "1"){

                    $wk_h_keisiki = explode("\n", $wk_item[48]["item_select"]);
                    $i =  "";
                    $set_select = array();
                    foreach($wk_h_keisiki as $num => $val){
                        $i = $num+1;
                        $set_select[$i] = $val;
                    }
                    $obj->itemData[$i_key]["select"] = $set_select;

                }

            }
            
            // 利用する任意項目を格納
            if($obj->itemData[$i_key]["disp"] != "1" && $data["controltype"] == "1") {
                $obj->option_item[$data["group_id"]][] = $i_key;
                $group3[2][$item_id] = $item_id;
            }

            $obj->arrItemData[$data["group_id"]][$i_key] = $obj->itemData[$i_key];
            // グループ3は見出しを表示するため詳細に分ける
            if($data["group_id"] == 3){
                $item_id = $obj->itemData[$i_key]['item_id'];
                if(in_array($item_id, $group3[0])){
                    $obj->arrItemData[$data["group_id"]][$i_key]['group3'] = 1;
                    $obj->arrItemData[$data["group_id"]][$i_key]['is_use'] = 1;

                }elseif(in_array($item_id, $group3[1])){
                    $obj->arrItemData[$data["group_id"]][$i_key]['group3'] = 2;

                }else{
                    $obj->arrItemData[$data["group_id"]][$i_key]['group3'] = 3;
                }

                $group3_x = $obj->arrItemData[$data["group_id"]][$i_key]['group3'];
                if(!$group3use[$group3_x]){
                    $group3use[$group3_x] = ($obj->itemData[$i_key]["disp"] != "1")
                                          ? true
                                          : false;
                }


            }
        }
        $obj->group3use  = $group3use;
        $obj->group3item = $group3;
        return;
    }


    /**
     * 共通アサインクラス
     */
    function commonAssign($obj) {
        
        //---------------------------------
        // 各種文言情報取得
        //---------------------------------
        $obj->formWord =  $obj->o_form->getFormWord($obj->form_id);
        $obj->assign("formWord",$obj->formWord);
        
        //---------------------------------
        // ヘッダー画像
        //---------------------------------
        $ws_header_img = ($obj->formdata["head_image"] != "") ? IMG_URL."header/".$obj->formdata["head_image"]: "";
        $obj->assign("va_header", $ws_header_img);        //ヘッダー
        
        //--------------------------------------------
        //定数情報取得
        //--------------------------------------------
        //都道府県
        if($obj->formdata["lang"] == "1"){
            $obj->assign("va_ken",$GLOBALS["prefectureList"]);
        }

        //日本語
        if($obj->formdata["lang"] == "1"){
            $obj->wa_contact = $GLOBALS["contact_J"];        //連絡先
            $obj->wa_kaiin = $GLOBALS["member_J"];            //会員・非会員
            $obj->wa_coAuthor = $GLOBALS["coAuthor_J"];    //共著者の有無

        }
        else{

            $obj->wa_contact = $GLOBALS["contact_E"];    //連絡先
            $obj->wa_kaiin = $GLOBALS["member_E"];
            $obj->wa_coAuthor = $GLOBALS["coAuthor_E"];

        }


        // フォーム別のカスタムcss,jsの読み込み
        $form_id = $obj->form_id;
        Usr_initial::readCustomfiles($obj, $form_id);

        // jQuery読み込みフラグ
        $obj->assign('useJquery',   $obj->useJquery);

        $obj->assign("va_contact",$obj->wa_contact);
        $obj->assign("va_kaiin",$obj->wa_kaiin);
        $obj->assign("va_coAuthor",$obj->wa_coAuthor);

        
        return;
        
    }
    
    
    // フォーム別のカスタムcss,jsの読み込み
    static function readCustomfiles($obj, $form_id){
        // フォーム別のカスタマイズ用CSS
        $isCustomCss = false;
        $css = 'css/'.$form_id.'.css';
        $baseDir   = 'customDir/'.$form_id."/";
        $customDir = ROOT_DIR.$baseDir;
        $ex = glob($customDir.$css);
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                $isCustomCss = true;
                break;
            }
        }
        $obj->assign("is_custom_css", $isCustomCss);
        $obj->assign("customCss",     APP_ROOT.$baseDir.$css);

        // フォーム別のカスタマイズ用JS
        $isCustomJs = false;
        $js = 'js/'.$form_id.'.js';
        $baseDir   = 'customDir/'.$form_id."/";
        $customDir = ROOT_DIR.$baseDir;
        $ex = glob($customDir.$js);
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                $isCustomJs = true;
                break;
            }
        }
        $obj->assign("is_custom_js", $isCustomJs);
        $obj->assign("customJs",     APP_ROOT.$baseDir.$js);
    }


    function setTemplate($obj) {
        $includeDir = TEMPLATES_DIR."Usr/include/".$obj->form_id."/";
        
        //------------------------
        // 表示HTMLの設定
        //------------------------
        if($obj->formdata["lang"] == "1"){
            $obj->_processTemplate = "Usr/form/Usr_entry_j.html";
            $obj->_title = $obj->formdata["form_name"];
        }
        else{
            $obj->_processTemplate = "Usr/form/Usr_entry_e.html";
            $obj->_title = $obj->formdata["form_name"];
        }
        

        // 同意文の外部テンプレート
        $tmpfile = $obj->form_id."agree.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("agree_inc_flg", 1);
            $obj->assign("tpl_agree", $tmpfile);
        }

        $path = TEMPLATES_DIR."Usr/include/";
        for($i = 1; $i < 4; $i++) {
            
            $tmpfile = $obj->form_id."block".$i.".html";
            if(file_exists($path.$tmpfile)) {
                $obj->assign("block".$i."_inc_flg", 1);
                $obj->assign("tpl_block".$i."_name", $tmpfile);
            }
            $tmpfile = $obj->form_id."block".$i."_confirm.html";
            if(file_exists($path.$tmpfile)) {
                $obj->assign("block".$i."_confirm_inc_flg", 1);
                $obj->assign("tpl_block".$i."_confirm_name", $tmpfile);
            }
            
        }

        $obj->assign("includeDir", $includeDir);
        return;
    }

}

?>

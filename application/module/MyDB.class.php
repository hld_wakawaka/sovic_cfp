<?php
require_once("DB.php");


define("PEAR_LOG_DEBUG", "");

/**
 * MyDBクラス
 *
 * データベースクラス
 *
 **/
class MyDB{

	// データベース接続DSN
	var $_dsn;
	// データベースコネクション
	var $_con;
	// 直近に実行したSQL
	var $_sql;

	/**
	 * コンストラクタ
	 * @access	public
	 * @param	string	$dsn	データベース接続DSN
	 */
	function MyDB($dsn = DB_DSN) {
		$this->_dsn = $dsn;
	}
 
	 

	/**
	 * インスタンスを得る
	 * @access	public
	 * @param	string	$dsn	データベース接続DSN
	 * @return reference of instance
	 */
	function &getInstance($dsn = DB_DSN) {

		//DB接続をstaticスコープで実装 使い回す
		static $db;

		if(!isset($db)){
			$db = new MyDB($dsn);
			$db->connect();

		}

		return $db;

	}

	/**
	 * データベースへの接続を行う
	 * @access	public
	 * @param	boolean	$pConnect	持続接続時はtrueを設定
	 */
	function connect($pConnect = false){

		$pConnect = array(
			'debug' => 2,
			'portability' => DB_PORTABILITY_ALL,
		);
		$this->_con = DB::connect($this->_dsn, $pConnect);
		
		// エラーチェック
		if (DB::isError($this->_con)) { 
			$this->error($this->_con->getMessage());
		}
		

	}

	/**
	 * データベースの切断処理を行う
	 * @access	public
	 */
	function disconnect(){

		if (DB::isConnection($this->_con)) {
			$this->_con->disconnect();
		}

	}

	/**
	 * トランザクションを開始する
	 * @access	public
	 */
	function begin(){
		$this->_con->autoCommit(false);
	}

	/**
	 * トランザクションを確定する
	 * @access	public
	 */
	function commit(){
		$this->_con->commit();
	}

	/**
	 * トランザクションを取り消す
	 * @access	public
	 */
	function rollback(){
		$this->_con->rollback();
	}

	/**
	 * クエリーを実行する
	 * @access	public
	 * @param	string	$sql	SQL文字列
	 * @param	mixed	$param	パラメータ配列
	 * @param	string	$file	呼び出し元ファイル名
	 * @param	string	$line	呼び出し元行数
	 * @return	mixed	DB_result
	 */
	function query($sql, $param = null, $file = "", $line = ""){

		// ログの出力
		/*
		if (isset($GLOBALS["log"])) {
			$GLOBALS["log"]->write_log_DB($sql." ".PEAR_LOG_DEBUG);
		}
		*/

		$this->_sql = $sql;

		if (is_null($param)) {
			$res = $this->_con->query($sql);
		} else {
			$res = $this->_con->query($sql, $param);
		}

		// エラーチェック
		if (DB::isError($res)){
			$this->error($res->getMessage().":".$sql, $file, $line);
		}

		return $res;

	}

	/**
	 * 更新クエリーを実行する
	 * @access	public
	 * @param	string	$sql	SQL文字列
	 * @param	mixed	$param	パラメータ配列
	 * @param	string	$file	呼び出し元ファイル名
	 * @param	string	$line	呼び出し元行数
	 * @return	integer	影響を受けた件数
	 */
	function simpleQuery($sql, $param = null, $file = "", $line = ""){

		// ログの出力
		if (isset($GLOBALS["log"])) {
			$GLOBALS["log"]->write_log_DB($sql." ".PEAR_LOG_DEBUG);
		}

		$this->_sql = $sql;

		if (is_null($param)) {
			$res = $this->_con->simpleQuery($sql);
		} else {
			$res = $this->_con->simpleQuery($sql, $param);
		}

		// エラーチェック
		if (DB::isError($res)){
			$this->error($res->getMessage().":".$sql, $file, $line);
		}

		return $this->_con->affectedRows();

	}

	/**
	 * クエリーを実行する（結果が一つの場合）
	 * @access	public
	 * @param	string	$sql	SQL文字列
	 * @param	mixed	$param	パラメータ配列
	 * @param	string	$file	呼び出し元ファイル名
	 * @param	string	$line	呼び出し元行数
	 * @return	string	結果文字列
	 */
	function getOne($sql, $param = null, $file = "", $line = ""){

		// ログの出力
		if (isset($GLOBALS["log"])) {
			$GLOBALS["log"]->write_log_DB($sql." ".PEAR_LOG_DEBUG);
		}

		$this->_sql = $sql;

		if (is_null($param)) {
			$res = $this->_con->getOne($sql);
		} else {
			$res = $this->_con->getOne($sql, $param);
		}
		

		// エラーチェック
		if (DB::isError($res)){
			$this->error($res->getMessage().":".$sql, $file, $line);
		}

		return $res;

	}

	/**
	 * リテラルとして安全に使用できるように入力内容を整形する
	 * @access	public
	 * @param	string	$param	文字列
	 * @return	string	整形文字列
	 */
	function quoteSmart($param){

		return $this->_con->quoteSmart($param);

	}

	/**
	 * 現在の DBMS の規則に基づいて文字列をエスケープする
	 * @access	public
	 * @param	string	$param	文字列
	 * @return	string	整形文字列
	 */
	function escapeSimple($param){

		return $this->_con->escapeSimple($param);

	}

	/**
	 * エラー処理を行う
	 * @access	public
	 * @param	string	$errMsg	エラーメッセージ
	 * @param	string	$file	呼び出し元ファイル名
	 * @param	string	$line	呼び出し元行数
	 */
	function error($errMsg, $file = "", $line = ""){

		if ($file != "") {
			$errMsg .= ":FILE=".$file;
		}
		if ($line != "") {
			$errMsg .= ":LINE=".$line;
		}

		//$errMsg = mb_convert_encoding($errMsg, "UTF-8");

		// ログの出力
		if (isset($GLOBALS["log"])) {
			$GLOBALS["log"]->write_log_DB($errMsg." ".PEAR_LOG_DEBUG);
		}

		//$errMsg = mb_convert_encoding($errMsg, "UTF-8", "SJIS");

		if (DEBUG) {
			Error::showSystemError($GLOBALS["msg"]["err_db_system"]."<br>".$errMsg);
		} else {
			Error::showSystemError($GLOBALS["msg"]["err_db_system"]);
		}

		exit;

	}
}
?>

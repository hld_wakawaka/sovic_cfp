<?php

class item_ini {
	
	var $db;
	var $cnt;

    function item_ini() {

    	$this->db = new DbGeneral();
    	
    	$this->cnt = $this->db->_getOne("max(item_id)", "item_ini", null, __FILE__, __LINE__);
    	if(!$this->cnt) $this->cnt = 0;
     	
    }
    
    function getDefName($formtype=0, $lang=1) {
    	
    	switch($formtype) {
    		case "1":
    		case "2":
    			$where[] = "form_type = 0";
    			break;
    		case "3":
    			$where[] = "form_type = 0";
    			break;
    		default:
    			return;
    	}
    	
    	$where[] = "lang = ".$this->db->quote($lang);
    	$orderby = "group_id, sort";
    	$rs = $this->db->getListData("item_id, defname", "item_ini", $where, $orderby, -1, -1, __FILE__, __LINE__);
    	
    	if(!$rs) return false;
    	
    	foreach($rs as $val) {
    		$name[$val["item_id"]] = $val["defname"];
    	}
    	
    	for($i = 1; $i <= $this->cnt; $i++) {
    		if(isset($name[$i])) {
    			$buffer[$i] = $name[$i];
    		} else {
    			$buffer[$i] = "";
    		}
    	}
    	
    	return $buffer;
    	
    }
    
    function getList($formtype=0, $lang=1) {
    	
    	switch($formtype) {
    		case "1":
    		case "2":
    			$where[] = "form_type = 0";
    		case "3":
    			$where[] = "form_type = 0";
    			break;
    		default:
    			return;
    	}
    	
    	$where[] = "lang = ".$this->db->quote($lang);
    	$orderby = "group_id, sort";
    	$rs = $this->db->getListData("*", "item_ini", $where, $orderby, -1, -1, __FILE__, __LINE__);
    	
    	if(!$rs) return false;
    	
    	foreach($rs as $val) {
    		$buffer[$val["item_id"]] = $val;
    	}
    	
    	return $buffer;
    	
    }
}
?>
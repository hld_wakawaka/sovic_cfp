<?php

/**
 * 会議関連メソッド
 * 
 * @package 論文フォーム
 * @subpackage module
 * @author salon
 * 
 */
class Meeting {
	
	var $db;

	/**
	 * コンストラクタ
	 */
    function Meeting() {
    	$this->db = new DbGeneral;
    }

	/**
	 * 会議一覧データ取得
	 * 　　複数会議情報を取得する
	 * 
	 * @access public
	 */
	function getMeetingList($page = 1, $limit){
		
   		$column = "*";
   		$from = "meeting";
    	$where = "form_id = 1";
    	$where .= " and del_flg = 0";
    	
    	$orderby = "open_date desc, mid";
    	
    	$offset = $limit * ($page - 1);
    	
    	//--------------------------
    	// 検索条件付与
    	//--------------------------
//    	$ret_where = $this->_mekeListWhere();
//
//    	$where .= $ret_where;
    	
    	
    	
		$rs = $this->db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
    	
    	if(!$rs) {
    		return false;
    	}
    	
    	return $rs; 		
		
		
	}	 
 

	/**
	 * 会議一覧　where文生成
	 * 
	 */
	function _mekeListWhere(){
		
     	$where = "";
     	
    	// セッションに保存した検索条件を取得
    	$search_key = $GLOBALS["session"]->getVar("form_param");     	
 
    	
     	if($search_key != "") {
  	
  			//会議名が指定されている場合
  			if($search_key["sk_name"] != ""){
  	
  				$search_key["sk_name"] = htmlspecialchars($search_key["sk_name"], ENT_QUOTES);
  				
  				$where .= " AND title LIKE ".$this->db->quote("%".$search_key["sk_name"]."%");
  			}
  			
 			
 			//開催日が指定されている場合
 			if($search_key["sk_oyear"] != "" && $search_key["sk_omonth"] != "" && $search_key["sk_oday"] != ""){
 				$wk_sdate = $search_key["sk_oyear"].$search_key["sk_omonth"].$search_key["sk_oday"];
 				
 				$where .= " AND (to_date(to_char(open_date,'yyyymmdd'),'yyyymmdd')  > to_date(".$this->db->quote($wk_sdate).",'yyyymmdd')";
 			}


 			//開始日が指定されている場合
 			if($search_key["sk_syear"] != "" && $search_key["sk_smonth"] != "" && $search_key["sk_sday"] != ""){
 				$wk_sdate = $search_key["sk_syear"].$search_key["sk_smonth"].$search_key["sk_sday"];
 				
 				$where .= " AND (to_date(to_char(reception_date1,'yyyymmdd'),'yyyymmdd')  > to_date(".$this->db->quote($wk_sdate).",'yyyymmdd')";
 			}

 			
 			//終了日が指定されている場合
  			if($search_key["sk_eyear"] != ""  && $search_key["sk_emonth"] != "" && $search_key["sk_eday"] > 0){
 				$wk_edate = $search_key["sk_eyear"].$search_key["sk_emonth"].$search_key["sk_eday"];
 				
 				$where .= " AND ( to_date(to_char(reception_date2,'yyyymmdd'),'yyyymmdd')  < to_date(".$this->db->quote($wk_edate).",'yyyymmdd')";
 			}			
 			
    	}

		return $where; 		
		
	}	 


	/**
	 * 会議情報取得
	 * 	指定したIDの会議情報を取得する
	 * 
	 * @access public
	 * @param int 会議ID
	 * @return mix
	 * 
	 */    
    function getMeeting($mid) {
    	
    	if(!$mid > 0) return;
    	
    	$where[] = "mid = ".$this->db->quote($mid);
    	$where[] = "del_flg = 0";
    	
    	$rs = $this->db->getData("*", "meeting", $where, __FILE__, __LINE__);
    	
    	return $rs;
    	
    }
    
    
    
	/**
	 * 会議応募者情報取得
	 * 	応募者一覧表示データを取得する
	 * 
	 * @access public
	 * @param datatype paramname
	 * @return mix description
	 */
	function getAppliesList($ps_mid	, $page = 1, $limit){
		
   		$column = "*";
   		$from = "applies";
    	$where = "mid = ".$this->db->quote($ps_mid);
    	$where .= " and del_flg = 0";
 
     	// セッションに保存した検索条件を取得
    	$search_key = $GLOBALS["session"]->getVar("applies_param"); 
    	
		if($search_key != ""){
			
  			if($search_key["sk_user_name"] != ""){
  	
  				$search_key["sk_user_name"] = htmlspecialchars($search_key["sk_user_name"], ENT_QUOTES);
  				
  				$where .= " AND m_name LIKE ".$this->db->quote("%".$search_key["sk_user_name"]."%");
  			}			
			
		}
 
    	
    	$orderby = "aid";
    	
    	$offset = $limit * ($page - 1);	

		$rs = $this->db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
    	
    	if(!$rs) {
    		return false;
    	}
    	
    	return $rs; 
    	
	} 
	    
}
?>
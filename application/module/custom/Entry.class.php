<?php

/**
 * 論文エントリーメソッド
 *
 * @package 論文フォーム
 * @subpackage module
 * @author salon
 *
 */
class Entry {


	//for update でレコードロックを実行するためDBオブジェクトはパラメータで渡す

	/**
	 * コンストラクタ
	 */
    function Entry() {

    }

	/**
	 * 論文フォーム　項目情報取得
	 * 　　
	 *
	 * @access public
	 * @param int form_id
	 */
    function getFormItem($po_db, $form_id) {

    	$column = "*";
    	$from = "form_item";
    	$where[] = "form_id = ".$po_db->quote($form_id);
    	$orderby = "item_id";

		$rs = $po_db->getListData($column, $from, $where, $orderby, -1, -1, __FILE__, __LINE__);

    	if(!$rs) {
    		return false;
    	}

    	return $rs;

    }

	/**
	 * 論文フォーム　エントリー情報取得
	 * 　　
	 *
	 * @access public
	 * @param int eid
	 */
	function getRntry_r($po_db, $pn_eid, $form_id){

    	$column = "*";
    	$from = "entory_r";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "form_id = ".$po_db->quote($form_id);
    	$where[] = "del_flg = 0";

    	$rs = $po_db->getData($column, $from, $where, __FILE__, __LINE__);

    	return $rs;
	}



	/**
	 * 論文フォーム　エントリー情報(共著者)取得
	 * 　　
	 *
	 * @access public
	 * @param int eid
	 */
	function getRntry_aff($po_db, $pn_eid){

    	$column = "*";
    	$from = "entory_aff";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "del_flg = 0";
    	$orderby = "id";

		$rs = $po_db->getListData($column, $from, $where, $orderby);

    	return $rs;
	}


    /**
     * 採番テーブルのレコード作成
     *
     * @access public
     * @param  object  $po_db
     * @param  integer $formId
     * @return boolean レコード作成の有無
     */
    function registEntryNumber($po_db, $form_id) {
        $id = $po_db->_getOne("id", "entory_number", "form_id = ".$form_id, __FILE__, __LINE__);
        if(!is_null($id) || strlen($id) > 0) return true;

        $param = array();
        $param["form_id"]      = $form_id;
        $param["entory_count"] = "0";

        $query = $po_db->insert("entory_number", $param, __FILE__, __LINE__);
        if(!$query) {
            $po_db->rollback();
            $this->complete("フォームの初期設定に失敗しました。");
            return false;
        }

        return true;
    }


    /**
     * エントリー番号　最終番号取得
     *
     * @access public
     * @param  object  $po_db
     * @param  integer $formId
     * @return integer $count
     */
    function getEntryNum($po_db, $form_id){

        $column = "*";
        $from   = "entory_number";
        $where  = array();
        $where[]= "form_id = ".$po_db->quote($form_id)." FOR UPDATE";

        $count = $po_db->getData($column, $from, $where, __FILE__, __LINE__);
        if(!$count) {
            $this->registEntryNumber($po_db, $form_id);
            return 1;
        }

        return $count;
    }


 	/**
	 * エントリー番号採番
	 *
	 * @access public
	 * @param object DB
	 * @param int フォームID
	 */
	function updEntryNum($po_db, $form_id, $num){


		$param["entory_count"] = $num;

		$param["udate"] = "NOW";

		$where = "form_id = ".$po_db->quote($form_id);


		$rs = $po_db->update("entory_number", $param, $where, __FILE__, __LINE__);

		if(!$rs) {
			return false;
		}

		return true;

	}


	/**
	 *　パスワード自動生成
	 *
	 * @access public
	 * @param int 生成するパスワードの長さ
	 * @param string パスワードで使用する文字列の組み合わせタイプ
	 *
	 */
	function makePasswd($length , $mode = 'alnum'){

		if ($length < 1 || $length > 256) {
			return false;
		}

		$password = "";

		$smallAlphabet = 'abcdefghijkmnopqrstuvwxyz';
		$largeAlphabet = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
		$numeric       = '23456789';

		switch ($mode){
			//----------------
			// 小文字英字
			//----------------
			case 'small':
			    $chars = $smallAlphabet;
			    break;

			//-----------------
			// 大文字英字
			//-----------------
			case 'large':
			    $chars = $largeAlphabet;
			    break;

			//-----------------
			// 小文字英数字
			//-----------------
			case 'smallalnum':
			    $chars = $smallAlphabet . $numeric;
			    break;

			//-----------------
			// 大文字英数字
			//-----------------
			case 'largealnum':
			    $chars = $largeAlphabet . $numeric;
			    break;

			//-----------------
			// 数字
			//-----------------
			case 'num':
			    $chars = $numeric;
			    break;

			//-------------------
			// 大小文字英字
			//-------------------
			case 'alphabet':
			    $chars = $smallAlphabet . $largeAlphabet;
			    break;

			//--------------------
			// 大小文字英数字
			//--------------------
			case 'alnum':
			default:
			    $chars = $smallAlphabet . $largeAlphabet . $numeric;
			    break;
		}

		$charsLength = strlen($chars);


		for ($i = 0; $i < $length; $i++) {
		    $num = mt_rand(0, $charsLength - 1);
		    $password .= $chars{$num};
		}

		return $password;

	}

	function getListEntry($form_id, $condition=array(), $page=1, $limit=ROW_LIMIT) {

		//フォーム頭文字の長さ
		$len_formhead = strlen($GLOBALS["userData"]["head"])  + 1;

		$from = $this->getTable($GLOBALS["userData"]["type"]);

		$condition["user_name"]	= isset($condition["user_name"])	? $condition["user_name"]	: "";
		$condition["status1"]	= isset($condition["status1"])		? $condition["status1"]		: "";
		$condition["status2"]	= isset($condition["status2"])		? $condition["status2"]		: "";
		$condition["status3"]	= isset($condition["status3"])		? $condition["status3"]		: "";
		$condition["status4"]	= isset($condition["status4"])		? $condition["status4"]		: "";
    	$condition["syear"] 	= isset($condition["syear"])		? $condition["syear"]		: "";
    	$condition["smonth"]	= isset($condition["smonth"])		? $condition["smonth"]		: "";
    	$condition["sday"]		= isset($condition["sday"])			? $condition["sday"]		: "";
    	$condition["eyear"]		= isset($condition["eyear"])		? $condition["eyear"]		: "";
    	$condition["emonth"]	= isset($condition["emonth"])		? $condition["emonth"]		: "";
    	$condition["eday"]		= isset($condition["eday"])			? $condition["eday"]		: "";
    	$condition["payment_method"]	= isset($condition["payment_method"])	? $condition["payment_method"]	: "";
    	$condition["payment_status"]	= isset($condition["payment_status"])	? $condition["payment_status"]	: "";

		$condition["s_entry_no"]	= isset($condition["s_entry_no"])	? $condition["s_entry_no"]	: "";
		$condition["e_entry_no"]	= isset($condition["e_entry_no"])	? $condition["e_entry_no"]	: "";
		$condition["country"]	= isset($condition["country"])	? $condition["country"]	: "";
    	$condition["upd_syear"] 	= isset($condition["upd_syear"])		? $condition["upd_syear"]		: "";
    	$condition["upd_smonth"]	= isset($condition["upd_smonth"])		? $condition["upd_smonth"]		: "";
    	$condition["upd_sday"]		= isset($condition["upd_sday"])			? $condition["upd_sday"]		: "";
    	$condition["upd_eyear"]		= isset($condition["upd_eyear"])		? $condition["upd_eyear"]		: "";
    	$condition["upd_emonth"]	= isset($condition["upd_emonth"])		? $condition["upd_emonth"]		: "";
    	$condition["upd_eday"]		= isset($condition["upd_eday"])			? $condition["upd_eday"]		: "";

		$condition["user_name_kana"]	= isset($condition["user_name_kana"])	? $condition["user_name_kana"]	: "";
        $condition["country_list"]   = isset($condition["country_list"])  ? $condition["country_list"] : "";



		$condition["sort_name"]	= isset($condition["sort_name"])	? $condition["sort_name"]	: "";
		$condition["sort"]	= isset($condition["sort"])	? $condition["sort"]	: "";

		$db = new DbGeneral;

		$where[] = "del_flg = 0";

		$where[] = "form_id = ".$GLOBALS["userData"]["form_id"];

		//　応募者
		if($condition["user_name"] != "") {

            $condition["user_name"] = mb_convert_kana($condition["user_name"], "naKV", "utf8");
            $condition["user_name"] = strtolower($condition["user_name"]);


			$arrval = GeneralFnc::customExplode($condition["user_name"]);

			if(count($arrval) > 0) {

				foreach($arrval as $val) {


                    $subwhere[] = "(lower(translate(edata1 ||edata2 ,'－０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ', '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')) LIKE ".$db->quote("%".$val."%").") or (lower(translate(edata7 ,'－０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ', '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')) LIKE ".$db->quote("%".$val."%").")";

					//$subwhere[] = "edata1 like '%".$val."%'";

				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}

		// ステータス
		if($condition["status1"] == "1") $subwhere[] = "status = 1";
		if($condition["status2"] == "2") $subwhere[] = "status = 0";
		if($condition["status3"] == "3") $subwhere[] = "status = 2";
		if($condition["status4"] == "4") $subwhere[] = "status = 99";

		if(isset($subwhere)) {
			$where[] = "(".implode(" or ", $subwhere).")";
			unset($subwhere);
		}

		// 受付期間
		if($condition["syear"] > 0) {
			if($condition["smonth"] == "") $condition["smonth"] = "1";
			if($condition["sday"] == "") $condition["sday"] = "1";
			$sdate = sprintf("%04d", $condition["syear"])."-".sprintf("%02d", $condition["smonth"])."-".sprintf("%02d", $condition["sday"]);
			$where[] = "rdate >= ".$db->quote($sdate);
		}
		if($condition["eyear"] > 0) {
			if($condition["emonth"] == "") $condition["emonth"] = "12";
			if($condition["eday"] == "") {
				$edate = date("Y-m-d", mktime(0, 0, 0, $condition["emonth"]+1, 0, $condition["eyear"]));
			} else {
				$edate = sprintf("%04d", $condition["eyear"])."-".sprintf("%02d", $condition["emonth"])."-".sprintf("%02d", $condition["eday"]);
			}

			$where[] = "rdate <= ".$db->quote($edate)."'";
		}

		// 支払方法
		if(is_array($condition["payment_method"])) {
			if(count($condition["payment_method"]) > 0) {
				foreach($condition["payment_method"] as $val) {
					$subwhere[] = "payment_method = ".$db->quote($val);
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}
		// 支払ステータス
		if(is_array($condition["payment_status"])) {
			if(count($condition["payment_status"]) > 0) {
				foreach($condition["payment_status"] as $val) {
					$subwhere[] = "payment_status = ".$db->quote($val);
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}
		}

		//登録No 開始
		if($condition["s_entry_no"] != "") {

            $condition["s_entry_no"] = mb_convert_kana($condition["s_entry_no"], "naKV", "utf8");
            $condition["s_entry_no"] = strtolower($condition["s_entry_no"]);

			//桁数を0埋め５桁にする
			$s_entry_no = sprintf("%05d", $condition["s_entry_no"]);
			//$where[]  = "substr(e_user_id, ".$len_formhead.") >= '".$s_entry_no."'";

			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) >= ".$db->quote($s_entry_no);
		}

		//登録No 終了
		if($condition["e_entry_no"] != "") {

            $condition["e_entry_no"] = mb_convert_kana($condition["e_entry_no"], "naKV", "utf8");
            $condition["e_entry_no"] = strtolower($condition["e_entry_no"]);

			//桁数を0埋め５桁にする
			$e_entry_no = sprintf("%05d", $condition["e_entry_no"]);
			//$where[]  = "substr(e_user_id, ".$len_formhead.") <= '".$e_entry_no."'";

			$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) <= ".$db->quote($e_entry_no);

		}

		//国名
		if($condition["country"] != "") {
			//$where[] = "edata60 like '%".$condition["country"]."%'";

            $condition["country"] = strtolower($condition["country"]);
            $where[] = "(lower(translate(edata60 ,'－０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ', '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')) LIKE ".$db->quote("%".$condition["country"]."%").")";
		}

        //country
        if($condition["country_list"] != "") {
            $where[] = "edata114 = '".$condition["country_list"]."'";
        }

		// 受付期間
		if($condition["upd_syear"] > 0) {
			if($condition["upd_smonth"] == "") $condition["upd_smonth"] = "1";
			if($condition["upd_sday"] == "") $condition["upd_sday"] = "1";
			$upd_sdate = sprintf("%04d", $condition["upd_syear"])."-".sprintf("%02d", $condition["upd_smonth"])."-".sprintf("%02d", $condition["upd_sday"]);
			$where[] = "udate >= ".$db->quote($upd_sdate);
		}
		if($condition["upd_eyear"] > 0) {
			if($condition["upd_emonth"] == "") $condition["upd_emonth"] = "12";
			if($condition["upd_eday"] == "") {
				$upd_edate = date("Y-m-d", mktime(0, 0, 0, $condition["upd_emonth"]+1, 0, $condition["upd_eyear"]));
			} else {
				$upd_edate = sprintf("%04d", $condition["upd_eyear"])."-".sprintf("%02d", $condition["upd_emonth"])."-".sprintf("%02d", $condition["upd_eday"]);
			}

			$where[] = "udate <= ".$db->quote($upd_edate);
		}

		//カナ
		if($condition["user_name_kana"] != "") {
			$arrval = GeneralFnc::customExplode($condition["user_name_kana"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata3 like ".$db->quote("%".$val,"%");
					$subwhere[] = "edata4 like ".$db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}



		$count = $db->_getOne("COUNT(eid)", $from, $where, __FILE__, __LINE__);

		if(!$count) {
			return array("0", "");
		}


		//------------------------------------
		//表示順
		//------------------------------------
		$wk_order_by = "udate desc";

		if($condition["sort_name"] != "" && $condition["sort"] != ""){
			$wk_order_by  = "";
			if($condition["sort_name"] == "2"){
				$wk_order_by .= "entry_no ";
			}
			else{
				$wk_order_by .= "udate ";
			}

			if($condition["sort"] == "1"){
				$wk_order_by .= "desc";
			}
			else{
				$wk_order_by .= "asc";
			}
		}

		$orderby = $wk_order_by;
		$offset = $limit * ($page - 1);



//		$rs = $db->getListData("*, replace(substr(e_user_id, ".$len_formhead."),'-','') as entry_no", $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);

		$rs = $db->getListData("*, substr(e_user_id, length(e_user_id)-4 ,5) as entry_no", $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);


		if(!$rs) return array($count, "");

		return array($count, $rs);

	}

	/**
	 * 決済情報取得
	 * 	指定したエントリーの決済情報を取得する
	 */
	function getAuthorize($po_db, $pn_eid){

    	$column = "*";
    	$from = "payment";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "del_flg = 0";

    	$rs = $po_db->getData($column, $from, $where, __FILE__, __LINE__);

    	return $rs;


	}

	/**
	 * 支払詳細項目取得
	 */
	function getPaymentDetail($po_db, $pn_eid, $ps_type=""){


    	$from = "payment_detail";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "del_flg = 0";

    	if($ps_type != ""){
    		$where[] = "type = ".$ps_type;
    	}


		$orderby = "detail_id, type";

		$rs = $po_db->getListData("*", $from, $where, $orderby, -1, -1, __FILE__, __LINE__);

		return $rs;



	}

	function getListFromId($arrId=array()) {

		if(!count($arrId) > 0) return;

		$from = $this->getTable($GLOBALS["userData"]["type"]);

		$db = new DbGeneral;

		$where[] = "eid in (".implode(",", $arrId).")";
		$where[] = "del_flg = 0";
		$where[] = "form_id = ".$db->quote($GLOBALS["userData"]["form_id"]);
		$orderby = "udate desc";

		$rs = $db->getListData("*", $from, $where, $orderby, -1, -1, __FILE__, __LINE__);

		return $rs;

	}

	function getTable($type) {

		switch($type) {
			case "1":
			case "2":
			case "3":
				return "v_entry";
				break;
			default:
				return "";
				break;

		}
		return "v_entry";
	}
}
?>
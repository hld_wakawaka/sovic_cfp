<?php

class User {
	
	var $db;

    function User() {
    	
    	$this->db = new DbGeneral;
    }
    
    function get($user_id) {
    	
    	if(!$user_id > 0) return;
    	
    	$where[] = "user_id = ".$this->db->quote($user_id);
    	$where[] = "del_flg = 0";
    	
    	$rs = $this->db->getData("*", "users", $where, __FILE__, __LINE__);
    	
    	return $rs;
    	
    }
    
    function getList($condition, $page=1, $limit=ROW_LIMIT) {
    	
    	$column = "*";
    	$from = "users";
    	$where[] = "del_flg = 0";
    	
    	if($condition["sk_name"] != "") {
    		$condition["sk_name"] = str_replace("　", " ", trim($condition["sk_name"]));
    		$arrkey = explode(" ", $condition["sk_name"]);
    		foreach($arrkey as $val) {
    			$where[] = "user_name like ".$this->db->quote("%".$val."%");
    		}
    	}
    	
    	$offset = $limit * ($page - 1);
    	
    	$orderby = "user_id";
    	
    	$rs = $this->db->getListData("*", $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
    	
    	if(!$rs) {
    		$buffer["allrow"] = 0;
    	} else {
    		$buffer["list"] = $rs;
    		$buffer["allrow"] = $this->db->_rows;
    		$buffer["row"] = $this->db->_rowCount;
    	}
    	
    	return $buffer;
    	
    	
    }
}
?>
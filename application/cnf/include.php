<?php
/**
 * システム設定ファイル
 *
 * @author salon
 *
 */

// アプリケーション基底パス
$baseDir = realpath(dirname(__FILE__) . '/../../../');

//インクルードパスの設定
set_include_path("/Users/kawauchi/workspace4/sovic_cfp/application/library");

//-----------------------------------------
// 実行環境
//-----------------------------------------
define("ENV_TYPE1", "local");
define("ENV_TYPE2", "dev");
define("ENV_TYPE3", "evt");

// 実行環境を設定
// if(strpos(__FILE__, "testhp.jp") !== false) {
//     define("ENV", ENV_TYPE2);
// } elseif(strpos(__FILE__, "doishun") !== false) {
//     define("ENV", ENV_TYPE1);
// }else{
//     define("ENV", ENV_TYPE3);
// }
define("ENV", ENV_TYPE1);

// 実行環境別の設定の読み込み
if(ENV == ENV_TYPE1){  // ローカル環境
    define("APP_ROOT", "/sovic_cfp/");
    define("DOMAIN",   "http://localhost".APP_ROOT);
    define("DOMAINS",  "http://localhost".APP_ROOT);
    define("URL_ROOT",       "http://".$_SERVER["HTTP_HOST"].APP_ROOT);
    define("SSL_URL_ROOT",   "http://".$_SERVER["HTTP_HOST"].APP_ROOT);
    define("SSL_URL_CUSTOM", "http://".$_SERVER["HTTP_HOST"].APP_ROOT."customDir/");
    // DB定義
    define("DB_ENV_ONE"  , "cfp_dev");
    define("DB_ENV_OTHRE", "link_reg3_dev");
    define("DB_DSN", "pgsql://evt_dev:tp1DviZK@localhost/".DB_ENV_ONE);
}
if(ENV == ENV_TYPE2){   // テスト環境
    define("APP_ROOT", "/cfp/");
//    define("ROOT_DIR", "/var/www/vhosts/testhp.jp/httpdocs".APP_ROOT);
    define("URL_ROOT", "http://".$_SERVER["HTTP_HOST"].APP_ROOT);
    define("SSL_URL_ROOT", "http://".$_SERVER["HTTP_HOST"].APP_ROOT);
}
if(ENV == ENV_TYPE3){   // 本番環境
    define("APP_ROOT", "/cfp/");
//    define("ROOT_DIR", "/var/www/vhosts/evt-reg.jp/httpdocs".APP_ROOT);
    define("URL_ROOT", "https://".$_SERVER["HTTP_HOST"].APP_ROOT);
    define("SSL_URL_ROOT", "https://".$_SERVER["HTTP_HOST"].APP_ROOT);
}


//-----------------------------------------
// サイト名
//-----------------------------------------
define("APP_TITLE", "CFP");


//-----------------------------------------
// ルートディレクトリ（ファイルシステムでのルートディレクトリを絶対パスで指定）
//-----------------------------------------
define("ROOT_DIR", $baseDir.APP_ROOT); unset($baseDir);

//-------------------------------------------
// ライブラリディレクトリ（ファイルシステム上でのライブラリディレクトリ）
//-------------------------------------------
define("LIB_DIR", ROOT_DIR."application/library/");

//-------------------------------------------
// モジュールディレクトリ（モジュール）
//-------------------------------------------
define("MODULE_DIR", ROOT_DIR."application/module/");


// 結果
define('TXN_FAILURE_CODE', 'failure');
define('TXN_PENDING_CODE', 'pending');
define('TXN_SUCCESS_CODE', 'success');

//-------------------------------------------
// Smartyのテンプレートディレクトリ（メール本文）
//-------------------------------------------
define("TEMPLATES_DIR", ROOT_DIR."templates/");
// Smartyのコンパイルディレクトリ
define("COMPILE_DIR", ROOT_DIR."templates_c/");

//-----------------------------------------------------
// timezoneの設定
//-----------------------------------------------------
ini_set("date.timezone", "Asia/Tokyo");

//--------------------------------------------
// ログディレクトリ
//--------------------------------------------
define("LOG_DIR", ROOT_DIR."application/Log/");


//--------------------------------------------
// エラーメールの送信先
//--------------------------------------------
define("ERROR_MAIL", "info@salon.ne.jp");

//--------------------------------------------
// フォーム管理画面ログインページ
//--------------------------------------------
define("MNG_LOGIN_PAGE", SSL_URL_ROOT."Mng/login/");

//--------------------------------------------
// フォーム管理画面URL
//--------------------------------------------
define("MNG_URL", SSL_URL_ROOT."Mng/");

//--------------------------------------------
// 管理画面URL
//--------------------------------------------
define("ADMIN_URL", URL_ROOT."Sys/");

// エラーページURL、テンプレート
if(strpos($_SERVER["PHP_SELF"], "/Sys/") !== false) {
	$pagerank = "sys";
	define("TMPL_FILE_ERROR", "Sys/Sys_error.html");
	define("URL_MESSAGE_PAGE", URL_ROOT."Sys/error.php");
} elseif(strpos($_SERVER["PHP_SELF"], "/Mng/") !== false) {
	$pagerank = "mng";
	define("TMPL_FILE_ERROR", "Mng/Mng_error.html");
	define("URL_MESSAGE_PAGE", URL_ROOT."Mng/error.php");
} else {
	$pagerank = "usr";
	define("TMPL_FILE_ERROR", "Usr/Usr_error.html");
	define("URL_MESSAGE_PAGE", URL_ROOT."Usr/error.php");
}
define("PAGE_RANK", $pagerank);

// アップロードファイルディレクトリ
define("UPLOAD_PATH", ROOT_DIR."upload/");
// アップロードファイルURL
define("UPLOAD_URL", APP_ROOT."upload/");
// アップロードファイルURL フルパス
define("FULL_UPLOAD_URL", URL_ROOT."upload/");

//　画像ファイルディレクトリ
define("IMG_PATH", ROOT_DIR."htdocs/images/");
// 画像ファイルURL
define("IMG_URL", URL_ROOT."htdocs/images/");

//-----------------------------------------------------
// timezoneの設定
//-----------------------------------------------------
ini_set("date.timezone", "Asia/Tokyo");

//-----------------------------------------------------
// include_pathの再設定
//-----------------------------------------------------
ini_set("include_path", ini_get("include_path").":".LIB_DIR.":".TGMDK_ORG_DIR.":".TGMDK_COPY_DIR);
//ini_set("include_path", ini_get("include_path").":".LIB_DIR."pear/");

// デバックモードの設定
define("DEBUG", false);

// 表示するエラーの設定

if (DEBUG) {
	ini_set("error_reporting", E_ALL | ~E_NOTICE | ~E_STRICT);
	ini_set("display_errors", true);
    error_reporting(error_reporting() ^ E_STRICT);
} else {
	ini_set("error_reporting", E_ALL | ~E_NOTICE | ~E_WARNING | ~E_DEPRECATED | ~E_STRICT);
	ini_set("display_errors", false);
    error_reporting(error_reporting() ^ E_STRICT);
}

/**
 * PEAR ****************************
 */

require_once("PEAR.php");
require_once("DB.php");
require_once("Pager/Pager.php");

// モジュール別
require_once("constants.ini.php");
require_once(MODULE_DIR."Error.class.php");
require_once(MODULE_DIR."GeneralFnc.class.php");
require_once(MODULE_DIR."HSession.class.php");
require_once(MODULE_DIR."LoginMember.class.php");
require_once(MODULE_DIR."LoginAdmin.class.php");
require_once(MODULE_DIR."LoginEntry.class.php");
require_once(MODULE_DIR."MyDB.class.php");
require_once(MODULE_DIR."MySmarty.class.php");
require_once(MODULE_DIR."ProcessBase.class.php");
require_once(MODULE_DIR."Reload.class.php");
require_once(MODULE_DIR."SmartyForm.class.php");
require_once(MODULE_DIR."Validate.class.php");
require_once(MODULE_DIR."log.class.php");
//require_once("Upload.class.php");
//require_once("Download.class.php");


// セッションを開始
$GLOBALS["session"] = new HSession;

// DBクラス
require_once(MODULE_DIR."dbClass/DbGeneral.class.php");

// カスタムクラス
require_once(MODULE_DIR."custom/User.class.php");
require_once(MODULE_DIR."custom/Form.class.php");
require_once(MODULE_DIR."custom/item_ini.class.php");

// DB接続
$notConnectDb = false;
if (!$notConnectDb) {
	// DB接続処理
	$GLOBALS["db"] = &MyDB::getInstance();
}

// ログクラス
$GLOBALS["log"] = new log_class;

$GLOBALS["form"] = new Form;

if($pagerank == "usr" && $GLOBALS["form"]->isForm) {
	if($GLOBALS["form"]->formData["lang"] == "2") require_once("en.php");
}

// ログイン済みの場合には、ログイン情報をグローバル変数に保存する
if ($GLOBALS["session"]->getVar("isLoginEntry")) {
	$loginEntry = $GLOBALS["session"]->getVar("entryData");
	$GLOBALS["entryData"] = $loginEntry;
}

if ($GLOBALS["session"]->getVar("isLogin")) {
	$loginUser = $GLOBALS["session"]->getVar("userData");
	$GLOBALS["userData"] = $loginUser;
}

$GLOBALS["syslogin"] = 0;
if ($GLOBALS["session"]->getVar("isLoginAdmin")) {
	$adminData = $GLOBALS["session"]->getVar("adminData");
	$GLOBALS["adminData"] = $adminData;
	$GLOBALS["syslogin"] = 1;
}


?>

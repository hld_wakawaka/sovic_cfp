<?php
/**
 * システム設定ファイル
 *
 * @author salon
 *
 */


//-----------------------------------------
// サイト名
//-----------------------------------------
define("APP_TITLE", "");


//-----------------------------------------
// アプリケーションルート（WebのROOTディレクトリ）
//-----------------------------------------
define("APP_ROOT", "/cfp/");

//-----------------------------------------
// ルートディレクトリ（ファイルシステムでのルートディレクトリを絶対パスで指定）
//-----------------------------------------
if(strpos(__FILE__, "testhp.jp") !== false) {
	define("ROOT_DIR", "/var/www/vhosts/testhp.jp/httpdocs".APP_ROOT);
} else {
	define("ROOT_DIR", "/var/www/vhosts/evt-reg.jp/httpdocs".APP_ROOT);
}


//-----------------------------------------
// サイトURL
//-----------------------------------------
if(strpos(__FILE__, "testhp.jp") !== false) {
    define("URL_ROOT", "http://testhp.jp".APP_ROOT);
}
else{
	define("URL_ROOT", "https://evt-reg.jp".APP_ROOT);
}


//-------------------------------------------
// ライブラリディレクトリ（ファイルシステム上でのライブラリディレクトリ）
//-------------------------------------------
define("LIB_DIR", ROOT_DIR."application/library/");

//-------------------------------------------
// モジュールディレクトリ（モジュール）
//-------------------------------------------
define("MODULE_DIR", ROOT_DIR."application/module/");

//-------------------------------------------
// Smartyのテンプレートディレクトリ（メール本文）
//-------------------------------------------
define("TEMPLATES_DIR", ROOT_DIR."templates/");
// Smartyのコンパイルディレクトリ
define("COMPILE_DIR", ROOT_DIR."templates_c/");

//--------------------------------------------
// ログディレクトリ
//--------------------------------------------
define("LOG_DIR", ROOT_DIR."application/Log/");

//決済ログ
define("LOG_TGMDK_DIR", ROOT_DIR."application/Log/tgMdk/");

//--------------------------------------------
// エラーメールの送信先
//--------------------------------------------
define("ERROR_MAIL", "info@salon.ne.jp");




// アップロードファイルディレクトリ
define("UPLOAD_PATH", ROOT_DIR."upload/");
// アップロードファイルURL
define("UPLOAD_URL", APP_ROOT."upload/");

//　画像ファイルディレクトリ
define("IMG_PATH", ROOT_DIR."htdocs/images/");


//-----------------------------------------------------
// include_pathの再設定
//-----------------------------------------------------
ini_set("include_path", ini_get("include_path").":".LIB_DIR);
//ini_set("include_path", ini_get("include_path").":".LIB_DIR."pear/");

// デバックモードの設定
if (file_exists(LIB_DIR."DEBUG")) {
	define("DEBUG", true);
} else {
	define("DEBUG", false);
}

// 表示するエラーの設定

if (DEBUG) {
	ini_set("error_reporting", E_ALL ^ E_NOTICE);
	ini_set("display_errors", true);
} else {
	ini_set("error_reporting", E_ALL ^ E_NOTICE ^ E_WARNING);
	ini_set("display_errors", false);
}

/**
 * PEAR ****************************
 */

require_once("PEAR.php");
require_once("DB.php");
require_once("Pager/Pager.php");

// モジュール別
require_once("constants.ini.php");
require_once(MODULE_DIR."Error.class.php");
require_once(MODULE_DIR."GeneralFnc.class.php");
require_once(MODULE_DIR."HSession.class.php");
require_once(MODULE_DIR."LoginMember.class.php");
require_once(MODULE_DIR."LoginAdmin.class.php");
require_once(MODULE_DIR."LoginEntry.class.php");
require_once(MODULE_DIR."MyDB.class.php");
require_once(MODULE_DIR."MySmarty.class.php");
require_once(MODULE_DIR."ProcessBase.class.php");
require_once(MODULE_DIR."Reload.class.php");
require_once(MODULE_DIR."SmartyForm.class.php");
require_once(MODULE_DIR."Validate.class.php");
require_once(MODULE_DIR."log.class.php");
//require_once("Upload.class.php");
//require_once("Download.class.php");




// DBクラス
require_once(MODULE_DIR."dbClass/DbGeneral.class.php");

// カスタムクラス
require_once(MODULE_DIR."custom/User.class.php");
require_once(MODULE_DIR."custom/Form.class.php");
require_once(MODULE_DIR."custom/item_ini.class.php");

// DB接続
$notConnectDb = false;
if (!$notConnectDb) {


	// DB接続処理
	$GLOBALS["db"] = &MyDB::getInstance();
	//$GLOBALS["db"] = new Myadodb(DB_DSN);

}

// ログクラス
$GLOBALS["log"] = new log_class;

$GLOBALS["form"] = new Form;


ini_set("date.timezone", "Asia/Tokyo");


?>

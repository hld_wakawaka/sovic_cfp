<?php
/**
 * システム設定ファイル
 * 
 * @author salon
 * 
 */


//-----------------------------------------
// サイト名
//-----------------------------------------
define("APP_TITLE", "");


//-----------------------------------------
// アプリケーションルート（WebのROOTディレクトリ）
//-----------------------------------------
define("APP_ROOT", "/");

//-----------------------------------------
// ルートディレクトリ（ファイルシステムでのルートディレクトリを絶対パスで指定）
//-----------------------------------------
define("ROOT_DIR", "/home/httpd/vhosts/evt-reg.jp/httpsdocs".APP_ROOT);

//-----------------------------------------
// サイトURL
//-----------------------------------------
define("URL_ROOT", "http://".$_SERVER["HTTP_HOST"].APP_ROOT);

//-----------------------------------------
// SSL対応URL
//-----------------------------------------
define("SSL_URL_ROOT", "http://".$_SERVER["HTTP_HOST"].APP_ROOT);

//-------------------------------------------
// ライブラリディレクトリ（ファイルシステム上でのライブラリディレクトリ）
//-------------------------------------------
define("LIB_DIR", ROOT_DIR."application/library/");

//-------------------------------------------
// モジュールディレクトリ（モジュール）
//-------------------------------------------
define("MODULE_DIR", ROOT_DIR."application/module/");

//-------------------------------------------
// Smartyのテンプレートディレクトリ（メール本文）
//-------------------------------------------
define("TEMPLATES_DIR", ROOT_DIR."templates/");
// Smartyのコンパイルディレクトリ
define("COMPILE_DIR", ROOT_DIR."templates_c/");

//--------------------------------------------
// ログディレクトリ
//--------------------------------------------
define("LOG_DIR", ROOT_DIR."application/Log/");

//--------------------------------------------
// エラーメールの送信先
//--------------------------------------------
define("ERROR_MAIL", "info@salon.ne.jp");

//--------------------------------------------
// フォーム管理画面ログインページ
//--------------------------------------------
define("MNG_LOGIN_PAGE", SSL_URL_ROOT."Mng/login/");

//--------------------------------------------
// フォーム管理画面URL
//--------------------------------------------
define("MNG_URL", SSL_URL_ROOT."Mng/");

//--------------------------------------------
// 管理画面URL
//--------------------------------------------
define("ADMIN_URL", URL_ROOT."Sys/");

// エラーページURL、テンプレート
if(strpos($_SERVER["PHP_SELF"], "/Sys/") !== false) {
	$pagerank = "sys";
	define("TMPL_FILE_ERROR", "Sys/Sys_error.html");
	define("URL_MESSAGE_PAGE", URL_ROOT."Sys/error.php");
} elseif(strpos($_SERVER["PHP_SELF"], "/Mng/") !== false) {
	$pagerank = "mng";
	define("TMPL_FILE_ERROR", "Mng/Mng_error.html");
	define("URL_MESSAGE_PAGE", URL_ROOT."Mng/error.php");
} else {
	$pagerank = "usr";
	define("TMPL_FILE_ERROR", "Usr/Usr_error.html");
	define("URL_MESSAGE_PAGE", URL_ROOT."Usr/error.php");
}

// アップロードファイルディレクトリ
define("UPLOAD_PATH", ROOT_DIR."upload/");
// アップロードファイルURL
define("UPLOAD_URL", APP_ROOT."upload/");
// アップロードファイルURL フルパス
define("FULL_UPLOAD_URL", URL_ROOT."upload/");

//　画像ファイルディレクトリ
define("IMG_PATH", ROOT_DIR."htdocs/images/");
// 画像ファイルURL
define("IMG_URL", URL_ROOT."htdocs/images/");

//-----------------------------------------------------
// include_pathの再設定
//-----------------------------------------------------
ini_set("include_path", ini_get("include_path").":".LIB_DIR);


// デバックモードの設定

if (file_exists(LIB_DIR."DEBUG")) {
	define("DEBUG", true);	
} else {
	define("DEBUG", false);	
}

// 表示するエラーの設定

if (DEBUG) {
	ini_set("error_reporting", E_ALL ^ E_NOTICE);
	ini_set("display_errors", true);
} else {
	ini_set("error_reporting", E_ALL ^ E_NOTICE ^ E_WARNING);
	ini_set("display_errors", false);
}

/**
 * PEAR ****************************
 */

require_once("PEAR.php");
require_once("DB.php");
require_once("Pager/Pager.php");

// モジュール別
require_once("constants.ini.php");
require_once(MODULE_DIR."Error.class.php");
require_once(MODULE_DIR."GeneralFnc.class.php");
require_once(MODULE_DIR."HSession.class.php");
require_once(MODULE_DIR."LoginMember.class.php");
require_once(MODULE_DIR."LoginAdmin.class.php");
require_once(MODULE_DIR."LoginEntry.class.php");
require_once(MODULE_DIR."MyDB.class.php");
require_once(MODULE_DIR."MySmarty.class.php");
require_once(MODULE_DIR."ProcessBase.class.php");
require_once(MODULE_DIR."Reload.class.php");
require_once(MODULE_DIR."SmartyForm.class.php");
require_once(MODULE_DIR."Validate.class.php");
require_once(MODULE_DIR."log.class.php");
//require_once("Upload.class.php");
//require_once("Download.class.php");


// セッションを開始
$GLOBALS["session"] = new HSession;

// DBクラス
require_once(MODULE_DIR."dbClass/DbGeneral.class.php");

// カスタムクラス
require_once(MODULE_DIR."custom/User.class.php");
require_once(MODULE_DIR."custom/Form.class.php");
require_once(MODULE_DIR."custom/item_ini.class.php");

// DB接続
$notConnectDb = false;
if (!$notConnectDb) {
	// DB接続処理
	$GLOBALS["db"] = &MyDB::getInstance();
	//$GLOBALS["db"] = new Myadodb(DB_DSN);
}

// ログクラス
$GLOBALS["log"] = new log_class;

$GLOBALS["form"] = new Form;

if($pagerank == "usr" && $GLOBALS["form"]->isForm) {
	if($GLOBALS["form"]->formData["lang"] == "2") require_once("en.php");
}

// ログイン済みの場合には、ログイン情報をグローバル変数に保存する
if ($GLOBALS["session"]->getVar("isLoginEntry")) {
	$loginEntry = $GLOBALS["session"]->getVar("entryData");
	$GLOBALS["entryData"] = $loginEntry;
}

if ($GLOBALS["session"]->getVar("isLogin")) {
	$loginUser = $GLOBALS["session"]->getVar("userData");
	$GLOBALS["userData"] = $loginUser;
}

if ($GLOBALS["session"]->getVar("isLoginAdmin")) {
	$adminData = $GLOBALS["session"]->getVar("adminData");
	$GLOBALS["adminData"] = $adminData;
}


?>

<?php
include('../application/cnf/include.php');

class message extends ProcessBase {

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function message() {
		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 * @access	public
	 */
	function main() {

		
		// 表示HTMLの設定
		$this->_processTemplate = TMPL_FILE_ERROR;
		
		$errorParam = $GLOBALS["session"]->getVar("errorParam");

		$this->_smarty->assign("errorParam", $errorParam);

		// 親クラスに処理を任せる
		parent::main();
	}

}

/**
 * メイン処理開始
 **/

$c = new message();
$c->main();

?>
<?php

/**
 * システム管理者　フォーム登録・編集
 * 	フォームの基本情報部分入力画面表示
 * 
 * @subpackage Sys
 * @author salon
 *
 */
class sys_common {


	/**
	 * コンストラクタ
	 */
	function sys_common(){

	}
	
	function menu() {
		
		$su_menu[0] = array("menu_name" => "ユーザ管理", "prg" => APP_ROOT."Sys/index.php");
		$su_menu[1] = array("menu_name" => "パスワード変更", "prg" => APP_ROOT."Sys/account/edit.php");
		$su_menu[2] = array("menu_name" => "ログアウト", "prg" => APP_ROOT."Sys/login/index.php?mode=logout");
		
		return $su_menu;
	}
	
}

?>
<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');
include(MODULE_DIR.'custom/Entry.class.php');

/**
 * システム管理者　フォーム登録・編集
 * 	フォームの基本情報部分入力画面表示
 *
 * @subpackage Sys
 * @author salon
 *
 */
class formbase extends ProcessBase {

	var $objForm;


	/**
	 * コンストラクタ
	 */
	function formbase(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){

		LoginAdmin::checkLoginRidirect();

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");

		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//-----------------------
		//フォームパラメータ
		//-----------------------

		$formparam1 = GeneralFnc::convertParam($this->init(), $_REQUEST);
		$formparam2 = GeneralFnc::convertParam($this->init2(), $_REQUEST);
		$this->arrForm = array_merge($formparam1, $formparam2);





		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_formbase.html";
		$this->_title = "スーパー管理者ページ";

		//----------------------
		// オブジェクト生成
		//----------------------
		$this->objForm    = new Form;
		$this->objEntry   = new Entry();
		$this->objItemIni = new item_ini;
		$objUser = new User;

		//------------------------
		// 処理対象者
		//------------------------
		if($this->arrForm["user_id"] == "") {
			$this->complete("ユーザーが選択されていません。");
		}

		$user_data = $objUser->get($this->arrForm["user_id"]);
		$this->assign("user_data", $user_data);

		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$limityear = date("Y",strtotime("+2 year"));
		for($i = 2011; $i < $limityear; $i++) {
			$this->arrYear[$i] = $i;
		}

		for($i = 1; $i < 13; $i++) {
			$tmp_i = sprintf('%02d', $i);
			$this->arrMonth[$tmp_i] = $tmp_i;
		}

		for($i = 1; $i < 32; $i++) {
			$tmp_i = sprintf('%02d', $i);
			$this->arrDay[$tmp_i] = $tmp_i;
		}

		for($i = 0; $i < 24; $i++) {
			$tmp_i = sprintf('%02d', $i);
			$this->arrHour[$tmp_i] = $tmp_i;
		}

		$this->assign("syear", $this->arrYear);				//開始（年）
		$this->assign("smonth", $this->arrMonth);			//開始（月）
		$this->assign("sday", $this->arrDay);				//開始（日）
		$this->assign("shour", $this->arrHour);				//開始（時）

		$this->assign("eyear", $this->arrYear);				//終了（年）
		$this->assign("emonth", $this->arrMonth);			//終了（月）
		$this->assign("eday", $this->arrDay);				//終了（日）
		$this->assign("ehour", $this->arrHour);				//開始（時）



		//---------------------------------
		//アクション別処理
		//---------------------------------

		$this->arrErr = array();
		switch($ws_action){

			case "save":
			case "save2":

				$this->check();
				$imgerr = $this->upload_image();
				if($imgerr != "") $this->arrErr["heade_image"] = $imgerr;
				if(count($this->arrErr) > 0) break;


				$item = "0";
				if($ws_action == "save2") $item = "1";

				if($this->arrForm["form_id"] == "") {
					$this->insert('1', $item);
				} else {
					$this->update('1', $item);
				}

				break;

			case "next":

				$this->check();
				$imgerr = $this->upload_image();
				if($imgerr != "") $this->arrErr["heade_image"] = $imgerr;
				if(count($this->arrErr) > 0) break;


				$this->_processTemplate = "Sys/form/Sys_formitem.html";

				 break;

			case "confirm":

				$this->check();
				$this->check_item();
				if(count($this->arrErr) > 0) {
					$this->_processTemplate = "Sys/form/Sys_formitem.html";
					break;
				}

				$this->_processTemplate = "Sys/form/Sys_form_confirm.html";



				break;

			case "complete":

				if($this->_reload->isReload()) $this->complete("既に実行した可能性があります。");

				$this->check();
				$this->check_item();
				if(count($this->arrErr) > 0) {
					$this->_processTemplate = "Sys/form/Sys_formitem.html";
					break;
				}



				if($this->arrForm["form_id"] == "") {
					$this->insert(0, 1);
				} else {
					$this->update(0, 1);
				}

				break;

			case "back":

				break;

			/**
			 * 初期表示時
			 */
			default:

				if($this->arrForm["form_id"] != "") {
					$this->arrForm = $this->objForm->get($this->arrForm["form_id"]);
					if($this->arrForm["auto_change_day"] != "") {
						$str = str_split($this->arrForm["auto_change_day"], 2);
						$this->arrForm["myear"] = $str[0].$str[1];
						$this->arrForm["mmonth"] = $str[2];
						$this->arrForm["mday"] = $str[3];
						$this->arrForm["mhour"] = $str[4];
					}


					unset($this->arrForm["password"]);
					if($this->arrForm["reception_date1"] != "") {
						$this->arrForm["syear"] = date('Y', strtotime($this->arrForm["reception_date1"]));
						$this->arrForm["smonth"] = date('m', strtotime($this->arrForm["reception_date1"]));
						$this->arrForm["sday"] = date('d', strtotime($this->arrForm["reception_date1"]));
						$this->arrForm["shour"] = date('H', strtotime($this->arrForm["reception_date1"]));
					}
					if($this->arrForm["reception_date2"] != "") {
						$this->arrForm["eyear"] = date('Y', strtotime($this->arrForm["reception_date2"]));
						$this->arrForm["emonth"] = date('m', strtotime($this->arrForm["reception_date2"]));
						$this->arrForm["eday"] = date('d', strtotime($this->arrForm["reception_date2"]));
						$this->arrForm["ehour"] = date('H', strtotime($this->arrForm["reception_date2"]));
					}
					if($this->arrForm["primary_reception_date"] != "") {
						$this->arrForm["fst_syear"] = date('Y', strtotime($this->arrForm["primary_reception_date"]));
						$this->arrForm["fst_smonth"] = date('m', strtotime($this->arrForm["primary_reception_date"]));
						$this->arrForm["fst_sday"] = date('d', strtotime($this->arrForm["primary_reception_date"]));
						$this->arrForm["fst_shour"] = date('H', strtotime($this->arrForm["primary_reception_date"]));
					}
					if($this->arrForm["secondary_reception_date"] != "") {
						$this->arrForm["fst_eyear"] = date('Y', strtotime($this->arrForm["secondary_reception_date"]));
						$this->arrForm["fst_emonth"] = date('m', strtotime($this->arrForm["secondary_reception_date"]));
						$this->arrForm["fst_eday"] = date('d', strtotime($this->arrForm["secondary_reception_date"]));
						$this->arrForm["fst_ehour"] = date('H', strtotime($this->arrForm["secondary_reception_date"]));
					}
					$this->arrForm["dst_head_image"] = $this->arrForm["head_image"];

					if($this->arrForm["group3"] != "") {
						list($this->arrForm["group3_1"], $this->arrForm["group3_2"], $this->arrForm["group3_3"]) = explode("|", $this->arrForm["group3"]);
					}

					$arrFormItem = $this->objForm->getListItem($this->arrForm["form_id"]);

					if($arrFormItem) {
						$this->arrForm = array_merge($this->arrForm, $arrFormItem);
					}



				}else{
					// 新規登録の場合は見出しをデフォルト@4/18
					$this->arrForm["ather_price_head0"] = $GLOBALS["regist"]["0"];
					$this->arrForm["ather_price_head1"] = $GLOBALS["regist"]["1"];
					$this->arrForm["price_head0"] 		= $GLOBALS["regist"]["0"];
					$this->arrForm["price_head1"] 		= $GLOBALS["regist"]["1"];
					$this->arrForm["auto_change_flg"]	=　0;
					$this->arrForm["disp"]	=　1;
				}

				if(!isset($this->arrForm["agree"]) || $this->arrForm["agree"] == "") $this->arrForm["agree"] = "0";
				if(!isset($this->arrForm["edit_flg"]) || $this->arrForm["edit_flg"] == "") $this->arrForm["edit_flg"] = "1";

			 	break;


		}

		$this->init_field();
		$this->assign("arrErr", $this->arrErr);

		// 親クラスに処理を任せる
		parent::main();

	}

	function init() {

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
		$key[] = array("form_id", 		"フォームID", 				array(),		array(),				"n",	0,	0);
		$key[] = array("login_id",		"ログインID", 				array(0, 0),	array("NULL"),			"an",	1,	0);
		$key[] = array("password",		"パスワード", 				array(5, 12),	array("LEN"),			"an",	1,	0);
		$key[] = array("form_name",		"フォーム名", 				array(0, 0),	array("NULL"),			"KV",	1,	0);
		$key[] = array("lang", 			"言語", 					array(),		array("SELECT"),		"n",	1,	1);
		$key[] = array("head", 			"フォーム頭文字", 			array(),		array("NULL"),			"a",	1,	0);
		$key[] = array("form_desc", 	"フォーム説明",			array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("form_mail", 	"メールアドレス",			array(0, 0),	array("NULL", "MAIL"),	"a",	1,	0);
		$key[] = array("form_mail2", 	"メールアドレス2",			array(0, 0),	array("MAIL"),	"a",	1,	0);
		$key[] = array("syear", 		"受付期間開始日(年)",	array(0, 4),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("smonth", 		"受付期間開始日(月)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("sday", 			"受付期間開始日(日)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("shour", 		"受付期間開始日(時)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("eyear", 		"受付期間終了日(年)",	array(0, 4),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("emonth", 		"受付期間終了日(月)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("eday", 			"受付期間終了日(日)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("ehour", 		"受付期間終了日(時)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("type", 			"フォーム種別",			array(0, 1),	array("SELECT", "LEN"),	"n",	1,	1);
		$key[] = array("fst_syear", 	"一次受付期間終了日(年)",	array(0, 4),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_smonth", 	"一次受付期間終了日(月)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_sday", 		"一次受付期間終了日(日)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_shour", 	"一次受付期間終了日(時)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_eyear", 	"二次受付期間開始日(年)",	array(0, 4),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_emonth", 	"二次受付期間開始日(月)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_eday", 		"二次受付期間開始日(日)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_ehour", 	"二次受付期間開始日(時)",	array(0, 2),	array("LEN"),			"n",	0,	0);




		$key[] = array("edit_flg", 		"編集許可", 				array(),		array(),				"n",	1,	1);
		$key[] = array("contact", 		"お問い合せ先", 			array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("dst_head_image","ヘッダ画像", 				array(0, 0),	array(),				"a",	0,	0);
		$key[] = array("temp_image", 	"アップロード画像", 			array(0, 0),	array(),				"a",	0,	0);
		$key[] = array("image_del", 	"画像削除", 				array(0, 0),	array(),				"n",	0,	0);
		$key[] = array("agree", 		"個人情報の確認", 		array(0, 0),	array(),				"n",	1,	1);
		$key[] = array("agree_text", 	"個人情報の本文", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group1", 		"グループ項目名1", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group2", 		"グループ項目名2", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group3", 		"グループ項目名3", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group3_1", 		"グループ項目名3_1", 		array(0, 0),	array(),				"KV",	0,	0);
		$key[] = array("group3_2", 		"グループ項目名3_2", 		array(0, 0),	array(),				"KV",	0,	0);
		$key[] = array("group3_3", 		"グループ項目名3_3", 		array(0, 0),	array(),				"KV",	0,	0);
		$key[] = array("group4", 		"グループ項目名4", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group5", 		"グループ項目名5", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group2_use", 	"グループ項目2利用フラグ", 	array(0, 0),	array(),				"n",	1,	1);
		$key[] = array("group3_use", 	"グループ項目3利用フラグ", 	array(0, 0),	array(),				"n",	1,	1);
		$key[] = array("user_id", 		"ユーザID", 				array(0, 0),	array("NULL"),			"n",	1,	0);
		$key[] = array("group3_layout", "グループ項目3　任意項目レイアウト", 	array(),	array(),				"n",	1,	1);

        // 追加項目#Mng CSV-Basic
		$key[] = array("use_csvbasic", 	"フォーム管理者　CSVDL-Basic認証設定",	array(),	array("SELECT"),	"n",	1,	0);

		$key[] = array("temp", 			"一時保存", 				array(0, 0),	array(),				"n",	0,	1);

		return $key;

	}

	function init2() {

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
		$key[] = array("item_name", 		"項目名", 				array(),		array(),	"KV",	1,	0);
		$key[] = array("item_view", 		"非表示フラグ", 		array(),		array(),	"KV",	1,	0);
		$key[] = array("item_check", 		"チェックタイプ", 		array(),		array(),	"KV",	1,	0);
		$key[] = array("item_len", 			"制限サイズ", 			array(),		array(),	"KV",	1,	0);
		$key[] = array("item_pview", 		"一次非表示フラグ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_pview2", 		"二次非表示フラグ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_pview3", 		"二次編集不可フラグ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_mail", 		"メール非表示フラグ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_type", 		"コントロールタイプ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_select",		"選択肢項目", 			array(),		array(),	"KV",	1,	0);
		$key[] = array("item_memo",			"備考欄", 				array(),		array(),	"KV",	1,	0);

		return $key;

	}

	function init_field() {

		if(!isset($this->arrForm["type"]) || !isset($this->arrForm["lang"])) return;
		if(!$this->arrForm["type"] > 0) return;
		if(!$this->arrForm["lang"] > 0) return;

		$arrIni = $this->objItemIni->getList($this->arrForm["type"], $this->arrForm["lang"]);

		if(!$arrIni) return;

		switch($this->arrForm["type"]) {
			case "1":
			case "2":
				if($this->arrForm["lang"] == "1") {
					$group[1]["title"] = $this->arrForm["group1"] != "" ? $this->arrForm["group1"] : "筆頭著者";
					$group[2]["title"] = $this->arrForm["group2"] != "" ? $this->arrForm["group2"] : "共著者";
					$group[3]["title"] = $this->arrForm["group3"] != "" ? $this->arrForm["group3"] : "抄録";
				} else {
					$group[1]["title"] = $this->arrForm["group1"] != "" ? $this->arrForm["group1"] : "筆頭著者";
					$group[2]["title"] = $this->arrForm["group2"] != "" ? $this->arrForm["group2"] : "共著者";
					$group[3]["title"] = $this->arrForm["group3"] != "" ? $this->arrForm["group3"] : "抄録";
				}
				break;

			default:
				break;
		}

		for($i = 1; $i <= $this->objItemIni->cnt; $i++) {

			if(!isset($this->arrForm["item_check"][$i])) $this->arrForm["item_check"][$i] = array();

			$forms["item_check"][$i] = SmartyForm::createRadioChecked("item_check[$i]", $GLOBALS["checktypeList"], $this->arrForm["item_check"][$i], "checkbox", "form", 3);

		}

		foreach($arrIni as $val) {
			if(!isset($this->arrForm["item_memo"][$val["item_id"]])) {
				if($val["hidden_flg"] == "1") {
					$this->arrForm["item_view"][$val["item_id"]]   = 1;
					$this->arrForm["item_mail"][$val["item_id"]]   = 1;
					$this->arrForm["item_pview"][$val["item_id"]]  = 1;
					$this->arrForm["item_pview2"][$val["item_id"]] = 1;
					$this->arrForm["item_pview3"][$val["item_id"]] = 1;
				}
			}
		}

		$this->assign("forms", $forms);
		$this->assign("group", $group);
		$this->assign("field", $arrIni);


	}

	function check() {

		$objErr = new Validate;

		$objErr->check($this->init(), $this->arrForm);


		//パスワードチェック
		if($this->arrForm["form_id"] == "" && $this->arrForm["password"] == "") {
			$objErr->addErr("パスワードが入力されていません。", "password");
		}

		//ログインIDの重複チェック
		if($this->arrForm["login_id"] != "" && !$this->objForm->checkLoginId($this->arrForm["form_id"], $this->arrForm["login_id"])) {
			$objErr->addErr("ログインIDは他のフォームで登録されています。", "login_id");
		}


		//フォーム頭文字の重複チェック
		/*
		if($this->arrForm["head"] != ""){
			if(!$this->objForm->checkFormHead($this->arrForm["form_id"], $this->arrForm["head"])){
				$objErr->addErr("フォーム頭文字は他のフォームで登録されています。", "head");
			}
		}
		*/


		// 受付期間
		$this->arrForm["reception_date1"] = "";
		if($this->arrForm["syear"] > 0 && $this->arrForm["smonth"] > 0 && $this->arrForm["sday"] > 0) {
			$this->arrForm["shour"] = $this->arrForm["shour"] != "" ? $this->arrForm["shour"] : "00";
			$this->arrForm["reception_date1"] = $this->arrForm["syear"]."-".$this->arrForm["smonth"]."-".$this->arrForm["sday"]." ".$this->arrForm["shour"].":00:00";
			if(!$objErr->isDate($this->arrForm["reception_date1"])) {
				$objErr->addErr("受付期間開始日を確認して下さい。", "reception_date1");
			}
		}
		$this->arrForm["reception_date2"] = "";
		if($this->arrForm["eyear"] > 0 && $this->arrForm["emonth"] > 0 && $this->arrForm["eday"] > 0) {
			$this->arrForm["ehour"] = $this->arrForm["ehour"] != "" ? $this->arrForm["ehour"] : "00";
			$this->arrForm["reception_date2"] = $this->arrForm["eyear"]."-".$this->arrForm["emonth"]."-".$this->arrForm["eday"]." ".$this->arrForm["ehour"].":00:00";
			if(!$objErr->isDate($this->arrForm["reception_date2"])) {
				$objErr->addErr("受付期間終了日を確認して下さい。", "reception_date2");
			}
		}
		if($this->arrForm["reception_date1"] != "" && $this->arrForm["reception_date2"] != "") {
			if(strtotime($this->arrForm["reception_date1"]) >= strtotime($this->arrForm["reception_date2"])) {
				$objErr->addErr("受付期間終了日は、開始日よりも未来に設定して下さい。", "reception_date1");
			}
		}

		//　一次受付期間
		switch($this->arrForm["type"]) {
			case "1":
				$this->arrForm["primary_reception_date"] = "";
				$this->arrForm["secondary_reception_date"] = "";
				if($this->arrForm["fst_syear"] > 0 || $this->arrForm["fst_smonth"] > 0 || $this->arrForm["fst_sday"] > 0 ||
				$this->arrForm["fst_eyear"] > 0 || $this->arrForm["fst_emonth"] > 0 || $this->arrForm["fst_eday"] > 0) {
					$this->arrForm["fst_shour"] = $this->arrForm["fst_shour"] != "" ? $this->arrForm["fst_shour"] : "00";
					$this->arrForm["fst_ehour"] = $this->arrForm["fst_ehour"] != "" ? $this->arrForm["fst_ehour"] : "00";
					$this->arrForm["primary_reception_date"] = $this->arrForm["fst_syear"]."-".$this->arrForm["fst_smonth"]."-".$this->arrForm["fst_sday"]." ".$this->arrForm["fst_shour"].":00:00";
					if(!$objErr->isDate($this->arrForm["primary_reception_date"])) {
						$objErr->addErr("一次受付期間終了日を確認して下さい。", "primary_reception_date");
					}
					$this->arrForm["secondary_reception_date"] = $this->arrForm["fst_eyear"]."-".$this->arrForm["fst_emonth"]."-".$this->arrForm["fst_eday"]." ".$this->arrForm["fst_ehour"].":00:00";
					if(!$objErr->isDate($this->arrForm["secondary_reception_date"])) {
						$objErr->addErr("二次受付期間開始日を確認して下さい。", "secondary_reception_date");
					}
				}

				if($this->arrForm["primary_reception_date"] != "" && $this->arrForm["secondary_reception_date"] != "") {
					if(strtotime($this->arrForm["primary_reception_date"]) >= strtotime($this->arrForm["secondary_reception_date"])) {
						$objErr->addErr("二次受付期間開始日は、一次受付期間終了日よりも未来に設定して下さい。", "primary_reception_date");
					}
					if((strtotime($this->arrForm["reception_date1"]) >= strtotime($this->arrForm["primary_reception_date"]))
						|| (strtotime($this->arrForm["reception_date2"]) < strtotime($this->arrForm["primary_reception_date"]))) {
						$objErr->addErr("一次受付期間終了日は受付期間内に設定して下さい。", "primary_reception_date");
					}
					if((strtotime($this->arrForm["reception_date1"]) > strtotime($this->arrForm["secondary_reception_date"]))
						|| (strtotime($this->arrForm["reception_date2"]) <= strtotime($this->arrForm["secondary_reception_date"]))) {
						$objErr->addErr("二次受付期間開始日は受付期間内に設定して下さい。", "primary_reception_date1");
					}
				} else {
					$objErr->addErr("アブストラクトを選択した場合は、一次受付期間終了日と二次受付期間開始日は必須です。", "primary_reception_date");
				}
				break;

		}


		$this->arrErr = $objErr->_err;
	}


	function check_item() {

		foreach($this->arrForm["item_name"] as $key=>$val) {

			$len = 0;
			$textformat = 0;

			if(isset($this->arrForm["item_check"][$key])) {
				foreach($this->arrForm["item_check"][$key] as $ck) {
					switch($ck) {
						case "1":
						case "2":
						case "3":
							$len++;
							break;
						case "4":
						case "5":
							$textformat++;
							break;
						default:
							break;
					}
				}

				if($len > 1) {
					$this->arrErr["item_check"][$key] = "長さのチェックはバイト・文字数・単語のうち、いずれか1つ選択して下さい。";
				}
				if($textformat > 1) {
					$this->arrErr["item_check"][$key] = "半角・全角のチェックはいずれか１つ選択して下さい。";
				}
				if($len == 1) {
					if($this->arrForm["item_len"][$key] == "" || $this->arrForm["item_len"][$key] == "0") {
						$this->arrErr["item_len"][$key] = "長さ(ファイル容量)のチェックをする場合は、サイズを指定して下さい。";
					}
				}

			}

			if(isset($this->arrForm["item_type"][$key])) {

				if($this->arrForm["item_type"][$key] == 2 || $this->arrForm["item_type"][$key] == 3 || $this->arrForm["item_type"][$key] == 4) {

					if($this->arrForm["item_select"][$key] == "") {

						$this->arrErr["item_select"][$key] = "選択肢を入力して下さい。";
					}
				}
			}

            if(in_array($key, array(51,52,53))){
                $ext = preg_replace('/[\s　]+/u', '', $this->arrForm["item_select"][$key]);
                if(strlen($ext) == 0) continue;

                if(!Validate::isAlphaNumeric($ext)){
						$this->arrErr["item_select"][$key] = "拡張子は半角英数で入力して下さい。[". $ext."]";
                }
            }
		}
	}

	// DB新規登録　$temp（一時保存の場合1）　$item(フォーム項目の更新も行う場合1)
	function insert($temp=0, $item=0) {

		$db = new DbGeneral;

		$db->begin();

		foreach($this->init() as $val) {

			if(!isset($this->arrForm[$val[0]])) $this->arrForm[$val[0]] = "";

			if($val[5] == "1") {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
			if($val[6] == "1") {
				if(empty($param[$val[0]]) || $param[$val[0]] == "") $param[$val[0]] = "0";
			}
		}

		$param["group3"] = $this->arrForm["group3_1"]."|".$this->arrForm["group3_2"]."|".$this->arrForm["group3_3"];

		if($temp == "0") {
			$param["temp"] = "0";
		} else {
			$param["temp"] = "1";
		}

		if($this->arrForm["reception_date1"] != "" && $this->arrForm["reception_date2"] != "") {
			$param["reception_date1"] = $this->arrForm["reception_date1"];
			$param["reception_date2"] = $this->arrForm["reception_date2"];
		}
		if($this->arrForm["type"] == "1") {
			if($this->arrForm["primary_reception_date"] != "" && $this->arrForm["secondary_reception_date"] != "") {
				$param["primary_reception_date"] = $this->arrForm["primary_reception_date"];
				$param["secondary_reception_date"] = $this->arrForm["secondary_reception_date"];
			}
		}

		$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);

		if($this->arrForm["temp_image"] != "") {
			if($this->arrForm["image_del"] == "1") {
				if(file_exists(IMG_PATH."header/".$this->arrForm["temp_image"])) {
					unlink(IMG_PATH."header/".$this->arrForm["temp_image"]);
				}
			} else {
				$param["head_image"] = $this->arrForm["temp_image"];
			}
		}

		$rs = $db->insert("form", $param, __FILE__, __LINE__);

		if(!$rs) {
			$db->rollback();
			if($temp == "0") {
				$this->complete("フォーム情報の登録に失敗しました。");
			} else {
				$this->complete("フォーム情報の一次保存に失敗しました。");
			}
		}

		$this->arrForm["form_id"] = $db->_getOne("max(form_id)", "form", "del_flg = 0", __FILE__, __LINE__);
		if(!$this->arrForm["form_id"]) {
			$db->rollback();
			$this->complete("フォームIDの取得に失敗しました。");
		}

		if($item == "1") {

			$rs = $this->setitem($db, $temp);

			if(!$rs) {
				$db->rollback();
				if($temp == "0") {
					$this->complete("フォーム情報の登録に失敗しました。");
				} else {
					$this->complete("フォーム情報の一次保存に失敗しました。");
				}
			}
		}

        // 採番テーブル作成
        $this->objEntry->registEntryNumber($db, $this->arrForm["form_id"]);
        // フォーム管理者テーブルへの追加
        $this->registFormAdmin($db);

		$db->commit();
		if($temp == "0") {
			$this->complete("フォーム情報を登録しました。");
		} else {
			$this->complete("フォーム情報を一次保存しました。");
		}

	}


	// フォーム項目の更新を行う。
	function setitem(&$db, $temp) {

		foreach($this->arrForm["item_name"] as $key=>$val) {

			$check = $this->checkItem($db, $this->arrForm["form_id"], $key);

			$param["item_name"]   = $val;
			$param["item_view"]   = isset($this->arrForm["item_view"][$key]) ? "1" : "0";
			$param["item_check"]  = isset($this->arrForm["item_check"][$key]) ? $this->cnvCheckData($this->arrForm["item_check"][$key]) : "";
			$param["item_len"]    = $this->arrForm["item_len"][$key];
			$param["item_pview"]  = isset($this->arrForm["item_pview"][$key])  ? "1" : "0";
			$param["item_pview2"] = isset($this->arrForm["item_pview2"][$key]) ? "1" : "0";
			$param["item_pview3"] = isset($this->arrForm["item_pview3"][$key]) ? "1" : "0";
			$param["item_mail"]   = isset($this->arrForm["item_mail"][$key]) ? "1" : "0";
			$param["item_type"]   = isset($this->arrForm["item_type"][$key]) ? $this->arrForm["item_type"][$key] : "";
			$param["item_select"] = isset($this->arrForm["item_select"][$key]) ? $this->arrForm["item_select"][$key] : "";
			$param["item_memo"]  = $this->arrForm["item_memo"][$key];

			if(!$check) {
				// 登録
				$param["form_id"] = $this->arrForm["form_id"];
				$param["item_id"] = $key;

				$rs = $db->insert("form_item", $param, __FILE__, __LINE__);

			} else {
				// 更新
				$param["udate"] = "NOW";
				$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);
				$where[] = "item_id = ".$db->quote($key);

				$rs = $db->update("form_item", $param, $where, __FILE__, __LINE__);

				unset($where);
			}

			if(!$rs) {
				return false;
			}

			unset($param);
		}

		return true;

	}

	// チェック項目を配列から文字列へ
	function cnvCheckData($arrcheck=array()) {

		if(!count($arrcheck) > 0) return "";

		return implode("|", $arrcheck);
	}

	// フォーム項目に登録があるか確認　ある場合true　ない場合false
	function checkItem(&$db, $form_id, $item_id) {

		$where[] = "form_id = ".$db->quote($form_id);
		$where[] = "item_id = ".$db->quote($item_id);

		$rs = $db->_getOne("form_id", "form_item", $where, __FILE__, __LINE__);

		if(!$rs) return false;

		return true;

	}

	// DB更新　$temp（一時保存の場合1）　$item(フォーム項目の更新も行う場合1)
	function update($temp=0, $item=0) {

		$db = new DbGeneral;

		$db->begin();

		foreach($this->init() as $val) {

			if(!isset($this->arrForm[$val[0]])) $this->arrForm[$val[0]] = "";

			if($val[5] == "1") {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
			if($val[6] == "1") {
				if(empty($param[$val[0]]) || $param[$val[0]] == "") $param[$val[0]] = "0";
			}
		}

		$param["group3"] = $this->arrForm["group3_1"]."|".$this->arrForm["group3_2"]."|".$this->arrForm["group3_3"];

		unset($param["password"]);
		if($this->arrForm["password"] != "") {
			$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		}

		if($this->arrForm["reception_date1"] != "" && $this->arrForm["reception_date2"] != "") {
			$param["reception_date1"] = $this->arrForm["reception_date1"];
			$param["reception_date2"] = $this->arrForm["reception_date2"];
		}
		if($this->arrForm["type"] == "1") {
			if($this->arrForm["primary_reception_date"] != "" && $this->arrForm["secondary_reception_date"] != "") {
				$param["primary_reception_date"] = $this->arrForm["primary_reception_date"];
				$param["secondary_reception_date"] = $this->arrForm["secondary_reception_date"];
			}
		} else {
			$param["primary_reception_date"] = NULL;
			$param["secondary_reception_date"] = NULL;
		}


		// 登録済み画像があり、削除フラグがある場合　又は、登録済みと新たにアップロードした画像がある場合は、登録済み画像を削除する。
		if(($this->arrForm["dst_head_image"] != "" && $this->arrForm["image_del"] == "1") || ($this->arrForm["dst_head_image"] != "" && $this->arrForm["temp_image"] != "")) {
			$param["head_image"] = "";
			if(file_exists(IMG_PATH."header/".$this->arrForm["dst_head_image"])) {
				unlink(IMG_PATH."header/".$this->arrForm["dst_head_image"]);
			}
		}
		// 登録済み画像がなく、新規アップロードファイルがあり、削除フラグがある場合は、新規アップロードファイルを削除する。
		if($this->arrForm["dst_head_image"] == "" && $this->arrForm["temp_image"] != "" && $this->arrForm["image_del"] == "1") {
			if(file_exists(IMG_PATH."header/".$this->arrForm["temp_image"])) {
				unlink(IMG_PATH."header/".$this->arrForm["temp_image"]);
			}
			$this->arrForm["temp_image"] = "";
		}

		if($this->arrForm["temp_image"] != "") $param["head_image"] = $this->arrForm["temp_image"];

		if($temp == "0") {
			$param["temp"] = "0";
		} else {
			$param["temp"] = "1";
		}

		$param["udate"] = "NOW";


		$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);

		$rs = $db->update("form", $param, $where, __FILE__, __LINE__);

		if(!$rs) {
			$db->rollback();
			if($temp == "0") {
				$this->complete("フォーム情報の更新に失敗しました。");
			} else {
				$this->complete("フォーム情報の一次保存に失敗しました。");
			}
		}

		// $itemが1の場合はフォーム項目の更新も行う
		if($item == "1") {

			$rs = $this->setitem($db, $temp);

			if(!$rs) {
				$db->rollback();
				if($temp == "0") {
					$this->complete("フォーム情報の更新に失敗しました。");
				} else {
					$this->complete("フォーム情報の一次保存に失敗しました。");
				}
			}
		}

		$this->updateFormAdmin($db);

		$db->commit();
		if($temp == "0") {
			$this->complete("フォーム情報を更新しました。");
		} else {
			$this->complete("フォーム情報を一次保存しました。");
		}



	}

	// 完了画面の表示
	function complete($msg) {

		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";

		$this->arrForm["url"] = "Sys_formlist.php?user_id=".$this->arrForm["user_id"];

		// 親クラスに処理を任せる
		parent::main();
		exit;

	}

	function upload_image() {

		if(empty($_FILES)) return;
		if(!$_FILES["upfile"]["size"] > 0) return;

		// ファイルがアップロードされた時
		if($_FILES["upfile"]["size"] > 0) {

			$savedir = IMG_PATH."header";
			if(!is_dir($savedir)) {
				mkdir($savedir, 0777);
			}

			if($_FILES["upfile"]["error"] > 0) return "ファイルのアップロードに失敗しました。";

			if($this->arrForm["temp_image"] != "") {
				if(file_exists(IMG_PATH."header/".$this->arrForm["temp_image"])) {
					unlink(IMG_PATH."header/".$this->arrForm["temp_image"]);
				}
			}

			$filename = date("YmdHis");
			$file_info = pathinfo($_FILES["upfile"]["name"]);

			$this->arrForm["temp_image"] = date("YmdHis").".".$file_info["extension"];
			$rs = @move_uploaded_file($_FILES["upfile"]["tmp_name"], IMG_PATH."header/".$this->arrForm["temp_image"]);

			if(!$rs) return "ファイルの一時保存に失敗しました。";

			$this->arrForm["image_del"] = "0";

		}

		return;

	}

	function registFormAdmin(&$db) {

		$param["form_id"] = $this->arrForm["form_id"];
		$param["super_flg"] = "1";
		$param["login_id"] = $this->arrForm["login_id"];
		if($this->arrForm["password"] != "") {
			$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		} else {

			$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);

			$rs = $db->_getOne("password", "form", $where, __FILE__, __LINE__);

			if(!$rs) {
				$db->rollback();
				$this->complete("フォーム管理者の登録に失敗しました。");
			}

			$param["password"] = $rs;


		}

		$rs = $db->insert("form_admin", $param, __FILE__, __LINE__);

		if(!$rs) {
			$db->rollback();
			$this->complete("フォーム管理者の登録に失敗しました。");
		}

		return;

	}

	function updateFormAdmin(&$db) {

		$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);
		$where[] = "super_flg = 1";

		$rs = $db->_getOne("formadmin_id", "form_admin", $where, __FILE__, __LINE__);

		if(!$rs) {
			return $this->registFormAdmin($db);
		}

		$param["login_id"] = $this->arrForm["login_id"];
		if($this->arrForm["password"] != "") {
			$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		}
		$param["udate"] = "NOW";

		$rs = $db->update("form_admin", $param, $where);


		if(!$rs) {
			$db->rollback();
			$this->complete("フォーム管理者の登録に失敗しました。");
		}

		return;

	}
}

/**
 * メイン処理開始
 **/

$c = new formbase();
$c->main();







?>
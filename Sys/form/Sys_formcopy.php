<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');



/**
 * システム管理者　フォーム登録・編集
 *     フォームの基本情報部分入力画面表示
 *
 * @subpackage Sys
 * @author salon
 *
 */
class formbase extends ProcessBase {

    var $objForm;


    /**
     * コンストラクタ
     */
    function formbase(){
        LoginAdmin::checkLoginRidirect();

        parent::ProcessBase();

        //ログイン者情報
        $this->assign("user_name", "システム管理者");

        //スーパー管理者メニュー
        $this->assign("va_menu", sys_common::menu());

        // 表示HTMLの設定
        $this->_processTemplate = "Sys/form/Sys_formcopy.html";
        $this->_title = "スーパー管理者ページ";

        // オブジェクト生成
        $this->objForm    = new Form;
        $this->objItemIni = new item_ini;
        $this->objUser    = new User;
        $this->db         = new DbGeneral;

        $this->arrErr  = array();
        $this->form_id = isset($_POST['form_id']) ? $_POST['form_id'] : "";
    }


    /**
     * メイン処理
     */
    function main(){

        // エラー
        if(!is_numeric($this->form_id) || strlen($this->form_id) == 0){
        }

        $this->getUserList();
        $this->getCopyBase();



        //-----------------------
        //フォームパラメータ
        //-----------------------
        $this->arrForm = GeneralFnc::convertParam($this->init(), $_REQUEST);
        if(strlen($this->arrForm['user_type']) == "") $this->arrForm['user_type'] = 1;


        //---------------------------------
        //アクション別処理
        //---------------------------------
        $ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        switch($ws_action){
            case "confirm":
                $this->check();
                if(count($this->arrErr) > 0) break;

                $this->_processTemplate = "Sys/form/Sys_formcopy_confirm.html";
                break;

            case "complete":
                $this->copyform();
                break;

            case "back":
                break;

            /**
             * 初期表示時
             */
            default:

                 break;


        }


        $this->assign("arrErr", $this->arrErr);

        // 親クラスに処理を任せる
        parent::main();
    }


    function getCopyBase(){
        $this->arrCopyBase = $this->objForm->get($this->form_id);
        unset($this->arrCopyBase["password"]);

        $this->assign("arrCopyBase", $this->arrCopyBase);
    }


    function init() {
        $key = array();

        //(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
        $key[] = array("user_type",  "ユーザ区分",      array(0, 0),    array("NULL"),        "n",    1,  0);
        $key[] = array("user_id",    "ユーザ",          array(0, 0),    array(),              "n",    1,  0);
        $key[] = array("user_name",  "ユーザ",          array(0, 40),   array( "MLEN"),       "KV",   1,  0);

        $key[] = array("form_id",    "フォームID",      array(),        array(),              "n",    0,  0);
        $key[] = array("form_name",  "フォーム名",      array(0, 0),    array("NULL"),        "KV",   1,  0);
        $key[] = array("head",       "フォーム頭文字",  array(),        array("NULL"),        "a",    1,  0);
        $key[] = array("login_id",   "ログインID",      array(0, 0),    array("NULL"),        "an",   1,  0);
        $key[] = array("password",   "パスワード",      array(5, 12),   array("NULL","LEN"),  "an",   1,  0);

        return $key;
    }


    function check() {
        $objErr = new Validate;
        $objErr->check($this->init(), $this->arrForm);

        // 既存ユーザ
        if($this->arrForm['user_type'] == 1){
            if(strlen($this->arrForm['user_id']) == 0){
                $objErr->addErr("ユーザを選択して下さい。", "user_id");
            }

        // 新規ユーザ
        }else{
            if(strlen($this->arrForm['user_name']) == 0){
                $objErr->addErr("ユーザ名を入力して下さい。。", "user_name");
            }
        }

        //ログインIDの重複チェック
        if($this->arrForm["login_id"] != "" && !$this->objForm->checkLoginId(0, $this->arrForm["login_id"])) {
            $objErr->addErr("ログインIDは他のフォームで登録されています。", "login_id");
        }

        $objErr->sortErr($this->init());
        $this->arrErr = $objErr->_err;
    }


    /** ユーザ一覧を取得しassignする */
    function getUserList(){
        $res = $this->db->getListData("user_id, user_name", "users", "del_flg = 0", "user_id");
        $arrUser = array();
        if($res){
            foreach($res as $_key => $_arrUser){
                $user_id = $_arrUser['user_id'];
                $arrUser[$user_id] = $_arrUser['user_name'];
            }
        }
        $this->assign("arrUser", $arrUser);
    }


    function copyform(){
        $password = md5($this->arrForm["password"].PASS_PHRASE);


        $this->db->begin();

        // ユーザ新規登録
        if($this->arrForm["user_type"] == 2){
            $param = array();
            $param['user_id'] = $GLOBALS["db"]->getOne("select nextval('users_user_id_seq')");
            $param['user_name']  = $this->arrForm['user_name'];
            $param['udate'] = 'NOW()';
            $param['rdate'] = 'NOW()';

            $res = $this->db->insert("users", $param, __FILE__, __LINE__);
            if(!$res) {
                $this->db->rollback();
                $this->complete("ユーザの作成に失敗しました。");
            }
            $this->arrForm['user_id'] = $param['user_id'];
        }


        // 共通where文
        $where = "form_id = ".$this->db->quote($this->form_id);

        // 複製元のform情報を取得
        $param = $this->db->getData("*", "form", $where, __FILE__, __LINE__);
        // 入力値を反映
        unset($param["form_id"]);
        $param["user_id"]   = $this->arrForm["user_id"];
        $param["form_name"] = $this->arrForm["form_name"];
        $param["head"]      = $this->arrForm["head"];
        $param["login_id"]  = $this->arrForm["login_id"];
        $param["password"]  = $password;
        $param['udate'] = 'NOW()';
        $param['rdate'] = 'NOW()';
        $param = array_filter($param, 'strlen');  // 空要素を削除

        // ヘッダ画像を複製
        if(isset($param["head_image"])){
            $newfile = date("YmdHis").".".pathinfo($param["head_image"], PATHINFO_EXTENSION);
            if(!copy(IMG_PATH."header/".$param["head_image"], IMG_PATH."header/".$newfile)){
                $this->complete("ヘッダ画像の複製に失敗しました。");
            }
            $param["head_image"] = $newfile;
        }

        $res = $this->db->insert("form", $param, __FILE__, __LINE__);
        if(!$res) {
            $this->db->rollback();
            $this->complete("フォーム情報の複製に失敗しました。");
        }

        $this->arrForm["form_id"] = $this->db->_getOne("max(form_id)", "form", "del_flg = 0", __FILE__, __LINE__);
        if(!$this->arrForm["form_id"]) {
            $this->db->rollback();
            $this->complete("フォームIDの取得に失敗しました。");
        }


        // 複製元の項目情報を取得
        $arrParam = $this->db->getListData("*", "form_item", $where, null, null, null, __FILE__, __LINE__);
        foreach($arrParam as $_key => $param){
            $param["form_id"] = $this->arrForm["form_id"];
            $param['udate'] = 'NOW()';
            $param['rdate'] = 'NOW()';
            $param = array_filter($param, 'strlen');  // 空要素を削除

            $res = $this->db->insert("form_item", $param, __FILE__, __LINE__);
            if(!$res) {
                $this->db->rollback();
                $this->complete("フォーム項目情報の複製に失敗しました。");
            }
        }


        // 採番テーブルを新規作成
        $res = $this->db->_getOne("id", "entory_number", $where, __FILE__, __LINE__);
        if(!$res){
            $param = array();
            $param["form_id"] = $this->arrForm["form_id"];
            $param["entory_count"] = "0";
            $param['udate'] = 'NOW()';
            $param['rdate'] = 'NOW()';
            $param = array_filter($param, 'strlen');  // 空要素を削除

            $res = $this->db->insert("entory_number", $param, __FILE__, __LINE__);
            if(!$res) {
                $this->db->rollback();
                $this->complete("フォームの初期設定に失敗しました。");
            }
        }


        // フォーム文言情報を複製
        $param = $this->db->getData("*", "form_word", $where, __FILE__, __LINE__);
        $param["form_id"] = $this->arrForm["form_id"];
        $param = array_filter($param, 'strlen');  // 空要素を削除

        $res = $this->db->insert("form_word", $param, __FILE__, __LINE__);
        if(!$res) {
            $this->db->rollback();
            $this->complete("フォーム文言情報の複製に失敗しました。");
        }


        // フォーム管理者を新規作成
        $param = array();
        $param["form_id"]   = $this->arrForm["form_id"];
        $param["super_flg"] = "1";
        $param["login_id"]  = $this->arrForm["login_id"];
        $param["password"]  = $password;
        $param['udate'] = 'NOW()';
        $param['rdate'] = 'NOW()';
        $param = array_filter($param, 'strlen');  // 空要素を削除

        $rs = $this->db->insert("form_admin", $param, __FILE__, __LINE__);
        if(!$res) {
            $this->db->rollback();
            $this->complete("フォーム管理者の登録に失敗しました。");
        }


        $this->db->commit();
        $this->complete("フォームの複製が完了しました。");
    }


    // 完了画面の表示
    function complete($msg) {

        $this->assign("msg", $msg);
        $this->_processTemplate = "Sys/Sys_complete.html";

        $this->arrForm["url"] = "Sys_formlist.php?user_id=".$this->arrForm["user_id"];

        // 親クラスに処理を任せる
        parent::main();
        exit;
    }


}

/**
 * メイン処理開始
 **/

$c = new formbase();
$c->main();







?>
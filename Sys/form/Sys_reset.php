<?php

include('../../application/cnf/batch_include.php');
include_once(MODULE_DIR.'mail_class.php');
//include_once('../../application/module/custom/Form.class.php');
include('../../Usr/form/function.php');

/**
 * システム管理者　リセット
 * 	エントリー情報をリセットする
 *
 * @subpackage Sys
 * @author salon
 *
 */
class form_reset{

	/**
	 * コンストラクタ
	 */
	function form_reset($argc, $pn_formid){

		$this->form_id = $pn_formid;

	}

	/**
	 * メイン処理
	 */
	function main(){

		//----------------------
		//インスタンス
		//----------------------
		$this->db = new DbGeneral;
		$this->o_mail = new Mail();


		//フォーム情報取得
    	$column = "form_id, form_name, type, lang, temp, edit_flg, form_desc, form_mail, contact, head, head_image, reception_date1, reception_date2, primary_reception_date, secondary_reception_date, agree, agree_text, group1, group2, group3, group4, group5, credit, pricetype, price";
    	$from = "form";
    	$where[] = "form_id = ".$this->db->quote($this->form_id);
    	$where[] = "del_flg = 0";

    	$formData = $this->db->getData($column, $from, $where, __FILE__, __LINE__);



		//---------------------------
		//トランザクション
		//---------------------------
		$this->db->begin();
		unset($where);
		
		
		// エントリー対象のeidリストを取得
		$arrData = $this->getEntryList($this->db, $this->form_id);
		
		// [機能改修]
		//削除処理（物理）
 		$param["del_flg"] = 1;
		$param["udate"] = "NOW";
		$where[] = "form_id = ".$this->db->quote($this->form_id);
//		$wb_ret1 = $this->db->update("entory_r", $param, $where, __FILE__, __LINE__);
		$wb_ret1 = $this->db->delete("entory_r", $where, __FILE__, __LINE__);

		// 同伴者情報削除
		$wb_ret2 = $this->db->delete("entory_aff", $where, __FILE__, __LINE__);
		
		// フォームに関連付くエントリー採番テーブルのリセットを実施
		$wb_ret3 = $this->db->delete("entory_number", $where, __FILE__, __LINE__);

		// メール履歴の削除
		$wb_ret4 = $this->db->delete("mail_history", $where, __FILE__, __LINE__);
		
		// mail_sendentryの削除
		foreach($arrData as $value){
		    unset($where);
		    if($value['eid'] != "") {
		        $where[] = "eid = " .$value['eid'];
		        // eidのあるレコードを削除
		        $wb_ret5 = $this->db->delete("mail_sendentry", $where, __FILE__, __LINE__);
		    }
		}
		

		//エントリー数
		unset($param);
		unset($where);
		$param["entory_count"] = 0;
		$param["udate"] = "NOW";
		$where[] = "form_id = ".$this->db->quote($this->form_id);
		$wb_ret2 = $this->db->update("entory_number", $param, $where, __FILE__, __LINE__);

		$subject = "エントリー情報リセット（".$formData["form_name"]."）";

		if(!$wb_ret1 || !$wb_ret2) {

			$ws_mailBody = "エントリー情報のリセットを行いましたが、正常に処理を終了することができませんでした。\n";
			$ws_mailBody .= "エントリー情報テーブル、エントリー数テーブルをご確認ください。\n";
		}
		else{

			//削除するディレクトリのパス
			$dir= UPLOAD_PATH."Usr/form".$this->form_id;

			$wo_fnc = new Usr_function();
			$ret = $wo_fnc->deleteDir($dir);

			if(!$ret){

				$ws_mailBody = "エントリー情報のリセットを行いましたが、正常に処理を終了することができませんでした。\n";
				$ws_mailBody .= "ファイルアップロードディレクトリをご確認ください。\n";

				$this->db->rollback();
			}
			else{

				$ws_mailBody = "エントリー情報のリセットが完了しました。\n";


				//---------------------------
				//コミット
				//---------------------------
				$this->db->commit();
			}
		}


		//メール送信
		if($formData["form_mail"] != ""){
			//$this->o_mail->SendMail($formData["form_mail"], $subject, $ws_mailBody,"");
		}

	}


	/**
	 * フォームに関連付くエントリー採番テーブルのリセットを実施
	 *
	 * @param  object  $db
	 * @param  integer $form_id
	 * @return array   $arrData
	 * @throws Exception
	 **/
	function getEntryList($db, $form_id){
        $arrData = $db->getListData('eid', 'entory_r', "form_id = ".$db->quote($form_id));
	    return $arrData;
	}


}

/**
 * メイン処理開始
 **/
$c = new form_reset($argc, $argv[1]);
$c->main();







?>
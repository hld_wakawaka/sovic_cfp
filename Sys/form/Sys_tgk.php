<?php
/*
 * TGKパッケージの複製処理を行う
 *
 */

include_once('../../application/cnf/include.php');
include_once(MODULE_DIR.'mail_class.php');

class Sys_tgk{

	/**
	 * コンストラクタ
	 */
	function __construct($argc, $pn_form_id){

		if($argc != 2){

			//--------------------------------
			//終了処理
			//--------------------------------
			$msg = "tgMdkパッケージコピーを実施しようとしましたが、コマンド起動不正のため中止しました。\n\n";
			$this->_complete($msg);
			exit;
		}

		$this->form_id =$pn_form_id;
	}

	/**
	 * メイン処理
	 */
	function main(){


		//-------------------------------
		//パッケージコピー
		//-------------------------------
		//ディレクトリ存在チェック
		if(!is_dir(TGMDK_COPY_DIR."/".$this->form_id)){
			if(!mkdir(TGMDK_COPY_DIR."/".$this->form_id, 0777 ,true)){


				//--------------------------------
				//終了処理
				//--------------------------------
				$msg = "tgMdkパッケージコピーを実施しようとしましたが、ディレクトリ作成失敗したため中止しました。\n\n";
				$msg .="フォームID:".$this->form_id."\n";
				$msg .="ディレクトリ:".TGMDK_COPY_DIR."/".$this->form_id;
				$this->_complete($msg);
				exit;
			}
			//オリジナルパッケージをコピー
			$cmd = "cp -rp ".TGMDK_ORG_DIR." ".TGMDK_COPY_DIR."/".$this->form_id;
			system($cmd);

		}


		//-------------------------------
		//定義ファイルの書き換え
		//-------------------------------
		$ws_conf = "";

		//対象の文字列を取得する
		$this->db = new DbGeneral;

		$getData = $this->db->getData("*", "form", "form_id = ".$this->form_id, __FILE__, __LINE__);
		if(!$getData){
			$msg = "定義ファイル作成のための置き換え文字列のが取得できませんでした。\nDBの内容を確認してください。\n";
			$msg .="フォームID:".$this->form_id;
			$this->_complete("");
		}



		//文字列置換対象ファイル
		$conf_file = TGMDK_COPY_DIR."/".$this->form_id."/tgMdk2/3GPSMDK.properties";

		if(file_exists($conf_file)){


		    $file = fopen($conf_file, "r");
		    flock($file, LOCK_EX);
		    while($string = fgets($file))
		    {

				$pos1 = strpos($string, "MERCHANT_CC_ID");
				$pos2 = strpos($string, "MERCHANT_SECRET_KEY");
				if($pos1 !== false){
					$buff = "MERCHANT_CC_ID                 =  ".$getData["merchant_cc_id"]."\r\n";
				}
				else if($pos2 !== false){
					$buff = "MERCHANT_SECRET_KEY            = ".$getData["merchant_sec_key"]."\r\n";
				}
				else{
					$buff = $string;
				}
				$ws_conf .= $buff;
		    }

		    flock($file, LOCK_UN);
		    fclose($file);


			//ファイルを作る
			$file = fopen($conf_file, "w");
		    flock($file, LOCK_EX);

			fwrite($file, $ws_conf);

		    flock($file, LOCK_UN);
		    fclose($file);

		}
		else{

			//--------------------------------
			//終了処理
			//--------------------------------
			$msg = "文字列置換失敗（対象ファイルが存在しないか、オープンに失敗しました。）\n\n";
			$msg .="フォームID:".$this->form_id."\n";
			$msg .="対象定義ファイル:".$conf_file;
			$this->_complete($msg);
			exit;

		}


		//--------------------------------
		//終了処理
		//--------------------------------
		$msg = "tgMdkパッケージコピーを実施しました。\n\n";
		$msg .="フォームID:".$this->form_id;
		$this->_complete($msg);

		return true;

	}

	/**
	 * 終了処理
	 */
	private function _complete($msg){

		$body = $msg;
		$subject = "【論文投稿システム　決済フォーム】tgMdkパッケージコピー実施";


		//管理者宛て送信
		$this->o_mail = new Mail();
		$this->o_mail->SendMail(SYSTEM_MAIL, $subject, $body, "");


	}

}

$c = new Sys_tgk($argc, $argv[1]);
$c->main();


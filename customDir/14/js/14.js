$(document).ready(function(){

	elementDisabled('input[name="edata27"]');
	$('select[name="edata26"]').bind("change", function(){
		var chk = $('select[name="edata26"]').val();
		if(chk == "210"){
			elementEnable('input[name="edata27"]');
			return;
        }else{
        	elementDisabled('input[name="edata27"]');
        	return;
    	}
    }).change();


	// その３
//	$('select[name^="edata32"]').bind("change", function(){
//		var chk = $('select[name^="edata32"]').val();
//		var element2 = $('input[name^="edata43"]');
//		if(chk == "210"){
//        	element2.prop("disabled", false);
//        }else{
//    		element2.prop("disabled", true);
//    	}
//    }).change();


	var edata56 = $('input[name="edata56"]');
	if(edata56.size() > 0){
    	elementDisabled('.edata67');
		edata56.bind("change", function(){
            var chk = edata56.filter(":checked").val(); // input:radio

            if(chk == 1) {
            	elementEnable('.edata67');
            	return;
            }
            if(chk == 2) {
            	elementDisabled('.edata67');
            	return;
            }
        }).change();
    }

	function elementDisabled(selector) {
		var element = $(selector);
        element.prop("disabled", true).css("background-color", "#D3D3D3");
	}

	function elementEnable(selector) {
		var element = $(selector);
        element.prop("disabled", false).css("background-color", "");
	}

});
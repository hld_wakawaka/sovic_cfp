<?php

include('../../application/cnf/include.php');
include_once('../../application/module/custom/Meeting.class.php');
include_once('../../application/module/Download.class.php');
include_once('../../application/module/mail_class.php');
include_once('../../Mng/function.php');

include_once('../../application/library/MPDF/mpdf.php');

/**
 * 一般ユーザ　会議参加フォーム
 * 	
 * @package 論文フォーム
 * @subpackage Usr
 * @author salon
 *
 */
class formbase extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function formbase(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
//ini_set("error_reporting", E_ALL);

		//-----------------------
		//インスタンス
		//-----------------------
		$this->objErr = New Validate;
		$this->db = new DbGeneral;	

		//-----------------------
		//会議ID
		//-----------------------
		$this->mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";
		$this->assign("mid", $this->mid);



		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
		



		

		//---------------------------------------------
		//管理者プレビュー?　or 利用者応募?
		//---------------------------------------------
		//管理者プレビュー画面から表示する場合
		if(isset($_REQUEST["preview"])){
			
				
				//---------------------------------
				//画面項目情報取得
				//---------------------------------
				$this->formItem = $GLOBALS["session"]->getVar("formItem");
				
				//受付期間生成
				$entry_term = $this->formItem["syear"]."/".$this->formItem["smonth"]."/".$this->formItem["sday"];
				$entry_term .="～";
				$entry_term .= $this->formItem["eyear"]."/".$this->formItem["emonth"]."/".$this->formItem["eday"];
				$this->formItem["entry_term"] = $entry_term;

				//開催日				
				$open_day = $this->formItem["oyear"]."/".$this->formItem["omonth"]."/".$this->formItem["oday"];
				$this->formItem["open_day"] = $open_day;				
				


				//ディレクトリパス
				$this->uploadDir = UPLOAD_PATH."Mng/form".$this->formItem["form_id"]."/";	//ファイルアップロードディレクトリ

				
				if($this->mid == ""){
					$head_dir = $this->uploadDir."tmp/header/tmp";
					$tmp_dir = $this->uploadDir. "tmp/tmp";
					$head_path = APP_ROOT."upload/Mng/form".$this->formItem["form_id"]."/tmp/header/tmp/";
				}
				else{
					$head_dir = $this->uploadDir.$this->mid ."/header/tmp";
					$tmp_dir = $this->uploadDir.$this->mid ."/tmp";
					$head_path = APP_ROOT."upload/Mng/form".$this->formItem["form_id"]."/".$this->mid ."/header/tmp/";
				}

				//-----------------------------------
				//ヘッダ画像取得
				//-----------------------------------
				$wa_header = Mng_function::getFiles($head_dir);
				
				
				//------------------------------------
				//添付資料取得
				//------------------------------------
				$wa_tmp_files = Mng_function::getFiles($tmp_dir);
				


			
		}
		//利用者　応募ページから表示する場合
		else{
			
			//-------------------------------
			//ログイン者情報
			//-------------------------------
			$this->assign("user_name", "システム管理者");
		
			//---------------------------------
			//画面項目情報取得
			//---------------------------------
			$this->formItem = Meeting::getMeeting($this->mid);
			
			//受付期間生成	
			$entry_term = substr($this->formItem["reception_date1"],0,4)."/".substr($this->formItem["reception_date1"],5,2)."/".substr($this->formItem["reception_date1"],8,2);
			$entry_term .="～";
			$entry_term .= substr($this->formItem["reception_date2"],0,4)."/".substr($this->formItem["reception_date2"],5,2)."/".substr($this->formItem["reception_date2"],8,2);
			$this->formItem["entry_term"] = $entry_term;

			//開催日	
			$open_day = substr($this->formItem["open_date"],0,4)."/".substr($this->formItem["open_date"],5,2)."/".substr($this->formItem["open_date"],8,2);
			$this->formItem["open_day"] = $open_day;


			//ディレクトリパス
			$this->uploadDir = UPLOAD_PATH."Mng/form".$this->formItem["form_id"]."/";	//ファイルアップロードディレクトリ


			$head_dir = $this->uploadDir.$this->mid ."/header";
			$tmp_dir = $this->uploadDir.$this->mid;
			$head_path = APP_ROOT."upload/Mng/form".$this->formItem["form_id"]."/".$this->mid ."/header/";


			//-----------------------------------
			//ヘッダ画像取得
			//-----------------------------------
			$wa_header = Mng_function::getFiles($head_dir);


			
			//----------------------------------
			//応募期間中であるか？
			//----------------------------------
			list($wb_ret, $msg) = $this->_chkTerm();
			if(!$wb_ret){
				$this->assign("head_path", $head_path);
				$this->assign("va_header", $wa_header);
				$this->assign("formItem", $this->formItem);		//画面表示項目
				$this->dispEnd($msg);
				
			}


				
				
			//------------------------------------
			//添付資料取得
			//------------------------------------
			$wa_tmp_files = Mng_function::getFiles($tmp_dir);
			
		}
		

		//------------------------
		//フォームパラメータ
		//------------------------
		$this->arrForm = GeneralFnc::convertParam($this->_init(), $_REQUEST);


		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Usr/meeting/Usr_meeting.html";
		$this->_title = "応募ページ";




		//--------------------------------
		//ヘッダ画像・添付資料
		//--------------------------------
		$this->assign("head_path", $head_path);
		$this->assign("tmp_dir", $tmp_dir);
		$this->assign("va_header", $wa_header);
		$this->assign("va_tmp_files", $wa_tmp_files);





		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){
			
			//-------------------
			//内容確認画面
			//-------------------
			case "confrim":
			
				//入力チェック
				$arrErr = $this->_check();
				
				if(count($arrErr) > 0){
					$this->assign("arrErr", $arrErr);
				}
				else{
					
					//確認画面表示
					$this->_processTemplate = "Usr/meeting/Usr_meeting_confirm.html";
					$this->_title = "応募内容確認";					
				}
			
			break;

			//--------------------
			//応募処理
			//--------------------
			case "complete":

				//------------------------
				//2重クリックチェック
				//------------------------



					//------------------------
					//トランザクション
					//------------------------
					$this->db->begin();
					
					$wb_ret = $this->insApplies();
					if(!$wb_ret){
						$this->db->rollback();
						$msg = "応募処理が正常に行われませんでした。管理者にお問い合わせください。";
						$this->dispEnd($msg);
					}



					//-------------------------
					//コミット
					//-------------------------
					$this->db->commit();


					//--------------------------
					//完了メール送信
					//--------------------------



					//画面表示
					$this->_title = "応募内容確認";
					$this->dispEnd("応募内容を受付ました。");

			 break;

			//-------------------
			//ダウンロード
			//-------------------
			case "download":
				Download::file($tmp_dir."/".$_REQUEST["down_file"],$_REQUEST["down_file"]);

		
			break;
			 
			//-------------------
			//初期表示時
			//-------------------
			default:
			

					
			 break;						
			
			
			
		}		

	

		$this->assign("formItem", $this->formItem);		//画面表示項目
		
		
		
		// 親クラスに処理を任せる
		//parent::main();



$html = $this->_smarty->fetch($this->_processTemplate);

$mpdf = new mPDF('sjis','A4','8');
$mpdf->WriteHTML($html);
$mpdf->Output(); 



		
	}	 

	/**
	 * 応募期間中チェック
	 * 
	 */
	function _chkTerm(){
		
		$start = mktime(0,0,0,substr($this->formItem["reception_date1"], 5,2), substr($this->formItem["reception_date1"], 8,2), substr($this->formItem["reception_date1"], 0,4));
		
		$end = mktime(0,0,0,substr($this->formItem["reception_date2"], 5,2), substr($this->formItem["reception_date2"], 8,2), substr($this->formItem["reception_date2"], 0,4));
		
		$today = date('Ymd');
		$today =  mktime(0,0,0,substr($today, 4,2), substr($today, 6,2), substr($today, 0,4));
		
		//比較
		if($today < $start){
			return array(false, "受付期間外のため応募できません。");
		}
		
		if($end < $today){
			return array(false, "受付は終了しました。");
		}
		
		return array(true, "");
	}	 

	/**
	 * 入力項目初期化
	 * 
	 * @return array
	 */
	function _init(){

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)）
		
		//名前
		if($this->formItem["f_name_show"] == "1"){
			$chk = "";
			if($this->formItem["f_name_need"] == "1"){
				$chk = "NULL";	
			}
			
			$key[] = array("m_name", $this->formItem["f_name"], array(),	array($chk),	"KV",	1);
		}
		
		//名前（かな）
		if($this->formItem["f_kname_show"] == "1"){
			$chk = "";
			if($this->formItem["f_kname_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_kname", $this->formItem["f_kname"], array(),	array($chk),	"KV",	1);
					
		}		

		//メールアドレス
		if($this->formItem["f_email_show"] == "1"){
			$chk = array("MAIL");
			if($this->formItem["f_email_need"] == "1"){
				array_push($chk, "NULL");
			}
			$key[] = array("m_email", $this->formItem["f_email"], array(),	$chk,	"a",	1);
		}

		//郵便番号
		if($this->formItem["f_zip_show"] == "1"){
			$chk = "";
			if($this->formItem["f_zip_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_zip", $this->formItem["f_zip"], array(),	array($chk),	"a",	1);
		}

		//住所
		if($this->formItem["f_address_show"] == "1"){
			$chk = "";
			if($this->formItem["f_address_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_address", $this->formItem["f_address"], array(),	array($chk),	"a",	1);
						
		}

		//電話番号
		if($this->formItem["f_tel_show"] == "1"){
			$chk = "";
			if($this->formItem["f_tel_show"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_tel", $this->formItem["f_tel"], array(),	array($chk),	"a",	1);
						
		}

		//内線
		if($this->formItem["f_extension_show"] == "1"){
			$chk = "";
			if($this->formItem["f_extension_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_extension", $this->formItem["f_extension"], array(),	array($chk),	"a",	1);
						
		}		

		//FAX
		if($this->formItem["f_fax_show"] == "1"){
			$chk = "";
			if($this->formItem["f_fax_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_fax", $this->formItem["f_fax"], array(),	array($chk),	"a",	1);
						
		}
		
		//その他郵便番号
		if($this->formItem["f_zip2_show"] == "1"){
			$chk = "";
			if($this->formItem["f_zip2_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_zip2", $this->formItem["f_zip2"], array(),	array($chk),	"a",	1);
					
		}		

		//その他住所
		if($this->formItem["f_address2_show"] == "1"){
			$chk = "";
			if($this->formItem["f_address2_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_address2", $this->formItem["f_address2"], array(),	array($chk),	"a",	1);
			
		}

		//その他電話番号
		if($this->formItem["f_tel2_show"] == "1"){
			$chk = "";
			if($this->formItem["f_tel2_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_tel2", $this->formItem["f_tel2"], array(),	array($chk),	"a",	1);
					
		}
		
		//その他電話番号の内線
		if($this->formItem["f_extension2_show"] == "1"){
			$chk = "";
			if($this->formItem["f_extension2_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_extension2", $this->formItem["f_extension2"], array(),	array($chk),	"a",	1);
									
		}					

		//その他FAX
		if($this->formItem["f_fax2_show"] == "1"){
			$chk = "";
			if($this->formItem["f_fax2_need"] == "1"){
				$chk = "NULL";	
			}
			$key[] = array("m_fax2", $this->formItem["f_fax2"], array(),	array($chk),	"a",	1);
			
		}


		return $key;	

	}
		 
	 
	/**
	 * 入力チェック
	 * 
	 * @return array
	 */
	function _check(){
		
		$this->objErr->check($this->_init(), $this->arrForm);
		return $this->objErr->_err;
							
	}	 

	 
	/**
	 * 応募内容登録処理
	 * 
	 * @return bool
	 */
	function insApplies(){
		
		$key = $this->_init();
		
		foreach($key as $val) {
			if($val[5] == 1) {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
		}
		
		$param["form_id"] = $this->formItem["form_id"];
		$param["mid"] = $this->mid;
		
		$rs = $this->db->insert("applies", $param, __FILE__, __LINE__);
		if(!$rs) {
			return false;
		}		
		

		return true;			
		
	}

	/**
	 * 終了処理
	 * 
	 * @access public
	 * @param  string
	 */
	function dispEnd($ps_msg){

		$this->_processTemplate = "Usr/meeting/Usr_meeting_complete.html";
		$this->assign("msg", $ps_msg);		
		
		// 親クラスに処理を任せる
		parent::main();
		exit;
		
	}		
}

/**
 * メイン処理開始
 **/

$c = new formbase();
$c->main();







?>
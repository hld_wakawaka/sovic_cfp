<?php

include('../application/cnf/include.php');

/**
 * 利用者ログイン
 * 
 * @author salon
 *
 */
class login16 extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function login(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
//	ini_set("error_reporting", E_ALL);
		
		// 表示HTMLの設定
		$this->_processTemplate = "Usr/Usr_Basic16.html";

		$this->arrForm = $_REQUEST;
		
		if(!$GLOBALS["form"]->isForm) Error::showErrorPage("ページが存在しません。");
		
		$this->_title = $GLOBALS["form"]->formData["lang"] == "2" ? "Login" : "ログイン";
		
		// 受付期間チェック
		//$GLOBALS["form"]->checkValidData_Ex();
		
		if(!isset($this->arrForm["mode"])) $this->arrForm["mode"] = "";
		$arrErr = array();
		
		switch($this->arrForm["mode"]) {
			
			case "login":
			
				$arrErr = $this->_check();
				
				if(count($arrErr) > 0) {
					break;
				}
				
				$err = $this->LoginCheck();
				
				if(!empty($err)) {
					//エラーの場合
					$arrErr = $err;
					$GLOBALS["log"]->write_log("euser_login failed", $_REQUEST["login_id"]);
					
				} else {
					
					$GLOBALS["log"]->write_log("euser_login success", $_REQUEST["login_id"]);
					$url = "./form/Usr_entry.php?form_id=".$GLOBALS["form"]->formData["form_id"];
					header("location: ".$url);
					
					exit;
				
				}
				
				break;
				
			default:
				
				//$this->_get();

				$GLOBALS["session"]->unsetVar("auth");
				$this->assign("msg", $GLOBALS["msg"]["msg_login"]);
				
				break;
		}
		
		$this->assign("arrErr", $arrErr);
		$this->assign("form_id", $GLOBALS["form"]->formData["form_id"]);


		// 親クラスに処理を任せる
		parent::main();
	
	}
	
	function _check() {
		
		$objErr = New Validate;
	
		if(!$objErr->isNull($this->arrForm["login_id"])) {
			$objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], "Registration No."));
		}
		if(!$objErr->isNull($this->arrForm["login_passwd"])) {
			$objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], "Abstract Submission Code"));
		}
		
		return $objErr->_err;
		
	}
	
	/**
	 * ログインチェック
	 */
	function LoginCheck() {
		
		if(strpos(__FILE__, "testhp.jp") !== false) {
			$dsn = "pgsql://evt:ZNsLupwY@localhost/dev_evt";
		} else {
			$dsn = "pgsql://evt:ZNsLupwY@localhost/evt2";
		}
		
		$CFPDB = new MyDB($dsn);
		$CFPDB->connect();
		
		$sql = "select * from entory_r " .
				"where form_id = 113 " .
				"and e_user_id = ? " .
				"and del_flg = 0";
				
		$rs = $CFPDB->query($sql, array($this->arrForm["login_id"]));
		
		if(!$rs) {
			$arrErr[] = "Incorrect login Number or Code";
		} else {
			$data = $rs->fetchRow(DB_FETCHMODE_ASSOC, 0);
			if($data["e_user_passwd"] != $this->arrForm["login_passwd"]) {
				$arrErr[] = "Incorrect login Number or Code";
			} else {
				$GLOBALS["session"]->setVar("auth", "1");
			}
		}
		
		return $arrErr;
		
	}
	 

	

}

/**
 * メイン処理開始
 **/

$c = new login16();
$c->main();







?>
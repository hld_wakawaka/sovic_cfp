<?php

include('../application/cnf/include.php');

/**
 * 利用者ログイン
 * 
 * @author salon
 *
 */
class login extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function login(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
	ini_set("error_reporting", E_ALL);
		
		// 表示HTMLの設定
		$this->_processTemplate = "Usr/Usr_login.html";
		
//		$this->assign("isTop", true);

		$this->arrForm = $_REQUEST;
		
		if(!$GLOBALS["form"]->isForm) Error::showErrorPage("ページが存在しません。");
		
		$this->_title = $GLOBALS["form"]->formData["lang"] == "2" ? "Login" : "ログイン";
		
		// 受付期間チェック
		$GLOBALS["form"]->checkValidDate();
		
		if(!isset($this->arrForm["mode"])) $this->arrForm["mode"] = "";
		$arrErr = array();
		
		switch($this->arrForm["mode"]) {
			
			case "login":
			
				$arrErr = $this->_check();
				
				if(count($arrErr) > 0) {
					break;
				}
				
				//ログイン処理
				$err = LoginEntry::doLogin($GLOBALS["form"]->formData["lang"], $_REQUEST["login_id"], $_REQUEST["password"]);
				
				if(!empty($err)) {
					//エラーの場合
					$arrErr[] = $err;
					$GLOBALS["log"]->write_log("euser_login failed", $_REQUEST["login_id"]);
					
				} else {
					
					/*
					if($this->arrForm["save"] == "1") {
						$this->_insert();
					} else {
						$this->_del();
					}*/
					
					$GLOBALS["log"]->write_log("euser_login Success", $GLOBALS["entryData"]["eid"]);
					
					$url = "./form/Usr_entry.php?form_id=".$GLOBALS["form"]->formData["form_id"];
					header("location: ".$url);
					
					exit;
				
				}
				
				break;
			case "logout":
			
				$eid = $GLOBALS["entryData"]["eid"];
			
				LoginEntry::doLogout();
				
				unset($this->arrForm);
				$this->arrForm = array();
				
				/*
				if (isset($_COOKIE[session_name()])) {
					setcookie(session_name(), '', time()-42000, '/');
				}
				*/
				
				//$this->_get();

				$this->_title = $GLOBALS["form"]->formData["lang"] == "2" ? "Logout" : "ログアウト";
				
				$GLOBALS["log"]->write_log("euser_logout", $eid);
				
				$this->assign("msg", $GLOBALS["msg"]["msg_logout"]);

				
				break;
				
			default:
				
				//$this->_get();

				if(!$GLOBALS["session"]->getVar("startmsg")) {
					$msg = $GLOBALS["msg"]["msg_logout"];
				} else {
					$msg = $GLOBALS["session"]->getVar("startmsg");
					$GLOBALS["session"]->unsetVar("startmsg");
				}
				
				$this->assign("msg", $msg);
				
				break;
		}
		
		$this->assign("arrErr", $arrErr);


		// 親クラスに処理を任せる
		parent::main();
	
	}
	
	function _check() {
		
		$objErr = New Validate;
	
		if(!$objErr->isNull($this->arrForm["login_id"])) {
			$objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $GLOBALS["msg"]["login_id"]));
		}
		if(!$objErr->isNull($this->arrForm["password"])) {
			$objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $GLOBALS["msg"]["login_passwd"]));
		}
		
		return $objErr->_err;
		
	}
	 

	

}

/**
 * メイン処理開始
 **/

$c = new login();
$c->main();







?>
<?php

include('../../application/cnf/include.php');
include('./function.php');
include_once(MODULE_DIR.'mail_class.php');
include_once(MODULE_DIR.'Download.class.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once(MODULE_DIR.'custom/Entry.class.php');

include_once(TGMDK_ORG_DIR.'/crypt.class.php');

if($_SERVER["REMOTE_ADDR"] == "122.215.116.48"){
    ini_set("error_reporting", E_ALL);
    ini_set("display_errors", 1);
}


/**
 * 論文エントリーフォーム
 *
 *
 * @subpackage Usr
 * @author salon
 *
 */
class Usr_Entry extends ProcessBase {

	// アップロードファイルの添付IDと添え字
	var $arrfile = array(
					"51" => "a",
					"52" => "b",
					"53" => "c"
				);


	/**
	 * コンストラクタ
	 */
	function Usr_Entry(){

		parent::ProcessBase();
		//-----------------------------
		//初期化
		//-----------------------------
		$this->block     = "1";
		$this->arrErr    = array();
		$this->admin_flg = "";				//管理者モードフラグ
		$this->inc_dir   = "../include/";		//インクルードパス
		$this->onload    = "";      // onload
		$this->yen_mark  = "JPY";   // 円マーク
		$this->need_mark = $GLOBALS["msg"]["need_mark"];  // 必須マーク
		/**
		 * 共著者のスタート地点
		 *    0:筆頭者から利用
		 *    1:共著者の1人目から利用
		 */
		$this->author_start_index = 0;

		//------------------------------
		//インスタンス生成
		//------------------------------
		$this->o_form = new Form();

		$this->o_entry = new Entry();
		$this->objErr = New Validate;
		$this->db = new DbGeneral;
		$this->o_itemini = new item_ini();
		$this->o_mail = new Mail();

		//---------------------------------------------2012.2.29
		// 強制的にメンテナンスとするフォームIDの配列
		//---------------------------------------------
		$this->mente_form = array();

		//---------------------------------------------2012.3.3
		// メールを送信しないフォーム
		//---------------------------------------------
		$this->sendmail_not = array();

        // jQuery読み込みフラグ
        $this->useJquery = true;

        // 必要クラスをinclude
        $ex = glob(MODULE_DIR.'entry_ex/*.php');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                include_once($script);
            }
        }
	}


	/**
	 * 簡易認証
	 */
	function pf_auth() {

		//$GLOBALS["session"]->unsetVar("auth");
		//$GLOBALS["session"]->unsetVar("auth_pas");

		$filename = ROOT_DIR."auth/form".$this->o_form->formData["form_id"];

		if(!file_exists($filename)) return;

		$phrase = file_get_contents(trim($filename));

		$auth_flg = $GLOBALS["session"]->getVar("auth_sysb");

		$_REQUEST["authpw"] = isset($_REQUEST["authpw"]) ? $_REQUEST["authpw"] : "";

		if($auth_flg == "1") {
			if($phrase == $GLOBALS["session"]->getVar("auth_pas_sysb")) return;
		}

		if($_REQUEST["authpw"] != "") {
			$phrase = file_get_contents(trim($filename));

			if($phrase == $_REQUEST["authpw"]) {
				$GLOBALS["session"]->setVar("auth_sysb", "1");
				$GLOBALS["session"]->setVar("auth_pas_sysb", $_REQUEST["authpw"]);
				$_REQUEST["mode"] = "";
				return;
			} else {
				$this->assign("msg", "認証に失敗しました。（Password Error)");
			}
		}

		$this->assign("form_id", $this->o_form->formData["form_id"]);
		$this->_processTemplate = "Usr/Usr_Basic.html";

		parent::main();
		exit;

	}

	/**
	 * メイン処理
	 */
	function main(){

		//-----------------------
		//ログ
		//-----------------------
		Usr_function::accessLog($_REQUEST);

		//-----------------------
		//フォームID
		//-----------------------
		$this->form_id = $this->o_form->formData["form_id"];
		if($this->form_id == ""){
			Error::showErrorPage("ページが存在しません。<br />Page not found.");
		}

		// 簡易認証
		$this->pf_auth();

		// セッション切れチェック
		$this->pf_checkSession();

		//----------------------
		//リクエスト取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";			// モード
		if($ws_action == "") $ws_action = "default";
		$this->wk_block = isset($_REQUEST["block"]) ? $_REQUEST["block"] : "1";		// ブロック

		$this->showMenteform();			// メンテナンス中
		$this->createUploadDir();		// ファイルアップロードディレクトリ
		$this->AuthAction();			// 管理者モードか一般モード判断。モードによる初期処理
		$this->chkSession($ws_action);			// セッション切れチェック
		$this->setFormData();			// フォームデータの取得・表示テンプレートの設定
		$this->setFormIni();			// フォーム項目の取得
        $this->sortFormIni($this); // フォーム項目の表示順序変更
		$this->commonAssign();			// 共通アサイン
		$this->setTemplate();
		$this->createPageflow();		// フロー表示


        // 二次期間中であれば編集不可の項目をデータベースより上書き
        $this->getEntryOverrideTerm2();


		//---------------------------------
		//アクション別処理
		//---------------------------------
		$actionName = $ws_action."Action";
        if(method_exists($this, $actionName)){
            $this->$actionName();
        }else{
            $this->defaultAction();
        }

		$this->workDir = $GLOBALS["session"]->getVar("workDir");

		$this->assign("onload",     $this->onload);
		$this->assign("workDir",    $this->workDir);
		$this->assign("block",      $this->block);
		$this->assign("arrErr",     $this->arrErr);	//エラー配列
		$this->assign("admin_flg",  $this->admin_flg);	//エラー配列
        $this->assign("arrItemData",$this->arrItemData);
        $this->assign("formItem",   $this->itemData);        //画面表示項目
        $this->assign("group3use",  $this->group3use);  // グループ3見出し表示フラグ
        $this->assign("group3item", $this->group3item); // グループ3のグループ分け
		$this->assign("need_mark",  $this->need_mark);	//必須マーク
        $this->assign("yen_mark",   $this->yen_mark);
        $this->assign("author_start_index", $this->author_start_index);

        // [cfp-007]「見積No1」日英共通ー共著者Affiliationの入力仕様変更
//         $authorCounts = count($this->arrItemData[1][30]['select']);

        // 最大共著者数を30に設定
        $this->NumofCoAuthors = 30;
        $this->assign("NumofCoAuthors", $this->NumofCoAuthors); //最大共著者数を30に設定


        // 親クラスをする前の前処理
        if(method_exists($this, 'premain')){
            $this->premain();
        }

		// 親クラスに処理を任せる
		parent::main();

        // 開発用デバッグ関数
        $this->developfunc();
	}

	function defaultAction() {

		$this->block = "1";

		//セッション情報クリア
		$GLOBALS["session"]->unsetVar("form_param1");
		$GLOBALS["session"]->unsetVar("form_param2");
		$GLOBALS["session"]->unsetVar("form_param3");
		$GLOBALS["session"]->unsetVar("kyouID");		//共著者ID
		$GLOBALS["session"]->unsetVar("SS_USER");		//応募者ログイン情報
		$GLOBALS["session"]->unsetVar("workDir");
		$GLOBALS["session"]->unsetVar("ss_order_no");		//登録No.

		if($this->eid != "") {

			// エントリーデータ取得
			$this->getEntry();

		} else {

			// NEWにディレクトリを作成し、セッションに格納する
			$this->createWorkDir();

			if($this->formdata["agree"] == "3" || $this->formdata["agree"] == "4"){
				//別画面で同意画面を表示する
				if($this->formdata["lang"] == "1"){
					$this->_title = $this->formdata["form_name"]." 同意書";
				}
				else{
					$this->_title = $this->formdata["form_name"]." Privacy Policy";
				}
				$this->_processTemplate = "Usr/form/Usr_entry_agree.html";
			}

			//初期値
			if($this->formdata["agree"] != "0"){
				$this->arrForm["agree"] = "";
			}


			if(!$GLOBALS["session"]->getVar("startmsg")) {
			} else {
				$this->arrErr = $GLOBALS["session"]->getVar("startmsg");
				$GLOBALS["session"]->unsetVar("startmsg");
			}

		}

	}

	function pageAction() {

		// フォームパラメータ
		$getFormData= GeneralFnc::convertParam($this->_init($this->wk_block), $_POST);

		// 共著者の所属機関
		$ret_param = $this->_make35param($this->wk_block);
		if(count($ret_param) > 0){
			$getFormData = array_merge($getFormData, $ret_param);
		}

		// チェック用の変数に入れる
		$this->arrParam = $getFormData;

		// エラーチェック
		$this->arrErr = $this->_check();

		//セッションにページパラメータをセットする
		$GLOBALS["session"]->setVar("form_param".$this->wk_block, $this->arrParam);

		// セッションパラメータ取得
		$this->getDispSession();

		// エラーがある場合
		if(count($this->arrErr) > 0){

			//エラーを自ページに表示
			$this->block = $this->wk_block;
			//$this->arrForm = $getFormData;

			return;
		}

		$blockaction = "pageAction".$this->wk_block;

		$this->$blockaction();

		return;


	}

	function pageAction1() {

		$fix_flg = "";

		//3ページ目を使用
		if($this->formdata["group3_use"] != "1"){
			$this->block = "3";
			$fix_flg = "1";

			$this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_j.html" : "Usr/form/Usr_entry_e.html";

		}


		//2ページ目使用
		if($this->formdata["group2_use"] != "1"){

			// 所属機関が有る場合は、必ず2ページ目を表示
			if($this->itemData[32]["disp"] != "1"){
				if(isset($this->arrParam["edata31"]) && $this->arrParam["edata31"] != ""){


					$this->block = "2";

					// 1番目の共著者に登録者を入れる
					$this->arrForm["edata330"] = isset($this->arrForm["edata1"]) ? $this->arrForm["edata1"] : "";
					$this->arrForm["edata340"] = isset($this->arrForm["edata2"]) ? $this->arrForm["edata2"] : "";
					$this->arrForm["edata350"] = isset($this->arrForm["edata3"]) ? $this->arrForm["edata3"] : "";
					$this->arrForm["edata360"] = isset($this->arrForm["edata4"]) ? $this->arrForm["edata4"] : "";
					$this->arrForm["edata370"] = isset($this->arrForm["edata5"]) ? $this->arrForm["edata5"] : "";
					$this->arrForm["edata380"] = isset($this->arrForm["edata6"]) ? $this->arrForm["edata6"] : "";
					$this->arrForm["edata390"] = isset($this->arrForm["edata7"]) ? $this->arrForm["edata7"] : "";
					$this->arrForm["edata400"] = isset($this->arrForm["edata25"]) ? $this->arrForm["edata25"] : "";
					$this->arrForm["edata410"] = isset($this->arrForm["edata8"]) ? $this->arrForm["edata8"] : "";
					$this->arrForm["edata420"] = isset($this->arrForm["edata9"]) ? $this->arrForm["edata9"] : "";
					$this->arrForm["edata610"] = isset($this->arrForm["edata57"]) ? $this->arrForm["edata57"] : "";
					$this->arrForm["edata620"] = isset($this->arrForm["edata7"]) ? $this->arrForm["edata7"] : "";

					$fix_flg = "1";
				}
			}
		}

		//上記のいずれも該当しなかった場合
		if($fix_flg == ""){
			$this->block = "4";
			$this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_confirm_j.html" : "Usr/form/Usr_entry_confirm_e.html";

		}

		return;

	}

	function pageAction2() {

		$fix_flg = "";

		//3ページ目を使用
		if($this->formdata["group3_use"] != "1"){
			$this->block = "3";
			$this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_j.html" : "Usr/form/Usr_entry_e.html";

			$fix_flg = "1";
		}

		//上記のいずれも該当しなかった場合
		if($fix_flg == ""){
			$this->block = "4";
			//$this->_processTemplate = "Usr/form/Usr_entry_confirm_j.html";
			$this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_confirm_j.html" : "Usr/form/Usr_entry_confirm_e.html";

		}

		return;

	}

	function pageAction3() {

		//確認画面へ
		$this->_confirm();

		return;

	}


	function confirmAction() {

		$this->_confirm();

		return;

	}

	function completeAction() {

		//------------------------------
		//二重登録チェック
		//------------------------------
		$rl = new Reload();
		if($rl->isReload()) {
			$msg = "既に登録は完了しています。";
			$this->complete($msg);

			return;
		}

		//----------------------------------------------
		//セッションパラメータ取得
		//----------------------------------------------
		$this->arrForm = array_merge((array)$GLOBALS["session"]->getVar("form_param1"), (array)$GLOBALS["session"]->getVar("form_param2"));
		if(empty($this->arrForm)){
			Error::showErrorPage("*セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
		}

		$this->arrForm = array_merge($this->arrForm, (array)$GLOBALS["session"]->getVar("form_param3"));

		if(empty($this->arrForm)){
			Error::showErrorPage("※セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
		}

		$this->arrForm = array_merge($this->arrForm, (array)$GLOBALS["session"]->getVar("form_parampay"));
		if(empty($this->arrForm)){
			Error::showErrorPage("セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
		}

		// 新規登録
		if($this->eid == ""){

			// ロック
			$this->lockStart();

			// トランザクション開始
			$this->db->begin();

			// エントリーIDの取得	$this->entry_numberの取得　エントリーIDを1UP
			$this->getEntryNumber();

			// ファイルアップ用に日時をセット
			$this->udate = date("YmdHis");

			// ファイルアップロード
			if(!$this->TempUploadAction()) {
                $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
                $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
				$this->db->rollback();
				$this->lockEnd();
                $this->complete($msg);
				return;
			}

			// 登録
			list($wb_ret, $user_id, $passwd) = $this->insertEntry();
			$this->eid = $this->ins_eid;

			//　ファイルの移動
			if(!$this->MoveFileAction()) {
                $msg = "ERROR:ファイルの移動に失敗しました。";
                $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
				$this->db->rollback();
				$this->lockEnd();
                $this->complete("ファイルの移動に失敗しました。");
				return;
			}

			//メール送信
			$this->sendRegistMail($user_id, $passwd);

			//完了メッセージ
			$this->msg = $this->replaceMsg($this->formWord["word1"]);

			// コミット
			$this->db->commit();
			$this->lockEnd();

			$this->showComplete();
			exit;

		}

		// 編集

		// トランザクション
		$this->db->begin();

		// ファイルアップロード
		if(!$this->TempUploadAction()) {
            $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
            $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
			$this->db->rollback();
			$this->lockEnd();
            $this->complete($msg);
			return;
		}

		//応募者のログイン情報をセッションから復元
		$ss_user = $GLOBALS["session"]->getVar("SS_USER");
		$user_id = $ss_user["e_user_id"];
		$passwd = $ss_user["e_user_passwd"];

		// 更新処理
		$wb_ret = $this->updateEntry();

		//　ファイルの移動
		if(!$this->MoveFileAction()) {
            $msg = "ERROR:ファイルの移動に失敗しました。";
            $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
			$this->db->rollback();
			$this->lockEnd();
            $this->complete("ファイルの移動に失敗しました。");
			return;
		}

		//メール送信
		$this->sendEditMail($user_id, $passwd);

		//完了メッセージ
		$this->msg = $this->replaceMsg($this->formWord["word2"]);

		$this->db->commit();
		$this->showComplete();

		exit;
	}

	function replaceMsg($msg) {
        $form_id   = $this->form_id;
        $eid       = $this->eid;
        $exec_type = 2;
        return Usr_function::wordReplace($msg, $this, compact("form_id", "eid", "exec_type"));
	}

	/*
	 * 完了ページの表示
	 */
	function showComplete() {

		//----------------------------------
		//セッション情報クリア
		//----------------------------------
		$GLOBALS["session"]->unsetVar("form_param1");
		$GLOBALS["session"]->unsetVar("form_param2");
		$GLOBALS["session"]->unsetVar("form_param3");
		$GLOBALS["session"]->unsetVar("kyouID");		//共著者ID
		$GLOBALS["session"]->unsetVar("SS_USER");		//応募者ログイン情報
		$GLOBALS["session"]->unsetVar("workDir");
		$GLOBALS["session"]->unsetVar("ss_order_no");		//登録No.

		$GLOBALS["session"]->unsetVar("isLoginEntry");
		$GLOBALS["session"]->unsetVar("entryData");
		$GLOBALS["session"]->unsetVar("auth");
		$GLOBALS["session"]->unsetVar("start");       //セッション切れチェック項目

		$this->assign("msg", $this->msg);
		$this->_processTemplate = "Usr/form/Usr_entry_complete.html";

		parent::main();
		exit;

	}



    /** 本番ディレクトリにファイル移動を行う準備 */
	function TempUploadAction() {
		$this->wk_moto = array();
		$workDir = $GLOBALS["session"]->getVar("workDir");
		if($workDir == "") return false;

		foreach($this->arrfile as $item_id => $n) {
			//アップロードするファイルが存在する場合、移動
			if(!isset($this->arrForm["n_data".$item_id]  ) || empty($this->arrForm["n_data".$item_id]  )) $this->arrForm["n_data".$item_id]   = "";
			if(!isset($this->arrForm["hd_file".$item_id] ) || empty($this->arrForm["hd_file".$item_id] )) $this->arrForm["hd_file".$item_id]  = "";
			if(!isset($this->arrForm["file_del".$item_id]) || empty($this->arrForm["file_del".$item_id])) $this->arrForm["file_del".$item_id] = "0";

			$this->wk_moto[$item_id] = "";
			if($this->arrForm["hd_file".$item_id] != "" && $this->arrForm["file_del".$item_id] == "0") {
				$this->wk_moto[$item_id] = $this->arrForm["hd_file".$item_id];
			}
		}
		return true;
	}



    // 本番ディレクトリへ移動
    function MoveFileAction() {
        // リネイム後のファイル名.[extension]
        $this->wk_file=array();

        // 作成したディレクトリへ移動する
        $workDir = $workDir = $GLOBALS["session"]->getVar("workDir");
        $afterDir = $this->uploadDir.$this->eid;

        if(!is_dir($afterDir)){
            mkdir($afterDir, 0777, true);
            chmod($afterDir, 0777);
        }

        $afterDir .= "/";
        $workDir .= "/";

        // 削除が有る場合
        if(isset($this->wk_del)) {
            foreach($this->wk_del as $key=>$file) {
                if(file_exists($afterDir.$file)) {
//                    unlink($afterDir.$file);
                }
            }
        }

        // ファイルをアップロードしていない場合
        if(count($this->wk_saki) == 0) return true;

        // 本アップロード処理
        foreach($this->wk_moto as $item_id => $mv_file){
            if($mv_file == "") continue;
            if(!isset($this->wk_saki[$item_id])) continue;

            if(is_file($workDir.$mv_file)){
                $from = $workDir.$mv_file;
                $to   = $afterDir.$this->wk_saki[$item_id];
                if(!copy($from, $to)){
                    $GLOBALS["log"]->write_log($_REQUEST["mode"], "ファイルの移動に失敗（".$from."→".$to.")");
                    return false;
                }
            }
        }

        // アップロード履歴を残す
        $list = explode('/', trim($workDir, '/'));
        $histryDir = $afterDir.$list[count($list)-1]."/";
        if(!is_dir($histryDir)){
            mkdir($histryDir, 0777, true);
            chmod($histryDir, 0777);
        }

        $files = glob($workDir."*");
        if(is_array($files)){
            foreach($files as $_key => $from){
                $to = $histryDir.pathinfo($from, PATHINFO_BASENAME);
                if(!copy($from, $to)){
                    $GLOBALS["log"]->write_log($_REQUEST["mode"], "アップロード履歴の作成に失敗 （".$from."→".$to.")");
                }
            }
        }
        return true;
    }


	function getEntry() {

		$workDir = $this->uploadDir.$this->eid."/".date("YmdHis");

		//ファイルアップロード用　ワークディレクトリ生成
		if(!is_dir($workDir)){
			mkdir($workDir, 0777, true);
			chmod($workDir, 0777);
		}
		//セッションにワークディレクトリ名をセット
		$GLOBALS["session"]->setVar("workDir", $workDir);

		//応募データ取得
		$wa_entrydata = $this->o_entry->getRntry_r($this->db, $this->eid, $this->form_id);
		if(!$wa_entrydata){
			Error::showErrorPage("応募情報の取得に失敗しました。");
		}

		// 共著者有無を保存しておく
		$this->arrForm["pre_edata29"] = $this->arrForm["edata29"];

		//応募者のID・パスワード
		$ss_entry_user = array("e_user_id" => $wa_entrydata["e_user_id"], "e_user_passwd" => $wa_entrydata["e_user_passwd"]);
		$GLOBALS["session"]->setVar("SS_USER", $ss_entry_user);

		//共著者データ取得
		$wa_aff = $this->o_entry->getRntry_aff($this->db, $this->eid);

		//----------------------------
		//管理者モードの場合
		//----------------------------
		if($this->admin_flg != ""){
			$wk_user_id["e_user_id"] =  $wa_entrydata["e_user_id"];
			$GLOBALS["session"]->setVar("isLoginEntry", true);
			$GLOBALS["session"]->setVar("entryData", $wk_user_id);
		}

		//----------------------
		//1ページ目データ
		//----------------------
		$start = 1;
		$end = 32;

		for($index=$start; $index < $end; $index++){

			if($index != 26 && $index != 27 && $index != 28){
				$this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
			}
			else{

				//任意項目
				$this->_default_arrFormNini(26, 29, $wa_entrydata);
			}
		}

		$this->_default_arrFormNini(63, 65, $wa_entrydata);

		//任意項目6～20
		$this->_default_arrFormNini(69, 84, $wa_entrydata);

		//任意項目21～50
		$this->_default_arrFormNini(115, 145, $wa_entrydata);

		//英語フォーム固有の項目
		$start = 57;
		$end = 61;
		for($index=$start; $index < $end; $index++){
			$this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
		}
		//国名リストボックスの値
		$this->arrForm["edata114"] = $wa_entrydata["edata114"];

		$this->arrForm["chkemail"] = $wa_entrydata["edata25"];

		$GLOBALS["session"]->setVar("form_param1", $this->arrForm);

		//----------------------
		//2ページ目データ
		//----------------------
		$this->arrForm = "";

		//共著者の登録がある場合
		if($wa_aff){
			foreach($wa_aff as $a_key => $data){
				$start = 32;
				$end = 46;

				for($index=$start; $index < $end; $index++){

					if($index != 43 && $index != 44 && $index != 45){

						//共著者の所属機関
						if($index == 32){
							$wk_checked  = explode("|", $data["edata".$index]);
							foreach($wk_checked as $chk_val){
								$this->arrForm["edata".$index.$a_key.$chk_val] = $chk_val;
							}
						}
						else{
							$this->arrForm["edata".$index.$a_key] = $data["edata".$index];
						}

					}
					else{

						//任意項目生成
						$this->_default_arrFormChosyaNini(43, 46, $data, $a_key);
					}
				}

				//英語フォーム固有の項目
				$start = 61;
				$end = 63;

				for($index=$start; $index < $end; $index++){
					$this->arrForm["edata".$index.$a_key] = $data["edata".$index];

				}


				//共著者ID
				$kyouid[$a_key] = $data["id"];

				//任意項目生成
				$this->_default_arrFormChosyaNini(65, 68, $data, $a_key);
				$this->_default_arrFormChosyaNini(84, 99, $data, $a_key);

			}
			$GLOBALS["session"]->setVar("form_param2", $this->arrForm);
			$GLOBALS["session"]->setVar("kyouID", $kyouid);

		}


		//----------------------
		//3ページ目データ
		//----------------------
		$this->arrForm = "";
		$start = 46;
		$end = 57;

		for($index=$start; $index < $end; $index++){

			if($index != 54 && $index != 55 && $index != 56){
				$this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
			}
			else{

				$this->_default_arrFormNini(54, 57, $wa_entrydata);
			}

			//添付資料
			if($index == 51 || $index == 52 || $index == 53){

				//--------------------------
				//ワークディレクトリにファイルを移動
				//--------------------------
				if(is_file($this->uploadDir.$this->eid."/".$wa_entrydata["edata".$index])){
					if (!copy($this->uploadDir.$this->eid."/".$wa_entrydata["edata".$index], $workDir."/".$wa_entrydata["edata".$index])) {
						$this->complete("ファイルのコピーに失敗しました。");
					}
				}
				$this->arrForm["hd_file".$index] = $wa_entrydata["edata".$index];
				$this->arrForm["n_data".$index] = $wa_entrydata["edata".$index];
			}
		}

		$this->_default_arrFormNini(67, 69, $wa_entrydata);
		$this->_default_arrFormNini(99, 114, $wa_entrydata);

		$this->_default_arrFormNini(145, 160, $wa_entrydata);

		$GLOBALS["session"]->setVar("form_param3", $this->arrForm);

		//-------------------------------
		//セッション取得
		//-------------------------------
		$this->getDispSession();

		return;

	}


    function getEntryOverrideTerm2(){
        // 編集不可の項目をデータベースより上書き
        // 編集不可の項目一覧
        // 二次期間中
        if($this->_chkTerm2() === true && $this->eid != "" && $this->formdata["type"] == 1){
            // グループ1, グループ3の編集項目
            $column = "*";
            $from = "array_to_string(ARRAY(select 'edata' || form_item.item_id from form_item join item_ini on form_item.item_id = item_ini.item_id where form_id =".$this->db->quote($this->form_id)." and item_pview3 = 1 and group_id in (1,3)), ',') as edata";
            $res = $this->db->getData($column, $from, NULL, __FILE__, __LINE__);
            if(isset($res['edata']) && strlen($res['edata']) > 0){
                $column  = $res['edata'];
                $from    = 'entory_r';
                $where   = array();
                $where[] = 'eid = '.$this->db->quote($this->eid);
                $edata = $this->db->getListData($column, $from, $where, NULL, -1, -1, __FILE__, __LINE__);
                if(isset($edata[0])){
                    foreach($edata[0] as $_key => $_val){
                        $_REQUEST[$_key] = $_val;
                        if(in_array($_key, array('edata51', 'edata52', 'edata53'))){
                            $key = str_replace("edata", "hd_file", $_key);
                            $_REQUEST[$key] = $_val;
                        }
                    }
                }
            }

            // グループ2の登録情報を復元
            if(intval($this->formdata["group2_use"]) != 0) return;

            // グループ2の編集項目
            $column = "*";
            $from = "array_to_string(ARRAY(select 'edata' || form_item.item_id from form_item join item_ini on form_item.item_id = item_ini.item_id where form_id =".$this->db->quote($this->form_id)." and item_pview3 = 1 and group_id = 2), ',') as edata";
            $res = $this->db->getData($column, $from, NULL, __FILE__, __LINE__);
            if(!$res) return;

            //共著者データ取得
            $wa_aff = $this->o_entry->getRntry_aff($this->db, $this->eid);
            if(!$wa_aff) return;

            // getEntryと同等の処理を行うことでグループ2の編集不可が可能
            $edata = explode(',', $res['edata']);
            foreach($wa_aff as $a_key => $data){
                foreach($edata as $_key => $_edata){
                    $item_id = substr($_edata, 5); // edataXXのXX

                    // チェックボックス
                    if($item_id == 32 || intval($this->arrItemData[2][$item_id]['item_type']) == 3){
                        $wk_checked  = explode("|", $data["edata".$item_id]);
                        foreach($wk_checked as $chk_val){
                            $_REQUEST["edata".$item_id.$a_key.$chk_val] = $chk_val;
                        }

                    // その他
                    }else{
                        $_REQUEST["edata".$item_id.$a_key] = $data["edata".$item_id];
                    }
                }
            }
        }
    }


	function getEntryNumber() {

		$saiban = $this->o_entry->getEntryNum($this->db, $this->formdata["form_id"]);

		$this->entry_number = $saiban["entory_count"] + 1;

		$rs = $this->o_entry->updEntryNum($this->db, $this->formdata["form_id"], $this->entry_number);
		if(!$rs) {
			return array(false, "", "");
		}

		return;

	}

	function lockStart() {

		//-----------------------------------------------
		//ロックファイル存在チェック
		//-----------------------------------------------
		$lock_file = ROOT_DIR."LockDir/lock".$this->form_id.".txt";
		While(file_exists($lock_file)){
			//存在する場合はスリープ
			$GLOBALS["log"]->write_log("SLEEP", "sleep");
			if(file_exists(ROOT_DIR."LockDir/stop".$this->form_id.".txt")){
				break;
			}
			sleep(5);
		}

		//ロックファイル生成
		error_log("lock start------".date("Y-m-d H:i:s"), 3 ,$lock_file);
		$GLOBALS["log"]->write_log("LOCK_START", "form_id => ".$this->form_id."| date =>".date("Y-m-d H:i:s"));
		chmod($lock_file, 0777);

		$exec_type = "1";

		return;

	}

	function lockEnd() {

		$GLOBALS["log"]->write_log("LOCK_END", "form_id => ".$this->form_id."| date =>".date("Y-m-d H:i:s"));
		if(file_exists(ROOT_DIR."LockDir/lock".$this->form_id.".txt")){
        	unlink(ROOT_DIR."LockDir/lock".$this->form_id.".txt");
        }

		return;
	}

	function backAction() {
		// 確認画面以外から戻るとき
		if($this->wk_block != "4"){
			$getFormData= GeneralFnc::convertParam($this->_init($this->wk_block), $_REQUEST);

			//共著者の所属機関
			$ret_param = $this->_make35param($this->wk_block);
			if(count($ret_param) > 0){
				$getFormData = array_merge($getFormData, $ret_param);
			}

            // ファイル引き継ぎ
    		foreach($this->arrfile as $item_id => $n){
                $key = "n_data".$item_id;
				$getFormData[$key] = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
			}

			// セッションにページパラメータをセットする
			$GLOBALS["session"]->setVar("form_param".$this->wk_block, $getFormData);
		}

		// セッションパラメータ取得
		$this->getDispSession();

		switch($this->wk_block) {
			case "1":
			case "2":
				$this->block = "1";
				break;

            // 3ページ目からの戻り
			case "3";
				$this->block = "1";

                // 2ページ目を使用
				if($this->formdata["group2_use"] != "1") {
					if($this->itemData[32]["disp"] != "1"){
						if(!isset($this->arrForm["edata31"])) $this->arrForm["edata31"] = "";
						if($this->arrForm["edata31"] != ""){
							$this->block = "2";
						}
					}
				}
				break;

			case "4":
				$this->block = "1";
				if($this->formdata["group3_use"] != "1") {
					$this->block = "3";
				} elseif($this->formdata["group2_use"] != "1") {
					$this->block = "2";
				}

		}

		if(isset($this->arrForm["edata31"])) {
			$wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);
			$this->arrForm["kikanlist"] = $wa_kikanlist;
		}

		return;

	}

	function agreeAction() {

		//同意が必要な場合
		if($this->formdata["agree"] == "3"){
			if($_REQUEST["agree"] == ""){

				if($this->formdata["lang"] == "1"){
					$msg = "以下をご覧になり、同意の上、お進みください。";
				}
				else{
					$msg = "Please read the agreement and proceed further only if you agree.";
				}
				$this->objErr->addErr($msg, "agree");
				$this->arrErr = $this->objErr->_err;

				if($this->formdata["lang"] == "1"){
					$this->_title = $this->formdata["form_name"]." 同意書";
				}
				else{
					$this->_title = $this->formdata["form_name"]." Privacy Policy";
				}
				$this->_processTemplate = "Usr/form/Usr_entry_agree.html";
			}
		}

		$this->block = "1";

		return;
	}

	function downloadAction() {

		//------------------------
		//フォームパラメータ
		//------------------------
		$getFormData= GeneralFnc::convertParam($this->_init($this->wk_block), $_REQUEST);

		//セッションにページパラメータをセットする
		$GLOBALS["session"]->setVar("form_param".$this->wk_block, $getFormData);


		//セッションパラメータ取得
		$this->getDispSession();

		$this->block = $this->wk_block;

		if($_REQUEST["down_num"] != ""){
			$down_num = $_REQUEST["down_num"];

			//ダウンロードするファイル
			$down_file = $this->uploadDir.$this->eid."/".$this->arrForm["n_data".$down_num];
			$fname = $this->arrForm["n_data".$down_num];

			if($_REQUEST["down_type"] == "2"){
				$tmp_dir = $GLOBALS["session"]->getVar("workDir");
				$fname = $this->arrForm["hd_file".$down_num];
				$down_file = $tmp_dir."/".$this->arrForm["hd_file".$down_num];
			}

			if(file_exists($down_file)){

				//ダウンロード実行
				$wo_download = new Download();
				$wo_download->file($down_file, $fname , "");
				exit;
			}
			else{
				$this->objErr->addErr("対象ファイルが存在しません。", "edata".$down_num);
				$this->arrErr = $this->objErr->_err;
			}
		}

		return;

	}


    /* メンテナンスを表示するフォームの場合は、メンテナンス画面を表示する */
    function showMenteform() {
        return Usr_initial::showMenteform($this);
    }

    /* アップロードディレクトリがない場合は作成 */
    function createUploadDir() {
        return Usr_initial::createUploadDir($this);
    }

    /* 管理者モードか一般モードか判断し、各処理を行う */
    function AuthAction() {
        return Usr_initial::AuthAction($this);
    }

    /* フォーム基本情報の取得・アサイン */
    function setFormData() {
        return Usr_initial::setFormData($this);
    }

    /* フォーム項目の取得及び、整形 */
    function setFormIni() {
        return Usr_initial::setFormIni($this);
    }

    /**
     * フォーム項目の並び順変更
     * 対象変数 : $this->arrItemData
     */
    function sortFormIni($obj) {}

    /* 共通アサインクラス */
    function commonAssign() {
        return Usr_initial::commonAssign($this);
    }

    function setTemplate() {
        return Usr_initial::setTemplate($this);
    }


    /** 応募期間チェック */
	function _chkPeriod() {
        return Usr_Check::_chkPeriod($this);
    }

	/**  一次応募期間中チェック */
	function _chkTerm(){
        return Usr_Check::_chkTerm($this);
	}

	/** 二次応募期間中チェック */
	function _chkTerm2(){
        return Usr_Check::_chkTerm2($this);
	}

	/** 入力チェック */
	function _check(){
        return Usr_Check::_check($this);
	}

    /** 1ページ目チェック */
	function _check1() {
        return Usr_Check::_check1($this);
	}

    /** 共著者チェック */
	function _checkAuthor() {
        return Usr_Check::_checkAuthor($this);
	}

    /** 2ページ目チェック */
	function _check2() {
        return Usr_Check::_check2($this);
	}

	// 3ページ目のチェック
	function _check3() {
        return Usr_Check::_check3($this);
	}

	/** 任意項目のチェック */
	function _checkNini($group){
        return Usr_Check::_checkNini($this, $group);
	}

	/** 共著者　任意項目のチェック */
	function _checkChosyaNini($i){
        return Usr_Check::_checkChosyaNini($this, $i);
	}

	/** メールアドレス、氏名重複チェック */
	function checkRepeat($pa_formparam){
        return Usr_Check::checkRepeat($this, $pa_formparam);
	}

	/** アップロードファイルチェック */
	function _checkfile(){
        return Usr_Check::_checkfile($this);
	}

    /**
     * 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init($ps_block){
        return Usr_init::_init($this, $ps_block);
    }


    /**
     * 1ページ目 項目情報初期化
     *
     * @param  void
     * @return array
     */
    function _init1(){
        return Usr_init::_init1($this);
    }


    /**
     * 2ページ目 項目情報初期化
     *
     * @param  void
     * @return array
     */
    function _init2() {
        return Usr_init::_init2($this);
    }


    /**
     * 3ページ目 項目情報初期化
     *
     * @param  void
     * @return array
     */
    function _init3(){
        return Usr_init::_init3($this);
    }


	/**
     * 任意項目用のパラメータ初期設定
     *
     * @param  array   $key     パラメータ配列
     * @param  integer $item_id 任意項目の項目ID
     * @param  integer $block   項目IDに対応するグループID
     * @return void
     */
    function _initNini(&$key, $item_id, $block){
        return Usr_init::_initNini($this, $key, $item_id, $block);
    }


    /**
     * パラメータ初期設定　共著者情報の任意項目
     *
     * @param  array   $key     パラメータ配列
     * @param  integer $item_id 任意項目の項目ID
     * @param  integer $i       現在の共著者番号
     * @param  string  $name    項目名
     * @return void
     */
    function _initChosyaNini(&$key, $item_id, $i, $name){
        return Usr_init::_initChosyaNini($this, $key, $item_id, $i, $name);
    }


    /**
	 * 所属期間名リストボックス生成
	 */
	function _makeListBox($ps_val){
		$retList = "";

		if(trim($ps_val) == ""){
			return $retList;
		}

		$ps_val = str_replace(array("\r", "\n", "\r\n", "\n\r"), "\n", $ps_val);
		$arrList = explode("\n", $ps_val);
		$arrList = array_values(array_filter($arrList));

		$num = 0;
		foreach($arrList as $key => $data){
			$num = $key+1;
			$retList[$num] = $data;
		}

		return $retList;

	}

    /**
     * 入力チェックの項目設定
     *
     * @access public
     * @param string パイプ区切りで登録されているチェックする項目
     * @param string 長さ
     * @param stirng モード　１：選択形式の項目 NULLの場合は入力形式の項目
     */
    function _setCheckMethod($ps_chk="", $pn_len="", $pn_type=""){
        return Usr_init::_setCheckMethod($this, $ps_chk, $pn_len, $pn_type);
    }


	function insertEntry() {
        //リネイム後のファイル名
        $this->wk_saki = array();

        /// 削除対象のファイル
        $this->wk_del = array();

		// 基本項目用パラメータの作成
		$param = $this->_makeDbParam();

		// エントリーID
		$this->ins_eid  = $GLOBALS["db"]->getOne("select nextval('entory_r_eid_seq')");
		$param["eid"] = $this->ins_eid;	//エントリーID

		// フォームID
		$param["form_id"] = $this->formdata["form_id"];

		// ユーザーID
		$user_id = $this->formdata["head"]."-".sprintf("%05d", $this->entry_number);
		$param["e_user_id"] = $user_id;

		//パスワード自動生成
		$passwd = $this->o_entry->makePasswd(PASSWD_LEN, PASSWD_TYPE);
		$param["e_user_passwd"] = $passwd;

        //アップロードファイル
        $date = date("Ymd");
        foreach($this->arrfile as $item_id => $n){
            if($this->arrForm["n_data".$item_id] != "" && $this->arrForm["file_del".$item_id] == "1") {
                $param["edata".$item_id] = "";
                $this->wk_del[$item_id] = $this->arrForm["n_data".$item_id];
            }

            // 画像をアップロードした場合のみファイル名を更新
            if(!isset($this->arrForm['file_upload'.$item_id])) continue;
            if($this->arrForm['file_upload'.$item_id] != 1)    continue;

            $wk_files = isset($this->wk_moto[$item_id]) ? $this->wk_moto[$item_id] : "";
            if($wk_files != "") {
                $extension = pathinfo($GLOBALS["session"]->getVar("workDir")."/".$wk_files, PATHINFO_EXTENSION);
                $this->wk_saki[$item_id] = $param["e_user_id"].$date.$n.".".$extension;
                $param["edata".$item_id] = $this->wk_saki[$item_id];
            }
        }

		$rs = $this->db->insert("entory_r", $param, __FILE__, __LINE__);

		unset($param);

		if(!$rs) return array(false, "", "");

		//共著者
		if(!$this->_chkInsertAuthor()) return array(true, $user_id, $passwd);

		if(!isset($this->arrForm["edata30"])) $this->arrForm["edata30"] = 0;

		for($i = 0; $i < $this->arrForm["edata30"]+1; $i++) {

			if(isset($this->arrForm["group2_del"]) && in_array($i, $this->arrForm["group2_del"])) continue;

			$param = $this->_makeDbParamAuthor($i);

			$param["form_id"] = $this->formdata["form_id"];
			$param["eid"] = $this->ins_eid;

			$rs = $this->db->insert("entory_aff", $param, __FILE__, __LINE__);

			if(!$rs) return array(false, "", "");

			unset($param);

		}

		return array(true, $user_id, $passwd);


	}

	function updateEntry() {

		// 基本項目用パラメータの作成
		$param = $this->_makeDbParam();

        // アップロードファイル
        $date = date('Ymd');

        /// 削除対象のファイル
        $this->wk_del = array();

        // リネイム後のファイル名
        $this->wk_saki = array();

        foreach($this->arrfile as $item_id => $n){
            if($this->arrForm["n_data".$item_id] != "" && $this->arrForm["file_del".$item_id] == "1") {
                $param["edata".$item_id] = "";
                $this->wk_del[$item_id] = $this->arrForm["n_data".$item_id];
            }

            // 画像をアップロードした場合のみファイル名を更新
            if(!isset($this->arrForm['file_upload'.$item_id])) continue;
            if($this->arrForm['file_upload'.$item_id] != 1)    continue;

            $wk_files = isset($this->wk_moto[$item_id]) ? $this->wk_moto[$item_id] : "";
            if($wk_files != "") {
                $extension = pathinfo($GLOBALS["session"]->getVar("workDir")."/".$wk_files, PATHINFO_EXTENSION);
                $this->wk_saki[$item_id] = $GLOBALS["entryData"]["e_user_id"].$date.$n.".".$extension;
                $param["edata".$item_id] = $this->wk_saki[$item_id];
            }
        }

		// 削除フラグが有る場合　共著者の数を変更する
		if(isset($this->arrForm["group2_del"])) {
				$param["edata30"] = $param["edata30"] - count($this->arrForm["group2_del"]);
		}

		$param["udate"] = "NOW";

		$where[] = "eid = ".$this->db->quote($this->eid);

		$rs = $this->db->update("entory_r", $param, $where, __FILE__, __LINE__);

		if(!$rs) return false;

		unset($param);
		unset($where);


        // 共著者のデータ更新
		if(!isset($this->arrForm["edata31"])) return true;

		// 共著者情報が無い場合は、削除フラグを立てて終わり
		if(!$this->_chkInsertAuthor()) {
			$param["udate"] = "NOW";
			$param["del_flg"] = "1";
			$where[] = "eid = ".$this->db->quote($this->eid);
			$where[] = "del_flg <> 1";
			$rs = $this->db->update("entory_aff", $param, $where, __FILE__, __LINE__);
			return;
		}

		// 登録済みの共著者ID
		$wa_kyouid = $GLOBALS["session"]->getVar("kyouID");
		$this->debug(print_r($wa_kyouid, true));

		if(!isset($this->arrForm["edata30"])) $this->arrForm["edata30"] = 0;
		for($i = 0; $i < $this->arrForm["edata30"]+1; $i++) {

			$kyouid = isset($wa_kyouid[$i]) ? $wa_kyouid[$i] : "0";
			$param["del_flg"] = "0";
			if(isset($this->arrForm["group2_del"])) {
				if(in_array($i, $this->arrForm["group2_del"])) {
					$param["del_flg"] = 1;
				}
			}

			if($kyouid == "0" && $param["del_flg"] == "1") {
				unset($param);
				continue;
			}

			$param2 = $this->_makeDbParamAuthor($i);

			$param = array_merge((array)$param, (array)$param2);
			unset($param2);

			if($kyouid == "0") {
				$param["form_id"] = $this->formdata["form_id"];
				$param["eid"] = $this->eid;

				$rs = $this->db->insert("entory_aff", $param, __FILE__, __LINE__);
			} else {
				$param["udate"] = "NOW";
				$where[] = "id = ".$kyouid;
				$rs = $this->db->update("entory_aff", $param, $where, __FILE__, __LINE__);
			}

			if(!$rs) return false;

			unset($param);
			unset($where);

		}

		return true;

	}

	/**
	 * 共著者テーブル挿入有無チェック
	 *
	 * tureの場合は、共著者登録が必要となる
	 */
	function _chkInsertAuthor() {

		if($this->formdata["group2_use"] == "2") false;
		/*if(!isset($this->arrForm["edata29"])) $this->arrForm["edata29"] = "";
		if($this->arrForm["edata29"] == "" || $this->arrForm["edata29"] == "2") false;
		if(!isset($this->arrForm["edata30"])) $this->arrForm["edata30"] = "0";
		if($this->arrForm["edata30"] == "" || !$this->arrForm["edata30"] > 0)  false;*/
		if(!isset($this->arrForm["edata31"])) return false;
		if($this->arrForm["edata31"] == "") return false;

		return true;

	}

	/**
	 * 登録パラメータの作成
	 */
	function _makeDbParam() {

		// パラメータの作成
		foreach($this->itemData as $item_id=>$val) {

			if($val["group_id"] == "2") continue;
			if($val["disp"] == "1") continue; // 非表示
			if($val["disp"] == "2") continue; // 編集不可

			switch($item_id) {

				default:

					// ファイル項目は飛ばす
					if(array_key_exists($item_id, $this->arrfile)) break;

					// チェックボックス系
					if($val["item_type"] == "3") {

						if(!count($val["select"]) > 0) break;

						$wk_arr = array();
						foreach($val["select"] as $i=>$v) {
							if(!isset($this->arrForm["edata".$item_id.$i])) continue;
							if($this->arrForm["edata".$item_id.$i] == $i) $wk_arr[] = $i;
						}
						$param["edata".$item_id] = implode("|", $wk_arr);

						break;

					}

					$param["edata".$item_id] = isset($this->arrForm["edata".$item_id])
						? $this->arrForm["edata".$item_id]
						: "";

					break;
			}

		}

		return $param;

	}

	/**
	 * 登録パラメータの生成　共著者
	 */
	function _makeDbParamAuthor($i) {

		$wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);

		foreach($this->itemData as $item_id=>$val) {
			if($val["group_id"] != "2") continue;
			if($val["disp"] == "1") continue;

			switch($item_id) {

				case "32":
						if(!count($wa_kikanlist) > 0) break;

						$wk_arr = array();
						foreach($wa_kikanlist as $key=>$val) {
							$wk_val = isset($this->arrForm["edata".$item_id.$i.$key]) ? $this->arrForm["edata".$item_id.$i.$key] : "";
							if($wk_val != "") $wk_arr[] = $key;
						}
						if(count($wk_arr) > 0) $param["edata".$item_id] = implode("|", $wk_arr);

					break;
				default:

						// チェックボックス系
						if($val["item_type"] == "3") {

							if(!count($val["select"]) > 0) break;
							$wk_arr = array();
							foreach($val["select"] as $selectkey=>$selectval) {
								if(!isset($this->arrForm["edata".$item_id.$i.$selectkey])) continue;
								if($this->arrForm["edata".$item_id.$i.$selectkey] == $selectkey) $wk_arr[] = $selectkey;
							}
							$param["edata".$item_id] = implode("|", $wk_arr);

							break;

						}

						if(!isset($this->arrForm["edata".$item_id.$i])) $this->arrForm["edata".$item_id.$i] = "";
						$param["edata".$item_id] = $this->arrForm["edata".$item_id.$i];

					break;

			}

		}

		return $param;
	}

	/**
	 * セッションパラメータ取得
	 * 	画面入力データをセッションから取得する
	 */
	function getDispSession(){

		$this->arrForm = array_merge((array)$GLOBALS["session"]->getVar("form_param1"), (array)$GLOBALS["session"]->getVar("form_param2"));
		$this->arrForm = array_merge((array)$this->arrForm, (array)$GLOBALS["session"]->getVar("form_param3"));

		if(isset($this->arrForm["edata31"])) {
			$wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);
			$this->arrForm["kikanlist"] = $wa_kikanlist;
		}
	}

	/**
	 * 確認画面表示
	 */
	function _confirm(){

		//------------------------
		//フォームパラメータ
		//------------------------
		$getFormData= GeneralFnc::convertParam($this->_init($this->wk_block), $_REQUEST);

		$this->arrParam = $getFormData;

		//入力チェック
		$this->arrErr = $this->_check();
        if(!(count($this->arrErr) > 0)){
            foreach($this->arrfile as $item_id => $n){
                if($this->itemData[$item_id]["disp"] != "1"){
                    //添付資料アップロード
                    list($wb_ret, $msg) = $this->fileTmp($item_id);
                    if(!$wb_ret){
                        $this->arrErr["file_error".$item_id] = $msg;
                    }
                }
            }
        }

		//セッションにページパラメータをセットする
		$GLOBALS["session"]->setVar("form_param".$this->wk_block, $this->arrParam);


		//セッションパラメータ取得
		$this->getDispSession();

		if(count($this->arrErr) > 0){
			//エラーを自ページに表示
			$this->block = $this->wk_block;

		}
		else{

			$this->block = "4";
			//テンプレート
			if($this->formdata["lang"] == "1"){
				$this->_processTemplate = "Usr/form/Usr_entry_confirm_j.html";
			}
			else{
				$this->_processTemplate = "Usr/form/Usr_entry_confirm_e.html";
			}

		}
	}

	/**
	 * 完了画面表示
	 */
	function complete($ps_msg, $pa_auth_err=""){

		$this->_processTemplate = "Usr/form/Usr_entry_complete.html";
		$this->assign("msg", $ps_msg);

		//----------------------------------------
		//決済処理でエラーが発生した場合
		//----------------------------------------
		if(!empty($pa_auth_err)){
			if(isset($pa_auth_err["order_id"])){
				$wa_auth_err["order_id"] = "オーダーID：".$pa_auth_err["order_id"];
			}

			if(isset($pa_auth_err["result_code"])){
				$wa_auth_err["result_code"] = "結果コード:".$pa_auth_err["result_code"];
			}

			if(isset($pa_auth_err["error_message"])){
				$wa_auth_err["error_message"] = $pa_auth_err["error_message"];
			}

		}


        //----------------------------------------
        //ロックファイルを削除する
        //----------------------------------------
        if(file_exists(ROOT_DIR."LockDir/lock".$this->formdata["form_id"].".txt")){
        	unlink(ROOT_DIR."LockDir/lock".$this->formdata["form_id"].".txt");
        }

		$this->assign("errAuth", $wa_auth_err);


		// 親クラスに処理を任せる
		parent::main();
		exit;

	}


    /* 登録完了メールの送信 */
    function sendRegistMail($user_id, $passwd) {
        return Usr_mail::sendRegistMail($this, $user_id, $passwd);
    }

    /* 編集完了メールの送信 */
    function sendEditMail($user_id, $passwd) {
        return Usr_mail::sendEditMail($this, $user_id, $passwd);
    }

    /* 完了メールの送信 */
    function sendCompleteMail($user_id, $passwd, $exec_type=1) {
        return Usr_mail::sendCompleteMail($this, $user_id, $passwd, $exec_type);
    }

    /* メールの上部コメントを作成する */
    function makeMailBody_header($user_id="", $passwd ="", $exec_type = "") {
        return Usr_mail::makeMailBody_header($this, $user_id, $passwd, $exec_type);
    }

    /* 完了メール作成 */
    function makeMailBody($user_id="", $passwd ="", $exec_type = ""){
        return Usr_mail::makeMailBody($this, $user_id, $passwd, $exec_type);
    }

    /** メール本文生成 グループ1 */
    function makeBodyGroup1(&$arrbody){
        return Usr_mail::makeBodyGroup1($this, $arrbody);
    }

    /** メール本文生成 グループ2 */
    function makeBodyGroup2(&$arrbody){
        return Usr_mail::makeBodyGroup2($this, $arrbody);
    }

    /** メール本文生成 グループ3 */
    function makeBodyGroup3(&$arrbody){
        return Usr_mail::makeBodyGroup3($this, $arrbody);
    }

    /* 会員・非会員 */
    function mailfunc8($name) {
        return Usr_mail::mailfunc8($this, $name);
    }

    /* 連絡先(勤務先・自宅) */
    function mailfunc16($name) {
        return Usr_mail::mailfunc16($this, $name);
    }

    /* 都道府県 */
    function mailfunc18($name) {
        return Usr_mail::mailfunc18($this, $name);
    }

    /* 共著者の有無 */
    function mailfunc29($name) {
        return Usr_mail::mailfunc29($this, $name);
    }

    /* 共著者の所属期間 */
    function mailfunc31($name) {
        return Usr_mail::mailfunc31($this, $name);
    }

    /* 共著者　所属期間リスト */
    function mailfunc32($name, $i) {
        return Usr_mail::mailfunc32($this, $name, $i);
    }

    /* 共著者・会員 */
    function mailfunc41($name, $i) {
        return Usr_mail::mailfunc41($this, $name, $i);
    }

    /* 発表形式 */
    function mailfunc48($name) {
        return Usr_mail::mailfunc48($this, $name);
    }

    /* 抄録 */
    function mailfunc49($name) {
        return Usr_mail::mailfunc49($this, $name);
    }

    /* 抄録英語 */
    function mailfunc50($name) {
        return Usr_mail::mailfunc50($this, $name);
    }

    /* ファイルアップロード */
    function mailfunc51($name) {
        return Usr_mail::mailfunc51($this, $name);
    }
    function mailfunc52($name) {
        return Usr_mail::mailfunc52($this, $name);
    }
    function mailfunc53($name) {
        return Usr_mail::mailfunc53($this, $name);
    }

    /* Mr. Mrs. */
    function mailfunc57($name) {
        return Usr_mail::mailfunc57($this, $name);
    }

    /* 共著者 Mr. Mrs. */
    function mailfunc61($name, $i) {
        return Usr_mail::mailfunc61($this, $name, $i);
    }

    /* 国名　リストボックス */
    function mailfunc114($name) {
        return Usr_mail::mailfunc114($this, $name);
    }

    function mailfuncFile($key, $name) {
        return Usr_mail::mailfuncFile($this, $key, $name);
    }

    /* 任意 */
    function mailfuncNini($key, $name, $i=null) {
        return Usr_mail::mailfuncNini($this, $key, $name, $i);
    }

    /**
     * 添付ファイルアップロード
     * 添付ディレクトリにアップロードする
     */
    function fileTmp($item_id){
        // 一次格納ディレクトリ
        $workDir = $GLOBALS["session"]->getVar("workDir")."/";
        // 引き継ぐキー
        $key = "hd_file".$item_id;
        $key2= "file_upload".$item_id;


        // 既に一度ワークディレクトリに添付済みの場合
        $arrForm3 = $GLOBALS["session"]->getVar("form_param3");
        if($arrForm3[$key]  != "") $this->arrParam[$key]  = $arrForm3[$key];
        if($arrForm3[$key2] != "") $this->arrParam[$key2] = $arrForm3[$key2];

        // ファイルなし
        if(!isset($_FILES["edata".$item_id])) return array(true, "");

        // ファイルがアップロードされた場合
        $pa_filedata = $_FILES["edata".$item_id];
        if($pa_filedata["size"] > 0){
            // アップロードログ出力
            Usr_function::uploadLog($pa_filedata);

            if($pa_filedata["error"] > 0){
                return array(false,"ファイルのアップロードに失敗しました。");
            }

            // 直前までにアップロードしたファイルが存在する場合
            if($this->arrParam[$key] != "") {
                if(file_exists($workDir.$this->arrParam[$key])) {
                    unlink($workDir.$this->arrParam[$key]);
                }
            }

            // ファイルコピー
            $this->arrParam[$key] = $pa_filedata["name"];

            $rs = @move_uploaded_file($pa_filedata["tmp_name"], $workDir.$pa_filedata["name"]);
            if(!$rs){
                $this->arrParam[$key] = "";
                return array(false,"ファイルの一次保存に失敗しました。");
            }
            $this->arrParam["file_del".$this->arrParam[$key]] = "0";

            // ファイルアップロードフラグを立てる
            $this->arrParam[$key2] = 1;
        }

        return array(true, "");
    }

	/**
	 * ファイルアップロード
	 * 	本番ディレクトリにファイル移動を行う
	 *
	 */
	function fileUpload($mode, $ps_eid){

		$this->wk_moto = array();
		$workDir = $GLOBALS["session"]->getVar("workDir");

		if($workDir == "") {
			$GLOBALS["log"]->write_log($_REQUEST["mode"],"ERROR:一次保存フォルダのセッション切れ");
			return array(false,"ファイルのコピーに失敗しました。");
		}

		//本番格納用ディレクトリが存在するか？
		if(!is_dir($this->uploadDir.$ps_eid)){
			mkdir($this->uploadDir.$ps_eid, 0777,true);
			chmod($this->uploadDir.$ps_eid, 0777);
		}

		for($i=51; $i<54; $i++){


			//アップロードするファイルが存在する場合、移動
			if( (isset($this->arrForm["hd_file".$i]) && $this->arrForm["hd_file".$i] != "") &&
					!isset($this->arrForm["file_del".$i]) ){
					$this->wk_moto[$i] = $this->arrForm["hd_file".$i];

				if (!copy($workDir."/".$this->arrForm["hd_file".$i], $this->uploadDir.$ps_eid."/".$this->arrForm["hd_file".$i])) {
					$GLOBALS["log"]->write_log($_REQUEST["mode"],"ERROR:作業ディレクトリから本番ディレクトリへのファイルコピーに失敗しました。 work_file=>".$workDir."/".$this->arrForm["hd_file".$i]. " | to_file=>".$this->uploadDir.$ps_eid."/".$this->arrForm["hd_file".$i]);
					return array(false,"ファイルのコピーに失敗しました。");
				}
			}
			else{
				$this->wk_moto[$i] = "";
			}
		}

		return array(true, "");

	}

//	/**
//	 * 文字列置換
//	 */
//	function wordReplace($ps_val, $pa_param){
//
//
//		$ret_val = "";
//
//		if($ps_val == ""){
//			return $ret_val;
//		}
//
//
//		$ret_val = $ps_val;
//		$ret_val = ereg_replace("_ID_", $pa_param["ID"], $ret_val);
//		$ret_val = ereg_replace("_PASSWORD_", $pa_param["PASSWORD"], $ret_val);
//		$ret_val = ereg_replace("_INSERT_DAY_", $pa_param["INSERT_DAY"], $ret_val);
//		$ret_val = ereg_replace("_UPDATE_DAY_", $pa_param["UPDATE_DAY"], $ret_val);
//		$ret_val = ereg_replace("_SEI_", $pa_param["SEI"], $ret_val);
//
//		$ret_val = ereg_replace("_MEI_", $pa_param["MEI"], $ret_val);
//		$ret_val = ereg_replace("_MIDDLE_", $pa_param["MIDDLE"], $ret_val);
//
//		//敬称が入力されていない場合
//		if($pa_param["TITLE"] == ""){
//			$ret_val = str_replace(array("_TITLE_"), "", $ret_val);
//		}
//		$ret_val = ereg_replace("_TITLE_", $pa_param["TITLE"], $ret_val);
//
//		$ret_val = ereg_replace("_SECTION_", $pa_param["SECTION"], $ret_val);
//
//		return $ret_val;
//	}

//	/**
//	 * 登録日、更新日の取得
//	 */
//	function getEntryDate($pn_id){
//
//    	$column = "to_char(rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(udate, 'yyyy/mm/dd hh24:mi:ss') as upday";
//    	$from = "entory_r";
//    	$where[] = "del_flg = 0";
//    	$where[] = "eid = ".$pn_id;
//
//    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);
//
//    	return $rs;
//	}


	/**
	 * 初期表示時、任意項目の値を生成
	 * 	項目の開始番号と終了番号を渡すと初期表示時を生成する
	 */
	function _default_arrFormNini($start, $end, $wa_entrydata){

		for($index=$start; $index < $end; $index++){

			if($this->itemData[$index]["item_type"] == "3"){

				$wk_checked  = explode("|", $wa_entrydata["edata".$index]);

				foreach($wk_checked as $chk_val){
					$this->arrForm["edata".$index.$chk_val] = $chk_val;
				}
			}
			else{
				$this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
			}
		}
	}

	/**
	 * 初期表示時、任意項目の値を生成（共著者情報部分）
	 * 	項目の開始番号と終了番号を渡すと初期表示時を生成する
	 */
	function _default_arrFormChosyaNini($start, $end, $data, $a_key){

		for($index=$start; $index < $end; $index++){

			$key = "edata".$index;
			if(!isset($data[$key]) || $data[$key] == "") continue;

			if($this->itemData[$index]["item_type"] == "3"){

				$wk_checked  = explode("|", $data[$key]);
				foreach($wk_checked as $chk_val){
					$this->arrForm["edata".$index.$a_key.$chk_val] = $chk_val;
				}

			}
			else{
				$this->arrForm["edata".$index.$a_key] = $data[$key];
			}
		}

	}



	/**
	 * 外部ファイルが存在するかチェック
	 * 	拡張子がPHPの場合はファイルをinclude_onceする
	 *
	 * @param stirng ディレクトリ
	 * @param int form_id
	 * @param string ファイル名
	 * @param string 拡張子
	 */
	function _chkIncFiles($ps_dir, $pn_form_id, $ps_file_name, $ps_extension){

		//探したいファイルの名前
		$find_file = $pn_form_id.$ps_file_name.".".$ps_extension;

		if (is_dir($ps_dir)) {
		    if ($dh = opendir($ps_dir)) {
		        while (($file = readdir($dh)) !== false) {
		        	//外部読み込みファイルが見つかった場合
		            if($file == $find_file){
		            	//PHPファイルの場合
		            	if($ps_extension == "php"){
							include_once($ps_dir.$find_file);
		            	}

						return true;
		            }
		        }
		        closedir($dh);
		    }
		}

		return false;
	}


	/**
	 * フォーム番号35　所属機関パラメータ生成
	 */
	private function _make35param($pn_block){

		//２ページ目の場合
		if($pn_block == 2){

			$ret_data = array();


			//共著者の数をセッションから復元
			$form1data = $GLOBALS["session"]->getVar("form_param1");
			$wa_kikanlist = $this->_makeListBox($form1data["edata31"]);

			if($form1data["edata31"] == "") return $ret_data;
			if(!isset($form1data["edata30"])) $form1data["edata30"] = 0;

			$cyosya = $form1data["edata30"]+1;

			for($i = 0; $i < $cyosya; $i++){

				foreach($wa_kikanlist as $key => $data ){

					if(isset($_REQUEST["edata32".$i.$key])){
						$ret_data["edata32".$i.$key] = $_REQUEST["edata32".$i.$key];
					}
				}
			}

			return $ret_data;

		}



	}

	function createWorkDir() {

		$workDir = $this->uploadDir."NEW/".date("YmdHis");
		//tmpディレクトリ作成（新規投稿用）
		if(!is_dir($workDir)){
			mkdir($workDir, 0777, true);
			chmod($workDir, 0777);
		}
		//セッションにワークディレクトリ名をセット
		$GLOBALS["session"]->setVar("workDir", $workDir);

		return;

	}

	/**
	 * セッション切れをチェックする
	 */
	function chkSession($ws_action) {

		if($ws_action == "default") return;

		$workDir = $GLOBALS["session"]->getVar("workDir");

		if($workDir != "") return;

		if($this->o_form->formData["lang"] == "2") {
			$msg = "Session timeout error.";
		} else {
			$msg = "セッションがタイムアウトしました。";
		}

		// メッセージをセッションに入れる
		$GLOBALS["session"]->setVar("startmsg", $msg);

		if(trim($this->eid) == "") {
			// 入力開始へリダイレクト
			$url = URL_ROOT."Usr/form/Usr_entry.php?form_id=".$this->o_form->formData["form_id"];
		} else {

			// ログイン画面へリダイレクト
			$url = URL_ROOT."Usr/Usr_login.php?form_id=".$this->o_form->formData["form_id"];
		}

		header("Location: ".$url);
		exit;

	}

	function createPageflow() {
		return;
	}

	function getAuthorMax() {

		return 15;

		/*

		if(!$this->itemData[30]["select"]) return;

		$count = count($this->itemData[30]["select"]);
		return $this->itemData[30]["select"][$count];
		*/
	}

	function debug($msg) {

		error_log("\n".$msg, 3, LOG_DIR."debug.log");
		return;

	}

	// ExtendからProcessBaseのmainを呼ぶため設置
	function basemain() {
		parent::main();
		exit;
	}


    /**
     * 開発用のデバッグ関数
     * 通常は空にしておく
     *
     * 継承して利用する.
     */
    function developfunc(){}



    /**
     * セッション切れしていないかチェック
     *
     * ファーストアクセスでセッションstartに、1をたて、
     * ページ遷移毎に、セッションstartに1があるかチェックし、1がない場合は、セッション切れ
     */
    private function pf_checkSession() {

    	$mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

    	//print_r("pf_checkSession(".$mode.")");

    	if($mode == "") {
    		$GLOBALS["session"]->setVar("start", "1");
    		//print_r(" ".$GLOBALS["session"]->getVar("start"));
    		return;
    	} else {
    		$start = $GLOBALS["session"]->getVar("start");
    		//print_r(" ".$start);
    		if($start == "1")	return;
    	}

    	$url  = SSL_URL_ROOT."Usr/form/Usr_entry.php?form_id=".$this->o_form->formData["form_id"];

    	Error::showErrorPage("*セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。", $url);

    }

}

/**
 * メイン処理開始
 **/

if(!isset($_REQUEST["form_id"]) || !$_REQUEST["form_id"] > 0) {

} else {
	$inc = "Usr_entry".$_REQUEST["form_id"];
	$inc_file = "../include/".$inc.".php";
	if(file_exists($inc_file)) {
		include($inc_file);
		if(class_exists($inc)) {
			$c = new $inc();
		}
	}
}

if(!isset($c)) {
	$c = new Usr_Entry();
}

$c->main();







?>

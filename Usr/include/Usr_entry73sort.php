<?php

/**
 * 73番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld_kawauchi
 * @since 2016.06.06
 * 
 */
class Usr_Entry73sort {

    function mng_detail_premain($obj){
        $obj->arrItemData[3][47]["disp"] = "";
        //$obj->arrItemData[3][47]["item_view"] = "";

        //if(method_exists($obj, "assign")){
        //    $obj->assign("arrItemData",$obj->arrItemData);
        //}
    }


    function doSort($obj){
        // [cfp-form73]:二次投稿時まで項目非表示
        $this->hideItem3($obj);
        
        
        // グループ3-1
        $arrItem1 = array();
        $group = 3 - 3;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){

                case 47:
                    break;

                default:
                    $arrItem1[] = $item_id;
                    break;
            }
        }
        $obj->group3item[$group] = $arrItem1;


        // [cfp-form73]:グループ3の①の項目を「A」の位置に移動
        // グループ3-2
        $arrItem2 = array();
        $group = 3 - 2;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){

                case 51:
                    $arrItem2[] = 47;
                    $arrItem2[] = $item_id;
                    break;

                default:
                    $arrItem2[] = $item_id;
                    break;
            }
        }
        $obj->group3item[$group] = $arrItem2;

        // グループ3-3
        $arrItem3 = array();
        $group = 3 - 1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                default:
                    $arrItem3[] = $item_id;
                    break;
            }
        }
        $obj->group3item[$group] = $arrItem3;


        $group = 3;
        $arrGroup = array();
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 47:
                    break;
                    
                case 48:
                    $arrGroup[$item_id] = $_arrItem;
                    $arrGroup[50] = $obj->arrItemData[3][50];
                    break;

                case 50:
                    break;
        
                case 51:
                    $arrGroup[47] = $obj->arrItemData[3][47];
                    $arrGroup[$item_id] = $_arrItem;
                    break;
        
                default:
                    $arrGroup[$item_id] = $_arrItem;
                    break;
            }
        }
        $obj->arrItemData[$group] = $arrGroup;
        $obj->arrItemData[3][47]["group3"] = 2;

        
    }


    function hideItem3($obj) {

        $nowDate = date("Y-m-d H:i:s");
        //$nowDate = "2016-06-15 00:00:00";
        $secondaryReceptionDate = $GLOBALS["form"]->formData["secondary_reception_date"];

        $obj->arrItemData[3][47]["disp"] = "1";
        
        // 二次期間
        if(strtotime($nowDate) >= strtotime($secondaryReceptionDate)) {
            $obj->arrItemData[3][47]["disp"] = "";
        }
        
    }


}

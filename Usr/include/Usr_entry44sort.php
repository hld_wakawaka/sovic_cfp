<?php

/**
 * 44番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * 
 */
class Usr_Entry44sort {

    /** CSVヘッダ-グループ2生成 */
    function entry_csv_entryMakeHeader2($obj, $all_flg=false){
        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $loop_cnt = $obj->loop +1;

            for($i=1; $i <= $loop_cnt; $i++ ){
                if($i==1) continue;
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    // 画面に表示する項目の名称
                    $name = strip_tags($_data["item_name"]);

                    $prefix = $obj->fix."共著者".($i-1)." ";

                    $item_id = $_data["item_id"];
                    switch($item_id){
                            case 32:
                                $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                                break;

                            // 共著者　姓名
                            case 33:
                                $groupHeader[$group][] =  $prefix."姓名".$obj->fix;
                                break;

                            // 共著者　姓名カナ
                            case 35:
                                $groupHeader[$group][] =  $prefix."姓名（カナ）".$obj->fix;
                                break;

                            case 34:
                            case 36:
                                break;

                            default:
                                $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                                break;
                    }
                }
            }
        }
        return $groupHeader[$group];
    }


    /** CSV出力データ グループ2生成 */
    function entry_csv_entryMakeData2($obj, $pa_param, $all_flg=false){
        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $cnt      = count($pa_param["chosya"]);
            $loop_cnt = $obj->loop +1;

            for($i=1; $i <= $loop_cnt; $i++ ){
                if($i==1) continue;
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    $item_id = $_data["item_id"];

                    // 共著者の員数を越えた場合はNULL埋め
                    if($cnt < $i){
                        if($item_id == "34") continue;
                        if($item_id == "36") continue;

                        $groupBody[$group][] = $obj->fix.$obj->fix;
                        continue;
                    }

                    $wk_body = "";
                    $chosya  = $pa_param["chosya"][$i-1];

                    // 氏名：名、カナ：名
                    if(in_array($item_id, array(34, 36))) continue;

                    // 任意項目
                    if($_data["controltype"] == "1"){
                        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $chosya["edata".$item_id], array(" ", ","), true);
                        $wk_body = trim($wk_body, ",");

                    // 標準項目
                    }else{
                        switch($item_id){
                            //共著者所属期間
                            case 32:
                                if(count($obj->set_select) > 0){
                                    if($chosya["edata".$item_id] != ""){
                                        $chkeck = explode("|", $chosya["edata".$item_id]);
                                        $chkeck_names = array();

                                        foreach($chkeck as $val){
                                            if($val != "" && array_key_exists($val, $obj->set_select)){
                                                $chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $obj->set_select[$val]);
                                            }
                                        }

                                        if(count($chkeck_names) > 0){
                                            $wk_aaa =   implode(",", $chkeck_names);
                                        }

                                    }
                                    else{
                                        $wk_aaa = "";
                                    }

                                    $wk_body = str_replace(array("\r\n","\n","\r"), '', trim($wk_aaa));
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            //共著者姓名
                            case 33:
                                $cyosya_name = "";
                                $wk_mark = array();
                                if($chosya["edata32"] != ""){
                                    $chkeck = explode("|", $chosya["edata32"]);
                                    foreach($chkeck as $val){
                                        $wk_mark[] = $val.")";
                                    }
                                    $cyosya_name = implode(", ", $wk_mark);

                                }
                                $cyosya_name .= $chosya["edata".$item_id]." ".$chosya["edata34"];
                                $wk_body = $cyosya_name;
                                break;

                            //共著者姓名（カナ）
                            case 35:
                                $wk_body = $chosya["edata".$item_id]." ".$chosya["edata36"];
                                break;

                            case 34:
                            case 36:
                                break;


                            //会員・非会員
                            case 41:
                                $wk_body = ($chosya["edata".$item_id] != "") ? $obj->wa_kaiin[$chosya["edata".$item_id]] : "";
                                break;

                            //Mr. Mis Dr
                            case 61:
                                if($chosya["edata".$item_id] != ""){
                                    $wk_body = $GLOBALS["titleList"][$chosya["edata".$item_id]];
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            case 46:
                            case 47:
                            default:
                                $wk_body = $chosya["edata".$item_id];
                                break;
                        }
                    }
                    $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
                }
            }
        }
        return $groupBody[$group];
    }


}

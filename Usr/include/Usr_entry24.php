<?php

/**
 * 24番専用カスタマイズフォーム
 * このフォームはreg環境73番の複製フォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2013.08.09
 * 
 */
class Usr_Entry24 extends Usr_Entry {

    /**
     * 開発用デバッグメソッド
     * super#mainの最後に実行される
     * 
     */
    function developfunc(){}


    /*
     * 完了メールの送信
     */
    function sendCompleteMail($user_id, $passwd, $exec_type=1) {
        
        if(in_array($this->form_id, $this->sendmail_not)) return;

        //------------------------------------
        //メール送信
        //------------------------------------
        //本文
        //メソッドチェック
        $ws_mailBody = "";
        $ws_mailBody = $this->makeMailBody($user_id, $passwd, $exec_type);

        $subject = ereg_replace("_ID_", $user_id, $this->subject);

        // 登録者へのメール
        if($this->arrForm["edata25"] != ""){
            $this->o_mail->SendMail($this->arrForm["edata25"], $this->subject, $ws_mailBody, $this->formdata["form_mail"], $this->formWord["word13"]);
        }

        // 管理者へのメール
        $this->o_mail->SendMail($this->formdata["form_mail"], $this->subject, $ws_mailBody, $this->formdata["form_mail"],$this->formWord["word13"]);

        return;
    }


    function mailfunc26($name){
        $str = $this->mailfuncNini(26, $name);
        return "\n". $str;
    }
}

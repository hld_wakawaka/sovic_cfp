<?php

/**
 * 17番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2014.03.11
 * 
 */
class Usr_Entry17sort {

    function doSort($obj){
        $arrGroup3 = array();
        foreach($obj->arrItemData[3] as $item_id => $_arrItem){
            switch($item_id){
                case 51:
                    $arrGroup3[$item_id] = $_arrItem;
                    $arrGroup3[101]      = $obj->arrItemData[3][101];
                    $arrGroup3[101]['group3'] = 2;
                    break;

                case 101:
                    break;

                default:
                    $arrGroup3[$item_id] = $_arrItem;
            }
        }
        $obj->arrItemData[3] = $arrGroup3;
    }
}

<?php

/**
 * 63番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.doi
 * 
 */
class Usr_Entry63 extends Usr_Entry
{

    public function getAuthorMax()
    {
        return 20;
    }


}

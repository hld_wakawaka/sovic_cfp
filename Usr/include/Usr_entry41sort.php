<?php

/**
 * 18番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2014.03.31
 * 
 */
class Usr_Entry41sort {

    //----------------------------------
    // CSVカスタマイズ
    //----------------------------------

    /**
     * CSVヘッダ生成
     */
    function entry_csv_entryMakeHeader($obj, $all_flg=false){

        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());

        $group = 1;
        $groupHeader[$group][] = "登録No.";
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = "\"".$name."\"";
        }

        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $loop_cnt = $obj->loop +1;

            for($i=1; $i < $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    // 画面に表示する項目の名称
                    $name = strip_tags($_data["item_name"]);

                    $prefix = "\"共著者".$i." ";

                    $item_id = $_data["item_id"];
                    switch($item_id){
                            case 32:
                                if($i == 1) break;
                                $groupHeader[$group][] =  $prefix.$name."\"";
                                break;

                            // 共著者　姓名
                            case 33:
                                $groupHeader[$group][] =  $prefix."姓名"."\"";
                                break;

                            // 共著者　姓名カナ
                            case 35:
                                $groupHeader[$group][] =  $prefix."姓名（カナ）"."\"";
                                break;

                            case 34:
                            case 36:
                                break;

                            default:
                                $groupHeader[$group][] =  $prefix.$name."\"";
                                break;
                    }
                }
            }
        }

        $group = 3;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = "\"".$name."\"";
        }

        $header = $groupHeader[1];
        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[]="状態";
        $header[]="エントリー登録日";
        $header[]="エントリー更新日";

        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }




    /**
     * CSV出力データ生成
     */
    function entry_csv_entryMakeData($obj, $pa_param, $all_flg=false){
        foreach($pa_param as $key=>$data) {
            $pa_param[$key] = str_replace('"', '""', $data);
        }


        // グループ別に生成
        $groupBody = array("1" => array(), "2" => array(), "3" => array());

        $group = 1;
        $groupBody[$group][] = $pa_param["entry_no"];
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            // 任意項目
            if($_data["controltype"] == "1"){
                $wk_body = trim(Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true), ",");

            // 標準項目
            }else{
                switch($item_id){
                    //会員・非会員
                    case 8:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_kaiin[$pa_param["edata".$item_id]] : "";
                        break;
                    //連絡先
                    case 16:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_contact[$pa_param["edata".$item_id]] : "";
                        break;
                    //都道府県
                    case 18:
                        //英語フォームの場合
                        if($GLOBALS["userData"]["lang"] == "2"){
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                        }
                        else{
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["prefectureList"][$pa_param["edata".$item_id]] : "";
                        }
                        break;
                    //共著者の有無
                    case 29:
                        $wk_body =  ($pa_param["edata".$item_id] != "") ? $obj->wa_coAuthor[$pa_param["edata".$item_id]] : "";
                        break;

                    //共著者の所属機関
                    case 31:
                        $wk_kikan_names = array();
                        $wk_kikanlist   = explode("\n", $pa_param["edata".$item_id]);

                        $i =  "";
                        $set_select = array();
                        foreach($wk_kikanlist as $num => $val){
                            $i = $num+1;
                            $set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
                            $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
                        }
                        $wk_body = str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names));
                        break;

                    //Mr. Mis Dr
                    case 57:
                        if($pa_param["edata".$item_id] != ""){
                            $wk_body = $GLOBALS["titleList"][$pa_param["edata".$item_id]];
                        }
                        else{
                            $wk_body = "";
                        }
                        break;
                    //国名リストボックス
                    case 114:
                        $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["country"][$pa_param["edata".$item_id]] : "";
                        break;
                    default:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                        break;
                }
            }
            $groupBody[$group][] = "\"".$wk_body."\"";
        }


        $group = 3;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            // 任意項目
            if($_data["controltype"] == "1"){
                $wk_body = trim(Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true), ",");

            // 標準項目
            }else{
                switch($item_id){
                    //発表形式
                    case 48:
                        $keisiki = "";
                        if($pa_param["edata".$item_id] != ""){
                            if($obj->itemData[$item_id]["select"]){
                                $keisiki = $obj->itemData[$item_id]["select"][$pa_param["edata".$item_id]];
                            }
                        }
                        $wk_body = str_replace(array("\r\n","\n","\r"), '', $keisiki);
                        break;

                    default:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                        break;
                }
            }
            $groupBody[$group][] = "\"".$wk_body."\"";
        }


        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $cnt      = count($pa_param["chosya"]);
            $loop_cnt = $obj->loop +1;

            for($i=1; $i < $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    $item_id = $_data["item_id"];

                    // 共著者の員数を越えた場合はNULL埋め
                    if($cnt < $i){
                        if($item_id == "34") continue;
                        if($item_id == "36") continue;

                        $groupBody[$group][] = "\"\"";
                        continue;
                    }

                    $wk_body = "";
                    $chosya  = $pa_param["chosya"][$i-1];

                    // 任意項目
                    if($_data["controltype"] == "1"){
                        $wk_body = trim(Usr_Assign::nini($obj, $group, $item_id, $chosya["edata".$item_id], array(" ", ","), true), ",");

                    // 標準項目
                    }else{
                        switch($item_id){
                            //共著者所属期間
                            case 32:
                                if($i == 1) continue 2;
                                if(count($set_select) > 0){
                                    if($chosya["edata".$item_id] != ""){
                                        $chkeck = explode("|", $chosya["edata".$item_id]);
                                        $chkeck_names = array();

                                        foreach($chkeck as $val){
                                            if($val != "" && array_key_exists($val, $set_select)){
                                                $chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $set_select[$val]);
                                            }
                                        }

                                        if(count($chkeck_names) > 0){
                                            $wk_aaa =   implode(",", $chkeck_names);
                                        }

                                    }
                                    else{
                                        $wk_aaa = "";
                                    }

                                    $wk_body = str_replace(array("\r\n","\n","\r"), '', trim($wk_aaa));
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            //共著者姓名
                            case 33:
                                $cyosya_name = "";
                                $wk_mark = array();
                                if($chosya["edata32"] != ""){
                                    $chkeck = explode("|", $chosya["edata32"]);
                                    foreach($chkeck as $val){
                                        $wk_mark[] = $val.")";
                                    }
                                    $cyosya_name = implode(", ", $wk_mark);

                                }
                                $cyosya_name .= $chosya["edata".$item_id]." ".$chosya["edata34"];
                                $wk_body = $cyosya_name;
                                break;

                            //共著者姓名（カナ）
                            case 35:
                                $wk_body = $chosya["edata".$item_id]." ".$chosya["edata36"];
                                break;

                            case 34:
                            case 36:
                                continue 2;
                                break;


                            //会員・非会員
                            case 41:
                                $wk_body = ($chosya["edata".$item_id] != "") ? $obj->wa_kaiin[$chosya["edata".$item_id]] : "";
                                break;

                            //Mr. Mis Dr
                            case 61:
                                if($chosya["edata".$item_id] != ""){
                                    $wk_body = $GLOBALS["titleList"][$chosya["edata".$item_id]];
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            case 46:
                            case 47:
                            default:
                                $wk_body = $chosya["edata".$item_id];
                                break;
                        }
                    }
//                    if(strlen($wk_body) == 0) continue;
                    $groupBody[$group][] = "\"".$wk_body."\"";
                }
            }
        }


        $body = $groupBody[1];
        if(count($groupBody[3]) > 0) $body = array_merge($body, $groupBody[3]);
        if(count($groupBody[2]) > 0 && !$all_flg) $body = array_merge($body, $groupBody[2]);
        //状態
        $body[] = ($pa_param["status"] == "0") ? "\"-\"" : "\"".$GLOBALS["entryStatusList"][$pa_param["status"]]."\"";
        $body[] = "\"".$pa_param["insday"]."\"";    // 登録日
        $body[] = "\"".$pa_param["upday"]."\"";     // 更新日

        $ret_buff .= implode($obj->delimiter, $body);
        $ret_buff .= "\n";

        return $ret_buff;
    }


}

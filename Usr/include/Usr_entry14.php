<?php

/**
 * cfp14番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2012.12.05
 *
 */
class Usr_Entry14 extends Usr_Entry {

	var $prefix = "edata";
	var $itemID = NULL;

    function __construct(){
        parent::__construct();

        // [cfp-007]「見積No3」jQuery多重読み込み調整のためコメントアウト
//        $this->useJquery = true;
        // 筆頭者は利用しない
        $this->author_start_index = 1;
    }

//    function developfunc(){
//        // メールデバッグ
//        $this->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
//    }


	function _check1() {
	    parent::_check1();

        $key26 = array_keys($this->itemData[26]['select']);
        if(end($key26) == $this->arrParam['edata26']){
            if(strlen($this->arrParam["edata27"]) == 0){
                $this->objErr->addErr('Please enter the country name in the edit field if you choose "Others" from the country list.'
                                    , 'edata27');
            }
        }

        // 共著者チェック
        if($this->arrParam['edata29'] == 1 && $this->arrParam['edata30'] > 0){
            $this->_check2();
        }
	}


    function _check2(){
        parent::_check2();

        $form1data = $GLOBALS["session"]->getVar("form_param1");

        $key26 = array_keys($this->itemData[26]['select']);

        //共著者の数
        for($i = $this->author_start_index; $i < $form1data["edata30"]+1; $i++) {
            $index = $i+1;
            if(end($key26) == $this->arrParam["edata32".$i."1"]){
                // Affiliationでotherを選択したがテキストボックスが空
                if(strlen($this->arrParam["edata43".$i]) == 0){
                    $this->objErr->addErr('Please enter the country name in the edit field if you choose "Othes" from the country list('.$index.').'
                                        , 'edata43'.$i);
                }
            }
        }
    }


    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }

    // 共著者チェック
	function _checkAuthor() {

		if($this->itemData[29]["disp"] == "1") return;

		if(!isset($this->arrParam["edata29"])) $this->arrParam["edata29"] = "";
		if($this->arrParam["edata29"] == "") {

			$msg = sprintf($GLOBALS["msg"]["err_require_select"], $this->itemData[29]['strip_tags']);
			$this->objErr->addErr($msg, "edata29");
			return;

		}

		//共著なしの場合
		if($this->arrParam["edata29"] == "2"){
		    if(strlen($this->arrParam["edata30"]) > 0){
    			$this->objErr->addErr('Inconsistency in the number of co-authors. If you would like to register co-author(s), please choose "Yes" in "Co-Author(s)".', "edata30");
		    }
			return;
		}

		//共著者の数
		if($this->itemData[30]["disp"] != "1"){

			if(!isset($this->arrParam["edata30"])) $this->arrParam["edata30"] = "";
			if($this->arrParam["edata30"] == ""){
				$msg = sprintf($GLOBALS["msg"]["err_require_input"], $this->itemData[30]['strip_tags']);
				$this->objErr->addErr($msg, "edata30");
			}


			//半角数字で入力されていない場合
			if(!$this->objErr->isNumeric($this->arrParam["edata30"])){
				$msg = sprintf($GLOBALS["msg"]["err_han_numeric"], $this->itemData[30]['strip_tags']);
				$this->objErr->addErr($msg, "edata30");
			}

			// 15人より大きい場合
			if($this->arrParam["edata30"] > $this->getAuthorMax()){
				$msg = sprintf($GLOBALS["msg"]["err_over_max_author"], $this->itemData[30]['strip_tags'], $this->getAuthorMax());
				$this->objErr->addErr($msg, "edata30");
			}

		}

		//共著者　所属機関
		if($this->itemData[32]["disp"] != "1"){
			if($this->arrParam["edata31"] == ""){
				$msg = sprintf($GLOBALS["msg"]["err_require_input"], $this->itemData[31]['strip_tags']);
				$this->objErr->addErr($msg, "edata31");
			}
		}

		return;

	}


	// 3ページ目のチェック
	function _check3() {
		parent::_check3();

        // エラーメッセージ変更
        if(isset($this->objErr->_err['edata50'])){

        	if(strpos($this->objErr->_err['edata50'], "Enter") !== false) {
        		$this->objErr->_err["edata50"] = "Please enter the abstract of your paper within 150 words.";
        	}
        	if(strpos($this->objErr->_err['edata50'], "single-byte alphanumeric characters") !== false) {
        		$this->objErr->_err["edata50"]
        			= "Please re-enter the abstract of your paper within 150 words in single-byte alphanumeric characters and symbols.";
        	}
        	if(strpos($this->objErr->_err['edata50'], "cannot exceed") !== false) {
        		$this->objErr->_err["edata50"] = "Please enter the abstract of your paper within 150 words.";
        	}
        }

        if(isset($this->objErr->_err['edata51'])){
            $this->objErr->_err['edata51'] = "Please upload your full paper in PDF file.";
        }
        if(isset($this->objErr->_err['edata52'])){
            $this->objErr->_err['edata52'] = "Please upload the copyright transfer form in PDF file.";
        }

        // 拡張子チェック
        if(isset($_FILES['edata51']) && !$this->checkExt($_FILES['edata51'], array("pdf"))){
            $this->objErr->addErr("Please upload your full paper in PDF file.", "edata51");
            $this->arrParam['hd_file51'] = "";
        }
        if(isset($_FILES['edata52']) && !$this->checkExt($_FILES['edata52'], array("pdf"))){
            $this->objErr->addErr("Please upload the copyright transfer form in PDF file.", "edata52");
            $this->arrParam['hd_file52'] = "";
        }


//        $this->itemID = 56;
//        $itemData56 = $this->getItemDataChecked($this->itemData[56]['select'], $this->arrParam);
//        if(count($this->itemData[56]['select']) != count($itemData56)){
//            $this->objErr->addErr("Check all for Before submitting your paper, you must confirm the following:.", "edata56");
//        }
	}


	// ファイルの拡張子チェック
	function checkExt($fileInfo, $arrExt=""){

		if($fileInfo['error'] == 4) return true;

		$ext = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));

		$res = false;
		foreach($arrExt as $checkExt){
			if($checkExt == $ext){
				$res = true;
			}
		}
		return $res;
	}


    function getItemDataChecked($itemData, $arrParam){
        return array_intersect_key($this->getItemDataArray($itemData), $arrParam);
    }


    /**
     * itemIDの要素分だけ要素名の配列を返す
     * @return array array([] => "edata"."itemID"."要素の添え字")
     */
    function getItemDataArray($itemData){
        return array_flip(array_map(array($this, "prefixArrayValue"), array_keys($itemData)));
    }


    /**
     * 配列の要素に接頭語をつけて返す
     * @use array_map
     */
    function prefixArrayValue($item){
        return $this->prefix . $this->itemID . $item;
    }


    /**
     * 完了メール作成
     */
    function makeMailBody($user_id="", $passwd ="", $exec_type = ""){

        $body = "";
        // メール上部コメントの作成
        $head_comment = $this->makeMailBody_header($user_id, $passwd, $exec_type);
        $body .= $head_comment."\n\n";

        // 言語別設定
        $this->point_mark = ($this->formdata["lang"] == LANG_JPN) ? "■" : "*";

        // グループ別メール本文
        $arrbody = array();

        // グループ1
        $this->makeBodyGroup1($arrbody);

        // グループ2
        $this->makeBodyGroup2($arrbody);

        // グループ3
        $this->makeBodyGroup3($arrbody);


        //----------------------------------------
        //本文生成
        //----------------------------------------
        $body .= $arrbody[1];
        if($this->formdata["group2_use"] != "1") $body .= $arrbody[2];
        if($this->formdata["group3_use"] != "1") $body .= $arrbody[3];

        //$body .= print_r($this->arrForm, true);

        $body .= "\n";
        $body .= "\n";
        $body .= "For any inquiries regarding paper submission, please contact the Paper Submission Office at vlsisymp@jtbcom.co.jp.\n";
        $body .= "Please make sure to indicate your ID in all correspondence.\n";
        $body .= "\n";
        $body .= "Thank you again for your submission.\n";
        $body .= "\n";
        $body .= "Sincerely,";

        $body .= "\n\n------------------------------------------------------------------------------\n";
        $body .= $this->formdata["contact"];
        $body .= "\n------------------------------------------------------------------------------\n";

        // 2011/09/21 タグ除去追加 TODO
//        $body = strip_tags($body);


        return str_replace(array("\r\n", "\r"), "\n", $body);

    }


	function mailfunc7($name) {
	    $str  = "";
		$str .= $this->point_mark.$name.": ".$this->arrForm["edata7"]."\n";
		$str .= $this->point_mark.$this->itemData[26]['item_name'].": ";

		if($this->arrForm["edata26"] > 0 && strlen($this->arrForm["edata26"]) > 0){
		    $str .= str_replace(array("\r\n","\r","\n"), '', $this->itemData[26]['select'][$this->arrForm["edata26"]]);
		}

		if(strlen($this->arrForm["edata27"]) > 0){
            $str .= " ".$this->arrForm["edata27"];
		}
		$str .= "\n";

		return $str;
    }

	function mailfunc48($name) {
        $str  = parent::mailfunc48($name);
        $str .= parent::mailfuncNini(54, strip_tags($this->itemData[54]["item_name"]));

        return $str;
    }

    // affliation#select
	function mailfunc26($name) {
		return "";
    }
    // affliation#text
	function mailfunc27($name) {
		return "";
    }
    // Paper Type#select
	function mailfunc54($name) {
		return "";
    }

	function getAuthorMax() {
		return 35;
	}

    // Affiliations of Authors
	function mailfunc31($name) {
		return "";
    }

    // Affiliations#other
	function mailfunc43($name) {
		return "";
    }


    /**
     * 共著者　所属期間リスト
     */
    function mailfunc32($name, $i) {

        $str = $this->point_mark.$name.": ";

        if(!isset($this->arrForm["edata31"]) || $this->arrForm["edata31"] == "") return $str."\n";

        if(isset($this->arrForm["edata31"])) {
            $wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);
            $this->arrForm["kikanlist"] = $wa_kikanlist;
        }

        foreach($this->arrForm["kikanlist"] as $key=>$val) {

            $p_key = 1;

            if(isset($this->arrForm["edata32".$i."1"])) {
                if($this->arrForm["edata32".$i."1"] == $key) {
                    $str .= trim($val)." ";
                    break;
                }
            }

        }

        $str .= $this->arrForm["edata43".$i];

        return $str."\n";
    }



	/**
	 * 登録パラメータの生成　共著者
	 */
	function _makeDbParamAuthor($i) {

		$wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);

		foreach($this->itemData as $item_id=>$val) {
			if($val["group_id"] != "2") continue;
			if($val["disp"] == "1") continue;

			switch($item_id) {

				case "32":
						if(!count($wa_kikanlist) > 0) break;

						$wk_arr = array();
						foreach($wa_kikanlist as $key=>$val) {
							$wk_val = isset($this->arrForm["edata".$item_id.$i.$key]) ? $this->arrForm["edata".$item_id.$i.$key] : "";
							if($wk_val != ""){
							    $wk_arr[] = $wk_val;
							    break;
							}
						}
						if(count($wk_arr) > 0){
						    $param["edata".$item_id] = implode("|", $wk_arr);
						    $GLOBALS["log"]->write_log("cfp-form14-共著者", $param["edata".$item_id].print_r($this->arrForm,true));
						}

					break;
				default:

						// チェックボックス系
						if($val["item_type"] == "3") {

							if(!count($val["select"]) > 0) break;
							$wk_arr = array();
							foreach($val["select"] as $selectkey=>$selectval) {
								if(!isset($this->arrForm["edata".$item_id.$i.$selectkey])) continue;
								if($this->arrForm["edata".$item_id.$i.$selectkey] == $selectkey) $wk_arr[] = $selectkey;
							}
							$param["edata".$item_id] = implode("|", $wk_arr);

							break;

						}

						if(!isset($this->arrForm["edata".$item_id.$i])) $this->arrForm["edata".$item_id.$i] = "";
						$param["edata".$item_id] = $this->arrForm["edata".$item_id.$i];

					break;

			}

		}

		return $param;
	}


	function getEntry() {

		$workDir = $this->uploadDir.$this->eid."/".date("YmdHis");

		//ファイルアップロード用　ワークディレクトリ生成
		if(!is_dir($workDir)){
			mkdir($workDir, 0777, true);
			chmod($workDir, 0777);
		}
		//セッションにワークディレクトリ名をセット
		$GLOBALS["session"]->setVar("workDir", $workDir);

		//応募データ取得
		$wa_entrydata = $this->o_entry->getRntry_r($this->db, $this->eid, $this->form_id);
		if(!$wa_entrydata){
			Error::showErrorPage("応募情報の取得に失敗しました。");
		}

		// 共著者有無を保存しておく
		$this->arrForm["pre_edata29"] = $this->arrForm["edata29"];

		//応募者のID・パスワード
		$ss_entry_user = array("e_user_id" => $wa_entrydata["e_user_id"], "e_user_passwd" => $wa_entrydata["e_user_passwd"]);
		$GLOBALS["session"]->setVar("SS_USER", $ss_entry_user);

		//共著者データ取得
		$wa_aff = $this->o_entry->getRntry_aff($this->db, $this->eid);

		//----------------------------
		//管理者モードの場合
		//----------------------------
		if($this->admin_flg != ""){
			$wk_user_id["e_user_id"] =  $wa_entrydata["e_user_id"];
			$GLOBALS["session"]->setVar("isLoginEntry", true);
			$GLOBALS["session"]->setVar("entryData", $wk_user_id);
		}

		//----------------------
		//1ページ目データ
		//----------------------
		$start = 1;
		$end = 32;

		for($index=$start; $index < $end; $index++){

			if($index != 26 && $index != 27 && $index != 28){
				$this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
			}
			else{

				//任意項目
				$this->_default_arrFormNini(26, 29, $wa_entrydata);
			}
		}

		$this->_default_arrFormNini(63, 65, $wa_entrydata);

		//任意項目6～20
		$this->_default_arrFormNini(69, 84, $wa_entrydata);

		//任意項目21～50
		$this->_default_arrFormNini(115, 145, $wa_entrydata);

		//英語フォーム固有の項目
		$start = 57;
		$end = 61;
		for($index=$start; $index < $end; $index++){
			$this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
		}
		//国名リストボックスの値
		$this->arrForm["edata114"] = $wa_entrydata["edata114"];

		$this->arrForm["chkemail"] = $wa_entrydata["edata25"];

		$GLOBALS["session"]->setVar("form_param1", $this->arrForm);

		//----------------------
		//2ページ目データ
		//----------------------
		$this->arrForm = "";

		//共著者の登録がある場合
		if($wa_aff){
			foreach($wa_aff as $a_key => $data){
				$start = 32;
				$end = 46;

				for($index=$start; $index < $end; $index++){

					if($index != 43 && $index != 44 && $index != 45){

						//共著者の所属機関
						if($index == 32){
							$wk_checked  = explode("|", $data["edata".$index]);
							foreach($wk_checked as $_key => $chk_val){
								$this->arrForm["edata".$index.$a_key."1"] = $chk_val;
							}
						}
						else{
							$this->arrForm["edata".$index.$a_key] = $data["edata".$index];
						}

					}
					else{

						//任意項目生成
						$this->_default_arrFormChosyaNini(43, 46, $data, $a_key);
					}
				}

				//英語フォーム固有の項目
				$start = 61;
				$end = 63;

				for($index=$start; $index < $end; $index++){
					$this->arrForm["edata".$index.$a_key] = $data["edata".$index];

				}


				//共著者ID
				$kyouid[$a_key] = $data["id"];

				//任意項目生成
				$this->_default_arrFormChosyaNini(65, 68, $data, $a_key);
				$this->_default_arrFormChosyaNini(84, 99, $data, $a_key);

			}
			$GLOBALS["session"]->setVar("form_param2", $this->arrForm);
			$GLOBALS["session"]->setVar("kyouID", $kyouid);

		}


		//----------------------
		//3ページ目データ
		//----------------------
		$this->arrForm = "";
		$start = 46;
		$end = 57;

		for($index=$start; $index < $end; $index++){

			if($index != 54 && $index != 55 && $index != 56){
				$this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
			}
			else{

				$this->_default_arrFormNini(54, 57, $wa_entrydata);
			}

			//添付資料
			if($index == 51 || $index == 52 || $index == 53){

				//--------------------------
				//ワークディレクトリにファイルを移動
				//--------------------------
				if(is_file($this->uploadDir.$this->eid."/".$wa_entrydata["edata".$index])){
					if (!copy($this->uploadDir.$this->eid."/".$wa_entrydata["edata".$index], $workDir."/".$wa_entrydata["edata".$index])) {
						$this->complete("ファイルのコピーに失敗しました。");
					}



				}
				$this->arrForm["hd_file".$index] = $wa_entrydata["edata".$index];
				$this->arrForm["n_data".$index] = $wa_entrydata["edata".$index];
			}
		}

		$this->_default_arrFormNini(67, 69, $wa_entrydata);
		$this->_default_arrFormNini(99, 114, $wa_entrydata);

		$this->_default_arrFormNini(145, 160, $wa_entrydata);

		$GLOBALS["session"]->setVar("form_param3", $this->arrForm);

		//-------------------------------
		//セッション取得
		//-------------------------------
		$this->getDispSession();

		return;

	}



    function ajaxAction(){
        $arrForm = $GLOBALS["session"]->getVar("form_param1");
//         if(!isset($arrForm['edata31'])){
//             $arrForm['edata31'] = isset($_REQUEST['edata31'])
//                                 ? $_REQUEST['edata31']
//                                 : $this->arrItemData[1][26]['item_select'];
//             $GLOBALS["session"]->setVar("form_param1", $arrForm);
//         }
        $arrForm['edata31'] = $this->arrItemData[1][26]['item_select'];
        $GLOBALS["session"]->setVar("form_param1", $arrForm);
        $this->getDispSession();

        // チェック用の変数に入れる
//        $this->arrForm = GeneralFnc::convertParam($this->_init(1), $_REQUEST);


        $num = $_POST['num'];


        // 共著者なし
        if(empty($num)){
            echo ''; exit;
        }

        $this->arrForm['edata30'] = $num;
        $this->arrForm["edata330"] = isset($this->arrForm["edata1"])  ? $this->arrForm["edata1"]  : "";
        $this->arrForm["edata340"] = isset($this->arrForm["edata2"])  ? $this->arrForm["edata2"]  : "";
        $this->arrForm["edata350"] = isset($this->arrForm["edata3"])  ? $this->arrForm["edata3"]  : "";
        $this->arrForm["edata360"] = isset($this->arrForm["edata4"])  ? $this->arrForm["edata4"]  : "";
        $this->arrForm["edata370"] = isset($this->arrForm["edata5"])  ? $this->arrForm["edata5"]  : "";
        $this->arrForm["edata380"] = isset($this->arrForm["edata6"])  ? $this->arrForm["edata6"]  : "";
        $this->arrForm["edata390"] = isset($this->arrForm["edata7"])  ? $this->arrForm["edata7"]  : "";
        $this->arrForm["edata400"] = isset($this->arrForm["edata25"]) ? $this->arrForm["edata25"] : "";
        $this->arrForm["edata410"] = isset($this->arrForm["edata8"])  ? $this->arrForm["edata8"]  : "";
        $this->arrForm["edata420"] = isset($this->arrForm["edata9"])  ? $this->arrForm["edata9"]  : "";
        $this->arrForm["edata610"] = isset($this->arrForm["edata57"]) ? $this->arrForm["edata57"] : "";
        $this->arrForm["edata620"] = isset($this->arrForm["edata7"])  ? $this->arrForm["edata7"]  : "";

        $this->assign("onload",     $this->onload);
        $this->assign("workDir",    $this->workDir);
        $this->assign("block",      $this->block);
        $this->assign("arrErr",     $this->arrErr);	//エラー配列
        $this->assign("admin_flg",  $this->admin_flg);	//エラー配列
        $this->assign("arrItemData",$this->arrItemData);
        $this->assign("formItem",   $this->itemData);        //画面表示項目
        $this->assign("group3use",  $this->group3use);  // グループ3見出し表示フラグ
        $this->assign("group3item", $this->group3item); // グループ3のグループ分け
        $this->assign("need_mark",  $this->need_mark);	//必須マーク
        $this->assign("yen_mark",   $this->yen_mark);
        $this->assign("arrForm" ,   $this->arrForm);
        $this->assign("author_start_index", $this->author_start_index);

        $block2 = $this->_smarty->fetch("Usr/include/14block2.html");
        echo $block2;
        exit;
    }


    function pageAction1() {
        $fix_flg = "";

        //3ページ目を使用
        if($this->formdata["group3_use"] != "1"){
            $this->block = "3";
            $fix_flg = "1";

            $this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_j.html" : "Usr/form/Usr_entry_e.html";
        }

        //上記のいずれも該当しなかった場合
        if($fix_flg == ""){
            $this->block = "4";
            $this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_confirm_j.html" : "Usr/form/Usr_entry_confirm_e.html";
        }

        return;
    }


    function backAction() {
        // 確認画面以外から戻るとき
        if($this->wk_block != "4"){
            $getFormData= GeneralFnc::convertParam($this->_init($this->wk_block), $_REQUEST);

            // ファイル引き継ぎ
            foreach($this->arrfile as $item_id => $n){
                $key = "n_data".$item_id;
                $getFormData[$key] = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
            }

            // セッションにページパラメータをセットする
            $GLOBALS["session"]->setVar("form_param".$this->wk_block, $getFormData);
        }

        // セッションパラメータ取得
        $this->getDispSession();

        switch($this->wk_block) {
            case "1":
            case "2":
                $this->block = "1";
                break;

            // 3ページ目からの戻り
            case "3";
                $this->block = "1";
                break;

            case "4":
                $this->block = "1";
                if($this->formdata["group3_use"] != "1") {
                    $this->block = "3";
                } else if($this->formdata["group2_use"] != "1") {
                    $this->block = "2";
                }
        }

        if(isset($this->arrForm["edata31"])) {
            $wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);
            $this->arrForm["kikanlist"] = $wa_kikanlist;
        }

        return;
    }



    function pageAction() {
        // フォームパラメータ
        $getFormData= GeneralFnc::convertParam($this->_init($this->wk_block), $_REQUEST);

        // チェック用の変数に入れる
        $this->arrParam = $getFormData;

        //セッションにページパラメータをセットする
        $GLOBALS["session"]->setVar("form_param".$this->wk_block, $this->arrParam);
        // ブロック1にブロック2を含める
        if($this->wk_block == 1){
            $arrForm2 = GeneralFnc::convertParam($this->_init(2), $_REQUEST);
            $GLOBALS["session"]->setVar("form_param2", $arrForm2);
            // 入力パラメータをマージ
            $this->arrParam = array_merge($getFormData, $arrForm2);
        }

        // エラーチェック
        $this->arrErr = $this->_check();

        // セッションパラメータ取得
        $this->getDispSession();

        // エラーがある場合
        if(count($this->arrErr) > 0){
            //エラーを自ページに表示
            $this->block = $this->wk_block;
            //$this->arrForm = $getFormData;
            return;
        }

        $blockaction = "pageAction".$this->wk_block;
        $this->$blockaction();
        return;
    }

}

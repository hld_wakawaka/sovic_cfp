<?php

/**
 * 64番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.kawauchi
 * 
 */
class Usr_Entry72sort
{
    /**  CSVヘッダ生成 */
    function entry_csv_entryMakeHeader($obj, $all_flg=false){

        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());

        // グループ1ッダ
        $groupHeader[1] = $obj->entryMakeHeader1($obj, $all_flg);

        // グループ2ヘッダ # 日英で分岐
        $group2_func = 'entryMakeHeader2';
        if($GLOBALS["userData"]["lang"] == LANG_ENG) $group2_func = $group2_func.'_'.$GLOBALS["userData"]["lang"]; // 英語
        $groupHeader[2] = $obj->$group2_func($obj, $all_flg);

        // グループ3ヘッダ
        $groupHeader[3] = $obj->entryMakeHeader3($obj, $all_flg);

        // [cfp-form72] 見積No1:帯名を「Presenting Author」から「Corresponding (Presenting) Author 」への変更
        $groupHeader[1][17] = "Number of Co-Author(s)not including a Corresponding (Presenting) Author";
        $groupHeader[3][6] = "3) Registration Number of a Corresponding (Presenting) Author";

        $header = $groupHeader[1];

        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[]="状態";
        $header[]="エントリー登録日";
        $header[]="エントリー更新日";

        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }
    
    function doSort($obj){
        $GLOBALS["msg"]["entrant"] = "Corresponding (Presenting) Author";
        $GLOBALS["langtxt"]["entrant"][2] = "Corresponding (Presenting) Author";
    }

}

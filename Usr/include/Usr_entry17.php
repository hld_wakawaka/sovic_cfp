<?php

/**
 * 17番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2013.03.29
 * 
 */
class Usr_Entry17 extends Usr_Entry {

	var $prefix = "edata";
	var $itemID = NULL;
	
	function __construct(){
	    parent::__construct();

        $GLOBALS["msg"]["cofirm_title"]			= "<b>Confirmation screen for your full paper submission</b>";
        $GLOBALS["msg"]["cofirm_desc2"]			= "<b>*Your full paper submission has not been completed yet. <br />Please be sure to click the [submit] button to complete the procedure.</b>";
	}


    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }

	/**
	 * 2ページ目の1人目の項目を隠すに関連して、1人目のデータでエラーが有る場合は、削除
	 */
	function _check() {
		
		parent::_check();
		
	    unset($this->objErr->_err["edata3201"]);
	    unset($this->objErr->_err["edata330"]);
	    unset($this->objErr->_err["edata340"]);
	    
	    return $this->objErr->_err;
		
	}
	
	/**
	 * Presenting AuthorのEmailの一致チェック追加
	 * Presenting Authorのエラーメッセージに[Presenting Author]を追加
	 */
	function _check1() {
		
		parent::_check1();
		
		//メールアドレス一致チェック
		if($this->arrParam["edata70"] != "" && $this->itemData[70]["mail"] == "1"){
			if($this->arrParam["edata70"] != $_REQUEST["chkemail2"]){
				$msg = sprintf($GLOBALS["msg"]["err_mail_match"], $this->itemData[70]['strip_tags'], $this->itemData[70]['strip_tags']);
				$this->objErr->addErr($msg, "edata70");
			}
		}
		
		// Presenting Authorのエラーの場合は、[Presenting Author]をつける
		
		$arrKey = array("edata26", "edata27", "edata28", "edata63", "edata64", "edata69", "edata70");
		foreach($arrKey as $key) {
			if(isset($this->objErr->_err[$key]) && $this->objErr->_err[$key] != "") {
			$this->objErr->_err[$key] = "[Presenting Author]".$this->objErr->_err[$key];
			}
		}
		
		return;
		
	}

	function _check3() {
		
		// ファイルのエラーメッセージを変更する
		$GLOBALS["msg"]["err_file_null"] = "Please confirm the box(es) of Abstract File.";
		
	    parent::_check3();
	    
	    
	    $count = count($this->itemData[67]['select']);
	    for($i=1; $i<=$count; $i++){
	        if(!isset($this->arrParam["edata67".$i]) || $this->arrParam["edata67".$i] != $i){
	            $this->objErr->addErr("Check all for ".$this->itemData[67]['strip_tags'].".", 'edata67');
	            break;
	        }
	    }

//        // 拡張子チェック
//		if(!$this->checkExt($_FILES['edata51'], array("doc", "docx"))){
//			$this->objErr->addErr("Abstract File (Word) in MS word format.", "edata53");
//			$this->arrParam['hd_file51'] = "";
//        }


        $this->itemID = 55;
        $itemData55 = $this->getItemDataChecked($this->itemData[55]['select'], $this->arrParam);
        if(count($itemData55) > 1){
            $this->objErr->addErr("Check one for ".$this->itemData[55]['strip_tags'].".", "edata55");
        }

        $this->itemID = 56;
        $itemData56 = $this->getItemDataChecked($this->itemData[56]['select'], $this->arrParam);
        if(count($itemData56) > 1){
            $this->objErr->addErr("Check one for ".$this->itemData[56]['strip_tags'].".", "edata56");
        }

        // 単語数を300以上～500未満に制限
        if(!in_array($this->arrItemData[3][50]['disp'], array(1,2))){
            $edata50 = preg_replace("/(\.|\,|\?|\!)+/", " ", $this->arrParam["edata50"]);
            $edata50 = preg_replace('/[\s　]+/u', ' ', $edata50);
            $edata50 = trim($edata50);
            $count50 = count(explode(" ", $edata50));

            if(!(300 <= $count50 && $count50 < 500)){
                $this->objErr->addErr("Enter ".$this->itemData[50]['strip_tags'].".", "edata50");
            }
        }
	}
	
	/**
	 * Presenting Authorの確認用を追加
	 */
	function _init($ps_block){
		
		$key = parent::_init($ps_block);
		
		$key[] = array("chkemail2", "", array(),	array(),	"",	0);
		
		return $key;
		
		
	}


    function getItemDataChecked($itemData, $arrParam){
        return array_intersect_key($this->getItemDataArray($itemData), $arrParam);
    }


    /**
     * itemIDの要素分だけ要素名の配列を返す
     * @return array array([] => "edata"."itemID"."要素の添え字")
     */
    function getItemDataArray($itemData){
        return array_flip(array_map(array($this, "prefixArrayValue"), array_keys($itemData)));
    }


    /**
     * 配列の要素に接頭語をつけて返す
     * @use array_map
     */
    function prefixArrayValue($item){
        return $this->prefix . $this->itemID . $item;
    }


	/*
	 * ファイルの拡張子チェック
	 */ 
	function checkExt($fileInfo, $arrExt=""){

		if($fileInfo['error'] == 4) return true;

		$ext = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));

		$res = false;
		foreach($arrExt as $checkExt){
			if($checkExt == $ext){
				$res = true;
			}
		}
		return $res;
	}
	
	/**
	 * 完了メール作成
	 */
	function makeMailBody($user_id="", $passwd ="", $exec_type = ""){

		$yen_mark = "JPY";

		$body = "";
		
		// メール上部コメントの作成
		$head_comment = $this->makeMailBody_header($user_id, $passwd, $exec_type);
		$body .= $head_comment."\n\n";

		
		// 言語別設定
		if($this->formdata["lang"] == "1"){
			$arrbody[1] = ($this->formdata["group1"] != "") ? "【".$this->formdata["group1"]."】\n\n" : "";
			
			$body2_title[1] = "\n\n【".$GLOBALS["msg"]["entrant"]."】\n\n";
			$body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n【".$this->formdata["group2"]."】\n\n" : "\n\n";
			
			$arrbody[2] = "";
			$arrbody[3] = ($this->formdata["group3"] != "") ? "\n\n【".$this->formdata["group3"]."】\n\n" : "\n\n";
			$this->point_mark = "■";
		}
		else{
			$arrbody[1] = ($this->formdata["group1"] != "") ? "[".$this->formdata["group1"]."]\n\n" : "";
			
			$body2_title[1] = "\n\n[".$GLOBALS["msg"]["entrant"]."]\n\n";
			$body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n[".$this->formdata["group2"]."]\n\n" : "\n\n";
			
			$arrbody[2] = "";
//			$arrbody[3] = ($this->formdata["group3"] != "") ? "\n\n[".$this->formdata["group3"]."]\n\n" : "\n\n";
			$arrbody[3] = "\n\n";
			$this->point_mark = "*";
		}
		
		$group2_key = array();	//グループ2で利用するitem_id
		
		// グループ1とグループ3
		foreach($this->itemData as $key => $data){
			if($data["disp"] == "1" || $data["item_mail"] == "1") continue;
			if($data["group_id"] == "2") {
				$group2_key[] = $key;
				continue;
			}
			
			// 項目名（タグをとる)
			$data["item_name"] = strip_tags($data["item_name"]);
			$name = $data["item_name"];
			$group = $data["group_id"];

			$methodname = "mailfunc".$key;
			if(method_exists($this, $methodname)) {
				$arrbody[$group] .= $this->$methodname($name);
			} else {
				if($data["controltype"] == "1") {
					$arrbody[$group] .= $this->mailfuncNini($key, $name);	//任意
				} else {
					$arrbody[$group] .= $this->point_mark.$name.": ".$this->arrForm["edata".$key]."\n";
				}
			}
		}
		
		// グループ2が存在する時
		// 0は出力しない（カスタマイズ）
		if($this->formdata["group2_use"] != "1") {
			
			if(isset($this->arrForm["edata31"]) && $this->arrForm["edata31"] != "") {
			
			if(!isset($this->arrForm["edata30"])) $this->arrForm["edata30"] = 0;
			for($i = 1; $i < $this->arrForm["edata30"]+1; $i++) {
				
				if(isset($this->arrForm["group2_del"])) {
					if(in_array($i, $this->arrForm["group2_del"])) {
						continue;
					}
				}
				
				if($i == "0") {
					$arrbody[2] .= $body2_title[1];
				} elseif($i == 1) {
					$arrbody[2] .= $body2_title[2];
				}
				
				foreach($group2_key as $key) {
					
					$name = $this->itemData[$key]["strip_tags"];
					
					$methodname = "mailfunc".$key;
					if(method_exists($this, $methodname)) {
						$arrbody[2] .= $this->$methodname($name, $i);
					} else {
						if($this->itemData[$key]["controltype"] == "1") {
							$arrbody[2] .= $this->mailfuncNini($key, $name, $i);	//任意
						} else {
							$arrbody[2] .= $this->point_mark.$name.": ".$this->arrForm["edata".$key.$i]."\n";
						}
					}
				
				}
				
				$arrbody[2] .= "\n";
				
			}
			
			}
			
		} 


		//----------------------------------------
		//本文生成
		//----------------------------------------
		$body .= $arrbody[1];
		if($this->formdata["group2_use"] != "1") $body .= $arrbody[2];
		if($this->formdata["group3_use"] != "1") $body .= $arrbody[3];
		
		//$body .= print_r($this->arrForm, true);

		$body .= "\n\n------------------------------------------------------------------------------\n";
		$body .= $this->formdata["contact"];
		$body .= "\n------------------------------------------------------------------------------\n";
		
		// 2011/09/21 タグ除去追加 TODO
//		$body = strip_tags($body);
		

		return str_replace(array("\r\n", "\r"), "\n", $body);

	}
	
	/**
	 * 帯の追加
	 */
	function mailfunc26($name) {
		
		// タイトル付与（カスタマイズ）
		$str = "\n[Presenting Author]\n\n";
		//$str .= $this->point_mark.$name.": ";
		$str .= $this->point_mark.$name.": ".$GLOBALS["titleList"][$this->arrForm["edata26"]]."\n";
		
		return $str;
		
	}
	
	/**
	 * 帯の追加
	 */
	function mailfunc29($name) {
		
		// タイトル付与（カスタマイズ）
		$str = "\n[Infomation off All Researchers]\n\n";
		//$str .= $this->point_mark.$name.": ";
		$str .= $this->point_mark.$name.": ".$this->wa_coAuthor[$this->arrForm["edata29"]]."\n";
		
		return $str;
		
	}

	/**
	 * 54の上に帯追加
	 */
	function mailfunc54($name) {

		// タイトル付与（カスタマイズ）
	    $str  = "\n";
	    $title = (strlen($this->formdata["arrgroup3"][2]) > 0)
	           ? "[".$this->formdata["arrgroup3"][2]."]"
	           : "";

        $str .= $title."\n\n";
        $str .= $this->point_mark.$name.": ";
        $str .= "\n".$this->arrForm["edata54"];
		$str .= "\n";

		return $str;
	}
	
	/**
	 * 帯の追加
	 */
	function mailfunc47($name) {
	    $str  = "";
	    $title = (strlen($this->formdata["arrgroup3"][0]) > 0)
	           ? "[".$this->formdata["arrgroup3"][0]."]"
	           : "";

        $str .= $title."\n\n";
        $str .= $this->point_mark.$name.": ".$this->arrForm["edata47"]."\n";
		return $str;
	}
    function mailfunc51($name) {
	    $str  = "";
	    $title = (strlen($this->formdata["arrgroup3"][1]) > 0)
	           ? "[".$this->formdata["arrgroup3"][1]."]"
	           : "";

        $str .= $title."\n\n";
        $str .= $this->mailfuncFile(51, $name);
        return $str;
    }

	/**
	 * ページフローを表示する
	 */
	function createPageflow() {
		
			$pageflow[0] = "Corresponding/Presenting Authors";
//			$pageflow[] = "Corresponding/Presenting Authors &amp; All Researchers";
//			$pageflow[] = "Co-Author(s)";
			$pageflow[2] = "Full Paper &amp; Questionnaire";
			$pageflow[3] = "Confirmation";
			$pageflow[4] = "Completing";
			
			$this->assign("pageflow", $pageflow);
		
		return;
		
	}
	
	/**
	 * 登録データ取得
	 * Presenting AuthorのEmailを確認用へ代入を追加
	 */
	function getEntry() {
		
		parent::getEntry();
		
		$entryData = $GLOBALS["session"]->getVar("form_param1");
		
		$entryData["chkemail2"] = $entryData["edata70"];
		
		$GLOBALS["session"]->setVar("form_param1", $entryData);
		
		//-------------------------------
		//セッション取得
		//-------------------------------
		$this->getDispSession();
		
		return;
		
		
	}


}

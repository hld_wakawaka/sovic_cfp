<?php

/**
 * 18番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2013.07.23
 * 
 */
class Usr_Entry18 extends Usr_Entry {

    /**
     * 開発用デバッグメソッド
     * super#mainの最後に実行される
     * 
     */
    function developfunc(){}


	function _check1() {
	    parent::_check1();

        // 応募者と論文発表者が異なる場合
        if(isset($this->arrParam['edata26']) && $this->arrParam['edata26'] == 2){
            $existCheck = array('27', '28', '63', '64', '69');

            // 必須チェック
            foreach($existCheck as $_key => $_chkKey){
                $chkKey = 'edata'.$_chkKey;
                if(!$this->objErr->isNull($this->arrParam[$chkKey])){
                    $this->objErr->addErr("Enter {$this->itemData[$_chkKey]['strip_tags']}."
                                        , $chkKey);
                }
            }
        }
		return;
	}


    function _check3() {
        parent::_check3();

        // 拡張子チェック
        $chkKey = 51;
        $chkString = 'edata'.$chkKey;
        if(isset($_FILES[$chkString]) && !$this->checkExt($_FILES[$chkString], array("doc", "docx"))){
            $this->objErr->addErr("Please upload {$this->itemData[$chkKey]['strip_tags']} in MS Word format (.doc/.docx) only.", $chkString);
            $this->arrParam['hd_file'.$chkKey] = "";
        }
        return;
    }


    // ファイルの拡張子チェック
    function checkExt($fileInfo, $arrExt=array()){
        if($fileInfo['error'] == 4) return true;

        $ext = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));
        return in_array($ext, $arrExt);
    }


}

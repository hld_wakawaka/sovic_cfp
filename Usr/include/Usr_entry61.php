<?php

/**
 * 61番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author kawauchi
 * 
 */

class Usr_Entry61 extends Usr_Entry {

    function __construct(){
        parent::__construct();
    }


    function developfunc(){
//        // メールデバッグ
//        $this->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }

    
    function premain() {
        $this->itemNonDisplay();
    }

    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }


    function _check3() {
        // No.4 # [yes]を選択したらファイルアップロードを必須
        if($this->arrParam["edata56"] == 1){
            foreach(array(52,53) as $item_id){
                $this->itemData[$item_id]["need"] = 1;
            }
        }
        parent::_check3();
        
        // No.3 # カンマ区切りでキーワードを3つまで指定可
        if(strlen($this->arrParam["edata55"]) > 0){
//            $edata55 = preg_replace('/[\s　]+/u', ',', $this->arrParam["edata55"]);
            $edata55 = $this->arrParam["edata55"];
            $edata55 = array_filter(explode(",", $edata55));
            $this->arrParam["edata55"] = implode(",", $edata55);
            if(count($edata55) > 3){
                $this->objErr->_err["edata55"] = "A maximum of up to 3 key words can be entered. ";
            }
        }
    }


    // [cfp-form61] 見積No.6:4項目を再オープン時に入力画面、確認画面、メールから非表示にする(・「Presentation Type」・「Would yout like to apply for the Young Investigator Awards ?」
    //・「Recommendation from Supervisior/superior(PDF Format only)」・「ID to prove your age(PDF, JPG, JPEG only)」
    function itemNonDisplay() {
        // [cfp-form61] チェックボタン「Is the font of abstract same as designated template?」を画面でのみ表示。メールでは非表示
        $this->arrItemData[3][67]['disp'] = "";
        unset($this->arrItemData[3]['54']);
        unset($this->arrItemData[3]['56']);
        unset($this->arrItemData[3]['52']);
        unset($this->arrItemData[3]['53']);
        $this->assign("arrItemData", $this->arrItemData);

        // 帯名の非表示
        $this->group3use[3] = false;
        $this->assign("group3use",$this->group3use);

        // メールでの非表示
        unset($this->formdata["arrgroup3"][2]);
    }

    // [cfp-form61] 見積No.6:上記4項目をメールから非表示にする場合、メールにも非表示のメソッドが反映されるようにする
    /** メール本文生成 グループ3 */
    function makeBodyGroup3(&$arrbody) {
        // [cfp-form61] チェックボタン「Is the font of abstract same as designated template?」を画面でのみ表示。メールでは非表示
        $this->itemNonDisplay();
        $this->arrItemData[3][67]['disp'] = "1";
        return Usr_mail::makeBodyGroup3($this, $arrbody);
    }

}

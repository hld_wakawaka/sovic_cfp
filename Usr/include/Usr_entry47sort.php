<?php

/**
 * 47番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * 
 */
class Usr_Entry47sort {

    function doSort($obj){
        $group = 3;
        $arrGroup3 = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 50:    // グループ3-1
                    $arrGroup3[$item_id] = $_arrItem;
                    $arrGroup3[54]       = $obj->arrItemData[$group][54];
                    $arrGroup3[55]       = $obj->arrItemData[$group][55];
                    $arrGroup3[56]       = $obj->arrItemData[$group][56];
                    $arrGroup3[106]      = $obj->arrItemData[$group][106];
                    $arrGroup3[67]       = $obj->arrItemData[$group][67];
                    $arrGroup3[68]       = $obj->arrItemData[$group][68];
                    $arrGroup3[99]       = $obj->arrItemData[$group][99];
                    $arrGroup3[100]      = $obj->arrItemData[$group][100];
                    $arrGroup3[101]      = $obj->arrItemData[$group][101];
                    $arrGroup3[102]      = $obj->arrItemData[$group][102];
                    break;

                case 51:    // グループ3-2
                    $arrGroup3[103]      = $obj->arrItemData[$group][103];
                    $arrGroup3[$item_id] = $_arrItem;
                    break;

                case 54:
                case 55:
                case 56:
                case 106:
                case 67:
                case 68:
                case 99:
                case 100:
                case 101:
                case 102:
                case 103:
                    break;

                default:
                    $arrGroup3[$item_id] = $_arrItem;
            }
        }
        $obj->arrItemData[$group] = $arrGroup3;


        // グループ3-3から移動
        // -> グループ3-1
        $obj->arrItemData[$group][54]['group3']  = 1;  unset($obj->group3item[2][54]);
        $obj->arrItemData[$group][55]['group3']  = 1;  unset($obj->group3item[2][55]);
        $obj->arrItemData[$group][56]['group3']  = 1;  unset($obj->group3item[2][56]);
        $obj->arrItemData[$group][106]['group3'] = 1;  unset($obj->group3item[2][106]);
        $obj->arrItemData[$group][67]['group3']  = 1;  unset($obj->group3item[2][67]);
        $obj->arrItemData[$group][68]['group3']  = 1;  unset($obj->group3item[2][68]);
        $obj->arrItemData[$group][99]['group3']  = 1;  unset($obj->group3item[2][99]);
        $obj->arrItemData[$group][100]['group3'] = 1;  unset($obj->group3item[2][100]);
        $obj->arrItemData[$group][101]['group3'] = 1;  unset($obj->group3item[2][101]);
        $obj->arrItemData[$group][102]['group3'] = 1;  unset($obj->group3item[2][102]);

        $arrItem = array();
        foreach($obj->group3item[0] as $_key => $item_id){
            switch($item_id){
                case 50:
                    $arrItem[] = $item_id;
                    $arrItem[] = 54;
                    $arrItem[] = 55;
                    $arrItem[] = 56;
                    $arrItem[] = 106;
                    $arrItem[] = 67;
                    $arrItem[] = 68;
                    $arrItem[] = 99;
                    $arrItem[] = 100;
                    $arrItem[] = 101;
                    $arrItem[] = 102;
                    break;

                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[0] = $arrItem;

        // -> グループ3-2
        $obj->arrItemData[$group][105]['group3'] = 1;  unset($obj->group3item[2][105]);
        $arrItem = array();
        foreach($obj->group3item[0] as $_key => $item_id){
            switch($item_id){
                case 100:
                    $arrItem[] = $item_id;
                    $arrItem[] = 105;
                    break;

                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[0] = $arrItem;

        // 項目利用の有無を更新
        foreach($obj->group3item as $_group => $arrGroup3item){
            if(!(count($arrGroup3item) > 0)) $obj->group3use[$_group+1] = false;
        }


        // Category for Contribute Abstracts
        $choice = array(56, 106);
        foreach($choice as $item_id){
            $select = $obj->itemData[$item_id]['select'];
            $dispSelect = array();
            foreach($select as $_key => $_item){
                $dispSelect[$_key] = substr($_item, 3);
            }
            $obj->itemData[$item_id]['select']        = $dispSelect;
            $obj->arrItemData[3][$item_id]['select']  = $dispSelect;
            $obj->arrItemData[3][$item_id]['arrayselect'] = $select;

            $obj->itemData[$item_id]['item_select'] = implode("\n", $dispSelect);
            $obj->arrItemData[3][$item_id]['item_select'] = implode("\n", $dispSelect);
        }
    }



    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    function __constructMngCSV($obj){
        $obj->loop = 20;
    }

    /** CSVヘッダ-グループ3生成 */
    function entry_csv_entryMakeHeader3($obj, $all_flg=false){
        $skip = array(68, 99, 101, 102); // Keywords, 日本栄養食糧学会

        $group = 3;
        foreach($obj->arrItemData[$group] as $item_id => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;
            if(in_array($item_id, $skip)){
                $obj->arrItemData[$group][$item_id]["item_view"] = "1";
                continue;
            }

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);
            if(in_array($item_id, array(56, 106))){
                $name = $name.":".$obj->arrItemData[$group][$item_id]["item_memo"];
            }

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }

        return $groupHeader[$group];
    }


    function csvfunc67($obj, $group, $pa_param, $item_id){
        $edata67 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata67 = trim($edata67, ",");

        $item_id = 68;
        $edata68 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata68 = trim($edata68, ",");

        $item_id = 99;
        $edata99 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata99 = trim($edata99, ",");

        return $edata67.",".$edata68.",".$edata99;
    }
    function csvfunc100($obj, $group, $pa_param, $item_id){
        $edata100 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata100 = trim($edata100, ",");

        $item_id  = 101;
        $edata101 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata101 = trim($edata101, ",");

        $item_id  = 102;
        $edata102 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata102 = trim($edata102, ",");

        return $edata100." ".$edata101." ".$edata102;
    }


}










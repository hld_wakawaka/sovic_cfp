<?php

/**
 * 56番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author doishun
 * 
 */
class Usr_Entry56 extends Usr_Entry {

    function __construct(){
        parent::__construct();
    }


    function developfunc(){
//        // メールデバッグ
//        $this->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }



    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }


    function _check3() {
        parent::_check3();

        // 発表形式の選択項目でエラーチェック
        $item_id = 48;
        $key     = "edata".$item_id;
        // 1:パネルディスカッションを選択したら54を必須
        if(strlen($this->arrParam[$key]) > 0 && in_array($this->arrParam[$key], array(1,2))){
            if($this->arrParam[$key] == 1){
                $item_id = 54;
                $key     = "edata".$item_id;
                if(!$this->objErr->isNull($this->arrParam[$key])){
                    $name = Usr_init::getItemInfo($this, $item_id);
                    $method = Usr_init::getItemErrMsg($this, $item_id);
                    $this->objErr->addErr(sprintf($method, $name), $key);
                }
            }else
            // 2:主題を選択したら55を必須
            if($this->arrParam[$key] == 2){
                $item_id = 55;
                $key     = "edata".$item_id;
                if(!$this->objErr->isNull($this->arrParam[$key])){
                    $name = Usr_init::getItemInfo($this, $item_id);
                    $method = Usr_init::getItemErrMsg($this, $item_id);
                    $this->objErr->addErr(sprintf($method, $name), $key);
                }
            }
        }
    }


}

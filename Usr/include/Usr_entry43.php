<?php

/**
 * 43番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * 
 */
class Usr_Entry43 extends Usr_Entry {

    function __construct(){
        parent::__construct();
    }


    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }


    function _check2() {
        parent::_check2();

        // 筆頭者のエラーを無視
        $keys = array(32,33,34,35,36,41,42);
        foreach($keys as $_key => $item_id){
            $key = 'edata'.$item_id.'0';
            if(!isset($this->objErr->_err[$key])) continue;

            unset($this->objErr->_err[$key]);
        }
    }





    function makeBodyGroup2(&$arrbody){
        // グループ2が存在する時
        $group = 2;
        if($this->formdata["group2_use"] != "1") {

            // 言語別設定
            $arrbody[$group] = "";

            // 言語別設定
            $body2_title = array();
            if($this->formdata["lang"] == LANG_JPN){
                $body2_title[1] = "\n\n【".$GLOBALS["msg"]["entrant"]."】\n\n";
                $body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n【".$this->formdata["group2"]."】\n\n" : "\n\n";
            }else{
                $body2_title[1] = "\n\n[".$GLOBALS["msg"]["entrant"]."]\n\n";
                $body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n[".$this->formdata["group2"]."]\n\n" : "\n\n";
            }


            if(isset($this->arrForm["edata31"]) && $this->arrForm["edata31"] != "") {
                if(!isset($this->arrForm["edata30"])) $this->arrForm["edata30"] = 0;
                for($i = 1; $i < $this->arrForm["edata30"]+1; $i++) {
                    if(isset($this->arrForm["group2_del"])) {
                        if(in_array($i, $this->arrForm["group2_del"])) {
                            continue;
                        }
                    }

                    if($i == "0") {
                        $arrbody[$group] .= $body2_title[1];
                    } elseif($i == 1) {
                        $arrbody[$group] .= $body2_title[2];
                    }

                    foreach($this->arrItemData[$group] as $_key => $data){
                        if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

                        $key = $data["item_id"];
                        $name = $data["strip_tags"];

                        if(!isset($this->arrForm["edata".$key])){
                            $this->arrForm["edata".$key] = "";
                        }

                        $methodname = "mailfunc".$key;
                        if(method_exists($this, $methodname)) {
                            $arrbody[$group] .= $this->$methodname($name, $i);
                        } else {
                            if($data["controltype"] == "1") {
                                $arrbody[$group] .= $this->mailfuncNini($key, $name, $i);    //任意
                            } else {
                                if(!isset($this->arrForm["edata".$key.$i])) $this->arrForm["edata".$key.$i] = "";
                                $arrbody[$group] .= $this->point_mark.$name.": ".$this->arrForm["edata".$key.$i]."\n";
                            }
                        }
                    }
                    $arrbody[$group] .= "\n";
                }
            }
        }
    }
    /**
     * 開発用のデバッグ関数
     * 通常は空にしておく
     * 
     * 継承して利用する.
     */
    function developfunc(){
//        // メールデバッグ
//        $this->ins_eid = 58;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('form43-00002', "Lk5sDS8f", 1));
//        print "</pre><br/><br/>";
    }

}

<?php

/**
 * 76番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.doishun
 * 
 */
class Usr_Entry76 extends Usr_Entry
{

    function __construct(){
        parent::__construct();
        
        // [cfp-007]「見積No3」jQuery多重読み込み調整のためコメントアウト
//         $this->useJquery = true;
        // 筆頭者は利用しない
        $this->author_start_index = 1;


        // フォーム管理者用設定の読み込み
        if(method_exists($this->exClass, "__constructMng")){
            $this->exClass->__constructMng($this);
        }

        // フォーム管理者CSV用設定の読み込み
        if(method_exists($this->exClass, "__constructMngCSV")){
            $this->exClass->__constructMngCSV($this);
        }
        
        $_POST["edata31"] = "dummy";
        $_REQUEST["edata31"] = "dummy";
    }



    function developfunc()
    {

        // メールデバッグ
        // $this->ins_eid = 1615;
        // print "--------------------<pre style='text-align:left;'>";
        // print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
        // print "</pre><br/><br/>";

    }


    function sortFormIni($obj)
    {
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }


    function _init2() {
        $block = 2;
        $key = array();

        // グループ1の入力内容を取得
        $form_param = $GLOBALS["session"]->getVar("form_param1");
        // 共著者の所属
        $kaiinList = $this->_makeListBox($form_param["edata31"]);
        // 共著者の数
        if(!isset($form_param["edata30"])) $form_param["edata30"] = "0";

        // 共著者の数分だけループ
        for($i = $this->author_start_index; $i < $form_param["edata30"]+1; $i++) {
            foreach($this->arrItemData[$block] as $item_id => $val) {
                if($val["disp"] == "1") continue;

                // [cfp-form76]_20160301対応:グループ2＞エラーメッセージの番号が(1) から表示されるように調整(標準項目)
                $name = $val["strip_tags"]."(".($i).")";

                // 任意項目
                if($val['controltype'] == 1){
                    $this->_initChosyaNini($key, $item_id, $i, $name);

                    // 標準項目
                }else{
                    switch($item_id) {
                        // 所属機関の場合
                    	case "32":
                    	    if(!count($kaiinList) > 0) break;
                    	    $key[] = array("edata32".$i, $name, array(), array(), "", 0);

                    	    foreach($kaiinList as $selectkey=>$selectval) {
                    	        $key[] = array("edata32".$i.$selectkey, $name, array(), array(), "", 0);
                    	    }
                    	    break;

                    	default:
                    	    $type = in_array($item_id, $this->defaultItemSelect[2]) ? 1 : 0;
                    	    list($arrChk, $arrChklen) = $this->_setCheckMethod($val["item_check"], $val["item_len"], $type);
                    	    $key[] = array("edata".$item_id.$i, $name, $arrChklen, $arrChk, "K", 1);
                    	    break;
                    }
                }
            }
        }

        // 削除フラグ
        $key[] = array("group2_del", "削除フラグ", array(), array(), "", 0);

        return $key;
    }
    
    
    function _check(){
        $checkmethod = "_check".$this->wk_block;
    
        if(method_exists($this, $checkmethod)) {
            $this->$checkmethod();
        }
    
        $this->objErr->sortErr($this->_init($this->wk_block));
        return $this->objErr->_err;
    }
    
    
    function _check1() {
        parent::_check1();

        // [cfp-form76] 識別子３："グループ1＞Workshop (250-300 Words)"
        $str = $this->arrParam['edata71'];
        //全角スペースが含まれる場合、半角スペースに統一
        $str = str_replace(array("　", "\n", "\r", "\r\n"), " ", $str);
        $wa_word = explode(" ", $str);

        $wa_chk_word = array();
        foreach($wa_word as $data){            
            if($data != ""){
                array_push($wa_chk_word, $data);
            }
        }
        $wa_word = $wa_chk_word;



        if(!(250 <= count($wa_word) && count($wa_word) <= 300)){
            $msg  = $this->arrItemData[1][71]["item_name"]." must be 250 characters or more but not more than 300 characters.";
            $this->objErr->addErr($msg, "_1");
        }



        //$this->author_start_index = 1;

        // [cfp-form76] グループ1＞「Affiliation of All Author(s);including Presenting Author」項目の必須エラーを解除
        unset($this->objErr->_err["edata31"]);

    }

    
    function _check2() {
        
        // 管理者はしない
        if($this->admin_flg == "1") return;
        
        $form1data = $GLOBALS["session"]->getVar("form_param1");
        
        //if(!isset($form1data["edata30"]) || !$form1data["edata30"] > 0) return;
        
        // 基本チェック
        $this->objErr->check($this->_init2(), $this->arrParam);
        
        $group2 = ($this->o_form->formData['group2'] == "")
        ? "共著者"
                : $this->o_form->formData['group2'];
        
        if(!isset($form1data["edata30"])) $form1data["edata30"] = "0";
        
        //共著者の数
        for($i = $this->author_start_index; $i < $form1data["edata30"]+1; $i++) {
        
            //----------------------------
            //共著者所属機関のチェック
            //----------------------------
            if($this->itemData[32]["need"] == "1"){
        
                $ok =0;
        
                $wa_kikanlist = $this->_makeListBox($form1data["edata31"]);
        
                foreach($wa_kikanlist as $key => $val){
                    $wk_val = isset($this->arrParam["edata32".$i.$key]) ? $this->arrParam["edata32".$i.$key] : "";
                    if($wk_val != "")$ok++;
                }
        
                // [cfp-form76]_20160301対応:グループ2＞エラーメッセージの番号が(1) から表示されるように調整
                if($ok == 0){
                    $msg = sprintf($GLOBALS["msg"]["err_require_select"], $this->itemData[32]['strip_tags']."(".($i).")");
                    $this->objErr->addErr($msg, "edata32".$i);
                }
            }
        
            //任意項目
            $this->_checkChosyaNini($i);
        
            // 削除フラグが立っている時は、エラーを消す
            if(isset($this->arrParam["group2_del"])) {
                if(in_array($i, $this->arrParam["group2_del"])) {
                    foreach($this->objErr->_err as $key=>$val) {
                        $i_len = "-".strlen($i);
                        if(substr($key, $i_len) == $i) {
                            unset($this->objErr->_err[$key]);
                        }
                    }
                }
            }
        }
        
        return;
        
    }
    
    
    // [cfp-form76]_20160301対応:グループ2＞エラーメッセージの番号が(1) から表示されるように調整(任意項目)
    function _checkChosyaNini($i){
    
        $group2 = ($this->o_form->formData['group2'] == "")
        ? "共著者"
                : $this->o_form->formData['group2'];
    
        $num = $i+1;
        $exNum = $i;
    
        foreach($this->option_item[2] as $item_id) {
    
            if($this->itemData[$item_id]["disp"] != "1" && $this->itemData[$item_id]["need"] == "1") {
    
                // チェックボックスじゃなければ次へ
                //if($obj->itemData[$item_id]["item_type"] != "3") continue;
    
                $ok =0;
    
                $wk_val = isset($this->arrParam["edata".$item_id.$i]) ? $this->arrParam["edata".$item_id.$i] : "";
                if($wk_val != "") $ok++;
    
                if($ok == 0){
                    $msg = sprintf($GLOBALS["msg"]["err_require_select"], $this->itemData[$item_id]['strip_tags']."(".$exNum.")");
                    $this->objErr->addErr($msg, "edata".$item_id.$i);
                }
            }
    
        }
    
        return;
    
    }
    

	function pageAction1() {
		
		$fix_flg = "";
	
		//3ページ目を使用
		if($this->formdata["group3_use"] != "1"){
			$this->block = "3";
			$fix_flg = "1";
	
			$this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_j.html" : "Usr/form/Usr_entry_e.html";
	
		}
		
	
		//2ページ目使用
		if($this->formdata["group2_use"] != "1"){
			
			// 所属機関が有る場合は、必ず2ページ目を表示
            if($this->arrParam["edata29"] == "1" && $this->arrParam["edata30"] > 0){
                $this->block = "2";
                
                // 1番目の共著者に登録者を入れる
                $this->arrForm["edata330"] = isset($this->arrForm["edata1"]) ? $this->arrForm["edata1"] : "";
                $this->arrForm["edata340"] = isset($this->arrForm["edata2"]) ? $this->arrForm["edata2"] : "";
                $this->arrForm["edata350"] = isset($this->arrForm["edata3"]) ? $this->arrForm["edata3"] : "";
                $this->arrForm["edata360"] = isset($this->arrForm["edata4"]) ? $this->arrForm["edata4"] : "";
                $this->arrForm["edata370"] = isset($this->arrForm["edata5"]) ? $this->arrForm["edata5"] : "";
                $this->arrForm["edata380"] = isset($this->arrForm["edata6"]) ? $this->arrForm["edata6"] : "";
                $this->arrForm["edata390"] = isset($this->arrForm["edata7"]) ? $this->arrForm["edata7"] : "";
                $this->arrForm["edata400"] = isset($this->arrForm["edata25"]) ? $this->arrForm["edata25"] : "";
                $this->arrForm["edata410"] = isset($this->arrForm["edata8"]) ? $this->arrForm["edata8"] : "";
                $this->arrForm["edata420"] = isset($this->arrForm["edata9"]) ? $this->arrForm["edata9"] : "";
                $this->arrForm["edata610"] = isset($this->arrForm["edata57"]) ? $this->arrForm["edata57"] : "";
                $this->arrForm["edata620"] = isset($this->arrForm["edata7"]) ? $this->arrForm["edata7"] : "";
        
                $fix_flg = "1";
			}
		}
	
		//上記のいずれも該当しなかった場合
		if($fix_flg == ""){
			$this->block = "4";
			$this->_processTemplate =  ($this->formdata["lang"] == "1") ? "Usr/form/Usr_entry_confirm_j.html" : "Usr/form/Usr_entry_confirm_e.html";
	
		}
		
		return;
		
	}
    


	function backAction() {
		// 確認画面以外から戻るとき
		if($this->wk_block != "4"){
			$getFormData= GeneralFnc::convertParam($this->_init($this->wk_block), $_REQUEST);

/*
			//共著者の所属機関
			$ret_param = $this->_make35param($this->wk_block);
			if(count($ret_param) > 0){
				$getFormData = array_merge($getFormData, $ret_param);
			}
*/
            
            // ファイル引き継ぎ
    		foreach($this->arrfile as $item_id => $n){
                $key = "n_data".$item_id;
				$getFormData[$key] = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
			}
			
			// セッションにページパラメータをセットする
			$GLOBALS["session"]->setVar("form_param".$this->wk_block, $getFormData);
		}
		
		// セッションパラメータ取得
		$this->getDispSession();
		
		switch($this->wk_block) {
			case "1":
			case "2":
				$this->block = "1";
				break;

            // 3ページ目からの戻り
			case "3";
				$this->block = "1";

                // 2ページ目を使用
				if($this->formdata["group2_use"] != "1") {
					if($this->itemData[32]["disp"] != "1"){
						if(!isset($this->arrForm["edata31"])) $this->arrForm["edata31"] = "";
						if($this->arrForm["edata31"] != ""){
							$this->block = "2";
						}
					}
				}
				break;

			case "4":
				$this->block = "1";
				if($this->formdata["group3_use"] != "1") {
					$this->block = "3";
				} elseif($this->formdata["group2_use"] != "1" && $this->arrForm["edata29"] == "1" && $this->arrForm["edata30"] > 0) {
					$this->block = "2";
				}
				
		}
		
		if(isset($this->arrForm["edata31"])) {
			$wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);
			$this->arrForm["kikanlist"] = $wa_kikanlist;
		}

		return;
		
	}
    
    
    function premain(){
        // override (Support only group3-3)
        // Add attribute disabled (default false)
        // User only
//      $this->arrItemData[3][$item_id]['disabled'] = true;

        // Separater of radio or checkbox (default br)
//      $this->arrItemData[3][$item_id]['sep'] = " ";

        // Size of text (default 40)
//      $this->arrItemData[3][$item_id]['size'] = val;

        // Size of textarea (default 5 and 85)
//      $this->arrItemData[3][$item_id]['rows'] = val;
//      $this->arrItemData[3][$item_id]['cols'] = val;
        
        
        $this->setTitle();
        
        // [cfp-form76] グループ1＞「Affiliation of All Author(s);including Presenting Author」項目を非表示
        $this->arrItemData[1][31]["disp"] = "1";
        
        // [cfp-form76]「識別子14」:・テキストボックスの横幅を調整(半角英数字6桁ほどの幅に調整)
        $this->arrItemData[1][28]['rows'] = 6;
        

        $this->arrItemData[2][32]['disp'] = '1';

        // Keywords
        $this->arrItemData[3][68]['size'] = 100;

        $this->assign("arrItemData",$this->arrItemData);
    }

    
    /* メールの上部コメントを作成する */
    function makeMailBody_header($user_id="", $passwd ="", $exec_type = "") {
        $this->setTitle();
        return Usr_mail::makeMailBody_header($this, $user_id, $passwd, $exec_type);
    }
    
    
    // [cfp-form76] 帯名追記
    function mailfunc73($name) {

        $group = 1;
        $item_id = 73;
        $key = "edata".$item_id;
        
        // タイトル付与（カスタマイズ）
        $str = "\n[Questionnaire]\n\n";
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
        $str  .= $this->point_mark.$name.": ";
        if(strlen($this->arrForm[$key]) > 0) $str .= Usr_Assign::nini($this, $group, $item_id, $this->arrForm["edata".$item_id], array(" ", ","), true);
        $str .= "\n\n";
        return $str;
        
    }


    // [cfp-form76] 帯名追記
    function mailfunc57($name) {
        
        $str = "\n[Workshop Organizer's Information]\n\n";
        $str .= $this->point_mark.$name.": ";
        
        if($this->arrForm["edata57"] != ""){
            $this->setTitle();
            $str .= $GLOBALS["titleList"][$this->arrForm["edata57"]];
        }
        
        $str .="\n";
        
        return $str;
    }


    function mailfunc31($name) {
        $group = 1;
        $item_id = 31;
        $key = "edata".$item_id;
        
        if(!isset($this->arrForm[$key]))  $str  = ""; //$this->arrForm[$key] = "";
        
        return $str;
    }
    
    
    // [cfp-form76] グループ1＞基本項目のTitle項目にラジオボタン追加
    // [cfp-form76]_20160229:グループ1>Title ・追加した「PhD」を非表示(削除ではなく非表⽰) ・「Dr.」→「Dr./PhD」へ変更
    function setTitle() {
        $GLOBALS["titleList"] = array();
        $GLOBALS["titleList"]["1"] = "Prof.";
        $GLOBALS["titleList"]["2"] = "Dr./PhD";
        //$GLOBALS["titleList"]["3"] = "PhD";
        $GLOBALS["titleList"]["4"] = "Mr.";
        $GLOBALS["titleList"]["5"] = "Ms.";
    }


}

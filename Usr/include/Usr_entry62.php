<?php

/**
 * 55番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author doishun
 * 
 */
class Usr_Entry62 extends Usr_Entry {

    function __construct(){
        parent::__construct();
        $this->useJquery = true;
    }
    


    function developfunc(){
//        // メールデバッグ
//        $this->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    function premain(){
        // override (Support only group3-3)
        // Add attribute disabled (default false)
        // User only
//      $this->arrItemData[3][$item_id]['disabled'] = true;

        // Separater of radio or checkbox (default br)
        $this->arrItemData[3][54]['sep'] = " ";
        $this->arrItemData[3][55]['sep'] = " ";
        $this->arrItemData[3][100]['sep'] = " ";
        $this->arrItemData[3][101]['sep'] = " ";

        // Size of text (default 40)
        $this->arrItemData[3][149]['size'] = 15;
        $this->arrItemData[3][150]['size'] = 15;
        $this->assign("arrItemData",$this->arrItemData);

        // Size of textarea (default 5 and 85)
//      $this->arrItemData[3][$item_id]['rows'] = val;
//      $this->arrItemData[3][$item_id]['cols'] = val;

        // 非表示にする項目
        $this->exClass->hidden_item($this);

//        if(isset($this->arrForm["edata70"])){
//            $this->arrForm["chkemail_70"] = $this->arrForm["edata70"];
//        }
    }


    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
        $s=1;
    }

    /**
     * ページフローを表示する
     */
    //[form62] 見積もり番号6：ページステップの名称を一部変更。Proposal→Abstract
    function createPageflow() {
        $pageflow   = array();
        $pageflow[0] = "Corresponding/Presenting Authors";
        $pageflow[2] = "Abstract Submission &amp; Questionnaire";
        $pageflow[3] = "Confirmation";
        $pageflow[4] = "Completing";

        $this->assign("pageflow", $pageflow);
    }



    /* メールの上部コメントを作成する */
    function makeMailBody_header($user_id="", $passwd ="", $exec_type = "") {
        $this->exClass->hidden_item($this);
        return Usr_mail::makeMailBody_header($this, $user_id, $passwd, $exec_type);
    }


    function mailfunc103($name) {
        $item_id = 103;
        $key = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";

        $str  = $this->point_mark.$name.": ";
        if(strlen($this->arrForm[$key]) > 0) $str .= "\n ".$this->arrForm[$key];

        foreach($this->keyword55 as $item_id){
            $key = "edata".$item_id;
            if(!isset($this->arrForm[$key]))     $this->arrForm[$key] = "";
            if(strlen($this->arrForm[$key]) > 0) $str .= "\n ".$this->arrForm[$key];
        }
        $str .= "\n";
        return $str;
    }


    function mailfunc108($name) {
        $item_id = 108;
        $key = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";

        $str  = $this->point_mark.$name.": ";
        if(strlen($this->arrForm[$key]) > 0) $str .= "\n ".$this->arrForm[$key];

        foreach($this->references55 as $item_id){
            $key = "edata".$item_id;
            if(!isset($this->arrForm[$key]))     $this->arrForm[$key] = "";
            if(strlen($this->arrForm[$key]) > 0) $str .= "\n ".$this->arrForm[$key];
        }
        $str .= "\n";
        return $str;
    }


    function mailfunc54($name) {
        $group = 3;
        $item_id = 54;
        $key = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
        $str  = $this->point_mark.$name.": ";
        if(strlen($this->arrForm[$key]) > 0) $str .= Usr_Assign::nini($this, $group, $item_id, $this->arrForm["edata".$item_id], array(" ", ","), true);

        // q1-other
        $item_id = 149;
        $key = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
        if(strlen($this->arrForm[$key]) > 0) $str .= " ".$this->arrForm[$key];

        $str .= "\n";
        return $str;
    }



    function mailfunc55($name) {
        $group = 3;
        $item_id = 55;
        $key = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
        $str  = $this->point_mark.$name.": ";
        if(strlen($this->arrForm[$key]) > 0) $str .= Usr_Assign::nini($this, $group, $item_id, $this->arrForm["edata".$item_id], array(" ", ","), true);

        // q1-other
        $item_id = 150;
        $key = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
        if(strlen($this->arrForm[$key]) > 0) $str .= " ".$this->arrForm[$key];

        $str .= "\n";
        return $str;
    }


    function mailfunc26($name) {
        $group = 1;
        $item_id = 26;
        $key = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
        $str   = "\n[Presenting Author]\n\n";
        $str  .= $this->point_mark.$name.": ";
//         if(strlen($this->arrForm[$key]) > 0) $str .= Usr_Assign::nini($this, $group, $item_id, $this->arrForm["edata".$item_id], array(" ", ","), true);
        if(strlen($this->arrForm[$key]) > 0) $str .= $this->arrForm[$key];
        $str .= "\n";

        return $str;
    }


    function mailfunc29($name) {
        $str   = "\n";
        $str  .= $this->point_mark.$name.": ".$this->wa_coAuthor[$this->arrForm["edata29"]]."\n";
        return $str;
    }


    // [form62] 見積もり番号9：項目『Topic Categories』にある二つのプルダウンを「備考：プルダウン」でレイアウト調整（メールでの反映）
    function mailfunc48($name) {
    	$group = 3;
    	$item_id = 48;
    	$key = "edata".$item_id;
    	
    	if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
    	
    	$str .= $this->point_mark.$name.": ";
    	$str .= "\n";
    	
    	// ラジオ、セレクト
    	if(isset($this->itemData[$item_id]["select"][$this->arrForm[$key]])) {
    		$str .= $this->itemData[$item_id]['item_memo'].":".$this->itemData[$item_id]["select"][$this->arrForm[$key]];
    	}

    	$str .= "\n";
    
    	return $str;
    }

    function mailfunc151($name) {
    	$group = 3;
    	$item_id = 151;
    	$key = "edata".$item_id;
    	
    	if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
    	
    	
    	// ラジオ、セレクト
    	if(isset($this->itemData[$item_id]['select'][$this->arrForm[$key]])) {
    		$str .= $this->itemData[$item_id]['item_memo'].":".$this->itemData[$item_id]['select'][$this->arrForm[$key]];
    	}
    
    	$str .= "\n";
    
    	return $str;
    }

    function _check1() {
        parent::_check1();

        //メールアドレス一致チェック
        $item_id = 70;
        $key     = "edata".$item_id;
        if(strlen($this->arrParam[$key]) > 0){
            if($this->arrParam[$key] != $_REQUEST["chkemail_".$item_id]){
                $msg = sprintf($GLOBALS["msg"]["err_mail_match"], $this->itemData[$item_id]['strip_tags'], $this->itemData[$item_id]['strip_tags']);
                $this->objErr->addErr($msg, $key);
            }
        }
        
        
    }


    function _check3() {
        parent::_check3();

//        // Abstract (200-300 words in English)
//        if(isset($this->arrParam['edata50']) && strlen($this->arrParam['edata50']) > 0){
//            $str = $this->arrParam['edata50'];
//
//            // 全角スペースが含まれる場合、半角スペースに統一
//            $str = str_replace(array("　", "\n", "\r", "\r\n"), " ", $str);
//            $word = explode(" ", $str);
//
//            $arrWords = array();
//            foreach($word as $data){
//                if($data != ""){
//                    array_push($arrWords, $data);
//                }
//            }
//
//            $cntWord = count($arrWords);
//
//            if(!(200 <= $cntWord && $cntWord <= 400)){
////                $this->objErr->addErr(sprintf($GLOBALS["msg"]["err_strlen_between"], "Abstract", 200, 300), "edata50");
//                $method = "%s must be %s Words or more but not more than %s Words.";
//                $this->objErr->addErr(sprintf($method, "Abstract", 200, 350), "edata50");
//            }
//        }


        // 設問1,2でOtherを選択したらテキストボックスを必須
        $arrCheck = array(array(54, 5, 149), array(55, 10, 150));
        foreach($arrCheck as $_key => $_arrCheck){
            $item_id = $_arrCheck[0]; // radio
            $value   = $_arrCheck[1]; // selected
            // otherを選択
            if($this->arrParam['edata'.$item_id] == $value){
                $key = 'edata'.$_arrCheck[2];
                if(!$this->objErr->isNull($this->arrParam[$key])){
                    $name = strip_tags($this->itemData[$item_id]['item_name']);
                    $this->objErr->addErr(sprintf("Enter %s Others.", $name), $key);
                }
            }
        }
        
        // [form62] 見積もり番号8：2つの選択肢が同じ場合はエラー。まず2つのプルダウンの両方を選択した場合にチェックを行う。
        if(!empty($this->arrParam['edata48']) && !empty($this->arrParam['edata151'])){
            if($this->arrParam['edata48'] == $this->arrParam['edata151']){
                $msg = "Please choose another Topic Category as 2nd Choice.";
                $this->objErr->addErr($msg, "edata48");
            }
        }
    }


    /**
     * 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init($ps_block){
        $key = parent::_init($ps_block);
        $key[] = array("chkemail_70", "E-mail確認用", array(),    array(),    "",    0);

        return $key;
    }


    function getEntry() {
        parent::getEntry();

        $arrForm = $GLOBALS["session"]->getVar("form_param1");
        $arrForm["chkemail_70"] = $arrForm["edata70"];
        $this->arrForm["chkemail_70"] = $arrForm["chkemail_70"];

        $GLOBALS["session"]->setVar("form_param1", $arrForm);
    }



    function makeBodyGroup2(&$arrbody){
        // グループ2が存在する時
        $group = 2;
        if($this->formdata["group2_use"] != "1") {

            // 言語別設定
            $arrbody[$group] = "";

            // 言語別設定
            $body2_title = array();
            if($this->formdata["lang"] == LANG_JPN){
                $body2_title[1] = "\n\n【".$GLOBALS["msg"]["entrant"]."】\n\n";
                $body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n【".$this->formdata["group2"]."】\n\n" : "\n\n";
            }else{
                $body2_title[1] = "\n\n[".$GLOBALS["msg"]["entrant"]."]\n\n";
                $body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n[".$this->formdata["group2"]."]\n\n" : "\n\n";
            }


            if(isset($this->arrForm["edata31"]) && $this->arrForm["edata31"] != "") {
                if(!isset($this->arrForm["edata30"])) $this->arrForm["edata30"] = 0;
                for($i = 0; $i < $this->arrForm["edata30"]+1; $i++) {
                    if(isset($this->arrForm["group2_del"])) {
                        if(in_array($i, $this->arrForm["group2_del"])) {
                            continue;
                        }
                    }

                    if($i == "0") continue; // 筆頭者情報を非表示

                    if($i == "0") {
                        $arrbody[$group] .= $body2_title[1];
                    } elseif($i == 1) {
                        $arrbody[$group] .= $body2_title[2];
                    }

                    foreach($this->arrItemData[$group] as $_key => $data){
                        if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

                        $key = $data["item_id"];
                        $name = $data["strip_tags"];

                        if(!isset($this->arrForm["edata".$key])){
                            $this->arrForm["edata".$key] = "";
                        }

                        $methodname = "mailfunc".$key;
                        if(method_exists($this, $methodname)) {
                            $arrbody[$group] .= $this->$methodname($name, $i);
                        } else {
                            if($data["controltype"] == "1") {
                                $arrbody[$group] .= $this->mailfuncNini($key, $name, $i);    //任意
                            } else {
                                if(!isset($this->arrForm["edata".$key.$i])) $this->arrForm["edata".$key.$i] = "";
                                $arrbody[$group] .= $this->point_mark.$name.": ".$this->arrForm["edata".$key.$i]."\n";
                            }
                        }
                    }
                    $arrbody[$group] .= "\n";
                }
            }
        }
    }



    function _check2() {
        parent::_check2();
        unset($this->objErr->_err['edata320']);
    }
}

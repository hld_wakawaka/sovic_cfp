<?php

/**
 * 64番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.kawauchi
 * 
 */
class Usr_Entry72 extends Usr_Entry
{

    function developfunc() {
//        // メールデバッグ
//        $this->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }

    // [cfp-form72] 見積No1:帯名を「Presenting Author」から「Corresponding (Presenting) Author 」への変更
    function premain() {
        $GLOBALS["msg"]["entrant"] = "Corresponding (Presenting) Author";
        $GLOBALS["langtxt"]["entrant"][2] = "Corresponding (Presenting) Author";
        
    }


    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }


}

<?php

/**
 * 10番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2012.06.12
 * 
 */
class Usr_Entry10 extends Usr_Entry {

    function developfunc(){
//
//        // キーを並び替える
//        // 48の下に54
//        foreach($this->itemData as $key => $data){
//            switch($key){
//                case 48:
//                    $wk_itemData[$key] = $data;
//                    $wk_itemData[54] = $this->itemData[54];
//                    break;
//
//                case 54: break;
//                default: $wk_itemData[$key] = $data;
//            }
//        }
//        print "--------------------<pre>";
//        print_r($wk_itemData);
//        print "</pre><br/><br/>";
    }


	/**
	 * 完了メール作成
	 */
	function makeMailBody($user_id="", $passwd ="", $exec_type = ""){

        // キーを並び替える
        // 48の下に54
        foreach($this->itemData as $key => $data){
            switch($key){
                case 48:
                    $wk_itemData[$key] = $data;
                    $wk_itemData[54] = $this->itemData[54];
                    break;

                case 54: break;
                default: $wk_itemData[$key] = $data;
            }
        }
        $this->itemData = $wk_itemData;


		$yen_mark = "JPY";

		$body = "";
		
		// メール上部コメントの作成
		$head_comment = $this->makeMailBody_header($user_id, $passwd, $exec_type);
		$body .= $head_comment."\n\n";

		
		// 言語別設定
		if($this->formdata["lang"] == "1"){
			$arrbody[1] = ($this->formdata["group1"] != "") ? "【".$this->formdata["group1"]."】\n\n" : "";
			
			$body2_title[1] = "\n\n【".$GLOBALS["msg"]["entrant"]."】\n\n";
			$body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n【".$this->formdata["group2"]."】\n\n" : "\n\n";
			
			$arrbody[2] = "";
			$arrbody[3] = ($this->formdata["group3"] != "") ? "\n\n【".$this->formdata["group3"]."】\n\n" : "\n\n";
			$this->point_mark = "■";
		}
		else{
			$arrbody[1] = ($this->formdata["group1"] != "") ? "[".$this->formdata["group1"]."]\n\n" : "";
			
			$body2_title[1] = "\n\n[".$GLOBALS["msg"]["entrant"]."]\n\n";
			$body2_title[2] = ($this->formdata["group2"] != "") ? "\n\n[".$this->formdata["group2"]."]\n\n" : "\n\n";
			
			$arrbody[2] = "";
			$arrbody[3] = ($this->formdata["group3"] != "") ? "\n\n[".$this->formdata["group3"]."]\n\n" : "\n\n";
			$this->point_mark = "*";
		}
		
		$group2_key = array();	//グループ2で利用するitem_id
		
		// グループ1とグループ3
		foreach($this->itemData as $key => $data){
			
			if($data["disp"] == "1" || $data["item_mail"] == "1") continue;
			if($data["group_id"] == "2") {
				$group2_key[] = $key;
				continue;
			}
			
			// 項目名（タグをとる)
			$data["item_name"] = strip_tags($data["item_name"]);
			$name = $data["item_name"];
			$group = $data["group_id"];
			
			$methodname = "mailfunc".$key;
			if(method_exists($this, $methodname)) {
				$arrbody[$group] .= $this->$methodname($name);
			} else {
				if($data["controltype"] == "1") {
					$arrbody[$group] .= $this->mailfuncNini($key, $name);	//任意
				} else {
					$arrbody[$group] .= $this->point_mark.$name.": ".$this->arrForm["edata".$key]."\n";
				}
			}
		}
		
		// グループ2が存在する時
		if($this->formdata["group2_use"] != "1") {
			
			if(isset($this->arrForm["edata31"]) && $this->arrForm["edata31"] != "") {
			
			if(!isset($this->arrForm["edata30"])) $this->arrForm["edata30"] = 0;
			for($i = 0; $i < $this->arrForm["edata30"]+1; $i++) {
				
				if(isset($this->arrForm["group2_del"])) {
					if(in_array($i, $this->arrForm["group2_del"])) {
						continue;
					}
				}
				
				if($i == "0") {
					$arrbody[2] .= $body2_title[1];
				} elseif($i == 1) {
					$arrbody[2] .= $body2_title[2];
				}
				
				foreach($group2_key as $key) {
					
					$name = $this->itemData[$key]["strip_tags"];
					
					$methodname = "mailfunc".$key;
					if(method_exists($this, $methodname)) {
						$arrbody[2] .= $this->$methodname($name, $i);
					} else {
						if($this->itemData[$key]["controltype"] == "1") {
							$arrbody[2] .= $this->mailfuncNini($key, $name, $i);	//任意
						} else {
							$arrbody[2] .= $this->point_mark.$name.": ".$this->arrForm["edata".$key.$i]."\n";
						}
					}
				
				}
				
				$arrbody[2] .= "\n";
				
			}
			
			}
			
		} 


		//----------------------------------------
		//本文生成
		//----------------------------------------
		$body .= $arrbody[1];
		if($this->formdata["group2_use"] != "1") $body .= $arrbody[2];
		if($this->formdata["group3_use"] != "1") $body .= $arrbody[3];
		
		//$body .= print_r($this->arrForm, true);

		$body .= "\n\n------------------------------------------------------------------------------\n";
		$body .= $this->formdata["contact"];
		$body .= "\n------------------------------------------------------------------------------\n";
		
		// 2011/09/21 タグ除去追加 TODO
//		$body = strip_tags($body);
		

		return $body;

	}
}

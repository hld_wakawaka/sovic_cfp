<?php

/**
 * 14番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.doi
 *
 */
class Usr_Entry14sort {

    function doSort($obj){
        $group = 1;
        $arrGroup1 = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 7:
                    $arrGroup1[$item_id] = $_arrItem;
                    $arrGroup1[26]       = $obj->arrItemData[$group][26];
                    $arrGroup1[27]       = $obj->arrItemData[$group][27];
                    $arrGroup1[13]       = $obj->arrItemData[$group][13];
                    $arrGroup1[28]       = $obj->arrItemData[$group][28];
                    $arrGroup1[63]       = $obj->arrItemData[$group][63];
                    break;

                case 26:
                case 27:
                case 13:
                case 28:
                case 63:
                    break;

                default:
                    $arrGroup1[$item_id] = $_arrItem;
            }
        }
        $obj->arrItemData[$group] = $arrGroup1;


        $group = 2;
        $arrGroup2 = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 32:
                    $arrGroup2[$item_id] = $_arrItem;
                    $arrGroup2[43]       = $obj->arrItemData[$group][43];
                    break;

                case 43:
                    break;

                default:
                    $arrGroup2[$item_id] = $_arrItem;
            }
        }
        $obj->arrItemData[$group] = $arrGroup2;


        $group = 3;
        $arrGroup3 = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 48:
                    $arrGroup3[$item_id] = $_arrItem;
                    $arrGroup3[54]       = $obj->arrItemData[$group][54];
                    break;

                case 54:
                    break;

                default:
                    $arrGroup3[$item_id] = $_arrItem;
            }
        }
        $obj->arrItemData[$group] = $arrGroup3;


        // グループ3-1
        $arrItem = array();
        $group = 1 -1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 48:
                    $arrItem[] = $item_id;
                    $arrItem[] = 54;        unset($obj->group3item[2][54]); // group3-3から3-1へ移動
                                            $obj->arrItemData[3][54]['group3'] = 1;
                    break;

                case 54:
                    break;

                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[$group] = $arrItem;

// 所属グループ確認用
//        foreach($obj->arrItemData[3] as $_key => $_arrItemData){
//            print $_arrItemData['item_id'].":".$_arrItemData['group3']."<br/>";
//        }
    }



    function __constructMngCSV($obj){
        // CSVでも筆頭者は表示しない
        // CSVでは1が筆頭者、それ以降が共著者になっている
        $obj->start = 2;

        // グループ2の所属は非表示
        $obj->arrItemData[1][31]['item_view'] = 1;

        $obj->loop = 35;
    }


    /**  CSVヘッダ生成 */
    function entry_csv_entryMakeHeader($obj, $all_flg=false){

    	// グループ別に生成
    	$groupHeader = array("1" => array(), "2" => array(), "3" => array());

    	// グループ1ッダ
    	$groupHeader[1] = $obj->entryMakeHeader1($obj, $all_flg);

    	// グループ2ヘッダ # 日英で分岐
    	$group2_func = 'entryMakeHeader2';
    	if($GLOBALS["userData"]["lang"] == LANG_ENG) $group2_func = $group2_func.'_'.$GLOBALS["userData"]["lang"]; // 英語
    	$groupHeader[2] = $obj->$group2_func($obj, $all_flg);

    	// グループ3ヘッダ
    	$groupHeader[3] = $obj->entryMakeHeader3($obj, $all_flg);


    	$header = $groupHeader[1];
    	if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
    	if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
    	$header[]="状態";
    	$header[]="エントリー登録日";
    	$header[]="エントリー更新日";

    	// 生成
    	$ret_buff = implode($obj->delimiter, $header)."\n";
    	return $ret_buff;
    }


    /** CSVヘッダ-グループ2生成 # 英語用 */
    function entry_csv_entryMakeHeader2($obj, $all_flg=false){

    	$group = 2;
    	// 共著者の数だけ、共著者ヘッダを生成する
    	if($obj->itemData[29]["item_view"] == "0"){
    		$loop_cnt = $obj->loop +1;

    		for($i=2; $i <= $loop_cnt; $i++ ){
    			foreach($obj->arrItemData[$group] as $_key => $_data){
    				// 表示する設定の場合出力
    				if($_data["item_view"] == "1") continue;

    				// 画面に表示する項目の名称
    				$name = strip_tags($_data["item_name"]);

    				$prefix = ($i == 1) ? "筆頭者" : "共著者".($i-1);


    				$prefix = $obj->fix.$prefix." ";

    				$item_id = $_data["item_id"];
    				switch($item_id){
    					case 32:
    						if($i == 2) $groupHeader[$group][] = "All Affiliation";

    						$groupHeader[$group][] =  $prefix.$name.$obj->fix;
    						$groupHeader[$group][] =  $prefix.$name." No.".$obj->fix;
    						break;

    					default:
    						$groupHeader[$group][] =  $prefix.$name.$obj->fix;
    						break;
    				}
    			}
    		}
    	}

    	return $groupHeader[$group];
    }


    /** CSV出力データ グループ2生成 # 英語用 */
    function entry_csv_entryMakeData2($obj, $pa_param, $all_flg=false){

    	$group = 2;
    	// 共著者の数だけ、共著者ヘッダを生成する
    	if($obj->itemData[29]["item_view"] == "0"){
    		$cnt      = count($pa_param["chosya"]);
    		$loop_cnt = $obj->loop +1;

    		for($i=2; $i <= $loop_cnt; $i++ ){
    			foreach($obj->arrItemData[$group] as $_key => $_data){
    				// 表示する設定の場合出力
    				if($_data["item_view"] == "1") continue;

    				$item_id = $_data["item_id"];

    				// 共著者の員数を越えた場合はNULL埋め
    				if($cnt < $i){
    					if($item_id == "36") continue;

    					$groupBody[$group][] = "\"\"";
    					if($item_id == 32){ // affiliation番号
    						$groupBody[$group][] = "\"\"";
    					}
    					continue;
    				}

    				$wk_body = "";
    				$chosya  = $pa_param["chosya"][$i-1];


    				$methodname = "csvfunc".$item_id;
    				// カスタマイズあり
    				if(method_exists($this->exClass, $methodname)) {
    					$wk_body = $this->exClass->$methodname($obj, $group, $chosya, $item_id);

    					// カスタマイズなし
    				} else {
    					// 任意項目
    					if($_data["controltype"] == "1"){
    						$wk_body = trim(Usr_Assign::nini($obj, $group, $item_id, $chosya["edata".$item_id], array(" ", ","), true), ",");

    						// 標準項目
    					}else{
    						switch($item_id){
    							//共著者所属期間
    							case 32:

    								if($i == 2) $groupBody[$group][] = $this->getAllAffilicationData($obj, $item_id, $pa_param);

    								$wk_affiliation_name = "";   // 選択したaffiliation名
    								$wk_affiliation_mark = "";   // 選択したaffiliation番号

    								if(count($obj->set_select) > 0){
    									if($chosya["edata".$item_id] != ""){
    										$chkeck = explode("|", $chosya["edata".$item_id]);
    										$chkeck_names = array();    // 選択したAffiliation名の配列
    										$wk_mark = array();         // 選択したAffiliation番号の配列

    										foreach($chkeck as $val){
    											if($val != "" && array_key_exists($val, $obj->set_select)){
    												$chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $obj->set_select[$val]);
    												$wk_mark[] = $val.")";
    											}
    										}

    										if(count($chkeck_names) > 0){
    											$wk_affiliation_name =   implode(",", $chkeck_names);
    											$wk_affiliation_mark =   implode(",", $wk_mark);
    										}

    									}
    									else{
    										$wk_affiliation_name = "";
    									}



    									$wk_body = str_replace(array("\r\n","\n","\r"), '', trim($wk_affiliation_name));
    								}
    								else{
    									$wk_body = "";
    								}

    								if($wk_body == "Others") {
    									$wk_body = $this->convertAffilicationValue($obj, $chosya, $item_id);
    								}

    								break;

    								//共著者姓名
    							case 33:
    								$wk_body = $chosya["edata".$item_id];
    								break;

    								//共著者姓名（カナ）
    							case 35:
    								$wk_body = $chosya["edata".$item_id]." ".$chosya["edata36"];
    								break;

    							case 36:
    								continue 2;
    								break;


    								//会員・非会員
    							case 41:
    								$wk_body = ($chosya["edata".$item_id] != "") ? $obj->wa_kaiin[$chosya["edata".$item_id]] : "";
    								break;

    								//Mr. Mis Dr
    							case 61:
    								if($chosya["edata".$item_id] != ""){
    									$wk_body = $GLOBALS["titleList"][$chosya["edata".$item_id]];
    								}
    								else{
    									$wk_body = "";
    								}
    								break;

    							case 46:
    							case 47:
    							default:
    								$wk_body = $chosya["edata".$item_id];
    								break;
    						}
    					}
    				}
    				//                    if(strlen($wk_body) == 0) continue;



    				$groupBody[$group][] = "\"".$wk_body."\"";

    				// affiliationの次の列にaffiliation番号を表示
    				if($item_id == 32){
    					$groupBody[$group][] = "\"".$wk_affiliation_mark."\"";
    					$wk_affiliation_mark = "";
    				}

    			}

    		}

    	}

    	return $groupBody[$group];
    }


	// item_idが32の時に行う。affilicationを全て
    function getAllAffilicationData($obj, $item_id, $pa_param) {

    	$allAffilicationData = array();

    	// まずはグループ１の筆頭者のAffilicationを登録。otherであれば以下の処理を行う
    	if(array_key_exists($pa_param["edata26"], $obj->arrItemData[1][26]['select'])) {
//    		$authAffilicationData = $obj->arrItemData[1][26]['select'][$pa_param["edata26"]];
    		$authAffilicationData = str_replace(array("\n\r","\r","\n"), "", $obj->arrItemData[1][26]['select'][$pa_param["edata26"]]);

    		if($authAffilicationData == "Others") {
    			$allAffilicationData[] = $pa_param["edata27"];
    		}else{
    			$allAffilicationData[] = $authAffilicationData;
    		}
    	}

    	$i = 1;
    	foreach($pa_param["chosya"] as $affilicationData){

    		if($affilicationData["edata".$item_id] != "" && $i >= 1){

    			$chkeck = explode("|", $affilicationData["edata".$item_id]);
    			$chkeck_names = array();    // 選択したAffiliation名の配列
    			$wk_mark = array();         // 選択したAffiliation番号の配列

    			foreach($chkeck as $val){
    				if($val != "" && array_key_exists($val, $obj->set_select)){
    					$chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $obj->set_select[$val]);
    					$wk_mark[] = $val.")";
    				}
    			}

    			if(count($chkeck_names) > 0){
    				$wk_affiliation_name =   implode(",", $chkeck_names);
    				$wk_affiliation_mark =   implode(",", $wk_mark);
    			}


    		}
    		else{
    			$wk_affiliation_name = "";
    		}

    		if($wk_affiliation_name == "Others") {
    			$wk_affiliation_name = $this->convertAffilicationValue($obj, $affilicationData, $item_id);
    		}

    		$allAffilicationData[] = $wk_affiliation_name;
    		$i++;

    	}

    	// 配列を成型化する
    	$allAffilicationData = $this->notEmptyArray($allAffilicationData);

    	$allAffilicationData = implode(',', $allAffilicationData);

    	return "\"".$allAffilicationData."\"";
    }


	/**
	 * 配列から空要素を削除。さらに添字を振り直して成型化
	 *
	 * return $array
	 **/
    function notEmptyArray(array $array) {
    	$array = array_filter($array, "strlen");
    	$array = array_values($array);

    	return $array;
    }


	function convertAffilicationValue($obj, $affilicationData, $item_id) {
		if($affilicationData["edata43"] != ""){
			$wk_affiliation_name = $affilicationData["edata43"];
		}
		else{
			$wk_affiliation_name = "";
		}

		return $wk_affiliation_name;

    }


    // 共著者の有無
    function csvfunc29($obj, $group, $pa_param, $item_id){
        $wk_body =  ($pa_param["edata".$item_id] != "") ? $obj->wa_coAuthor[$pa_param["edata".$item_id]] : "";

        // 共著者リスト
        $wk_kikan_names = array();
        $wk_kikanlist   = explode("\n", $pa_param["edata31"]);

        $i =  "";
        $obj->set_select = array();
        foreach($wk_kikanlist as $num => $val){
            $i = $num+1;
            $obj->set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
            $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
        }

        return $wk_body;
    }


    // 共著者の氏名
    function csvfunc33($obj, $group, $chosya, $item_id){
        return $chosya["edata".$item_id]." ".$chosya["edata34"];
    }

}

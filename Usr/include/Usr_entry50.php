<?php

/**
 * 50番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * 
 */
class Usr_Entry50 extends Usr_Entry {

    function developfunc(){
//
//        // キーを並び替える
//        // 48の下に54
//        foreach($this->itemData as $key => $data){
//            switch($key){
//                case 48:
//                    $wk_itemData[$key] = $data;
//                    $wk_itemData[54] = $this->itemData[54];
//                    break;
//
//                case 54: break;
//                default: $wk_itemData[$key] = $data;
//            }
//        }
//        print "--------------------<pre>";
//        print_r($wk_itemData);
//        print "</pre><br/><br/>";
    }


    /*
     * 完了メールの送信
     */
    function sendCompleteMail($user_id, $passwd, $exec_type=1) {

        // 編集は親に任せる
        if($exec_type != 1) return Usr_mail::sendCompleteMail($this, $user_id, $passwd, $exec_type);

        if(in_array($this->form_id, $this->sendmail_not)) return;
        
        //------------------------------------
        //メール送信
        //------------------------------------
        //本文
        //メソッドチェック
        $ws_mailBody = "";

        $ws_mailBody = $this->makeMailBody($user_id, $passwd, $exec_type);
        
        
        $subject = @ereg_replace("_ID_", $user_id, $this->subject);
        
        // 登録者へのメール
        if($this->admin_flg == "") {
            if($this->arrForm["edata25"] != ""){
                $this->o_mail->SendMail($this->arrForm["edata25"], $this->subject, $ws_mailBody, $this->formdata["form_mail"], $this->formWord["word13"], mb_encode_mimeheader("Yoshitaka Sarazawa")."<Yoshitaka.Sarazawa@arthrex.co.jp>");
            }
        }
        
        // 管理者へのメール
        $this->o_mail->SendMail($this->formdata["form_mail"], $this->subject, $ws_mailBody, $this->formdata["form_mail"],$this->formWord["word13"]);
        
        return;
        
    }

}

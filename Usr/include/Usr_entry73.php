<?php

/**
 * 73番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.kawauchi
 * 
 */
class Usr_Entry73 extends Usr_Entry
{

    function __construct(){
        parent::__construct();
 //       // 筆頭者は利用しない
 //        $this->author_start_index = 1;
    }
    

    function developfunc() {
//        // メールデバッグ
//        $this->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }

    
    function premain() {
    }


    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;
            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }


}

<?php

/**
 * 76番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.doishun
 * 
 */
class Usr_Entry76sort
{

    function doSort($obj)
    {
        $group = 3;
        $arrGroup = array();
        
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
        	switch($item_id){
        		case 47:
        			$arrGroup[68]       = $obj->arrItemData[3][68];
        			$arrGroup[$item_id] = $_arrItem;
        			break;
        		
        		case 68:
        			break;
        		
        		default:
        			$arrGroup[$item_id] = $_arrItem;
        			break;
        	}
        }
        
        $obj->arrItemData[$group] = $arrGroup;
        $obj->arrItemData[$group][68]['group3']  = 1;  unset($obj->group3item[2][68]);
        
        
        // [cfp-form76]:"グループ２＞項目移動
        //⑩「Title」項目を「Family Name」の上へ移動
        //⑪「Registration No.」項目と「Affilliation」項目を「e-mail」の上へ移動"			
        $group = 2;
        $arrGroup = array();
        
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
            	
                case 33:
                    $arrGroup[43]       = $obj->arrItemData[2][43];
                    $arrGroup[$item_id] = $_arrItem;
                    break;

                case 40:
                    $arrGroup[44]       = $obj->arrItemData[2][44];
                    $arrGroup[45]       = $obj->arrItemData[2][45];
                    $arrGroup[$item_id] = $_arrItem;
                    break;

                case 43:
                case 44:
                case 45:
                    break;

                case 47:
            	    $arrGroup[68]       = $obj->arrItemData[3][68];
            	    $arrGroup[$item_id] = $_arrItem;
            	    break;
        
            	case 68:
            	    break;
        
            	default:
            	    $arrGroup[$item_id] = $_arrItem;
            	    break;
            }
        }
        
        $obj->arrItemData[$group] = $arrGroup;
        
        
        
        //"グループ１＞項目移動
        //① 「Workshop Title (20 words)」項目～「Questionnaire」までを、「Workshop」の帯の下へ移動
        //③ 「Registration No.」と「Category」項目を「Middle Name」項目の下へ移動"			
        $group = 1;
        $arrGroup = array();
        
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){

                case 7:
                    $arrGroup[$item_id] = $_arrItem;
                    $arrGroup[28]       = $obj->arrItemData[1][28];
                    $arrGroup[63]       = $obj->arrItemData[1][63];
                    break;
            	
                case 28:
                    break;

            	case 57:
            	    $arrGroup[64]       = $obj->arrItemData[1][64];
            	    $arrGroup[69]       = $obj->arrItemData[1][69];
            	    $arrGroup[70]       = $obj->arrItemData[1][70];
            	    $arrGroup[71]       = $obj->arrItemData[1][71];
            	    $arrGroup[72]       = $obj->arrItemData[1][72];
            	    $arrGroup[73]       = $obj->arrItemData[1][73];
            	    $arrGroup[$item_id] = $_arrItem;
            	    break;
                
                case 63:
                case 64:
            	case 69:
            	case 70:
            	case 71:
            	case 72:
            	case 73:
            	    break;
        
            	default:
            	    $arrGroup[$item_id] = $_arrItem;
            	    break;
            }
        }
        
        $obj->arrItemData[$group] = $arrGroup;

        
        // グループ3-1
        $arrItem = array();
        $group = 3 - 3;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 47:
                    $arrItem[] = 68;
                    $arrItem[] = $item_id;
                    break;

                case 68:
                    break;

                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[$group] = $arrItem;
        
    }

    

    function __constructMng($obj){
        // [cfp-form76] 「Presenting Author」エリアを非表示
        $obj->author_start_index = 1;
    }

    function __constructMngCSV($obj){
        // CSVでも筆頭者は表示しない
        // CSVでは1が筆頭者、それ以降が共著者になっている
        // [cfp-form76] 「Presenting Author」エリアを非表示
        $obj->start = 2;
        
        $this->setTitle();

        // [cfp-form76] グループ1＞「Affiliation of All Author(s);including Presenting Author」項目を非表示
        $obj->arrItemData[1][31]['item_view'] = 1;
        $obj->arrItemData[1][31]['disp'] = '1';

    }


    function mng_detail_premain($obj){
        // [cfp-form76] グループ1＞「Affiliation of All Author(s);including Presenting Author」項目を非表示
        $obj->arrItemData[1][31]["disp"] = "1";
        $obj->arrItemData[1][31]['item_view'] = 1;
        
        $obj->assign("arrItemData",$obj->arrItemData);
        
        $this->setTitle();
    }
    
    // [cfp-form76] グループ1＞基本項目のTitle項目にラジオボタン追加
    // [cfp-form76]_20160229:グループ1>Title ・追加した「PhD」を非表示(削除ではなく非表⽰) ・「Dr.」→「Dr./PhD」へ変更
    function setTitle() {
        $GLOBALS["titleList"] = array();
        $GLOBALS["titleList"]["1"] = "Prof.";
        $GLOBALS["titleList"]["2"] = "Dr./PhD";
        //$GLOBALS["titleList"]["3"] = "PhD";
        $GLOBALS["titleList"]["4"] = "Mr.";
        $GLOBALS["titleList"]["5"] = "Ms.";
    }

}

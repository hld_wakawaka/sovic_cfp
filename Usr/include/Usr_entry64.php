<?php

/**
 * 64番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld.doi
 * 
 */
class Usr_Entry64 extends Usr_Entry
{

    public function getAuthorMax()
    {
        return 20;
    }


}

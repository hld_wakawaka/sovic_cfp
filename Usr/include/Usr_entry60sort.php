<?php

/**
 * 59番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author doishun
 * 
 */
class Usr_Entry60sort {

    function doSort($obj){
        // 筆頭者は非表示
        $obj->author_start_index = 1;
        // グループ2の筆頭者は非表示 # CSV用
        $obj->start = 2;

        // 共著者帯 # 変更の可能性あり
        $obj->obi_edata29 = "共同演者";

        // assign
        if (method_exists($obj, "assign")) {
            $obj->assign("author_start_index", $obj->author_start_index);
            $obj->assign("obi_edata29", $obj->obi_edata29);
        }


        // グループ3の項目をグループ1に持ってくる
        $group = 3;
        $arrGroup = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 46:
                    $arrGroup[48]       = $obj->arrItemData[3][48];    // 3-1
                    $arrGroup[54]       = $obj->arrItemData[3][54];    // 3-3
                    $arrGroup[$item_id] = $_arrItem;                   // 3-1
                    break;

                case 48:
                case 54:
                    break;

                default:
                    $arrGroup[$item_id] = $_arrItem;
                    break;
            }
        }
        $obj->arrItemData[$group] = $arrGroup;
        $obj->arrItemData[$group][54]['group3']  = 1;  unset($obj->group3item[2][54]);


        // グループ3-1
        $arrItem = array();
        $group = 1 -1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 46:
                    $arrItem[] = 48;
                    $arrItem[] = 54;
                    $arrItem[] = $item_id;
                    break;
        
                case 48:
                case 54:
                    break;
        
                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[$group] = $arrItem;
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------
    
    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        // グループ2の共著者の表示数
        $obj->loop = 6;

        $group = 1;
        $groupHeader[$group][] = "登録No.";
        foreach($obj->arrItemData[$group] as $_key => $_data){
            $item_id = $_data['item_id'];

            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;
    
            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);
            if(in_array($item_id, array(27, 28, 63, 64, 69, 70, 71))){
                $name = "筆頭演者 ". $name;
            }
    
            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }


}

?>
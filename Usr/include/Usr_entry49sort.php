<?php

/**
 * 49番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * 
 */
class Usr_Entry49sort {

    function doSort($obj){
        // グループ3の項目をグループ1に持ってくる
        $group = 1;
        $arrGroup1 = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 25 :
                    $arrGroup1[$item_id] = $_arrItem;
                    $arrGroup1[64]       = $obj->arrItemData[1][64];
                    $arrGroup1[69]       = $obj->arrItemData[1][69];
                    break;

                case 63:    // Date of Birth
                    $arrGroup1[$item_id] = $_arrItem;
                    $arrGroup1[51]       = $obj->arrItemData[3][51];
                    $arrGroup1[52]       = $obj->arrItemData[3][52];
                    break;

                case 64 :
                case 69 :
                    break;

                default:
                    $arrGroup1[$item_id] = $_arrItem;
            }
        }
        $obj->arrItemData[$group] = $arrGroup1;

        $obj->itemData[51]['group_id'] = 1;
        $obj->itemData[52]['group_id'] = 1;


        // 年月日の表示順
        $obj->dispDOB = array(27, 28, 63);
        if(isset($obj->_smarty)){
            $obj->assign("dispDOB", $obj->dispDOB);
        }
    }



    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $skip = array(28, 63); // Keywords, 日本栄養食糧学会

        $group = 1;
        $groupHeader[$group][] = "登録No.";
        foreach($obj->arrItemData[$group] as $item_id => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;
            if(in_array($item_id, $skip)){
                $obj->arrItemData[$group][$item_id]["item_view"] = "1";
                continue;
            }

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);
            if($item_id == 27) $name = "Date of Birth";

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }

        return $groupHeader[$group];
    }

    function csvfunc27($obj, $group, $pa_param, $item_id){
        $edata27 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata27 = trim($edata27, ",");

        $item_id = 28;
        $edata28 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata28 = trim($edata28, ",");

        $item_id = 63;
        $edata63 = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $edata63 = trim($edata63, ",");

        return $edata27."/".$edata28."/".$edata63;
    }

}










<?php

/**
 * 25番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2013.08.23
 * 
 */
class Usr_Entry25 extends Usr_Entry {

    /**
     * 開発用デバッグメソッド
     * super#mainの最後に実行される
     * 
     */
    function developfunc(){}


    function mailfunc26($name){
        $str = $this->mailfuncNini(26, $name);
        return "\n". $str;
    }
}

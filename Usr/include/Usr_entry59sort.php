<?php

/**
 * 59番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author doishun
 * 
 */
class Usr_Entry59sort {

    function doSort($obj){
        // グループ3の項目をグループ1に持ってくる
        $group = 3;
        $arrGroup = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 48:
                    $arrGroup[$item_id] = $_arrItem;                   // 3-1
                    $arrGroup[54]       = $obj->arrItemData[3][54];    // 3-3
                    $arrGroup[55]       = $obj->arrItemData[3][55];    // 3-3
                    break;

                case 54:
                case 55:
                    break;

                default:
                    $arrGroup[$item_id] = $_arrItem;
                    break;
            }
        }
        $obj->arrItemData[$group] = $arrGroup;

        $obj->arrItemData[$group][54]['group3']  = 1;  unset($obj->group3item[2][54]);
        $obj->arrItemData[$group][55]['group3']  = 1;  unset($obj->group3item[2][55]);


        // グループ3-1
        $arrItem = array();
        $group = 1 -1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 48:
                    $arrItem[] = $item_id;
                    $arrItem[] = 54;
                    $arrItem[] = 55;
                    break;

                case 54:
                case 55:
                    break;

                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[$group] = $arrItem;
    }

}










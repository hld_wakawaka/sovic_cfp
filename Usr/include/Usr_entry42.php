<?php

/**
 * 42番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * 
 */
class Usr_Entry42 extends Usr_Entry {

    /**
     * 開発用デバッグメソッド
     * super#mainの最後に実行される
     * 
     */
    /** 開発用のデバッグ関数 */
    function developfunc() {
//        // メールデバッグ
//        $this->ins_eid = 55;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('form42-00001', "U4cHSBMM", 1));
//        print "</pre><br/><br/>";
    }


//[form42]帯名を応答責任者から研究責任者に変更するために以下のように修正
    function mailfunc26($name){
//      $str  = "\n\n【応答責任者情報】\n\n";
    	$str  = "\n\n【研究責任者情報】\n\n";
        $str .= $this->mailfuncNini(26, $name);
        return $str;
    }



}

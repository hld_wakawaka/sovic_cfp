<?php

/**
 * 18番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2014.03.31
 * 
 */
class Usr_Entry41 extends Usr_Entry {

    /**
     * 開発用デバッグメソッド
     * super#mainの最後に実行される
     * 
     */
    /** 開発用のデバッグ関数 */
    function developfunc() {
//        // メールデバッグ
//        $this->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('form41-00008', "WHddGQSM", 1));
//        print "</pre><br/><br/>";
    }


    function premain(){
        // 1ページ目のPresenting Authorの利用項目
        $keys = array(115, 116, 117, 120, 121, 122, 123, 124, 118, 119, 125, 126, 127, 128, 129, 130, 131);
        $this->assign("authorkeys", $keys);

        // 1ページ目からの遷移
        if($this->wk_block == 1){
            $group_id = 1;
            $item_id  = 83;
            $key      = 'edata'.$item_id;
            // presenting author same person? # No
            if($this->arrItemData[$group_id][$item_id]['disp'] != 1 && isset($this->arrParam[$key]) && $this->arrParam[$key] == 2){
                // 1番目の共著者に登録者を入れる
                $this->arrForm["edata610"] = isset($this->arrForm["edata115"]) ? $this->arrForm["edata115"] : ""; // title
                $this->arrForm["edata330"] = isset($this->arrForm["edata116"]) ? $this->arrForm["edata116"] : ""; // family
                $this->arrForm["edata340"] = isset($this->arrForm["edata117"]) ? $this->arrForm["edata117"] : ""; // given
                $this->arrForm["edata410"] = isset($this->arrForm["edata121"]) ? $this->arrForm["edata121"] : ""; // are you...?
                $this->arrForm["edata620"] = isset($this->arrForm["edata118"]) ? $this->arrForm["edata118"] : ""; // middle
            // yes
            }else{
                $this->arrForm["edata610"] = isset($this->arrForm["edata57"]) ? $this->arrForm["edata57"] : "";
                $this->arrForm["edata330"] = isset($this->arrForm["edata1"]) ? $this->arrForm["edata1"] : "";
                $this->arrForm["edata340"] = isset($this->arrForm["edata2"]) ? $this->arrForm["edata2"] : "";
                $this->arrForm["edata410"] = isset($this->arrForm["edata8"]) ? $this->arrForm["edata8"] : "";
                $this->arrForm["edata620"] = isset($this->arrForm["edata7"]) ? $this->arrForm["edata7"] : "";
            }

            // 2ページ目を使用しない
            if($this->block == 3){
                $arrForm = $GLOBALS["session"]->getVar("form_param2");
                $arrForm["edata610"] = $this->arrForm["edata610"];
                $arrForm["edata330"] = $this->arrForm["edata330"];
                $arrForm["edata340"] = $this->arrForm["edata340"];
                $arrForm["edata410"] = $this->arrForm["edata410"];
                $arrForm["edata620"] = $this->arrForm["edata620"];
                $GLOBALS["session"]->setVar("form_param2", $arrForm);
            }
        }
    }


    function _check1() {
        $group_id = 1;

        // 筆頭者 != 共著者
        $item_id  = 83;
        $key      = 'edata'.$item_id;
        $disp29 = $this->itemData[29]["disp"];
        // presenting author same person? # No
        if($this->arrItemData[$group_id][$item_id]['disp'] != 1 && isset($this->arrParam[$key]) && $this->arrParam[$key] == 2){
            // 共著者チェックを行わない
            $this->itemData[29]["disp"] = "1";

            // 必須項目の設定
            $require = array(115, 116, 117, 120, 121, 122, 123, 124);
            foreach($require as $_key => $item_id){
                $this->itemData[$item_id]['need'] = "1";
                $this->itemData[$item_id]['item_check'] .= "|0";
                // 必須マークはつけない
//                $this->arrItemData[$group_id][$item_id]['need'] = 1;
            }
        }

        parent::_check1();

        // 共著者チェック
        $item_id  = 29;
        $key      = 'edata'.$item_id;
        if($this->arrItemData[$group_id][$item_id]['disp'] != 1 && isset($this->arrParam[$key])){
            if($this->arrParam[$key] == 2){
                if(isset($this->arrParam['edata30']) && $this->objErr->isNull($this->arrParam['edata30'])){
                    $name = strip_tags($this->itemData[30]['item_name']);
                    $this->objErr->addErr(sprintf("Don't Enter %s.", $name), 'edata30');
                }
                if(isset($this->arrParam['edata31']) && $this->objErr->isNull($this->arrParam['edata31'])){
                    $name = strip_tags($this->itemData[31]['item_name']);
                    $this->objErr->addErr(sprintf("Don't Enter %s.", $name), 'edata31');
                }
            }
        }

        // restore
        $this->itemData[29]["disp"] = $disp29;
    }


    function mailfunc32($name, $i){
        if($i == 0) return '';

        $str = Usr_mail::mailfunc32($this, $name, $i);
        return $str;
    }


    /** 帯追加 */
    function mailfunc83($name) {
        $str = "\n\n[Presenting Author]\n\n";
        $str .= Usr_mail::mailfuncNini($this, 83, $name);
        return $str;
    }
    function mailfunc29($name) {
        $str = "\n\n[Co-Author(s)]\n\n";
        $str .= Usr_mail::mailfuncNini($this, 83, $name);
        return $str;
    }

}

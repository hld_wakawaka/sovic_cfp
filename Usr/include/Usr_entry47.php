<?php

/**
 * 47番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2014.06.25
 * 
 */
class Usr_Entry47 extends Usr_Entry {

    private $inList = false;
    private $isHidden = false;


    /**
     * 簡易認証
     */
    function pf_auth() {
        Usr_initial::AuthAction($this);
        if(strlen($this->eid) > 0) return;

        $filename = ROOT_DIR."auth/form".$this->o_form->formData["form_id"];
        
        if(!file_exists($filename)) return;
        
        $phrase = file_get_contents(trim($filename));
        
        $auth_flg = $GLOBALS["session"]->getVar("auth_sysb");
        
        $_REQUEST["authpw"] = isset($_REQUEST["authpw"]) ? $_REQUEST["authpw"] : "";
        
        if($auth_flg == "1") {
            if($phrase == $GLOBALS["session"]->getVar("auth_pas_sysb")) return;
        }
        
        if($_REQUEST["authpw"] != "") {
            $phrase = file_get_contents(trim($filename));
            
            if($phrase == $_REQUEST["authpw"]) {
                $GLOBALS["session"]->setVar("auth_sysb", "1");
                $GLOBALS["session"]->setVar("auth_pas_sysb", $_REQUEST["authpw"]);
                $_REQUEST["mode"] = "";
                return;
            } else {
                $this->assign("msg", "認証に失敗しました。（Password Error)");
            }
        }
        
        $this->assign("form_id", $this->o_form->formData["form_id"]);
        $this->_processTemplate = "Usr/Usr_Basic.html";
        
        $this->basemain();
        exit;
        
    }


    function __construct(){
        parent::__construct();

        // [cfp-007] 「見積No3」jQuery多重読み込み調整のためコメントアウト
//         $this->useJquery = true;

        // 氏名リスト
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        $name = isset($_REQUEST['edata1'])
              ? $_REQUEST['edata1'].$_REQUEST['edata2']
              : $arrForm1['edata1'].$arrForm1['edata2'];
        // 氏名リストあり
        $form_id   = 47;
        $customDir = ROOT_DIR."customDir/".$form_id."/";
        $filename  = $customDir.'auth/entry_list.txt';
        if(file_exists($filename)){
            $list = file_get_contents($filename);
            $list = str_replace(array("\n", "\r", "\r\n"), "\n", $list);
            $arrList = explode("\n", $list);
            if(in_array($name, $arrList)){
                $this->inList = true;
            }
        }


        /**
         * inListを利用するかどうかのフラグ
         *    1:利用する > authファイルなし
         *    0:利用なし > authファイルあり
         */
        $filename = ROOT_DIR."auth/form".$form_id;
        $this->useNameList = (int)!file_exists($filename);
        $this->assign("useNameList", $this->useNameList);


        // 外部項目設定
        try{
            $customDir = ROOT_DIR."customDir/".$form_id."/";
            $file = glob($customDir.'pref/*');
            $file = array_filter($file, 'is_file');
            if(!is_array($file)) throw new Exception("該当ファイルなし");
            if(count($file) != 1)throw new Exception("該当ファイルなし");

            $date = pathinfo($file[0], PATHINFO_FILENAME);
            $date = preg_replace("/^(\d+)-(\d+)-(\d+)-(\d+)-(\d+)$/u", "$1-$2-$3 $4:$5:00", $date);
            if($date === pathinfo($file[0], PATHINFO_FILENAME)) throw new Exception("日付フォーマットエラー");

            $date = strtotime($date);
            $now  = strtotime(date("Y-m-d H:i:s"));
            $this->isHidden = ($date <= $now);

        }catch(Exception $e){
            $this->isHidden = false;
        }
    }


    /**
     * 開発用のデバッグ関数
     * 通常は空にしておく
     */
    function developfunc(){
//        // メールデバッグ
//        $this->ins_eid = 58;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('form43-00002', "Lk5sDS8f", 1));
//        print "</pre><br/><br/>";
    }

    // 共著者の上限を20
    function getAuthorMax(){ return 20; }

    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }

    function premain(){
        // override (Support only group3-3)
        // Add attribute disabled (default false)
        // User only
//      $this->arrItemData[3][$item_id]['disabled'] = true;

        // Separater of radio or checkbox (default br)
//      $this->arrItemData[3][$item_id]['sep'] = " ";

        // Size of text (default 40)
//      $this->arrItemData[3][$item_id]['size'] = val;

        // Size of textarea (default 5 and 85)
//      $this->arrItemData[3][$item_id]['rows'] = val;
//      $this->arrItemData[3][$item_id]['cols'] = val;


        // Keywords
        $this->arrItemData[3][67]['size'] = 25;
        $this->arrItemData[3][68]['size'] = 25;
        $this->arrItemData[3][99]['size'] = 25;

        // 日本栄養食糧学会
        $this->arrItemData[3][100]['size'] = 25;
        $this->arrItemData[3][101]['size'] = 25;
        $this->arrItemData[3][102]['size'] = 25;

        // 氏名リストに当てはまるかの有無
        $this->assign("inList", (int)$this->inList);

//        $this->assign("admin_flg","");
        $this->assign("arrItemData",$this->arrItemData);
    }

    //----------------------------------------------
    // 項目・帯の非表示
    /* フォーム基本情報の取得・アサイン */
    function setFormData() {
        Usr_initial::setFormData($this);
        if($this->isHidden){
            $customDir = ROOT_DIR."customDir/47/";
            $obis = glob($customDir.'pref/obi/*');
            $obis = array_filter($obis, 'is_file');
            if(is_array($obis) && count($obis) > 0){
                foreach($obis as $_key => $script){
                    $group3_id = pathinfo($script, PATHINFO_FILENAME);
                    $this->formdata['arrgroup3'][$group3_id] = "";
                }
                $this->assign("formData", $this->formdata);
            }
        }
    }
    /* フォーム項目の取得及び、整形 */
    function setFormIni() {
        Usr_initial::setFormIni($this);
        if($this->isHidden){
            $customDir = ROOT_DIR."customDir/47/";
            $items = glob($customDir.'pref/item/*');
            $items = array_filter($items, 'is_file');
            if(is_array($items) && count($items) > 0){
                foreach($items as $_key => $script){
                    $item_id = pathinfo($script, PATHINFO_FILENAME);
                    $this->arrItemData[3][$item_id]['disp'] = 1;
                }
            }
        }
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する

    function _check3() {
        $group_id = 3;

        parent::_check3();

        // yes選択 # ★5 Copyright Transferを必須
        $item_id = 103;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($this, $group_id, $item_id) && $this->arrParam[$key] == "1"){
            $item_id = 105;
            $key = "edata".$item_id;
            if(!$this->objErr->isNull($this->arrParam[$key."1"])){
                $name = Usr_init::getItemInfo($this, $item_id);
                $method = Usr_init::getItemErrMsg($this, $item_id);
                $this->objErr->addErr(sprintf($method, $name), $key);
            }
        }

        //-----------------------------------------------
        // Category for ...
        // Categlry for ...
        $count = 0;

        // 55 # Category for Symposia / Workshopss
        $item_id = 55;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($this, $group_id, $item_id) && strlen($this->arrParam[$key]) > 0) $count++;

        // 56 # Categlry for Contribute Abstracts
        $item_id = 56;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($this, $group_id, $item_id) && strlen($this->arrParam[$key]) > 0) $count++;

        if($count == 0){
//            $this->objErr->addErr("Please select the Category.", "edata55-56");
        }


        //-----------------------------------------------
        // 単語数
        $item_id = 50;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($this, $group_id, $item_id) && strlen($this->arrParam[$key]) > 0){
            // 全角スペースが含まれる場合、半角スペースに統一
            $str = $this->arrParam[$key];
            $str = str_replace(array("　", "\n", "\r", "\r\n"), " ", $str);
            $wa_word = explode(" ", $str);

            $wa_chk_word = array();
            foreach($wa_word as $data){
                if($data != ""){
                    array_push($wa_chk_word, $data);
                }
            }
            $c = count($wa_chk_word);
            if(!(150 <= $c && $c <= 300)){
                $this->objErr->addErr("Abstract must be 150 words or more but not more than 300 words.", $key);
            }
        }


        //-----------------------------------------------
        // 氏名リスト
        if($this->inList && $this->useNameList){
            $item_id = 54;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($this, $group_id, $item_id) && $this->arrParam[$key] == 1){
                $item_id = 55;
                $key = "edata".$item_id;
                if(Usr_init::isset_ex($this, $group_id, $item_id) && !$this->objErr->isNull($this->arrParam[$key])){
                    $name = Usr_init::getItemInfo($this, $item_id);
                    $method = Usr_init::getItemErrMsg($this, $item_id);
                    $this->objErr->addErr(sprintf($method, $name), $key);
                }
            }
        }
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function mailfunc68($name){  return ""; }
    function mailfunc99($name){  return ""; }
    function mailfunc101($name){ return ""; }
    function mailfunc102($name){ return ""; }
    function mailfunc67($name){
        $group   = 3;
        $item_id = 67;
        $key     = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";

        $str = $this->point_mark.$name.": ".Usr_Assign::nini($this, $group, $item_id, $this->arrForm[$key]);
        $str.= " ".Usr_Assign::nini($this, $group, 68, $this->arrForm['edata68']);
        $str.= " ".Usr_Assign::nini($this, $group, 99, $this->arrForm['edata99']);
        $str.= "\n";
        return $str;
    }
    function mailfunc100($name){
        $group   = 3;
        $item_id = 100;
        $key     = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";

        $str = $this->point_mark.$name.": ".Usr_Assign::nini($this, $group, $item_id, $this->arrForm[$key]);
        $str.= " ".Usr_Assign::nini($this, $group, 101, $this->arrForm['edata101']);
        $str.= " ".Usr_Assign::nini($this, $group, 102, $this->arrForm['edata102']);
        $str.= "\n";
        return $str;
    }


    function mailfunc106($name){ return ""; }
    function mailfunc56($name){
        $group   = 3;
        $item_id = 56;
        $key     = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";

        $str = $this->point_mark.$name."\n";
            $memo = $this->arrItemData[$group][$item_id]['item_memo'];
            $str.= "  ".$memo.": ".Usr_Assign::nini($this, $group, $item_id, $this->arrForm[$key])."\n";

            $item_id = 106;
            $key     = "edata".$item_id;
            if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";
            $memo = $this->arrItemData[$group][$item_id]['item_memo'];
            $str.= "  ".$memo.": ".Usr_Assign::nini($this, $group, $item_id, $this->arrForm[$key])."\n";

        return $str;
    }

}

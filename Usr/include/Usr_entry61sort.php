<?php

/**
 * 59番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author doishun
 * 
 */
class Usr_Entry61sort {

    function doSort($obj){
        // グループ3の項目をグループ1に持ってくる
        $group = 3;
        $arrGroup = array();
        // 項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 48:
                    $arrGroup[$item_id] = $_arrItem;                   // 3-1
                    $arrGroup[54]       = $obj->arrItemData[3][54];    // 3-3
                    $arrGroup[51]       = $obj->arrItemData[3][51];    // 3-2
                    $arrGroup[55]       = $obj->arrItemData[3][55];    // 3-3
                    break;

                case 54:
                case 51:
                case 52:
                case 53:
                case 55:
                    break;

                case 56:
                    $arrGroup[$item_id] = $_arrItem;                   // 3-3
                    $arrGroup[52]       = $obj->arrItemData[3][52];    // 3-2
                    $arrGroup[53]       = $obj->arrItemData[3][53];    // 3-2
                    break;

                default:
                    $arrGroup[$item_id] = $_arrItem;
                    break;
            }
        }
        $obj->arrItemData[$group] = $arrGroup;

        $obj->arrItemData[$group][54]['group3']  = 1;  unset($obj->group3item[2][54]);
        $obj->arrItemData[$group][51]['group3']  = 1;  unset($obj->group3item[1][0]);
        $obj->arrItemData[$group][55]['group3']  = 1;  unset($obj->group3item[2][55]);
        $obj->arrItemData[$group][52]['group3']  = 3;  unset($obj->group3item[1][1]);
        $obj->arrItemData[$group][53]['group3']  = 3;  unset($obj->group3item[1][2]);
        $obj->arrItemData[$group][52]['controltype']  = 1;
        $obj->arrItemData[$group][53]['controltype']  = 1;


        // グループ3-1
        $arrItem = array();
        $group = 1 -1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 48:
                    $arrItem[] = $item_id;
                    $arrItem[] = 54;
                    $arrItem[] = 51;
                    $arrItem[] = 55;
                    break;

                case 54:
                case 51:
                case 55:
                    break;

                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[$group] = $arrItem;

        // グループ3-1
        $arrItem = array();
        $group = 3 -1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 52:
                case 53:
                    break;
        
                case 56:
                    $arrItem[] = $item_id;
                    $arrItem[] = 52;
                    $arrItem[] = 53;
                    break;
        
                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[$group] = $arrItem;

        // グループ3-2は項目移動で実質使用不可
        $obj->group3use[2] = false;
//         print "----<pre>";
//         print_r($obj->group3item);
//         print "</pre><br/><br/>";
//         print "----<pre>";
//         print_r($obj->group3use);
//         print "</pre><br/><br/>";


        // 詳細とCSVはedata67の同意確認の項目は非表示にする
        $objName = get_class($obj);
        if($objName == "entrydetail" || $objName == "entry_csv"){
            $item_id = 67;
            $obj->itemData[$item_id]["disp"] = "1";
            $obj->arrItemData[3][$item_id]   = "1";
        }
    }
}

?>
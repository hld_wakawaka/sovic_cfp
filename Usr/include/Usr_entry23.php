<?php

/**
 * 23番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2013.08.23
 * 
 */
class Usr_Entry23 extends Usr_Entry {

    /**
     * 開発用デバッグメソッド
     * super#mainの最後に実行される
     * 
     */
    function developfunc(){}


    function mailfunc13($name){
        $str = $this->point_mark.$name.": ".$this->arrForm["edata13"]."\n";


        // 区分
        $data = $this->itemData[26];
        $data["item_name"] = strip_tags($data["item_name"]);
        $name = $data["item_name"];

        $str26 = $this->mailfuncNini(26, $name);	//任意

        return $str.$str26;
    }


    function mailfunc26($name){
        return "";
    }

}

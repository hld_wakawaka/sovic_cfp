<?php

/**
 * 55番専用カスタマイズフォーム(62番フォームへのコピー)
 *
 * @subpackage Usr
 * @author doishun
 * 
 */
class Usr_Entry62sort {
 
    function doSort($obj){
        // [cfp-form62] 項目移動の対象の項目はグループ1の項目なので、グループ１の項目について項目移動を行う
        $group = 1;
        $arrGroup = array();
        // [cfp-form62] グループ1 > 項目移動:「Presenting Author」をEmailの下へ移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
            switch($item_id){
                case 25:
                    $arrGroup[$item_id] = $_arrItem;
                    $arrGroup[71]      = $obj->arrItemData[1][71];
                    break;

                case 71:
                    break;
        
                default:
                    $arrGroup[$item_id] = $_arrItem;
                    break;
            }
        }
        $obj->arrItemData[$group] = $arrGroup;
        
        $group = 3;
        $arrGroup = array();
        // 項目移動
        // [form62] 見積もり番号7：項目151(Topic Category2)のプルダウンを項目48(Topic Category)の下に項目移動
        foreach($obj->arrItemData[$group] as $item_id => $_arrItem){
        	switch($item_id){
        		case 47:
        			$arrGroup[$item_id] = $_arrItem;
        			$arrGroup[100]      = $obj->arrItemData[3][100];   // 3-2
        			$arrGroup[50]       = $obj->arrItemData[3][50];    // 3-3
        			$arrGroup[101]      = $obj->arrItemData[3][101];   // 3-4
        			$arrGroup[48]       = $obj->arrItemData[3][48];    // 3-5
        			$arrGroup[151]      = $obj->arrItemData[3][151];
        			$arrGroup[102]      = $obj->arrItemData[3][102];   // 3-6
        			$arrGroup[103]      = $obj->arrItemData[3][103];   // 3-7
        			$arrGroup[104]      = $obj->arrItemData[3][104];
        			$arrGroup[105]      = $obj->arrItemData[3][105];
        			$arrGroup[106]      = $obj->arrItemData[3][106];
        			$arrGroup[107]      = $obj->arrItemData[3][107];
        			$arrGroup[108]      = $obj->arrItemData[3][108];   // 3-8
        			$arrGroup[109]      = $obj->arrItemData[3][109];
        			$arrGroup[110]      = $obj->arrItemData[3][110];
        			$arrGroup[111]      = $obj->arrItemData[3][111];
        			$arrGroup[112]      = $obj->arrItemData[3][112];
        			$arrGroup[113]      = $obj->arrItemData[3][113];
        			$arrGroup[145]      = $obj->arrItemData[3][145];
        			$arrGroup[146]      = $obj->arrItemData[3][146];
        			$arrGroup[147]      = $obj->arrItemData[3][147];
        			$arrGroup[148]      = $obj->arrItemData[3][148];
        			break;
        
        		case 50:
        		case 48:
        			break;
        		
        		case 108:
        			break;
        		case 109:
        		case 110:
        		case 111:
        		case 112:
        		case 113:
        		case 145:
        		case 146:
        		case 147:
        		case 148:
                case 149:
                case 150:
                case 151:
        			break;
        
        		// 設問1
        		case 54:
        			$arrGroup[$item_id] = $_arrItem;
        			$arrGroup[149]      = $obj->arrItemData[3][149];
        			break;
        
        		// 設問2
        		case 55:
        			$arrGroup[$item_id] = $_arrItem;
        			$arrGroup[150]      = $obj->arrItemData[3][150];
        			break;
       
        
        		case 149:
        		case 150:
        			break;
        
        		default:
        			$arrGroup[$item_id] = $_arrItem;
        			break;
        	}
        }
        
        $obj->arrItemData[$group] = $arrGroup;

        $obj->arrItemData[$group][100]['group3']  = 1;  unset($obj->group3item[2][100]);
        $obj->arrItemData[$group][101]['group3']  = 1;  unset($obj->group3item[2][101]);
        $obj->arrItemData[$group][102]['group3']  = 1;  unset($obj->group3item[2][102]);
        $obj->arrItemData[$group][103]['group3']  = 1;  unset($obj->group3item[2][103]);
        $obj->arrItemData[$group][104]['group3']  = 1;  unset($obj->group3item[2][104]);
        $obj->arrItemData[$group][105]['group3']  = 1;  unset($obj->group3item[2][105]);
        $obj->arrItemData[$group][106]['group3']  = 1;  unset($obj->group3item[2][106]);
        $obj->arrItemData[$group][107]['group3']  = 1;  unset($obj->group3item[2][107]);
        $obj->arrItemData[$group][108]['group3']  = 1;  unset($obj->group3item[2][108]);
        $obj->arrItemData[$group][109]['group3']  = 1;  unset($obj->group3item[2][109]);
        $obj->arrItemData[$group][110]['group3']  = 1;  unset($obj->group3item[2][110]);
        $obj->arrItemData[$group][111]['group3']  = 1;  unset($obj->group3item[2][111]);
        $obj->arrItemData[$group][112]['group3']  = 1;  unset($obj->group3item[2][112]);
        $obj->arrItemData[$group][113]['group3']  = 1;  unset($obj->group3item[2][113]);
        $obj->arrItemData[$group][145]['group3']  = 1;  unset($obj->group3item[2][145]);
        $obj->arrItemData[$group][146]['group3']  = 1;  unset($obj->group3item[2][146]);
        $obj->arrItemData[$group][147]['group3']  = 1;  unset($obj->group3item[2][147]);
        $obj->arrItemData[$group][148]['group3']  = 1;  unset($obj->group3item[2][148]);
        $obj->arrItemData[$group][151]['group3']  = 1;  unset($obj->group3item[2][151]);


        // グループ3-1
        $arrItem = array();
        $group = 1 -1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 47:
                    $arrItem[] = $item_id;
                    $arrItem[] = 100;
                    $arrItem[] = 50;
                    $arrItem[] = 101;
                    $arrItem[] = 48;
                    $arrItem[] = 151;
                    $arrItem[] = 102;
                    $arrItem[] = 103;
                    $arrItem[] = 104;
                    $arrItem[] = 105;
                    $arrItem[] = 106;
                    $arrItem[] = 107;
                    $arrItem[] = 108;
                    $arrItem[] = 109;
                    $arrItem[] = 110;
                    $arrItem[] = 111;
                    $arrItem[] = 112;
                    $arrItem[] = 113;
                    $arrItem[] = 145;
                    $arrItem[] = 146;
                    $arrItem[] = 147;
                    $arrItem[] = 148;
                    
                    break;

                case 50:
                case 48:
                    break;
                
                case 70:
                	$arrItem[] = $item_id;
                	$arrItem[] = 71;
                	break;
                	
                case 71:
                	break;
                case 151:
                	break;

                default:
                    $arrItem[] = $item_id;
                    break;
            }
        }
        $obj->group3item[$group] = $arrItem;

        

        // グループ3-3
        $arrItem = array();
        $group = 3 -1;
        foreach($obj->group3item[$group] as $_key => $item_id){
            switch($item_id){
                case 149:
                case 150:
                case 151:
                    break;

                // 設問1
                case 54:
                    $arrItem[] = $item_id;
                    $arrItem[] = 149;
                    break;

                // 設問2
                case 55:
                    $arrItem[] = $item_id;
                    $arrItem[] = 150;
                    break;

                default:
                    $arrItem[] = $item_id;
            }
        }
        $obj->group3item[$group] = $arrItem;
    }


    /**
     * autoloadで利用する呼び出しモードを設定する
     * 
     * @throws Exception
     * @param  void
     * @return void
     */
    private static function getMode(){
        $mode = "";

        if(strpos($_SERVER['PHP_SELF'], APP_ROOT."Usr") === 0){
            $mode = "Usr";
        }

        else if(strpos($_SERVER['PHP_SELF'], APP_ROOT."Mng") === 0){
            $mode = "Mng";
        }

        else if(strpos($_SERVER['PHP_SELF'], APP_ROOT."Sys") === 0){
            $mode = "Sys";
        }

        return $mode;
    }


    function hidden_item($obj){
        // 非表示にする項目
        $group_id = 3;
        $hidden_item = array(104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 145, 146, 147, 148,/*151*/);
        if($this->getMode() == "Usr"){
            array_push($hidden_item, 149, 150);
        }

        foreach($hidden_item as $item_id){
            $obj->arrItemData[$group_id][$item_id]['disp']      = "1";
            $obj->arrItemData[$group_id][$item_id]['item_view'] = "1";
        }

        $obj->keyword55     = array(104, 105, /*106, 107*/);
        $obj->references55 = array(109, 110, 111, 112, 113, 145, 146, 147, 148);

        if(method_exists($obj, "assign")){
            $obj->assign("arrItemData",$obj->arrItemData);
            $obj->assign("keyword55",    $obj->keyword55);
            $obj->assign("references55", $obj->references55);
        }
    }




    function mng_detail_premain($obj){
        $this->hidden_item($obj);
    }


    function __constructMngCSV($obj){
        // 1セルごとに出力する場合はコメントアウト
//        $this->hidden_item($obj);
        $obj->start = 2;
    }


//    // keyword
//    function csvfunc103($obj, $group, $pa_param, $item_id){
//        $edata = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $edata = trim($edata, ",");
//
//
//        foreach($obj->keyword55 as $item_id){
//            $_edata = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//            $_edata = trim($_edata, ",");
//            $edata .= " ".$_edata;
//        }
//        return $edata;
//    }
//
//
//    // references
//    function csvfunc108($obj, $group, $pa_param, $item_id){
//        $edata = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $edata = trim($edata, ",");
//
//
//        foreach($obj->references55 as $item_id){
//            $_edata = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//            $_edata = trim($_edata, ",");
//            $edata .= " ".$_edata;
//        }
//        return $edata;
//    }

}










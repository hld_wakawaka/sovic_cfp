<?php

/**
 * 37番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2014.02.05
 * 
 */
class Usr_Entry37sort {

    function doSort($obj){
        $arrGroup1 = array();
        foreach($obj->arrItemData[1] as $item_id => $_arrItem){
            switch($item_id){
                case 21:
                    $arrGroup1[$item_id] = $_arrItem;
                    $arrGroup1[26]       = $obj->arrItemData[1][26];
                    break;

                case 26:
                    break;

                default:
                    $arrGroup1[$item_id] = $_arrItem;
            }
        }
        $obj->arrItemData[1] = $arrGroup1;
    }
}

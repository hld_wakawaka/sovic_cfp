<?php

/**
 * 19番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2013.07.25
 * 
 */
class Usr_Entry19 extends Usr_Entry {

    const INPUT_TEXT   = 0;
    const INPUT_AREA   = 1;
    const INPUT_RADIO  = 2;
    const INPUT_CHECK  = 3;
    const INPUT_SELECT = 4;


    /**
     * 開発用デバッグメソッド
     * super#mainの最後に実行される
     * 
     */
    function developfunc(){
    }



    /**
     * アイテム項目の選択肢に一致する項目キーを返す
     * @param  array   $itemData    取得対象のアイテム情報
     * @param  string  $searchValue 検索キー
     * @return integer $key
     */
    function getItemKey($itemData, $searchValue){
        $key = NULL;
        $item_type = intval($itemData['item_type']);

        switch($item_type){
            case self::INPUT_TEXT:
            case self::INPUT_AREA:
                break;

            case self::INPUT_RADIO:
            case self::INPUT_CHECK:
            case self::INPUT_SELECT:
                $arrItem = array_map('strtolower', $itemData['select']);
                $key = array_search($searchValue, $arrItem);
                $key = ($key === false)
                     ? NULL
                     : intval($key);
                break;
        }
        return $key;
    }


    // ファイルの拡張子チェック
    function checkExt($fileInfo, $arrExt=array()){
        if($fileInfo['error'] == 4) return true;

        $ext = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));
        return in_array($ext, $arrExt);
    }



    function _check1() {
        parent::_check1();

        // Background#other選択時
        if(isset($this->arrParam['edata26'])){
            $chkKey    = '27';
            $chkString = 'edata'.$chkKey;

            if($this->arrParam['edata26'] == $this->getItemKey($this->itemData[26], "other")){
                // 必須チェック
                if(!$this->objErr->isNull($this->arrParam[$chkString])){
                    $this->objErr->addErr("Enter {$this->itemData[$chkKey]['strip_tags']}."
                                        , $chkString);
                }
            }else{
                $this->arrParam[$chkString] = "";
            }
        }

        // Position#other選択時
        if(isset($this->arrParam['edata28'])){
            $chkKey    = '63';
            $chkString = 'edata'.$chkKey;

            if($this->arrParam['edata28'] == $this->getItemKey($this->itemData[28], "other")){
                // 必須チェック
                if(!$this->objErr->isNull($this->arrParam[$chkString])){
                    $this->objErr->addErr("Enter {$this->itemData[$chkKey]['strip_tags']}."
                                        , $chkString);
                }
            }else{
                $this->arrParam[$chkString] = "";
            }
        }

        return;
    }



    function _check3() {
        parent::_check3();

        // Would you like to apply for the Travel Grant?#yes選択時
        if(isset($this->arrParam['edata541'])
        && $this->arrParam['edata541'] == $this->getItemKey($this->itemData[54], "yes")){
            // 必須チェック
            $arrExists = array(55, 56, 67, 68);
            foreach($arrExists as $_key => $chkKey){
                $chkString = 'edata'.$chkKey;
                if(!$this->objErr->isNull($this->arrParam[$chkString])){
                    $this->objErr->addErr("Enter {$this->itemData[$chkKey]['strip_tags']}."
                                        , $chkString);
                }
            }
        }


        // 拡張子チェック
        $chkKey = 51;
        $chkString = 'edata'.$chkKey;
        if(isset($_FILES[$chkString]) && !$this->checkExt($_FILES[$chkString], array("doc", "docx"))){
            $this->objErr->addErr("Please upload {$this->itemData[$chkKey]['strip_tags']} in MS Word format (.doc/.docx) only.", $chkString);
            $this->arrParam['hd_file'.$chkKey] = "";
        }
        return;
    }



	/**
	 * メール
	 */
	function mailfunc25($name) {
		$body = $this->point_mark.$name.": ".$this->arrForm["edata25"]."\n";
		$body .= "\n\n[PROFESSIONAL AND BACKGROUND INFORMATION]\n\n";
		return $body;
	}


	/**
	 * Background#other
	 */
	function mailfunc26($name) {
		$body = $this->mailfuncNini(26, $name);
        if($this->arrForm['edata26'] == $this->getItemKey($this->itemData[26], "other")){
    		$body .= " ".$this->arrForm["edata27"];
        }

		return $body."\n";
	}


	/**
	 * Background#other
	 */
	function mailfunc27($name) {
		return "";
	}


	/**
	 * Position#other
	 */
	function mailfunc28($name) {
		$body = $this->mailfuncNini(28, $name);
        if($this->arrForm['edata28'] == $this->getItemKey($this->itemData[28], "other")){
    		$body .= " ".$this->arrForm["edata63"];
        }

		return $body."\n";
	}


	/**
	 * Position#other
	 */
	function mailfunc63($name) {
		return "";
	}


	/**
	 * Date of Birth
	 */
	function mailfunc55($name) {
        // 55
		$wk_key = "edata55";
		$str = $this->point_mark.$name.": ";
		if(isset($this->itemData[55]["select"][$this->arrForm[$wk_key]])) {
			$str .= trim($this->itemData[55]["select"][$this->arrForm[$wk_key]]);
		}

        // 56
		$wk_key = "edata56";
		if(isset($this->itemData[56]["select"][$this->arrForm[$wk_key]])) {
			$str .= " ". trim($this->itemData[56]["select"][$this->arrForm[$wk_key]]);
		}

        // 67
		$wk_key = "edata67";
		if(isset($this->itemData[67]["select"][$this->arrForm[$wk_key]])) {
			$str .= " ". trim($this->itemData[67]["select"][$this->arrForm[$wk_key]]);
		}

		return $str."\n";
	}


	/**
	 * Date of Birth
	 */
	function mailfunc56($name) {
		return "";
	}


	/**
	 * Date of Birth
	 */
	function mailfunc67($name) {
		return "";
	}



	/**
	 * ファイルアップロード
	 */
	function mailfunc51($name) {
		$body  =  $this->mailfuncFile(51, $name);
		$body .= "\n\n[TRAVEL GRANT APPLICATION]\n\n";
		return $body;
	}



}

<?php

/**
 * 60番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author doishun
 * 
 */
class Usr_Entry60 extends Usr_Entry {

    function __construct(){
        parent::__construct();
    }


    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;
    
            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }



    function developfunc(){
//        // メールデバッグ
//         $this->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($this->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }



    /**
     * ページフローを表示する
     * */
    function createPageflow(){
        $pageflow[0] = "登録者情報";     // グループ1
        $pageflow[1] = "共同演者";     // グループ2
        $pageflow[2] = "演題情報";       // グループ3
        $pageflow[3] = "登録内容確認";    // 確認画面
        $pageflow[4] = "登録完了";       // 完了画面
        $this->assign("pageflow", $pageflow);
    }



    /** 項目情報初期化 */
    function _init($ps_block){
        $key = parent::_init($ps_block);
//         // 確認用E-mail拡張
//         if($ps_block == 1){
//             $key[] = array("chkemail_71", "E-mail確認用", array(),    array(),    "",    0);
//         }
        return $key;
    }


    function _check1() {
        parent::_check1();

        // No.10 # 改行数を10に制限
        $count = count(explode("\n", str_replace(array("\r\n", "\r"), "\n", $this->arrParam["edata31"])));
        if($count > 10){
            $this->objErr->_err["edata31"] = "登録できる所属は最大10件までです。";
        }

//         // No.6 # 拡張メールアドレスの一致チェック
//         $item_id = 71;
//         $key     = "edata".$item_id;
//         if(strlen($this->arrParam[$key]) > 0){
//             if($this->arrParam[$key] != $_REQUEST["chkemail_".$item_id]){
//                 $msg = $this->itemData[$item_id]['strip_tags']."と".$this->itemData[$item_id]['strip_tags']."（確認用）の入力内容が一致していません。";
//                 $this->objErr->addErr($msg, $key);
//             }
//         }
    }

    function _check3() {
        parent::_check3();

        // No.4 # 文字数チェック(600-1200)
        $str = $this->arrParam['edata49'];
        $str = str_replace(array("\r\n", "\r", "\n"), "", $str);
        $cnt = mb_strlen($str, "UTF-8");
        if(!(600 <= $cnt && $cnt <= 1200)){
            $this->objErr->addErr("演題内容は、600文字以上 1200文字以下で入力してください。", "edata49");
        }

        // No.2/17-No.1 # 一致するとエラー
        $item_id1 = 48; $key1 = "edata".$item_id1;
        $item_id2 = 54; $key2 = "edata".$item_id2;
        if(strlen($this->arrParam[$key1]) > 0 && strlen($this->arrParam[$key2]) > 0){
            if($this->arrParam[$key1] == $this->arrParam[$key2]){
                $this->objErr->addErr("第一希望と第二希望が重複しています。別々のものをお選び下さい。", "edata49");
            }
        }
    }



    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function mailfunc27($name){
        $obi = "\n\n【筆頭演者情報】\n\n";

        $group   = 3;
        $item_id = 27;
        $key = "edata".$item_id;
        $str = $this->point_mark . $name. ":". Usr_Assign::nini($this, $group, $item_id, $this->arrForm[$key])."\n";
        return $obi.$str;
    }


    function mailfunc29($name){
        $obi = "\n\n【{$this->obi_edata29}】\n\n";
        return $obi.$this->point_mark.$name.": ".$this->wa_coAuthor[$this->arrForm["edata29"]]."\n";
    }


}

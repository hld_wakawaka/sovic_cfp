<?php

/**
 * 49番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon
 * @since 2014.06.25
 * 
 */
class Usr_Entry49 extends Usr_Entry {

    function __construct(){
        parent::__construct();

//        $this->useJquery = true;
    }


    /**
     * 開発用のデバッグ関数
     * 通常は空にしておく
     */
    function developfunc(){
//        // メールデバッグ
//        $this->ins_eid = 58;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($this->makeMailBody('form43-00002', "Lk5sDS8f", 1));
//        print "</pre><br/><br/>";
    }

    function sortFormIni($obj){
        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isSortItemClass($obj->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // 項目並び替え
            if(method_exists($this->exClass, "doSort")){
                $this->exClass->doSort($this);
            }
        }
    }

    function premain(){
        // override (Support only group3-3)
        // Add attribute disabled (default false)
        // User only
//      $this->arrItemData[3][$item_id]['disabled'] = true;

        // Separater of radio or checkbox (default br)
//      $this->arrItemData[3][$item_id]['sep'] = " ";

        // Size of text (default 40)
//      $this->arrItemData[3][$item_id]['size'] = val;

        // Size of textarea (default 5 and 85)
//      $this->arrItemData[3][$item_id]['rows'] = val;
//      $this->arrItemData[3][$item_id]['cols'] = val;
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    function _check1() {
        parent::_check1();

        // ファイルのチェック/アップロード
        $this->_checkfile();
    }


    function pageAction1() {
        parent::pageAction1();

        if(strpos($this->_processTemplate, "Usr_entry_confirm_") !== false){
            $this->_confirm();
        }
    }



    function getEntry() {
    
        $workDir = $this->uploadDir.$this->eid."/".date("YmdHis");
    
        //ファイルアップロード用　ワークディレクトリ生成
        if(!is_dir($workDir)){
            mkdir($workDir, 0777, true);
            chmod($workDir, 0777);
        }
        //セッションにワークディレクトリ名をセット
        $GLOBALS["session"]->setVar("workDir", $workDir);
    
        //応募データ取得
        $wa_entrydata = $this->o_entry->getRntry_r($this->db, $this->eid, $this->form_id);
        if(!$wa_entrydata){
            Error::showErrorPage("応募情報の取得に失敗しました。");
        }
    
        // 共著者有無を保存しておく
        $this->arrForm["pre_edata29"] = $this->arrForm["edata29"];
    
        //応募者のID・パスワード
        $ss_entry_user = array("e_user_id" => $wa_entrydata["e_user_id"], "e_user_passwd" => $wa_entrydata["e_user_passwd"]);
        $GLOBALS["session"]->setVar("SS_USER", $ss_entry_user);
    
        //共著者データ取得
        $wa_aff = $this->o_entry->getRntry_aff($this->db, $this->eid);
    
        //----------------------------
        //管理者モードの場合
        //----------------------------
        if($this->admin_flg != ""){
            $wk_user_id["e_user_id"] =  $wa_entrydata["e_user_id"];
            $GLOBALS["session"]->setVar("isLoginEntry", true);
            $GLOBALS["session"]->setVar("entryData", $wk_user_id);
        }
    
        //----------------------
        //1ページ目データ
        //----------------------
        $start = 1;
        $end = 32;
    
        for($index=$start; $index < $end; $index++){
            	
            if($index != 26 && $index != 27 && $index != 28){
                $this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
            }
            else{
    
                //任意項目
                $this->_default_arrFormNini(26, 29, $wa_entrydata);
            }
        }
    
        $this->_default_arrFormNini(63, 65, $wa_entrydata);
    
        //任意項目6～20
        $this->_default_arrFormNini(69, 84, $wa_entrydata);
    
        //任意項目21～50
        $this->_default_arrFormNini(115, 145, $wa_entrydata);
    
        //添付資料
        foreach($this->arrfile as $index=>$val) {
            if($index == 51 || $index == 52 || $index == 53){
            
                //--------------------------
                //ワークディレクトリにファイルを移動
                //--------------------------
                if(is_file($this->uploadDir.$this->eid."/".$wa_entrydata["edata".$index])){
                    if (!copy($this->uploadDir.$this->eid."/".$wa_entrydata["edata".$index], $workDir."/".$wa_entrydata["edata".$index])) {
                        $this->complete("ファイルのコピーに失敗しました。");
                    }
                }
                $this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
                $this->arrForm["hd_file".$index] = $wa_entrydata["edata".$index];
                $this->arrForm["n_data".$index] = $wa_entrydata["edata".$index];
            }
        }

        //英語フォーム固有の項目
        $start = 57;
        $end = 61;
        for($index=$start; $index < $end; $index++){
            $this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
        }
        //国名リストボックスの値
        $this->arrForm["edata114"] = $wa_entrydata["edata114"];
    
        $this->arrForm["chkemail"] = $wa_entrydata["edata25"];
    
        $GLOBALS["session"]->setVar("form_param1", $this->arrForm);
    
        //----------------------
        //2ページ目データ
        //----------------------
        $this->arrForm = "";
    
        //共著者の登録がある場合
        if($wa_aff){
            foreach($wa_aff as $a_key => $data){
                $start = 32;
                $end = 46;
    
                for($index=$start; $index < $end; $index++){
                    	
                    if($index != 43 && $index != 44 && $index != 45){
    
                        //共著者の所属機関
                        if($index == 32){
                            $wk_checked  = explode("|", $data["edata".$index]);
                            foreach($wk_checked as $chk_val){
                                $this->arrForm["edata".$index.$a_key.$chk_val] = $chk_val;
                            }
                        }
                        else{
                            $this->arrForm["edata".$index.$a_key] = $data["edata".$index];
                        }
    
                    }
                    else{
    
                        //任意項目生成
                        $this->_default_arrFormChosyaNini(43, 46, $data, $a_key);
                    }
                }
    
                //英語フォーム固有の項目
                $start = 61;
                $end = 63;
    
                for($index=$start; $index < $end; $index++){
                    $this->arrForm["edata".$index.$a_key] = $data["edata".$index];
                    	
                }
    
    
                //共著者ID
                $kyouid[$a_key] = $data["id"];
    
                //任意項目生成
                $this->_default_arrFormChosyaNini(65, 68, $data, $a_key);
                $this->_default_arrFormChosyaNini(84, 99, $data, $a_key);
    
            }
            $GLOBALS["session"]->setVar("form_param2", $this->arrForm);
            $GLOBALS["session"]->setVar("kyouID", $kyouid);
            
        }
    
    
        //----------------------
        //3ページ目データ
        //----------------------
        $this->arrForm = "";
        $start = 46;
        $end = 57;
    
        for($index=$start; $index < $end; $index++){
            // ファイルアップロードはグループ3から除外
            if(array_key_exists($index, $this->arrfile)) continue;

            if($index != 54 && $index != 55 && $index != 56){
                $this->arrForm["edata".$index] = $wa_entrydata["edata".$index];
            }
            else{
    
                $this->_default_arrFormNini(54, 57, $wa_entrydata);
            }
        }
    
        $this->_default_arrFormNini(67, 69, $wa_entrydata);
        $this->_default_arrFormNini(99, 114, $wa_entrydata);
    
        $this->_default_arrFormNini(145, 160, $wa_entrydata);
    
        $GLOBALS["session"]->setVar("form_param3", $this->arrForm);
    
        //-------------------------------
        //セッション取得
        //-------------------------------
        $this->getDispSession();
        
        return;
    
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function mailfunc26($name){
        $group   = 1;
        $item_id = 26;
        $key     = "edata".$item_id;
        if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";

        $str = "\n";
        $str.= "[Questionnaire]\n";
        $str.= "\n";

        $str.= $this->point_mark.$name.": ".Usr_Assign::nini($this, $group, $item_id, $this->arrForm[$key]);
        $str.= "\n";
        return $str;
    }

    // DoB
    function mailfunc28($name){ return ''; }
    function mailfunc63($name){ return ''; }

    function mailfunc27($name){
        $group   = 1;
        $str = $this->point_mark.$name.":";

        foreach($this->dispDOB as $_key => $item_id){
            $key     = "edata".$item_id;
            $memo    = $this->arrItemData[$group][$item_id]['item_memo'];
            $memo    = ($item_id==63) ? "" : "/";
            if(!isset($this->arrForm[$key])) $this->arrForm[$key] = "";

            $str.= Usr_Assign::nini($this, $group, $item_id, $this->arrForm[$key])." ".$memo." ";
        }
        $str.= "\n";




        return $str;
    }

}
